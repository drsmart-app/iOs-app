//
//  DataBase.h
//  DFM
//
//  Created by Evgeniy Krivoshein on 01.02.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Word;
@class Group;
@class Task;
@class Reminder;
@class Friend;


@interface DataBase : NSObject
{
}
NSString* GUIDString();

+ (void) putDB:(NSDictionary*) result toTable:(NSString*) table withKey:(NSString*) key;
+ (NSObject*) getDB:(NSString*) table withKey:(NSString*) key andValue:(NSString*) value;
+ (void) killDB:(NSString*) table withKey:(NSString*) key andValue:(NSString*) value;
+ (NSArray*) getDBArray:(NSString*) table withKey:(NSString*) key andValue:(NSString*) value;

+ (void) registerTask:(NSString*)ObjId ObjType:(int)obj_type Command:(int) command;
+ (void) registerTask:(NSString*)ObjId ObjType:(int)obj_type Command:(int) command AdditionalId:(int) _num_id;
+ (NSArray*) getTasks:(int) limit;
+ (void) confirmGetTasks:(int) limit;
+ (NSArray*) getFriends:(Boolean)trusted Requested:(Boolean)requested;
+ (NSArray*) getFriends:(Boolean)trusted Requested:(Boolean)requested Except:(NSString*) reminder_id;
+ (NSArray*) getFriends:(Boolean)trusted Requested:(Boolean)requested ExceptFriends:(NSArray*) _except_friends;
+(Friend*) getFriend:(NSString*) _username;
+ (bool) addFriend:(Friend* ) friend friendRequestedTrusted:(BOOL) req_trust;
+ (BOOL) isFriendTrusted:(NSString* ) friend_username;
+ (bool) rejectFriend:(NSString* )friend_username; 
+ (bool) approveFriend:(Friend* )friend; 
+ (void) killAll;
+(BOOL) saveReminder:(Reminder*) rem;
+(BOOL) editReminder:(Reminder*) rem;
+(BOOL) rejectReminder:(NSString*) reminderId;
+ (NSArray*) getReminders:(Boolean)including_disabled;
+ (Reminder*) getReminder:(NSString*) _internal_id;
+(NSDictionary*) get_sync;
+ (NSString*) getReminderInternalIdByExtenal:(int) ext_id;

+(void)DBLog:(NSString*) log_text;
+(void)DBLog:(NSString*) log_text withData:(NSObject*) with_data;
+(BOOL) clearDBLogs;
+ (NSArray*) getDBLogs;

+(BOOL) unregisterMonitoredReminder:(NSString *) reminder_id;
+(BOOL) clearMonitoredReminders;
+(BOOL) registerMonitoredReminder:(NSString*) reminder_id;
+ (NSArray*) getMonitoredReminders;

+(BOOL) unregisterEnteredLocation:(NSString *) reminder_id;
+(BOOL) clearEnteredLocations;
+(BOOL) registerEnteredLocation:(NSString*) reminder_id;
+ (NSArray*) getEnteredLocations;
+(BOOL) registerLastFireTime:(Reminder*) rem;

+(BOOL) relinkLocalReminders:(NSString*) new_user;

+(BOOL) enableReminder:(Reminder*) rem;
+(BOOL) disableReminder:(Reminder*) rem;
+(BOOL) setReminderRadius:(Reminder*) rem;

@end
