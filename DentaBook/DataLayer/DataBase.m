//
//  DataBase.m
//  DFM
//
//  Created by Evgeniy Krivoshein on 01.02.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "sqlite3.h"
#import "DataBase.h"
//#import "Word.h"
#import "FileUtils.h"
#import "SharedErrorMessage.h"
#import "Param.h"
#import "SBJSON.h"

@implementation DataBase

static sqlite3 *dataBase = nil;
static NSString * dataBaseFileName = @"remindme.db3";
static NSString * docPath = @"Documents/";

void Err(NSString* title, NSString* message);

+ (BOOL) isNumeric:(NSString*) newString
{
    NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
    BOOL isDecimal = [nf numberFromString:newString] != nil;
    return isDecimal;
}


NSString* GUIDString()
{
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
    CFRelease(theUUID);
    return (__bridge NSString *)string;
}

+ (void) closeDataBase
{
    if(dataBase != nil)
    {
        sqlite3_close(dataBase);
        dataBase = nil;
    }
}

+ (Boolean) openDataBase
{
    if(dataBase == nil)
    {
        // Полный путь до файла БД
        NSMutableString *dataBaseName = [NSMutableString stringWithString:docPath];
        [dataBaseName appendString:dataBaseFileName];
        
        BOOL existBase = [FileUtils existsFile:dataBaseName];
        [dataBaseName insertString:@"/" atIndex:0];
        [dataBaseName insertString:NSHomeDirectory() atIndex:0];
        
        if( existBase == NO)
        {
            NSString *orgBase = [FileUtils findFile:dataBaseFileName withExtension:nil];
            [FileUtils copyFile:orgBase withNewName:dataBaseName];
        }
        
        int result = sqlite3_open([dataBaseName UTF8String], &dataBase);
        if (result != SQLITE_OK)
        {
            NSLog(@"Error opening the database. Error ""%s"". File: %@", sqlite3_errmsg(dataBase), dataBaseName);
            [self closeDataBase];
            
            NSLog(@"Abs path: %@", NSHomeDirectory());
            NSFileManager *fileManager = [NSFileManager defaultManager];
            NSDirectoryEnumerator *dirEnum = [fileManager enumeratorAtPath:NSHomeDirectory()];
            NSString *curPath;
            while (curPath = [dirEnum nextObject])
            {
                NSLog(@"Found %@", curPath);
            }
        }
        else
        {
            NSLog(@"Open database ""%@"".", dataBaseName);
        }
        
        return dataBase != nil;
    }
    return YES;
    
}

+ (NSString *) convertChar:(char *)ch
{
    if(ch == nil)
    {
        return nil;
    }
    else
    {
        return [[NSString alloc] initWithUTF8String:(ch)];
    }
}

+ (NSString *) convertCharJSON:(char *)ch
{
    if(ch == nil)
    {
        return @"";
    }
    else
    {
        return [[NSString alloc] initWithUTF8String:(ch)];
    }
}

// Выполнение запроса в БД
// Возвращается кол-во обработанных строк. -1 - ошибка.
+ (int) executeQuery:(char *)query
{
    int ret = -1;
    if([DataBase openDataBase])
    {
        char *errorMsg;
        BOOL result = (sqlite3_exec(dataBase, query, NULL, NULL, &errorMsg) == SQLITE_OK);
        if(result == NO) 
        {
            NSLog(@"Error ""%s"". Query ""%s""", errorMsg, query);
        } 
        else
        {
            ret = sqlite3_changes(dataBase);
        }
    }
    return ret;
}

// Выполнение запроса с одним числовым параметром
// Возвращается кол-во обработанных строк. -1 - ошибка.
+ (int) executeQuery:(char *)query param1:(NSString*)par1
{
    int ret = -1;
    if([DataBase openDataBase])
    {
        sqlite3_stmt *stmt;
        if(sqlite3_prepare_v2(dataBase, query, -1, &stmt, NULL) == SQLITE_OK)
        {
            sqlite3_bind_text(stmt, 1, [par1 UTF8String], -1, NULL);
            //sqlite3_int64 par = (par1);
            //SQLITE3_TEXT par1 = (par1);
            //sqlite3_bind_int64(stmt, 1, par);
            BOOL result =  (sqlite3_step(stmt) == SQLITE_DONE);
            if(result == NO)
            {
                NSLog(@"Error ""%s"". Query ""%s"", param1 ""%@"".", sqlite3_errmsg(dataBase), query, par1);
            }
            else
            {
                ret = sqlite3_changes(dataBase);
            }
        }
        sqlite3_finalize(stmt);
    }
    return ret;
}

+ (BOOL) isTableExists:(NSString*) tableName
{
    //LOG.debug("try to check_table: "+tableName);
    BOOL res=false;
    
    if([DataBase openDataBase])
    {
        const char *query = [[NSString stringWithFormat:@"SELECT * FROM sqlite_master WHERE type = 'table' AND name = '%@'", tableName] UTF8String];
        sqlite3_stmt *stmt;
        if(sqlite3_prepare_v2(dataBase, query, -1, &stmt, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(stmt) == SQLITE_ROW)
            {
                res=true;
            }
            
        }
        else
        {
            NSLog(@"Error ""%s"".", sqlite3_errmsg(dataBase));
            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
        }
        sqlite3_finalize(stmt);
    }
    
    return res;
}


/*TODO отэкранировать все кавычки, проверять набор параметров из таблицы и метаданных и если что добавлять колонки*/
+ (void) Serialize:(NSString*) table_name withPars:(NSArray*) pars andKey:(NSString*) key_field
{
    //DBHelper h=new DBHelper();
    [DataBase openDataBase];

    
    if(![DataBase isTableExists:@"metadata"])
    {
        NSString* create_clause=@"create table if not exists metadata(table_name VARCHAR, column_name VARCHAR, data_type VARCHAR);";
        if([DataBase openDataBase])
        {
            sqlite3_stmt *stmt;
            if(sqlite3_prepare_v2(dataBase, [create_clause UTF8String], -1, &stmt, NULL) == SQLITE_OK)
            {
                if(sqlite3_step(stmt) != SQLITE_DONE)
                {
                    NSLog(@"Error register task ""%s"".", sqlite3_errmsg(dataBase));
                    ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
                }
            }
            else
            {
                NSLog(@"Error register task ""%s"".", sqlite3_errmsg(dataBase));
                ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
            }
            sqlite3_finalize(stmt);
        }
    }
    
    

    
    if(![self isTableExists:table_name])
    {
        NSString* create_clause=[NSString stringWithFormat:@"create table if not exists %@(",table_name];
        int i=1;
        for(Param* p in pars)
        {
            create_clause = [create_clause stringByAppendingString:[NSString stringWithFormat:@"%@ %@ %@", p.name, [p castType], ([key_field isEqualToString:p.name]?@" PRIMARY KEY":@"")]];
            
            if(i!=pars.count)
                create_clause=[create_clause stringByAppendingString:@",\n"];
            i++;
        }
        
        create_clause=[create_clause stringByAppendingString:@");"];
        
        if([DataBase openDataBase])
        {
            sqlite3_stmt *stmt;
            if(sqlite3_prepare_v2(dataBase, [create_clause UTF8String], -1, &stmt, NULL) == SQLITE_OK)
            {
                if(sqlite3_step(stmt) != SQLITE_DONE)
                {
                    NSLog(@"Error register task ""%s"".", sqlite3_errmsg(dataBase));
                    ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
                }
            }
            else
            {
                NSLog(@"Error register task ""%s"".", sqlite3_errmsg(dataBase));
                ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
            }
            sqlite3_finalize(stmt);
        }
        
        for(Param* p in pars)
        {
            if([DataBase openDataBase])
            {
                NSString* clause_text=[NSString stringWithFormat:@"insert into metadata(table_name,column_name,data_type) values('%@','%@','%@')",table_name,p.name,p.type];
                sqlite3_stmt *stmt;
                if(sqlite3_prepare_v2(dataBase, [clause_text UTF8String], -1, &stmt, NULL) == SQLITE_OK)
                {
                    if(sqlite3_step(stmt) != SQLITE_DONE)
                    {
                        NSLog(@"Error register task ""%s"".", sqlite3_errmsg(dataBase));
                        ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
                    }
                }
                else
                {
                    NSLog(@"Error register task ""%s"".", sqlite3_errmsg(dataBase));
                    ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
                }
                sqlite3_finalize(stmt);
            }
        }
    }
    else
    {
        NSMutableArray* existing_pars=[[NSMutableArray alloc] init];
        NSString* select_clause=@"select ";
        
        if([DataBase openDataBase])
        {
            
            // Выборка слов
            const char *query = [[NSString stringWithFormat:@"select column_name,data_type from metadata where table_name='%@'",table_name] UTF8String];
            sqlite3_stmt *stmt;
            if(sqlite3_prepare_v2(dataBase, query, -1, &stmt, NULL) == SQLITE_OK)
            {
                Param *w;
                while(sqlite3_step(stmt) == SQLITE_ROW)
                {
                    w = [Param paramWithName:[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 0))] stringValue:@"" andType:[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 1))]];
    
                    [existing_pars addObject:w];
                }
                
            }
            else
            {
                NSLog(@"Error ""%s"".", sqlite3_errmsg(dataBase));
                ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
            }
            sqlite3_finalize(stmt);
            
        }        
        
        for(Param* p in pars)
        {
            BOOL already_exists=false;
            for(Param* pp in existing_pars)
            {
                if([[pp.name uppercaseString] isEqualToString:[p.name uppercaseString]])
                {
                    already_exists=true;
                    break;
                }
            }
            
            if(!already_exists)
            {
                NSString* add_clause=[NSString stringWithFormat:@"alter table %@ add column %@ %@;",table_name,p.name,[p castType]];
                if([DataBase openDataBase])
                {
                    sqlite3_stmt *stmt;
                    if(sqlite3_prepare_v2(dataBase, [add_clause UTF8String], -1, &stmt, NULL) == SQLITE_OK)
                    {
                        if(sqlite3_step(stmt) != SQLITE_DONE)
                        {
                            NSLog(@"Error register task ""%s"".", sqlite3_errmsg(dataBase));
                            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
                        }
                    }
                    else
                    {
                        NSLog(@"Error register task ""%s"".", sqlite3_errmsg(dataBase));
                        ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
                    }
                    sqlite3_finalize(stmt);
                }

                NSString* ins_clause=[NSString stringWithFormat:@"insert into metadata(table_name,column_name,data_type) values('%@','%@','%@');",table_name,p.name,p.type];
                if([DataBase openDataBase])
                {
                    sqlite3_stmt *stmt;
                    if(sqlite3_prepare_v2(dataBase, [ins_clause UTF8String], -1, &stmt, NULL) == SQLITE_OK)
                    {
                        if(sqlite3_step(stmt) != SQLITE_DONE)
                        {
                            NSLog(@"Error register task ""%s"".", sqlite3_errmsg(dataBase));
                            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
                        }
                    }
                    else
                    {
                        NSLog(@"Error register task ""%s"".", sqlite3_errmsg(dataBase));
                        ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
                    }
                    sqlite3_finalize(stmt);
                }
                
            }
        }
    }
    
    ///////////////////////////////////////////
    // CHECK PARAMS SET
    //////////////////////////////////////////
    
    
    BOOL has_same=false;
    NSString* select_clause = [NSString stringWithFormat:@"select 1 from %@ where %@=",table_name, key_field];
    for(Param* p in pars)
    {
        NSString* comma=([[p.type uppercaseString] isEqualToString:@"TEXT"]||[[p.type uppercaseString] isEqualToString:@"COMPLEX"]||[[p.type uppercaseString] isEqualToString:@"LIST"])?@"'":@"";
        select_clause=[select_clause stringByAppendingString:[[key_field uppercaseString] isEqualToString:[p.name uppercaseString]]?([NSString stringWithFormat:@"%@%@%@",comma, p.val, comma]):@""];
    }
    
    
    if([DataBase openDataBase])
    {
        
        // Выборка слов
        const char *query = [select_clause UTF8String];
        sqlite3_stmt *stmt;
        if(sqlite3_prepare_v2(dataBase, query, -1, &stmt, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(stmt) == SQLITE_ROW)
            {
                has_same=true;
                break;
            }
            
        }
        else
        {
            NSLog(@"Error ""%s"".", sqlite3_errmsg(dataBase));
            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
        }
        sqlite3_finalize(stmt);
        
    }
    
    if(has_same)
    {
        NSString* update_clause=[NSString stringWithFormat:@"update %@ set ",table_name];
        int i=1;
        for(Param* p in pars)
        {
            if(![[key_field uppercaseString] isEqualToString:[p.name uppercaseString]])
            {
                NSString* comma=([[p.type uppercaseString] isEqualToString:@"TEXT"]||[[p.type uppercaseString] isEqualToString:@"COMPLEX"]||[[p.type uppercaseString] isEqualToString:@"LIST"])?@"'":@"";
                update_clause=[update_clause stringByAppendingString:[NSString stringWithFormat:@"%@=%@%@%@",p.name,comma,p.val,comma]];

                if(i!=pars.count) update_clause= [update_clause stringByAppendingString:@",\n"];
            }
            i++;
        }
        
        NSRange r=[update_clause rangeOfString:@",\n" options:NSBackwardsSearch];
        
        if(r.location==[update_clause length]-2)
        {
            update_clause = [update_clause substringWithRange:NSMakeRange(0, [update_clause length]-2)];
        }
        
        update_clause=[update_clause stringByAppendingString:@" where "];
        i=1;
        for(Param* p in pars)
        {
            if([[key_field uppercaseString] isEqualToString:[p.name uppercaseString]])
            {
                NSString* comma=([[p.type uppercaseString] isEqualToString:@"TEXT"]||[[p.type uppercaseString] isEqualToString:@"COMPLEX"]||[[p.type uppercaseString] isEqualToString:@"LIST"])?@"'":@"";
                update_clause=[update_clause stringByAppendingString:[NSString stringWithFormat:@"%@=%@%@%@",p.name,comma,p.val,comma]];
            }
        }
        
        if([DataBase openDataBase])
        {
            sqlite3_stmt *stmt;
            if(sqlite3_prepare_v2(dataBase, [update_clause UTF8String], -1, &stmt, NULL) == SQLITE_OK)
            {
                if(sqlite3_step(stmt) != SQLITE_DONE)
                {
                    NSLog(@"Error register task ""%s"".", sqlite3_errmsg(dataBase));
                    ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
                }
            }
            else
            {
                NSLog(@"Error register task ""%s"".", sqlite3_errmsg(dataBase));
                ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
            }
            sqlite3_finalize(stmt);
        }
        
    }
    else
    {
        NSString* insert_clause= [NSString stringWithFormat:@"insert into %@ (",table_name];
        int i=1;
        for(Param* p in pars)
        {
            NSString* comma=([[p.type uppercaseString] isEqualToString:@"TEXT"]||[[p.type uppercaseString] isEqualToString:@"COMPLEX"]||[[p.type uppercaseString] isEqualToString:@"LIST"])?@"'":@"";
            insert_clause=[insert_clause stringByAppendingString:p.name];
            if(i!=pars.count) insert_clause=[insert_clause stringByAppendingString:@",\n"];
            i++;
        }
        
        insert_clause=[insert_clause stringByAppendingString:@") values ("];
        i=1;
        for(Param* p in pars)
        {
            NSString* comma=([[p.type uppercaseString] isEqualToString:@"TEXT"]||[[p.type uppercaseString] isEqualToString:@"COMPLEX"]||[[p.type uppercaseString] isEqualToString:@"LIST"])?@"'":@"";
            insert_clause = [insert_clause stringByAppendingString:[NSString stringWithFormat:@"%@%@%@",comma,p.val,comma]];
            if(i!=pars.count) insert_clause=[insert_clause stringByAppendingString:@",\n"];
            i++;
        }
        
        
        insert_clause=[insert_clause stringByAppendingString:@")"];
        
        if([DataBase openDataBase])
        {
            sqlite3_stmt *stmt;
            if(sqlite3_prepare_v2(dataBase, [insert_clause UTF8String], -1, &stmt, NULL) == SQLITE_OK)
            {
                if(sqlite3_step(stmt) != SQLITE_DONE)
                {
                    NSLog(@"Error register task ""%s"".", sqlite3_errmsg(dataBase));
                    ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
                }
            }
            else
            {
                NSLog(@"Error register task ""%s"".", sqlite3_errmsg(dataBase));
                ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
            }
            sqlite3_finalize(stmt);
        }
        
    }
    
    //h.closeDatabase();
}

+ (NSObject*) Deserialize:(NSString*) table_name withKey:(NSString*) key_field byValue:(NSString*) key_value
{
    NSMutableArray* array_result=nil;
    NSMutableDictionary* dict_result=nil;
    NSMutableArray* pars=[[NSMutableArray alloc] init];
    
    if([DataBase openDataBase])
    {
        
        // Выборка слов
        NSString* met_clause=[NSString stringWithFormat:@"select column_name,data_type from metadata where table_name='%@'",table_name];
        const char *query = [met_clause UTF8String];
        sqlite3_stmt *stmt;
        if(sqlite3_prepare_v2(dataBase, query, -1, &stmt, NULL) == SQLITE_OK)
        {
            Param *w;
            while(sqlite3_step(stmt) == SQLITE_ROW)
            {
                w = [Param paramWithName:[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 0))] stringValue:@"" andType:[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 1))]];
                
                [pars addObject:w];
            }
            
        }
        else
        {
            NSLog(@"Error ""%s"".", sqlite3_errmsg(dataBase));
            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
        }
        sqlite3_finalize(stmt);
        
    }
    
    if(![self isTableExists:table_name])
    {
        return nil;
    }
    
    NSString* select_clause=@"select ";
    
    int i=1;
    
    NSString* keyfield_type=@"";
    
    for(Param* p in pars)
    {
        if([[key_field uppercaseString] isEqualToString:[p.name uppercaseString]])
        {
            keyfield_type=p.type;
        }
        select_clause=[select_clause stringByAppendingString:p.name];
        if(i!=pars.count) select_clause = [select_clause stringByAppendingString:@",\n"];
        
        i++;
    }
    
    select_clause=[select_clause stringByAppendingString:[NSString stringWithFormat:@" from %@ ",table_name]];
    
    if(key_field!=nil)
    {
        if([key_field length]>0)
        {
            NSString* comma=([[keyfield_type uppercaseString] isEqualToString:@"TEXT"]||[[keyfield_type uppercaseString] isEqualToString:@"COMPLEX"]||[[keyfield_type uppercaseString] isEqualToString:@"LIST"])?@"'":@"";
            
            if(![DataBase isNumeric:key_value])
            {
                comma=@"'";
            }
            select_clause = [select_clause stringByAppendingString:[NSString stringWithFormat:@" where %@=%@%@%@",key_field,comma,key_value,comma]];
        }
    }
    
    select_clause=[select_clause stringByAppendingString:@";"];
    int count_records=0;
    
    if([DataBase openDataBase])
    {
        const char *query = [select_clause UTF8String];
        sqlite3_stmt *stmt;
        if(sqlite3_prepare_v2(dataBase, query, -1, &stmt, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(stmt) == SQLITE_ROW)
            {
                count_records++;
            }
            
        }
        else
        {
            NSLog(@"Error ""%s"".", sqlite3_errmsg(dataBase));
            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
        }
        sqlite3_finalize(stmt);
        
    }
    
    if(count_records>1) // make array
    {
        array_result=[[NSMutableArray alloc] init];

        if([DataBase openDataBase])
        {
            const char *query = [select_clause UTF8String];
            sqlite3_stmt *stmt;
            if(sqlite3_prepare_v2(dataBase, query, -1, &stmt, NULL) == SQLITE_OK)
            {
                while(sqlite3_step(stmt) == SQLITE_ROW)
                {
                    int j=0;
                    NSMutableDictionary* md=[[NSMutableDictionary alloc] init];
                    for(Param* p in pars)
                    {
                        if([[p.type uppercaseString] isEqualToString:@"TEXT"])
                        {
                            md[p.name]=[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, j))];
                        }
                        else if([[p.type uppercaseString] isEqualToString:@"COMPLEX"])
                        {
                            if([DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, j))]!=nil)
                                md[p.name]=[[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, j))] JSONValue];
                        }
                        else if([[p.type uppercaseString] isEqualToString:@"LIST"])
                        {
                            NSLog(@"ListIs %@.", [DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, j))]);
                            if([DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, j))]!=nil)
                                md[p.name]=[[NSString stringWithFormat:@"{\"array\":%@}",[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, j))] ] JSONValue][@"array"];
                        }
                        else
                        {
                            md[p.name]=[NSNumber numberWithDouble:sqlite3_column_int(stmt, j)];
                        }
                        j++;
                    }
                    [array_result addObject:md];
                }
                
            }
            else
            {
                NSLog(@"Error ""%s"".", sqlite3_errmsg(dataBase));
                ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
            }
            sqlite3_finalize(stmt);
            
        }
    }
    else if(count_records==0) // make empty object
    {
        dict_result = nil;//[[NSMutableDictionary alloc] init];
    }
    else//make single object
    {
        dict_result=[[NSMutableDictionary alloc] init];
        
        if([DataBase openDataBase])
        {
            const char *query = [select_clause UTF8String];
            sqlite3_stmt *stmt;
            if(sqlite3_prepare_v2(dataBase, query, -1, &stmt, NULL) == SQLITE_OK)
            {
                while(sqlite3_step(stmt) == SQLITE_ROW)
                {
                    int j=0;

                    for(Param* p in pars)
                    {
                        if([[p.type uppercaseString] isEqualToString:@"TEXT"])
                        {
                            dict_result[p.name]=[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, j))];
                        }
                        else if([[p.type uppercaseString] isEqualToString:@"COMPLEX"])
                        {
                            dict_result[p.name]=[[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, j))] JSONValue];
                        }
                        else if([[p.type uppercaseString] isEqualToString:@"LIST"])
                        {
                            dict_result[p.name]=[[NSString stringWithFormat:@"{\"array\":%@}",[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, j))] ] JSONValue][@"array"];
                        }
                        else
                        {
                            dict_result[p.name]=[NSNumber numberWithDouble:sqlite3_column_int(stmt, j)];
                        }
                        j++;
                    }
                }
                
            }
            else
            {
                NSLog(@"Error ""%s"".", sqlite3_errmsg(dataBase));
                ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
            }
            sqlite3_finalize(stmt);
        }
    }
    
    return array_result==nil?dict_result:array_result;
}


+ (void) Kill:(NSString*) table_name withKey:(NSString*) key_field byValue:(NSString*) key_value
{
    if([(key_field==nil?@"":key_field) length]>0)
    {
        Param* pars=nil;
        
        if([DataBase openDataBase])
        {
            
            // Выборка слов
            NSString* met_clause=[NSString stringWithFormat:@"select column_name,data_type from metadata where table_name='%@' and column_name='%@'",table_name,key_field];
            const char *query = [met_clause UTF8String];
            sqlite3_stmt *stmt;
            if(sqlite3_prepare_v2(dataBase, query, -1, &stmt, NULL) == SQLITE_OK)
            {
                while(sqlite3_step(stmt) == SQLITE_ROW)
                {
                    pars = [Param paramWithName:[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 0))] stringValue:@"" andType:[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 1))]];
                }
                
            }
            else
            {
                NSLog(@"Error ""%s"".", sqlite3_errmsg(dataBase));
                ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
            }
            sqlite3_finalize(stmt);
            
        }
        
        if(pars!=nil)
        {
            NSString* comma=([[pars.type uppercaseString] isEqualToString:@"TEXT"]||[[pars.type uppercaseString] isEqualToString:@"COMPLEX"]||[[pars.type uppercaseString] isEqualToString:@"LIST"])?@"'":@"";;
            NSString* delete_clause= [NSString stringWithFormat:@"delete from %@ where %@=%@%@%@;",table_name, key_field,comma,key_value,comma];
            if([DataBase openDataBase])
            {
                sqlite3_stmt *stmt;
                if(sqlite3_prepare_v2(dataBase, [delete_clause UTF8String], -1, &stmt, NULL) == SQLITE_OK)
                {
                    if(sqlite3_step(stmt) != SQLITE_DONE)
                    {
                        NSLog(@"Error register task ""%s"".", sqlite3_errmsg(dataBase));
                        ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
                    }
                }
                else
                {
                    NSLog(@"Error register task ""%s"".", sqlite3_errmsg(dataBase));
                    ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
                }
                sqlite3_finalize(stmt);
            }
        } 
    }
    else
    {
        NSString* delete_clause=[NSString stringWithFormat:@"delete from %@;",table_name];
        if([DataBase openDataBase])
        {
            sqlite3_stmt *stmt;
            if(sqlite3_prepare_v2(dataBase, [delete_clause UTF8String], -1, &stmt, NULL) == SQLITE_OK)
            {
                if(sqlite3_step(stmt) != SQLITE_DONE)
                {
                    NSLog(@"Error register task ""%s"".", sqlite3_errmsg(dataBase));
                    ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
                }
            }
            else
            {
                NSLog(@"Error register task ""%s"".", sqlite3_errmsg(dataBase));
                ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
            }
            sqlite3_finalize(stmt);
        }
    }
}

+ (void) putDB:(NSDictionary*) result toTable:(NSString*) table withKey:(NSString*) key
{
    NSArray* keys=[result allKeys];

    NSMutableArray* params=[[NSMutableArray alloc] init];
    for(NSString* keyname in keys)
    {
        NSObject* val=result[keyname];
        if([val isKindOfClass:[NSString class]])
        {
            Param* p=[Param paramWithName:keyname stringValue:[(NSString*)val stringByReplacingOccurrencesOfString:@"'" withString:@""] andType:@"TEXT"];
            [params addObject:p];
        }
        /*if(val instanceof Integer)
        {
            params.add(new Param(keyname, (Integer)val, "INT"));
        }
        if(val instanceof Float)
        {
            params.add(new Param(keyname, (Float)val, "FLOAT"));
        }*/
        if([val isKindOfClass:[NSNumber class]])
        {
            Param* p=[Param paramWithName:keyname doubleValue:[(NSNumber*)val doubleValue]  andType:@"DOUBLE"];
            [params addObject:p];
        }
        if([val isKindOfClass:[NSDictionary class]])
        {
            Param* p=[Param paramWithName:keyname stringValue:[(NSDictionary*)val JSONRepresentation]  andType:@"COMPLEX"];
            [params addObject:p];
        }
        if([val isKindOfClass:[NSArray class]])
        {
            Param* p=[Param paramWithName:keyname stringValue:[(NSDictionary*)val JSONRepresentation]  andType:@"LIST"];
            [params addObject:p];
        }
    }
    
    [DataBase Serialize:table withPars:params andKey:key];
}

+ (NSObject*) getDB:(NSString*) table withKey:(NSString*) key andValue:(NSString*) value
{
    return [DataBase Deserialize:table withKey:key byValue:value];
}

+ (NSArray*) getDBArray:(NSString*) table withKey:(NSString*) key andValue:(NSString*) value
{
    NSArray* result=[[NSArray alloc] init];
    NSObject* obj = [DataBase Deserialize:table withKey:key byValue:value];
    
    if(obj!=nil)
    {
        if([obj isKindOfClass:[NSArray class]])
        {
            result = [NSArray arrayWithArray:(NSArray*)obj];
        }
        else
        {
            result = [NSArray arrayWithObject:(NSDictionary*)obj];
        }
    }
    
    return result;
}

+ (void) killDB:(NSString*) table withKey:(NSString*) key andValue:(NSString*) value
{
    [DataBase Kill:table withKey:key byValue:value];
}

+ (void) registerTask:(NSString*)ObjId ObjType:(int)obj_type Command:(int) command AdditionalId:(int) _num_id
{
    [DataBase openDataBase];
    const char *query = [@"insert into tasks(object_type, object_id, action, object_num_id) values(?,?,?,?);" UTF8String];
    sqlite3_stmt *stmt;
    if(sqlite3_prepare_v2(dataBase, query, -1, &stmt, NULL) == SQLITE_OK) 
    {
        
        sqlite3_bind_int(stmt, 1, obj_type);
        sqlite3_bind_text(stmt, 2, [ObjId UTF8String], -1, NULL);
        sqlite3_bind_int(stmt, 3, command);
        sqlite3_bind_int(stmt, 4, _num_id);
        
        if(sqlite3_step(stmt) != SQLITE_DONE)
        {
            NSLog(@"Error register task ""%s"".", sqlite3_errmsg(dataBase));
            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
        }
    }
    else
    {
        NSLog(@"Error register task ""%s"".", sqlite3_errmsg(dataBase));
        ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
    }
    sqlite3_finalize(stmt);
}

+ (void) registerTask:(NSString*)ObjId ObjType:(int)obj_type Command:(int) command
{
    [DataBase openDataBase];
    const char *query = [@"insert into tasks(object_type, object_id, action) values(?,?,?);" UTF8String];
    sqlite3_stmt *stmt;
    if(sqlite3_prepare_v2(dataBase, query, -1, &stmt, NULL) == SQLITE_OK) 
    {
        
        sqlite3_bind_int(stmt, 1, obj_type);
        sqlite3_bind_text(stmt, 2, [ObjId UTF8String], -1, NULL);
        sqlite3_bind_int(stmt, 3, command);
        
        if(sqlite3_step(stmt) != SQLITE_DONE)
        {
            NSLog(@"Error register task ""%s"".", sqlite3_errmsg(dataBase));
            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
        }
    }
    else
    {
        ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
        NSLog(@"Error register task ""%s"".", sqlite3_errmsg(dataBase));
    }
    sqlite3_finalize(stmt);
}

/*+ (NSArray*) getTasks:(int) limit
{   
    NSMutableArray *ret = [[NSMutableArray alloc] init];
    
    if([DataBase openDataBase])
    {          

        // Выборка слов
        const char *query = [@"select object_type, command, r_obj_id, first_word, second_word, id from tasks order by n asc limit ?;" UTF8String];
        sqlite3_stmt *stmt;
        if(sqlite3_prepare_v2(dataBase, query, -1, &stmt, NULL) == SQLITE_OK)
        {
            Task *w;
            sqlite3_bind_int(stmt, 1, limit);
            while(sqlite3_step(stmt) == SQLITE_ROW)
            {
                w = [[Task alloc] init];
                w.object_type = sqlite3_column_int(stmt, 0);
                w.command = sqlite3_column_int(stmt, 1);
                w.r_obj_id=[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 2))];
                w.firstWord = [DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 3))];
                w.secondWord = [DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 4))];
                w.objId = [DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 5))];
                
                [ret addObject:w];
                // todo debug
                //NSLog(@"%ld, %ld, %ld", currentWord.rndValue, w.rndValue, w.ID);
            }
            
        }
        else
        {
            NSLog(@"Error ""%s"".", sqlite3_errmsg(dataBase));
            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
        }
        sqlite3_finalize(stmt);

    }        
    
    return ret;
}

+ (void) confirmGetTasks:(int) limit
{
    if([DataBase openDataBase])
    {
        //очистка списка слов
        const char *query2 = [@"delete from tasks where n in (select n from tasks order by n asc limit ?);" UTF8String];
        sqlite3_stmt *stmt2;
        if(sqlite3_prepare_v2(dataBase, query2, -1, &stmt2, NULL) == SQLITE_OK)
        {
            sqlite3_bind_int(stmt2, 1, limit);
            if(sqlite3_step(stmt2) != SQLITE_DONE)
            {
                NSLog(@"Error confirm tasks ""%s"".", sqlite3_errmsg(dataBase));
                ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
            }            
        }
        else
        {
            NSLog(@"Error ""%s"".", sqlite3_errmsg(dataBase));
            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
        }
        sqlite3_finalize(stmt2);
    }   
}

+ (void) confirmGetTasks
{
    if([DataBase openDataBase])
    {
        //очистка списка слов
        const char *query2 = [@"delete from tasks;" UTF8String];
        sqlite3_stmt *stmt2;
        if(sqlite3_prepare_v2(dataBase, query2, -1, &stmt2, NULL) == SQLITE_OK)
        {
            if(sqlite3_step(stmt2) != SQLITE_DONE)
            {
                NSLog(@"Error confirm tasks ""%s"".", sqlite3_errmsg(dataBase));
                ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
            }            
        }
        else
        {
            NSLog(@"Error ""%s"".", sqlite3_errmsg(dataBase));
            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
        }
        sqlite3_finalize(stmt2);
    }   
}

+ (NSArray*) getFriends:(Boolean)trusted Requested:(Boolean)requested Except:(NSString*) reminder_id
{   
    NSMutableArray *ret = [[NSMutableArray alloc] init];
    
    if([DataBase openDataBase])
    {          
        // Выборка слов
        const char *query;
        NSString* trusted_cond;
        if(trusted) trusted_cond=@""; else trusted_cond=@" NOT ";

        NSString* requested_cond;
        if(requested) requested_cond=@""; else requested_cond=@" NOT ";
        
        NSString* ns_query=[[NSString alloc] initWithFormat:@"select fr.username, fr.firstname, fr.lastname, fr.ava_link from friends fr where %@ trusted and %@ requested and not exists (select 1 from reminder_recipients rr where rr.reminder_id=? and rr.username=fr.username);",trusted_cond,requested_cond];
        query = [ns_query UTF8String];
        sqlite3_stmt *stmt;
        if(sqlite3_prepare_v2(dataBase, query, -1, &stmt, NULL) == SQLITE_OK)
        {
            Friend *f;
            sqlite3_bind_text(stmt, 1, [reminder_id UTF8String], -1, NULL);
            //sqlite3_bind_int(stmt, 1, reminder_id);
            while(sqlite3_step(stmt) == SQLITE_ROW)
            {
                f = [[Friend alloc] init];
                f.Username = [DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 0))];
                f.Firstname = [DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 1))];
                f.Lastname = [DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 2))];
                f.AvaLink = [DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 3))];
                [ret addObject:f];
                // todo debug
                //NSLog(@"%ld, %ld, %ld", currentWord.rndValue, w.rndValue, w.ID);
            }
            
        }
        else
        {
            NSLog(@"Error ""%s"".", sqlite3_errmsg(dataBase));
            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
        }
        sqlite3_finalize(stmt);
        
    }        
    
    return ret;
}

+ (NSArray*) getFriends:(Boolean)trusted Requested:(Boolean)requested ExceptFriends:(NSArray*) _except_friends
{   
    NSMutableArray *ret = [[NSMutableArray alloc] init];
    
    if([DataBase openDataBase])
    {          
        // Выборка слов
        const char *query;
        NSString* trusted_cond;
        if(trusted) trusted_cond=@""; else trusted_cond=@" NOT ";
        
        NSString* requested_cond;
        if(requested) requested_cond=@""; else requested_cond=@" NOT ";
        
        NSString* ns_query=[[NSString alloc] initWithFormat:@"select fr.username, fr.firstname, fr.lastname, fr.ava_link from friends fr where %@ trusted and %@ requested",trusted_cond,requested_cond];

        query = [ns_query UTF8String];
        sqlite3_stmt *stmt;
        if(sqlite3_prepare_v2(dataBase, query, -1, &stmt, NULL) == SQLITE_OK)
        {
            Friend *f;
            while(sqlite3_step(stmt) == SQLITE_ROW)
            {
                f = [[Friend alloc] init];
                f.Username = [DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 0))];
                f.Firstname = [DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 1))];
                f.Lastname = [DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 2))];
                f.AvaLink = [DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 3))];
                BOOL is_except=true;
                for(Friend* tmp_fr in _except_friends)
                {
                    if([tmp_fr.Username compare:f.Username]==NSOrderedSame)
                    {
                        is_except=false;
                        break;
                    }
                }
                if(is_except)
                    [ret addObject:f];
                // todo debug
                //NSLog(@"%ld, %ld, %ld", currentWord.rndValue, w.rndValue, w.ID);
            }
            
        }
        else
        {
            NSLog(@"Error ""%s"".", sqlite3_errmsg(dataBase));
            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
        }
        sqlite3_finalize(stmt);
        
    }        
    
    return ret;
}


+ (NSArray*) getFriends:(Boolean)trusted Requested:(Boolean)requested {   
    NSMutableArray *ret = [[NSMutableArray alloc] init];
    
    if([DataBase openDataBase])
    {          
        // Выборка слов
        const char *query;
        NSString* trusted_cond;
        if(trusted) trusted_cond=@""; else trusted_cond=@" NOT ";
        
        NSString* requested_cond;
        if(requested) requested_cond=@""; else requested_cond=@" NOT ";
        
        NSString* ns_query=[[NSString alloc] initWithFormat:@"select fr.username, fr.firstname, fr.lastname, fr.ava_link from friends fr where %@ trusted and %@ requested",trusted_cond,requested_cond];
        query = [ns_query UTF8String];
        sqlite3_stmt *stmt;
        if(sqlite3_prepare_v2(dataBase, query, -1, &stmt, NULL) == SQLITE_OK)
        {
            Friend *f;
            while(sqlite3_step(stmt) == SQLITE_ROW)
            {
                f = [[Friend alloc] init];
                f.Username = [DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 0))];
                f.Firstname = [DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 1))];
                f.Lastname = [DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 2))];
                f.AvaLink = [DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 3))];
                [ret addObject:f];
                // todo debug
                //NSLog(@"%ld, %ld, %ld", currentWord.rndValue, w.rndValue, w.ID);
            }
            
        }
        else
        {
            NSLog(@"Error ""%s"".", sqlite3_errmsg(dataBase));
            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
        }
        sqlite3_finalize(stmt);
        
    }        
    
    return ret;
}

+(Friend*) getFriend:(NSString*) _username
{
    Friend* result=nil;
    [DataBase openDataBase];
    
    const char *query = [@"select fr.username, fr.firstname, fr.lastname, fr.ava_link from friends fr where fr.username=?;" UTF8String];
    sqlite3_stmt *stmt;
    if(sqlite3_prepare_v2(dataBase, query, -1, &stmt, NULL) == SQLITE_OK) 
    {
        sqlite3_bind_text(stmt, 1, [_username UTF8String], -1, NULL);
        
        while(sqlite3_step(stmt) == SQLITE_ROW)
        {
            result=[[Friend alloc] init];
            result.Username=[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 0))];
            result.Firstname = [DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 1))];
            result.Lastname = [DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 2))];
            result.AvaLink = [DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 3))];
        }
    }
    else 
    {
        NSLog(@"Error getting friend ""%s"".", sqlite3_errmsg(dataBase));
        ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
        result=false;
    }
    
    sqlite3_finalize(stmt);
    
    return result;
}


+ (bool) addFriend:(Friend* ) friend friendRequestedTrusted:(BOOL) req_trust
{
    BOOL result=true;
    [DataBase openDataBase];
    NSString* requested_cond;
    if(req_trust) requested_cond=@"1, 0"; else requested_cond=@"0, 1";
    NSString* ns_query=[[NSString alloc] initWithFormat:@"insert into friends(username, firstname, lastname, ava_link, requested, trusted) values(?,?,?,?,%@);",requested_cond];
    
    const char *query = [ns_query UTF8String];
    sqlite3_stmt *stmt;
    if(sqlite3_prepare_v2(dataBase, query, -1, &stmt, NULL) == SQLITE_OK) 
    {
        sqlite3_bind_text(stmt, 1, [friend.Username UTF8String], -1, NULL);
        sqlite3_bind_text(stmt, 2, [friend.Firstname UTF8String], -1, NULL);
        sqlite3_bind_text(stmt, 3, [friend.Lastname UTF8String], -1, NULL);
        sqlite3_bind_text(stmt, 4, [friend.AvaLink UTF8String], -1, NULL);
        
        if(sqlite3_step(stmt) != SQLITE_DONE)
        {
            NSLog(@"Error register friend ""%s"".", sqlite3_errmsg(dataBase));
            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
            result=false;
        }
    }
    else 
    {
        NSLog(@"Error register friend ""%s"".", sqlite3_errmsg(dataBase));
        ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
        result=false;
    }
    
    sqlite3_finalize(stmt);
    
    return result;
}

+ (bool) approveFriend:(Friend* )friend 
{
    BOOL result=true;
    BOOL has_been_updated=false;
    //int trying_stopper=0;
    
    [DataBase openDataBase];
    
    const char *query = [@"update friends set trusted=1, requested=1 where username=?;" UTF8String];
    do
    {
        sqlite3_stmt *stmt;
        if(sqlite3_prepare_v2(dataBase, query, -1, &stmt, NULL) == SQLITE_OK) 
        {
            sqlite3_bind_text(stmt, 1, [friend.Username UTF8String], -1, NULL);
        
            if(sqlite3_step(stmt) != SQLITE_DONE)
            {
                NSLog(@"Error approve friend ""%s"".", sqlite3_errmsg(dataBase));
                ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
                result=false;
            }
            
            has_been_updated=(sqlite3_changes(dataBase)>0);
        }
        else 
        {
            NSLog(@"Error approve friend ""%s"".", sqlite3_errmsg(dataBase));
            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
            result=false;
        }
    
        sqlite3_finalize(stmt);
    
        if(!has_been_updated)
        {
            result=false;
            
            if(![self addFriend:friend friendRequestedTrusted:YES])
                break; // must stop this madness: no update no insert:(
            else
                result=true; // has been inserted anyway
        }
        
    } while (!has_been_updated);

    
    return result;
}

+ (bool) rejectFriend:(NSString* )friend_username 
{
    BOOL result=true;
    [DataBase openDataBase];
    
    const char *query = [@"delete from friends where username=?;" UTF8String];
    sqlite3_stmt *stmt;
    if(sqlite3_prepare_v2(dataBase, query, -1, &stmt, NULL) == SQLITE_OK) 
    {
        sqlite3_bind_text(stmt, 1, [friend_username UTF8String], -1, NULL);
        
        if(sqlite3_step(stmt) != SQLITE_DONE)
        {
            NSLog(@"Error delete friend ""%s"".", sqlite3_errmsg(dataBase));
            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
            result=false;
        }
    }
    else 
    {
        NSLog(@"Error delete friend ""%s"".", sqlite3_errmsg(dataBase));
        ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
        result=false;
    }
    
    sqlite3_finalize(stmt);
    
    return result;
}

+ (BOOL) isFriendTrusted:(NSString* ) friend_username
{
    
    if([DataBase openDataBase])
    {          
        // Выборка слов
         const char *query = [@"select trusted from friends where username=?;" UTF8String];
        sqlite3_stmt *stmt;
        if(sqlite3_prepare_v2(dataBase, query, -1, &stmt, NULL) == SQLITE_OK)
        {
            sqlite3_bind_text(stmt, 1, [friend_username UTF8String], -1, NULL);
            while(sqlite3_step(stmt) == SQLITE_ROW)
            {
                return sqlite3_column_int(stmt, 0);                // todo debug
            }
            
        }
        else
        {
            NSLog(@"Error ""%s"".", sqlite3_errmsg(dataBase));
            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
        }
        sqlite3_finalize(stmt);
        
    }        
    
    return false;
}

+(void) killAll
{
    if([DataBase openDataBase])
    {          
        // Выборка слов
        const char *query = [@"delete from friends" UTF8String];
        sqlite3_stmt *stmt;
        if(sqlite3_prepare_v2(dataBase, query, -1, &stmt, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(stmt) == SQLITE_DONE)
            {
             // todo debug
                NSLog(@"Kill friends");
            }
            
        }
        else
        {
            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
            NSLog(@"Error ""%s"".", sqlite3_errmsg(dataBase));
        }
        sqlite3_finalize(stmt);
        
        query = [@"delete from reminder_recipients where reminder_id in (select id from reminder where reminder_id>0)" UTF8String];
        if(sqlite3_prepare_v2(dataBase, query, -1, &stmt, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(stmt) == SQLITE_DONE)
            {
                // todo debug
                NSLog(@"Kill recipients");
            }
            
        }
        else
        {
            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
            NSLog(@"Error ""%s"".", sqlite3_errmsg(dataBase));
        }
        sqlite3_finalize(stmt);
        
        query = [@"delete from monitored_reminders where reminder_id in (select id from reminder where reminder_id>0)" UTF8String];
        if(sqlite3_prepare_v2(dataBase, query, -1, &stmt, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(stmt) == SQLITE_DONE)
            {
                // todo debug
                NSLog(@"Kill monitored");
            }
            
        }
        else
        {
            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
            NSLog(@"Error ""%s"".", sqlite3_errmsg(dataBase));
        }
        sqlite3_finalize(stmt);
        
        query = [@"delete from entered_locations where reminder_id in (select id from reminder where reminder_id>0)" UTF8String];
        if(sqlite3_prepare_v2(dataBase, query, -1, &stmt, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(stmt) == SQLITE_DONE)
            {
                // todo debug
                NSLog(@"Kill entered");
            }
            
        }
        else
        {
            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
            NSLog(@"Error ""%s"".", sqlite3_errmsg(dataBase));
        }
        sqlite3_finalize(stmt);
        
        query = [@"delete from reminder where reminder_id>0" UTF8String];
        if(sqlite3_prepare_v2(dataBase, query, -1, &stmt, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(stmt) == SQLITE_DONE)
            {
                // todo debug
                NSLog(@"Kill reminders");
            }
            
        }
        else
        {
            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
            NSLog(@"Error ""%s"".", sqlite3_errmsg(dataBase));
        }
        sqlite3_finalize(stmt);
        
        
    }  
}

+(BOOL) saveReminder:(Reminder*) rem
{
    BOOL result=true;
    [DataBase openDataBase];
    
    if(rem.internal_id==nil)
        rem.internal_id=GUIDString();
    
    const char *query = [@"insert into reminder(is_timed, is_placed, reminder_text, reminder_id, hours, minutes, self_remind, single_date, schedule, days_of_week, end_date, geomap_char, map_zoom, map_center_x, map_center_y, id, owner, radius, waiting_time, proximity_warning, warn_for) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" UTF8String];
    sqlite3_stmt *stmt;
    int tmp_timed=0;
    int tmp_mapped=0;
    int tmp_self_remind=0;
    int tmp_proximity_warning=0;
    int tmp_hours=0;
    int tmp_minutes=0;
    
    if(rem.schedule_type!=4) // not NONE
        tmp_timed=1;
    if(rem.mapped) 
        tmp_mapped=1;
    if(rem.self_remind)
        tmp_self_remind=1;
    if(rem.proximity_warning)
        tmp_proximity_warning=1;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH"];   
    tmp_hours = [[formatter stringFromDate:rem.time] intValue];
    [formatter setDateFormat:@"mm"];
    tmp_minutes = [[formatter stringFromDate:rem.time] intValue];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateFromString=[dateFormat stringFromDate:rem.date_start];
    NSString *dateToString=[dateFormat stringFromDate:rem.date_finish];
    
    NSString* tmp_map_center_x=[[NSString alloc] initWithFormat:@"%f",rem.x];
    NSString* tmp_map_center_y=[[NSString alloc] initWithFormat:@"%f",rem.y];
    
    if(sqlite3_prepare_v2(dataBase, query, -1, &stmt, NULL) == SQLITE_OK) 
    {
        sqlite3_bind_int(stmt, 1, tmp_timed);
        sqlite3_bind_int(stmt, 2, tmp_mapped);
        sqlite3_bind_text(stmt, 3, [rem.text UTF8String], -1, NULL);
        sqlite3_bind_int(stmt, 4, rem.id);
        sqlite3_bind_int(stmt, 5, tmp_hours);
        sqlite3_bind_int(stmt, 6, tmp_minutes);
        sqlite3_bind_int(stmt, 7, tmp_self_remind);
        sqlite3_bind_text(stmt, 8, [dateFromString UTF8String], -1, NULL);
        sqlite3_bind_int(stmt, 9, rem.schedule_type);
        sqlite3_bind_int(stmt, 10, [rem getDays]);
        sqlite3_bind_text(stmt, 11, [dateToString UTF8String], -1, NULL);
        sqlite3_bind_text(stmt, 12, [[rem getGeomapString] UTF8String], -1, NULL);      
        sqlite3_bind_int(stmt, 13, rem.zoom);//!!! map_zoom!!!
        sqlite3_bind_text(stmt, 14, [tmp_map_center_x UTF8String], -1, NULL);
        sqlite3_bind_text(stmt, 15, [tmp_map_center_y UTF8String], -1, NULL);   
        sqlite3_bind_text(stmt, 16, [rem.internal_id UTF8String], -1, NULL); 
        sqlite3_bind_text(stmt, 17, [rem.reminder_author UTF8String], -1, NULL); 
        sqlite3_bind_int(stmt, 18, rem.radius);
        sqlite3_bind_int(stmt, 19, rem.waiting_time);
        sqlite3_bind_int(stmt, 20, tmp_proximity_warning);
        sqlite3_bind_int(stmt, 21, rem.warn_for);
        if(sqlite3_step(stmt) != SQLITE_DONE)
        {
            NSLog(@"Error save reminder ""%s"".", sqlite3_errmsg(dataBase));
            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
            result=false;
        }

    }
    else 
    {
        NSLog(@"Error save reminder ""%s"".", sqlite3_errmsg(dataBase));
        ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
        result=false;
    }
    
    sqlite3_finalize(stmt);
    
    if(result&&rem.recipient.count>0)
    {
        const char *query_rec = [@"insert into reminder_recipients(reminder_id, username) values (?, ?)" UTF8String];
        
        
        for(Friend* rc in rem.recipient)
        {
            if(sqlite3_prepare_v2(dataBase, query_rec, -1, &stmt, NULL) == SQLITE_OK) 
            {
                sqlite3_bind_text(stmt, 1, [rem.internal_id UTF8String], -1, NULL);
                sqlite3_bind_text(stmt, 2, [rc.Username UTF8String], -1, NULL);
                if(sqlite3_step(stmt) != SQLITE_DONE)
                {
                    NSLog(@"Error adding recipient %@ ""%s"".", rc, sqlite3_errmsg(dataBase));
                    ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
                    result=false;
                }
            }
            else 
            {
                NSLog(@"Error adding recipients ""%s"".", sqlite3_errmsg(dataBase));
                ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
                result=false;
            }
            sqlite3_finalize(stmt);
        }

    }
    
    
    return result;

}

+(BOOL) editReminder:(Reminder*) rem
{
    BOOL result=true;
    
    if(rem.internal_id==nil||rem.internal_id.length==0)
    {
        rem.internal_id=[DataBase getReminderInternalIdByExtenal:rem.id];
    }
    
    if(rem.internal_id==nil||rem.internal_id.length==0) // we have no such reminder
    {
        result=[DataBase saveReminder:rem];
    }
    else
    {
        [DataBase openDataBase];
        
        const char *query; 
        
        if(rem.id>0)
        {
            query=[@"update reminder set is_timed=?, is_placed=?, reminder_text=?,  hours=?, minutes=?, self_remind=?, single_date=?, schedule=?, days_of_week=?, end_date=?, geomap_char=?, map_zoom=?, map_center_x=?, map_center_y=?, radius=?, waiting_time=?, proximity_warning=?, warn_for=? where reminder_id=?;" UTF8String];
        }
        else
        {
            query=[@"update reminder set is_timed=?, is_placed=?, reminder_text=?,  hours=?, minutes=?, self_remind=?, single_date=?, schedule=?, days_of_week=?, end_date=?, geomap_char=?, map_zoom=?, map_center_x=?, map_center_y=?, radius=?, waiting_time=?, proximity_warning=?, warn_for=? where id=?;" UTF8String];
        }
        
        
        sqlite3_stmt *stmt;
        int tmp_timed=0;
        int tmp_mapped=0;
        int tmp_self_remind=0;
        int tmp_proximity_warning=0;
        int tmp_hours=0;
        int tmp_minutes=0;
        
        if(rem.schedule_type!=4) // not NONE
            tmp_timed=1;
        if(rem.mapped) 
            tmp_mapped=1;
        if(rem.self_remind)
            tmp_self_remind=1;
        if(rem.proximity_warning)
            tmp_proximity_warning=1;
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"HH"];   
        tmp_hours = [[formatter stringFromDate:rem.time] intValue];
        [formatter setDateFormat:@"mm"];
        tmp_minutes = [[formatter stringFromDate:rem.time] intValue];
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *dateFromString=[dateFormat stringFromDate:rem.date_start];
        NSString *dateToString=[dateFormat stringFromDate:rem.date_finish];
        
        NSString* tmp_map_center_x=[[NSString alloc] initWithFormat:@"%f",rem.x];
        NSString* tmp_map_center_y=[[NSString alloc] initWithFormat:@"%f",rem.y];
        
        if(sqlite3_prepare_v2(dataBase, query, -1, &stmt, NULL) == SQLITE_OK) 
        {
            sqlite3_bind_int(stmt, 1, tmp_timed);
            sqlite3_bind_int(stmt, 2, tmp_mapped);
            sqlite3_bind_text(stmt, 3, [rem.text UTF8String], -1, NULL);
            sqlite3_bind_int(stmt, 4, tmp_hours);
            sqlite3_bind_int(stmt, 5, tmp_minutes);
            sqlite3_bind_int(stmt, 6, tmp_self_remind);
            sqlite3_bind_text(stmt, 7, [dateFromString UTF8String], -1, NULL);
            sqlite3_bind_int(stmt, 8, rem.schedule_type);
            sqlite3_bind_int(stmt, 9, [rem getDays]);
            sqlite3_bind_text(stmt, 10, [dateToString UTF8String], -1, NULL);
            sqlite3_bind_text(stmt, 11, [[rem getGeomapString] UTF8String], -1, NULL);      
            sqlite3_bind_int(stmt, 12, rem.zoom);//!!! map_zoom!!!
            sqlite3_bind_text(stmt, 13, [tmp_map_center_x UTF8String], -1, NULL);
            sqlite3_bind_text(stmt, 14, [tmp_map_center_y UTF8String], -1, NULL);
            sqlite3_bind_int(stmt, 15, rem.radius);
            sqlite3_bind_int(stmt, 16, rem.waiting_time);
            sqlite3_bind_int(stmt, 17, tmp_proximity_warning);
            sqlite3_bind_int(stmt, 18, rem.warn_for);
            if(rem.id>0)
                sqlite3_bind_int(stmt, 19, rem.id);
            else
                sqlite3_bind_text(stmt, 19, [rem.internal_id UTF8String], -1, NULL);
            if(sqlite3_step(stmt) != SQLITE_DONE)
            {
                NSLog(@"Error save reminder ""%s"".", sqlite3_errmsg(dataBase));
                ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
                result=false;
            }
        }
        else 
        {
            NSLog(@"Error save reminder ""%s"".", sqlite3_errmsg(dataBase));
            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
            result=false;
        }
        
        sqlite3_finalize(stmt);
        
        if(result&&rem.id>0)
        {
            ////////////////////////////////////////////
            const char *delete_rec = [@"delete from reminder_recipients where reminder_id=?;" UTF8String];
            if(sqlite3_prepare_v2(dataBase, delete_rec, -1, &stmt, NULL) == SQLITE_OK) 
            {
                sqlite3_bind_text(stmt, 1, [rem.internal_id UTF8String], -1, NULL); 
                
                if(sqlite3_step(stmt) != SQLITE_DONE)
                {
                    NSLog(@"Error removing recipients %s.", sqlite3_errmsg(dataBase));
                    ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
                    result=false;
                }
            }
            else 
            {
                NSLog(@"Error adding recipients ""%s"".", sqlite3_errmsg(dataBase));
                ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
                result=false;
            }
            sqlite3_finalize(stmt);
            
            ////////////////////////////////////////////
            const char *query_rec = [@"insert into reminder_recipients(reminder_id, username) values (?, ?)" UTF8String];
            
            
            for(Friend* rc in rem.recipient)
            {
                if(sqlite3_prepare_v2(dataBase, query_rec, -1, &stmt, NULL) == SQLITE_OK) 
                {
                    sqlite3_bind_text(stmt, 1, [rem.internal_id UTF8String], -1, NULL); 
                    sqlite3_bind_text(stmt, 2, [rc.Username UTF8String], -1, NULL);
                    if(sqlite3_step(stmt) != SQLITE_DONE)
                    {
                        NSLog(@"Error adding recipient %@ ""%s"".", rc, sqlite3_errmsg(dataBase));
                        ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
                        result=false;
                    }
                }
                else 
                {
                    NSLog(@"Error adding recipients ""%s"".", sqlite3_errmsg(dataBase));
                    ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
                    result=false;
                }
                sqlite3_finalize(stmt);
            }
            
        }
    }
    
    return result;
    
}


+ (NSArray*) getReminders:(Boolean)including_disabled
{   
    NSMutableArray *ret = [[NSMutableArray alloc] init];
    
    
    if([DataBase openDataBase])
    {          
        // Выборка слов
        const char *query;
        
        if(including_disabled)
        {
            query= [@"select is_timed, is_placed, reminder_text, reminder_id, hours, minutes, self_remind, single_date, schedule, days_of_week, end_date, geomap_char, map_zoom, map_center_x, map_center_y, id, owner, radius, waiting_time, last_fire_time, disabled, proximity_warning, warn_for from reminder" UTF8String];
        }
        else
        {
            query= [@"select is_timed, is_placed, reminder_text, reminder_id, hours, minutes, self_remind, single_date, schedule, days_of_week, end_date, geomap_char, map_zoom, map_center_x, map_center_y, id, owner, radius, waiting_time, last_fire_time, disabled, proximity_warning, warn_for from reminder where disabled=0" UTF8String];
        }
        sqlite3_stmt *stmt;
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];

        
        if(sqlite3_prepare_v2(dataBase, query, -1, &stmt, NULL) == SQLITE_OK)
        {
            Reminder *r;
            while(sqlite3_step(stmt) == SQLITE_ROW)
            {
                r = [[Reminder alloc] init];
                r.scheduled=sqlite3_column_int(stmt, 0)==1;
                r.mapped=sqlite3_column_int(stmt, 1)==1;
                r.text = [DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 2))];
                r.id = sqlite3_column_int(stmt, 3);
                //////////////// time /////////////////
                [dateFormatter setDateFormat:@"HH:mm"];
                NSString* tmpTime=[[NSString alloc] initWithFormat:@"%d:%d",sqlite3_column_int(stmt, 4), sqlite3_column_int(stmt, 5)];
                r.time=[dateFormatter dateFromString:tmpTime];
                //////////////////////////////////////
                r.self_remind=sqlite3_column_int(stmt, 6)==1;
                /////////////// date from ////////////
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                r.date_start=[dateFormatter dateFromString:[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 7))]];
                //////////////////////////////////////
                r.schedule_type=sqlite3_column_int(stmt, 8);
                [r setDays:sqlite3_column_int(stmt, 9)];
                /////////////// date to ////////////
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                r.date_finish=[dateFormatter dateFromString:[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 10))]];
                //////////////////////////////////////
                ///////////// geomap /////////////////
                [r setGeomapString:[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 11))]];
                ////////////////////////////////////// 
                r.zoom=sqlite3_column_int(stmt, 12);
                r.x=[[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 13))] doubleValue];
                r.y=[[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 14))] doubleValue];
                
                r.internal_id=[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 15))];
                // todo debug
                r.reminder_author=[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 16))];
                r.radius=  sqlite3_column_int(stmt, 17);      
                r.waiting_time=  sqlite3_column_int(stmt, 18);  
                //
                r.last_fire_time=[dateFormatter dateFromString:[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 19))]];
                r.disabled=sqlite3_column_int(stmt, 20)==1;
                r.proximity_warning=sqlite3_column_int(stmt, 21)==1;
                r.warn_for=sqlite3_column_int(stmt, 22);
                //
                [ret addObject:r];
            }
            
        }
        else
        {
            NSLog(@"Error ""%s"".", sqlite3_errmsg(dataBase));
        }
        sqlite3_finalize(stmt);
        
    }    
    
    NSMutableArray *result = [[NSMutableArray alloc] initWithCapacity:ret.count];
    
    for(Reminder* tmp_re in ret)
    {
        if(tmp_re.id!=-1)
        {
            sqlite3_stmt *stmt_rec;

            const char *query_rec= [@"select fr.username, fr.firstname, fr.lastname, fr.ava_link from reminder_recipients rr inner join friends fr on (fr.username like rr.username) where rr.reminder_id=?" UTF8String];
            if(sqlite3_prepare_v2(dataBase, query_rec, -1, &stmt_rec, NULL) == SQLITE_OK)
            {
                sqlite3_bind_text(stmt_rec, 1, [tmp_re.internal_id UTF8String], -1, NULL); 
                Friend* fr;
                while(sqlite3_step(stmt_rec) == SQLITE_ROW)
                {
                    if(tmp_re.recipient==nil)
                        tmp_re.recipient=[[NSMutableArray alloc] init];
                    fr = [[Friend alloc] init];
                    fr.Username=[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt_rec, 0))];
                    fr.Firstname=[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt_rec, 1))];
                    fr.Lastname=[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt_rec, 2))];
                //fr.ID=
                    fr.AvaLink=[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt_rec, 3))];
                //fr.trusted=
                //fr.requested=
                    [tmp_re.recipient addObject:fr];
            }
            }
            else
            {
                NSLog(@"Error ""%s"".", sqlite3_errmsg(dataBase));
                ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
            }
            sqlite3_finalize(stmt_rec);
        }
        [result addObject:tmp_re]; 
    }
    [ret removeAllObjects];
    
    return result;
}

+ (NSString*) getReminderInternalIdByExtenal:(int) ext_id
{
    NSString* internal_id;
    if([DataBase openDataBase])
    {          
        // Выборка слов
        const char *query= [@"select id from reminder where reminder_id=?" UTF8String];
        sqlite3_stmt *stmt;
               
        if(sqlite3_prepare_v2(dataBase, query, -1, &stmt, NULL) == SQLITE_OK)
        {
            sqlite3_bind_int(stmt, 1, ext_id);
            while(sqlite3_step(stmt) == SQLITE_ROW)
            {
                internal_id = [DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 0))];
            }
            
        }
        else
        {
            NSLog(@"Error ""%s"".", sqlite3_errmsg(dataBase));
            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
        }
        sqlite3_finalize(stmt);
        
    }  
    
    return internal_id;
}


+ (Reminder*) getReminder:(NSString*) _internal_id
{   
    Reminder *r = [[Reminder alloc] init];
    
    
    if([DataBase openDataBase])
    {          
        // Выборка слов
        const char *query= [@"select is_timed, is_placed, reminder_text, reminder_id, hours, minutes, self_remind, single_date, schedule, days_of_week, end_date, geomap_char, map_zoom, map_center_x, map_center_y, id, owner, radius, waiting_time, last_fire_time, disabled, proximity_warning, warn_for from reminder where id=?" UTF8String];
        sqlite3_stmt *stmt;
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        
        
        if(sqlite3_prepare_v2(dataBase, query, -1, &stmt, NULL) == SQLITE_OK)
        {
            sqlite3_bind_text(stmt, 1, [_internal_id UTF8String], -1, NULL);
            while(sqlite3_step(stmt) == SQLITE_ROW)
            {
                r.scheduled=sqlite3_column_int(stmt, 0)==1;
                r.mapped=sqlite3_column_int(stmt, 1)==1;
                r.text = [DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 2))];
                r.id = sqlite3_column_int(stmt, 3);
                //////////////// time /////////////////
                [dateFormatter setDateFormat:@"HH:mm"];
                NSString* tmpTime=[[NSString alloc] initWithFormat:@"%d:%d",sqlite3_column_int(stmt, 4), sqlite3_column_int(stmt, 5)];
                r.time=[dateFormatter dateFromString:tmpTime];
                //////////////////////////////////////
                r.self_remind=sqlite3_column_int(stmt, 6)==1;
                /////////////// date from ////////////
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                r.date_start=[dateFormatter dateFromString:[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 7))]];
                //////////////////////////////////////
                r.schedule_type=sqlite3_column_int(stmt, 8);
                [r setDays:sqlite3_column_int(stmt, 9)];
                /////////////// date to ////////////
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                r.date_finish=[dateFormatter dateFromString:[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 10))]];
                //////////////////////////////////////
                ///////////// geomap /////////////////
                [r setGeomapString:[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 11))]];
                ////////////////////////////////////// 
                r.zoom=sqlite3_column_int(stmt, 12);
                r.x=[[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 13))] doubleValue];
                r.y=[[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 14))] doubleValue];
                
                r.internal_id=[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 15))];
                
                r.reminder_author=[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 16))];
                r.radius=  sqlite3_column_int(stmt, 17); 
                r.waiting_time=  sqlite3_column_int(stmt, 18); 
                r.last_fire_time=[dateFormatter dateFromString:[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 19))]];
                r.disabled=sqlite3_column_int(stmt, 20)==1;
                r.proximity_warning=sqlite3_column_int(stmt, 21)==1;
                r.warn_for=sqlite3_column_int(stmt, 22);
                // todo debug
                //NSLog(@"%ld, %ld, %ld", currentWord.rndValue, w.rndValue, w.ID);
            }
            
        }
        else
        {
            NSLog(@"Error ""%s"".", sqlite3_errmsg(dataBase));
            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
        }
        sqlite3_finalize(stmt);
        
    }    
    
    if(r.id!=-1)
    {
        sqlite3_stmt *stmt_rec;

        const char *query_rec= [@"select fr.username, fr.firstname, fr.lastname, fr.ava_link from reminder_recipients rr inner join friends fr on (fr.username like rr.username) where rr.reminder_id=?" UTF8String];
        if(sqlite3_prepare_v2(dataBase, query_rec, -1, &stmt_rec, NULL) == SQLITE_OK)
        {
           sqlite3_bind_text(stmt_rec, 1, [r.internal_id UTF8String], -1, NULL);
           Friend* fr;
           while(sqlite3_step(stmt_rec) == SQLITE_ROW)
           {
              if(r.recipient==nil)
                 r.recipient=[[NSMutableArray alloc] init];
              fr = [[Friend alloc] init];
              fr.Username=[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt_rec, 0))];
              fr.Firstname=[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt_rec, 1))];
              fr.Lastname=[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt_rec, 2))];
                    //fr.ID=
              fr.AvaLink=[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt_rec, 3))];
                    //fr.trusted=
                    //fr.requested=
              [r.recipient addObject:fr];
            }
        }
        else
        {
            NSLog(@"Error ""%s"".", sqlite3_errmsg(dataBase));
            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
        }
        
        sqlite3_finalize(stmt_rec);
    }

    return r;
}

+(BOOL) rejectReminder:(NSString*) reminderId
{
    BOOL result=true;
    [DataBase openDataBase];
    
    const char *query = [@"delete from reminder_recipients where reminder_id in (select id from reminder where id=?);" UTF8String];
    sqlite3_stmt *stmt;
    if(sqlite3_prepare_v2(dataBase, query, -1, &stmt, NULL) == SQLITE_OK) 
    {
        sqlite3_bind_text(stmt, 1, [reminderId UTF8String], -1, NULL);
        
        if(sqlite3_step(stmt) != SQLITE_DONE)
        {
            NSLog(@"Error delete reminder ""%s"".", sqlite3_errmsg(dataBase));
            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
            result=false;
        }
    }
    else 
    {
        NSLog(@"Error delete reminder ""%s"".", sqlite3_errmsg(dataBase));
        ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
        result=false;
    }
    
    sqlite3_finalize(stmt);
    
    if(result)
    {
        const char *query1 = [@"delete from reminder where id=?;" UTF8String];
        sqlite3_stmt *stmt1;
        if(sqlite3_prepare_v2(dataBase, query1, -1, &stmt1, NULL) == SQLITE_OK) 
        {
            sqlite3_bind_text(stmt1, 1, [reminderId UTF8String], -1, NULL);
            
            if(sqlite3_step(stmt1) != SQLITE_DONE)
            {
                NSLog(@"Error delete reminder ""%s"".", sqlite3_errmsg(dataBase));
                ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
                result=false;
            }
        }
        else 
        {
            NSLog(@"Error delete reminder ""%s"".", sqlite3_errmsg(dataBase));
            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
            result=false;
        }
        
        sqlite3_finalize(stmt1);
    }
    
    return result;
}

// re-load changes from tasks
+(NSDictionary*) get_sync
{
    
    [DataBase openDataBase];
    
    [NSDateFormatter setDefaultFormatterBehavior:NSDateFormatterBehavior10_4];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    NSMutableArray* friends_added=[[NSMutableArray alloc] init];
    NSMutableArray* friends_approved=[[NSMutableArray alloc] init];
    NSMutableArray* friends_rejected=[[NSMutableArray alloc] init];
    //
    NSMutableArray* reminders_added_tmp=[[NSMutableArray alloc] init];
    NSMutableArray* reminders_edited_tmp=[[NSMutableArray alloc] init];
    NSMutableArray* reminders_deleted_tmp=[[NSMutableArray alloc] init];    
    //
    NSMutableArray* reminders_added=[[NSMutableArray alloc] init];
    NSMutableArray* reminders_edited=[[NSMutableArray alloc] init];
    NSMutableArray* reminders_deleted=[[NSMutableArray alloc] init]; 
    //
    sqlite3_stmt *stmt_fr;

    const char *query_fr= [@"select ts.object_id, fr.firstname, fr.lastname, fr.ava_link, ts.action from tasks ts left outer join friends fr on (ts.object_id=fr.username) where ts.object_type=1;" UTF8String];
    if(sqlite3_prepare_v2(dataBase, query_fr, -1, &stmt_fr, NULL) == SQLITE_OK)
    {
        Friend* fr;
        while(sqlite3_step(stmt_fr) == SQLITE_ROW)
        {
            fr = [[Friend alloc] init];
            fr.Username=[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt_fr, 0))];
            fr.Firstname=[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt_fr, 1))];
            fr.Lastname=[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt_fr, 2))];
            //fr.ID=
            fr.AvaLink=[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt_fr, 3))];
            //fr.trusted=
            //fr.requested=
            switch(sqlite3_column_int(stmt_fr, 4))
            {
                case 0: {[friends_added addObject:fr]; } break;
                case 1: {[friends_approved addObject:fr]; } break;
                case 2: {[friends_rejected addObject:fr]; } break;
            }
            //[r.recipient addObject:fr];
        }
    }
    else
    {
        NSLog(@"Error ""%s"".", sqlite3_errmsg(dataBase));
        ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
    }
    
    sqlite3_finalize(stmt_fr);

    //
    
    sqlite3_stmt *stmt_rem;

    const char *query_rem= [@"select rm.is_timed, rm.is_placed, rm.reminder_text, ts.object_num_id, rm.hours, rm.minutes, rm.self_remind, rm.single_date, rm.schedule, rm.days_of_week, rm.end_date, rm.geomap_char, rm.map_zoom, rm.map_center_x, rm.map_center_y, rm.id, rm.owner, rm.radius, rm.waiting_time, ts.action from tasks ts left outer join reminder rm on ((ts.object_num_id=rm.reminder_id and ts.object_num_id is not null) or (ts.object_id=rm.id and ts.object_num_id is null)) where ts.object_type=0;" UTF8String];
    if(sqlite3_prepare_v2(dataBase, query_rem, -1, &stmt_rem, NULL) == SQLITE_OK)
    {
        Reminder *r;
        while(sqlite3_step(stmt_rem) == SQLITE_ROW)
        {
            r = [[Reminder alloc] init];
            r.scheduled=sqlite3_column_int(stmt_rem, 0)==1;
            r.mapped=sqlite3_column_int(stmt_rem, 1)==1;
            r.text = [DataBase convertCharJSON:((char *)sqlite3_column_text(stmt_rem, 2))];
            r.id = sqlite3_column_int(stmt_rem, 3);
            //////////////// time /////////////////
            [dateFormatter setDateFormat:@"HH:mm"];
            NSString* tmpTime=[[NSString alloc] initWithFormat:@"%d:%d",sqlite3_column_int(stmt_rem, 4), sqlite3_column_int(stmt_rem, 5)];
            r.time=[dateFormatter dateFromString:tmpTime];
            //////////////////////////////////////
            r.self_remind=sqlite3_column_int(stmt_rem, 6)==1;
            /////////////// date from ////////////
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            r.date_start=[dateFormatter dateFromString:[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt_rem, 7))]];
            //////////////////////////////////////
            r.schedule_type=sqlite3_column_int(stmt_rem, 8);
            [r setDays:sqlite3_column_int(stmt_rem, 9)];
            /////////////// date to ////////////
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            r.date_finish=[dateFormatter dateFromString:[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt_rem, 10))]];
            //////////////////////////////////////
            ///////////// geomap /////////////////
            [r setGeomapString:[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt_rem, 11))]];
            ////////////////////////////////////// 
            r.zoom=sqlite3_column_int(stmt_rem, 12);
            r.x=[[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt_rem, 13))] doubleValue];
            r.y=[[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt_rem, 14))] doubleValue];
            
            r.internal_id=[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt_rem, 15))];
            // todo debug
            r.reminder_author=[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt_rem, 16))];
            r.radius=  sqlite3_column_int(stmt_rem, 17);  
            r.waiting_time=  sqlite3_column_int(stmt_rem, 18); 
            
            switch(sqlite3_column_int(stmt_rem, 19))
            {
                case 0: {[reminders_added_tmp addObject:r]; } break;
                case 1: {[reminders_edited_tmp addObject:r]; } break;
                case 2: {[reminders_deleted_tmp addObject:r]; } break;
            }
        }
    }
    else
    {
        NSLog(@"Error ""%s"".", sqlite3_errmsg(dataBase));
        ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
    }
    
    sqlite3_finalize(stmt_rem);
    
    NSArray* arrayz=[[NSArray alloc] initWithObjects:reminders_added_tmp, reminders_edited_tmp, reminders_deleted_tmp, nil];
    
    for(NSMutableArray* arr in arrayz)
    {
        for(Reminder* r in arr)
        {
            sqlite3_stmt *stmt_rec;

            const char *query_rec= [@"select fr.username, fr.firstname, fr.lastname, fr.ava_link from reminder_recipients rr inner join friends fr on (fr.username like rr.username) where rr.reminder_id=?" UTF8String];
            if(sqlite3_prepare_v2(dataBase, query_rec, -1, &stmt_rec, NULL) == SQLITE_OK)
            {
                sqlite3_bind_text(stmt_rec, 1, [r.internal_id UTF8String], -1, NULL);
                Friend* fr;
                while(sqlite3_step(stmt_rec) == SQLITE_ROW)
                {
                    if(r.recipient==nil)
                        r.recipient=[[NSMutableArray alloc] init];
                    fr = [[Friend alloc] init];
                    fr.Username=[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt_rec, 0))];
                    fr.Firstname=[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt_rec, 1))];
                    fr.Lastname=[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt_rec, 2))];
                    //fr.ID=
                    fr.AvaLink=[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt_rec, 3))];
                    //fr.trusted=
                    //fr.requested=
                    [r.recipient addObject:fr];
                }
            }
            else
            {
                NSLog(@"Error ""%s"".", sqlite3_errmsg(dataBase));
                ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
            }
            
            if([arr isEqual:reminders_added_tmp])
                [reminders_added addObject:r];
            if([arr isEqual:reminders_edited_tmp])
                [reminders_edited addObject:r];
            if([arr isEqual:reminders_deleted_tmp])
                [reminders_deleted addObject:r];
            
            sqlite3_finalize(stmt_rec);
        }
    }
    
    [reminders_added_tmp removeAllObjects];
    [reminders_edited_tmp removeAllObjects];
    [reminders_deleted_tmp removeAllObjects]; 
    
    NSDictionary* result=[NSDictionary dictionaryWithObjectsAndKeys:friends_added, @"friends_added",
                          friends_approved,@"friends_approved",friends_rejected,@"friends_rejected", reminders_added,@"reminders_added",reminders_edited,@"reminders_edited",reminders_deleted,@"reminders_deleted", nil];
    
    [DataBase confirmGetTasks];
    
    return result;
}

///////////////////////////////////////////////////////////////////////
/////////////////////// MONITORED REMINDERS ///////////////////////////
///////////////////////////////////////////////////////////////////////
+(BOOL) unregisterMonitoredReminder:(NSString *) reminder_id
{
    BOOL result=true;
    if([DataBase openDataBase])
    {   
        const char *delete_query = [@"delete from monitored_reminders where reminder_id=?;" UTF8String];
        sqlite3_stmt *stmt_del;
        if(sqlite3_prepare_v2(dataBase, delete_query, -1, &stmt_del, NULL) == SQLITE_OK) 
        {
            sqlite3_bind_text(stmt_del, 1, [reminder_id UTF8String], -1, NULL);
            
            if(sqlite3_step(stmt_del) != SQLITE_DONE)
            {
                NSLog(@"Error unregister monitored reminder ""%s"".", sqlite3_errmsg(dataBase));
                ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
                result=false;
            }
        }
        else 
        {
            NSLog(@"Error unregister monitored reminder ""%s"".", sqlite3_errmsg(dataBase));
            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
            result=false;
        }
        
        sqlite3_finalize(stmt_del);
    }
    return result;
}

+(BOOL) clearMonitoredReminders
{
    BOOL result=true;
    if([DataBase openDataBase])
    {   
        const char *delete_query = [@"delete from monitored_reminders;" UTF8String];
        sqlite3_stmt *stmt_del;
        if(sqlite3_prepare_v2(dataBase, delete_query, -1, &stmt_del, NULL) == SQLITE_OK) 
        {
            if(sqlite3_step(stmt_del) != SQLITE_DONE)
            {
                NSLog(@"Error delete monitored reminders ""%s"".", sqlite3_errmsg(dataBase));
                ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
                result=false;
            }
        }
        else 
        {
            NSLog(@"Error delete monitored reminders ""%s"".", sqlite3_errmsg(dataBase));
            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
            result=false;
        }
        
        sqlite3_finalize(stmt_del);
    }
    return result;
}

+(BOOL) registerMonitoredReminder:(NSString*) reminder_id
{
    
    BOOL result=[DataBase unregisterMonitoredReminder:reminder_id];
    
    NSDate* current_date=[NSDate date];

    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    
    if([DataBase openDataBase])
    {
        const char *query = [@"insert into monitored_reminders(start_monitoring_date, reminder_id) values(?,?);" UTF8String];
        sqlite3_stmt *stmt;
        if(sqlite3_prepare_v2(dataBase, query, -1, &stmt, NULL) == SQLITE_OK) 
        {
            [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSString *dateString=[dateFormat stringFromDate:current_date];
            sqlite3_bind_text(stmt, 1, [dateString UTF8String], -1, NULL); //SQLITE_TRANSIENT
            sqlite3_bind_text(stmt, 2, [reminder_id UTF8String], -1, NULL);
            
            if(sqlite3_step(stmt) != SQLITE_DONE)
            {
                NSLog(@"Error register monitored reminder ""%s"".", sqlite3_errmsg(dataBase));
                ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
                result=false;
            }
        }
        else 
        {
            NSLog(@"Error register monitored reminder ""%s"".", sqlite3_errmsg(dataBase));
            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
            result=false;
        }
        
        sqlite3_finalize(stmt);
    }
    
    return result;
}

+ (NSArray*) getMonitoredReminders
{   
    NSMutableArray *ret = [[NSMutableArray alloc] init];
    
    
    if([DataBase openDataBase])
    {          
        // Выборка слов
        const char *query= [@"select mr.reminder_id from monitored_reminders mr;" UTF8String];
        sqlite3_stmt *stmt;
               
        if(sqlite3_prepare_v2(dataBase, query, -1, &stmt, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(stmt) == SQLITE_ROW)
            {
               [ret addObject:[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 0))]];
            }
            
        }
        else
        {
            NSLog(@"Error get monitored reminders ""%s"".", sqlite3_errmsg(dataBase));
            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
        }
        sqlite3_finalize(stmt);
        
    }    
    
    return ret;
}

///////////////////////////////////////////////////////////////////////
/////////////////////// ENTERED LOCATIONS /////////////////////////////
///////////////////////////////////////////////////////////////////////
+(BOOL) unregisterEnteredLocation:(NSString *) reminder_id
{
    BOOL result=true;
    if([DataBase openDataBase])
    {   
        const char *delete_query = [@"delete from entered_locations where reminder_id=?;" UTF8String];
        sqlite3_stmt *stmt_del;
        if(sqlite3_prepare_v2(dataBase, delete_query, -1, &stmt_del, NULL) == SQLITE_OK) 
        {
            sqlite3_bind_text(stmt_del, 1, [reminder_id UTF8String], -1, NULL);
            
            if(sqlite3_step(stmt_del) != SQLITE_DONE)
            {
                NSLog(@"Error unregister entered_location ""%s"".", sqlite3_errmsg(dataBase));
                ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
                result=false;
            }
        }
        else 
        {
            NSLog(@"Error unregister entered_location ""%s"".", sqlite3_errmsg(dataBase));
            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
            result=false;
        }
        
        sqlite3_finalize(stmt_del);
    }
    return result;
}

+(BOOL) clearEnteredLocations
{
    BOOL result=true;
    if([DataBase openDataBase])
    {   
        const char *delete_query = [@"delete from entered_locations;" UTF8String];
        sqlite3_stmt *stmt_del;
        if(sqlite3_prepare_v2(dataBase, delete_query, -1, &stmt_del, NULL) == SQLITE_OK) 
        {
            if(sqlite3_step(stmt_del) != SQLITE_DONE)
            {
                NSLog(@"Error delete entered location ""%s"".", sqlite3_errmsg(dataBase));
                ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
                result=false;
            }
        }
        else 
        {
            NSLog(@"Error delete entered location ""%s"".", sqlite3_errmsg(dataBase));
            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
            result=false;
        }
        
        sqlite3_finalize(stmt_del);
    }
    return result;
}

+(BOOL) registerEnteredLocation:(NSString*) reminder_id
{
    
    BOOL result=[DataBase unregisterMonitoredReminder:reminder_id];
    
    NSDate* current_date=[NSDate date];
    [DataBase openDataBase];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    
    if([DataBase openDataBase])
    {
        const char *query = [@"insert into entered_locations(date_enter, reminder_id) values(?,?);" UTF8String];
        sqlite3_stmt *stmt;
        if(sqlite3_prepare_v2(dataBase, query, -1, &stmt, NULL) == SQLITE_OK) 
        {
            [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSString *dateString=[dateFormat stringFromDate:current_date];
            sqlite3_bind_text(stmt, 1, [dateString UTF8String], -1, NULL); //SQLITE_TRANSIENT
            sqlite3_bind_text(stmt, 2, [reminder_id UTF8String], -1, NULL);
            
            if(sqlite3_step(stmt) != SQLITE_DONE)
            {
                NSLog(@"Error register entered location ""%s"".", sqlite3_errmsg(dataBase));
                ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
                result=false;
            }
        }
        else 
        {
            NSLog(@"Error register entered location ""%s"".", sqlite3_errmsg(dataBase));
            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
            result=false;
        }
        
        sqlite3_finalize(stmt);
    }
    
    return result;
}

+ (NSArray*) getEnteredLocations
{   
    NSMutableArray *ret = [[NSMutableArray alloc] init];
    
    
    if([DataBase openDataBase])
    {          
        // Выборка слов
        const char *query= [@"select reminder_id from entered_locations;" UTF8String];
        sqlite3_stmt *stmt;
        
        if(sqlite3_prepare_v2(dataBase, query, -1, &stmt, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(stmt) == SQLITE_ROW)
            {
                [ret addObject:[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 0))]];
            }
            
        }
        else
        {
            NSLog(@"Error get entered locations ""%s"".", sqlite3_errmsg(dataBase));
            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
        }
        sqlite3_finalize(stmt);
        
    }    
    
    return ret;
}

///////////////////////////////////////////////////////////////////////
/////////////////////////// DATABASE LOGS /////////////////////////////
///////////////////////////////////////////////////////////////////////
+(void)DBLog:(NSString*) log_text
{
    NSDate* current_date=[NSDate date];
    [DataBase openDataBase];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    
    const char *query = [@"insert into application_log(log_when, log_text) values(?,?);" UTF8String];
    sqlite3_stmt *stmt;
    if(sqlite3_prepare_v2(dataBase, query, -1, &stmt, NULL) == SQLITE_OK) 
    {
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *dateString=[dateFormat stringFromDate:current_date];
        sqlite3_bind_text(stmt, 1, [dateString UTF8String], -1, NULL);
        sqlite3_bind_text(stmt, 2, [log_text UTF8String], -1, NULL);
        
        if(sqlite3_step(stmt) != SQLITE_DONE)
        {
            NSLog(@"Error register log ""%s"".", sqlite3_errmsg(dataBase));
            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
        }
    }
    else 
    {
        NSLog(@"Error delete reminder ""%s"".", sqlite3_errmsg(dataBase));
        ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
    }
    
    sqlite3_finalize(stmt);
    
}

+(void)DBLog:(NSString*) log_text withData:(NSObject*) with_data
{
    NSDate* current_date=[NSDate date];
    [DataBase openDataBase];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    
    NSString* tmp=[[NSString alloc] initWithFormat:@"%@ %@",log_text, with_data];
    
    const char *query = [@"insert into application_log(log_when, log_text) values(?,?);" UTF8String];
    sqlite3_stmt *stmt;
    if(sqlite3_prepare_v2(dataBase, query, -1, &stmt, NULL) == SQLITE_OK) 
    {
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *dateString=[dateFormat stringFromDate:current_date];
        sqlite3_bind_text(stmt, 1, [dateString UTF8String], -1, NULL);
        sqlite3_bind_text(stmt, 2, [tmp UTF8String], -1, NULL);
        
        if(sqlite3_step(stmt) != SQLITE_DONE)
        {
            NSLog(@"Error register log ""%s"".", sqlite3_errmsg(dataBase));
            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
        }
    }
    else 
    {
        NSLog(@"Error delete reminder ""%s"".", sqlite3_errmsg(dataBase));
        ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
    }
    
    sqlite3_finalize(stmt);
    
}

+ (NSArray*) getDBLogs
{   
    NSMutableArray *ret = [[NSMutableArray alloc] init];
    
    
    if([DataBase openDataBase])
    {          
        // Выборка слов
        const char *query= [@"select log_when, log_text from application_log;" UTF8String];
        sqlite3_stmt *stmt;
        
        if(sqlite3_prepare_v2(dataBase, query, -1, &stmt, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(stmt) == SQLITE_ROW)
            {
                NSString* result=[[NSString alloc] initWithFormat:@"%@  %@",[DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 0))], [DataBase convertCharJSON:((char *)sqlite3_column_text(stmt, 1))]]; 
                [ret addObject:result];
            }
        }
        else
        {
            NSLog(@"Error ""%s"".", sqlite3_errmsg(dataBase));
            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
        }
        sqlite3_finalize(stmt);
        
    }    
    
    return ret;
}

+(BOOL) clearDBLogs
{
    BOOL result=true;
    if([DataBase openDataBase])
    {   
        const char *delete_query = [@"delete from application_log;" UTF8String];
        sqlite3_stmt *stmt_del;
        if(sqlite3_prepare_v2(dataBase, delete_query, -1, &stmt_del, NULL) == SQLITE_OK) 
        {
            if(sqlite3_step(stmt_del) != SQLITE_DONE)
            {
                NSLog(@"Error delete application log ""%s"".", sqlite3_errmsg(dataBase));
                ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
                result=false;
            }
        }
        else 
        {
            NSLog(@"Error delete application log ""%s"".", sqlite3_errmsg(dataBase));
            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
            result=false;
        }
        
        sqlite3_finalize(stmt_del);
    }
    return result;
}

+(BOOL) registerLastFireTime:(Reminder*) rem
{
    BOOL result=true;

    if([DataBase openDataBase])
    {   
        sqlite3_stmt *stmt;
        const char *query=[@"update reminder set last_fire_time=? where id=?;" UTF8String];   
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *lastTimeString=[dateFormat stringFromDate:[NSDate date]];
        
        if(sqlite3_prepare_v2(dataBase, query, -1, &stmt, NULL) == SQLITE_OK) 
        {
            sqlite3_bind_text(stmt, 1, [lastTimeString UTF8String], -1, NULL);
            sqlite3_bind_text(stmt, 2, [rem.internal_id UTF8String], -1, NULL);
            
            if(sqlite3_step(stmt) != SQLITE_DONE)
            {
                NSLog(@"Error save reminder ""%s"".", sqlite3_errmsg(dataBase));
                ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
                result=false;
            }
        }
        else 
        {
            NSLog(@"Error save reminder ""%s"".", sqlite3_errmsg(dataBase));
            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
            result=false;
        }
        
        sqlite3_finalize(stmt);
    }
    else 
    {
        NSLog(@"Error save reminder ""%s"".", sqlite3_errmsg(dataBase));
        ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
        result=false;
    }
    
    return result;
}

+(BOOL) enableReminder:(Reminder*) rem
{
    return [DataBase setReminderDisabled:rem disabled:NO];
}

+(BOOL) disableReminder:(Reminder*) rem
{
    return [DataBase setReminderDisabled:rem disabled:YES];
    [DataBase unregisterMonitoredReminder:rem.internal_id];// we must unregister such monitored reminder
}

+(BOOL) setReminderDisabled:(Reminder*) rem disabled:(BOOL) _disabled
{
    BOOL result=true;

    if([DataBase openDataBase])
    {
        sqlite3_stmt *stmt;
        const char *query=[@"update reminder set disabled=? where id=?;" UTF8String];
                
        if(sqlite3_prepare_v2(dataBase, query, -1, &stmt, NULL) == SQLITE_OK)
        {
            sqlite3_bind_int(stmt, 1, (_disabled==YES)?1:0);
            sqlite3_bind_text(stmt, 2, [rem.internal_id UTF8String], -1, NULL);
            
            if(sqlite3_step(stmt) != SQLITE_DONE)
            {
                NSLog(@"Error enable|disable reminder ""%s"".", sqlite3_errmsg(dataBase));
                ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
                result=false;
            }
        }
        else
        {
            NSLog(@"Error enable|disable reminder ""%s"".", sqlite3_errmsg(dataBase));
            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
            result=false;
        }
        
        sqlite3_finalize(stmt);
    }
    else
    {
        NSLog(@"Error enable|disable reminder ""%s"".", sqlite3_errmsg(dataBase));
        ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
        result=false;
    }
    
    return result;
}

+(BOOL) setReminderRadius:(Reminder*) rem
{
    BOOL result=true;

    if([DataBase openDataBase])
    {
        sqlite3_stmt *stmt;
        const char *query=[@"update reminder set radius=? where id=?;" UTF8String];
        
        if(sqlite3_prepare_v2(dataBase, query, -1, &stmt, NULL) == SQLITE_OK)
        {
            sqlite3_bind_int(stmt, 1, rem.radius);
            sqlite3_bind_text(stmt, 2, [rem.internal_id UTF8String], -1, NULL);
            
            if(sqlite3_step(stmt) != SQLITE_DONE)
            {
                NSLog(@"Error set radius of reminder ""%s"".", sqlite3_errmsg(dataBase));
                ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
                result=false;
            }
        }
        else
        {
            NSLog(@"Error set radius of reminder ""%s"".", sqlite3_errmsg(dataBase));
            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
            result=false;
        }
        
        sqlite3_finalize(stmt);
    }
    else
    {
        NSLog(@"Error set radius of reminder ""%s"".", sqlite3_errmsg(dataBase));
        ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
        result=false;
    }
    
    return result;
}


+(BOOL) relinkLocalReminders:(NSString*) new_user
{
    BOOL result=true;
    [DataBase openDataBase];
    if([DataBase openDataBase])
    {   
        sqlite3_stmt *stmt;
        const char *query=[@"update reminder set owner=? where reminder_id<=0;" UTF8String];   
        
        if(sqlite3_prepare_v2(dataBase, query, -1, &stmt, NULL) == SQLITE_OK) 
        {
            sqlite3_bind_text(stmt, 1, [new_user UTF8String], -1, NULL);
            
            if(sqlite3_step(stmt) != SQLITE_DONE)
            {
                NSLog(@"Error update reminders ""%s"".", sqlite3_errmsg(dataBase));
                ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
                result=false;
            }
        }
        else 
        {
            NSLog(@"Error update reminders ""%s"".", sqlite3_errmsg(dataBase));
            ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
            result=false;
        }
        
        sqlite3_finalize(stmt);
        
    }
    else 
    {
        NSLog(@"Error update reminders ""%s"".", sqlite3_errmsg(dataBase));
        ErrC(NSLocalizedString(@"Error", @"Error"),sqlite3_errmsg(dataBase));
        result=false;
    }
    
    return result;
}*/



- (void)dealloc
{
    [DataBase closeDataBase];
}

@end
