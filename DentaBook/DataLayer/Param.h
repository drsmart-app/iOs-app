//
//  Param.h
//  DentaBook
//
//  Created by Mac Mini on 20.10.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Param : NSObject

@property(nonatomic, retain) NSString* name;
@property(nonatomic, retain) NSString* val;
@property(nonatomic, retain) NSString* type;
@property(nonatomic, retain) NSString* relation;

+ (Param*) paramWithName:(NSString*) name stringValue:(NSString*) val andType:(NSString*) type;
+ (Param*) paramWithName:(NSString*) name intValue:(int) val andType:(NSString*) type;
+ (Param*) paramWithName:(NSString*) name floatValue:(float) val andType:(NSString*) type;
+ (Param*) paramWithName:(NSString*) name doubleValue:(double) val andType:(NSString*) type;
- (NSString*) castType;

@end
