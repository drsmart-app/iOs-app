//
//  FileUtils.h
//  DFM
//
//  Created by Evgeniy Krivoshein on 01.02.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileUtils : NSObject

+ (BOOL) existsFile:(NSString *) fileName;
+ (void) copyFile:(NSString *) orgName withNewName: (NSString *)newName;
+ (NSString *) findFile:(NSString *)fileName withExtension:(NSString *)fileExt;

@end
