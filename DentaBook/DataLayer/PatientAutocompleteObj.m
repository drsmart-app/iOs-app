//
//  PatientAutocompleteObj.m
//  DentaBook
//
//  Created by Mac Mini on 14.11.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "PatientAutocompleteObj.h"

@implementation PatientAutocompleteObj
@synthesize pat;
-(id) initWithPatient:(NSDictionary*)patient  {
    self = [super init];
    if(self)
    {
        self.pat = patient;
    }
    return self;
}

- (NSString *)autocompleteString
{
    return [NSString stringWithFormat:@"%@ %@ %@", pat[@"firstname"], pat[@"middlename"], pat[@"lastname"]];
}
@end
