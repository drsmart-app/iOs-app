//
//  ErrorMessage.h
//  FuckinRotation
//
//  Created by Instream on 17.07.13.
//  Copyright (c) 2013 Instream. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SharedErrorMessage : NSObject
{
    NSString* title;
    NSString* message;
    
}
+(SharedErrorMessage*) sharedInstance;

// clue for improper use (produces compile time error)
+(SharedErrorMessage*) alloc __attribute__((unavailable("alloc not available, call sharedInstance instead")));
-(SharedErrorMessage*) init __attribute__((unavailable("init not available, call sharedInstance instead")));
+(SharedErrorMessage*) new __attribute__((unavailable("new not available, call sharedInstance instead")));
- (NSString*) description;
@property (nonatomic, retain) NSString* title;
@property (nonatomic, retain) NSString* message;

void Err(NSString* title, NSString* message);
void ErrC(NSString* title, const char* message);

@end
