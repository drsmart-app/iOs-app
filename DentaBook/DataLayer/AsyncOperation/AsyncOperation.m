//
//  AsyncOperation.m
//  FuckinRotation
//
//  Created by Instream on 17.07.13.
//  Copyright (c) 2013 Instream. All rights reserved.
//

#import "AsyncOperation.h"
#import "SharedErrorMessage.h"


@implementation AsyncOperation

@synthesize delegate;

//////////////////////////////////////////////////////
//background processes///////////////////////////
//////////////////////////////////////////////////////
- (void) doOperation
{
    SharedErrorMessage* mess=[SharedErrorMessage sharedInstance];
    @autoreleasepool
    {
        [self performSelectorOnMainThread:@selector(setStartIndicator) withObject:nil waitUntilDone:YES];
        
        @synchronized(mess)
        {
            if([delegate operationChain1])
            {
                if([delegate respondsToSelector:@selector(operationChain2)])
                {
                    if([delegate operationChain2])
                    {
                        if([delegate respondsToSelector:@selector(operationChain3)])
                        {
                            if([delegate operationChain3])
                            {
                                if([delegate respondsToSelector:@selector(postActions)])
                                {
                                    [(NSObject*)delegate performSelectorOnMainThread:@selector(postActions) withObject:nil waitUntilDone:YES];
                                }
                            }
                            else
                            {
                                if([delegate respondsToSelector:@selector(operationFail3)])
                                {
                                    [delegate operationFail3];
                                }
                                [self performSelectorOnMainThread:@selector(showError) withObject:nil waitUntilDone:YES];
                            }
                        }
                        else
                        {
                            if([delegate respondsToSelector:@selector(postActions)])
                            {
                                [(NSObject*)delegate performSelectorOnMainThread:@selector(postActions) withObject:nil waitUntilDone:YES];
                            }
                        }
                    }
                    else
                    {
                        if([delegate respondsToSelector:@selector(operationFail2)])
                        {
                            [delegate operationFail2];
                        }
                        [self performSelectorOnMainThread:@selector(showError) withObject:nil waitUntilDone:YES];
                    }
                }
                else
                {
                    if([delegate respondsToSelector:@selector(postActions)])
                    {
                        [(NSObject*)delegate performSelectorOnMainThread:@selector(postActions) withObject:nil waitUntilDone:YES];
                    }
                }
            }
            else
            {
                if([delegate respondsToSelector:@selector(operationFail1)])
                {
                    [delegate operationFail1];
                }
                [self performSelectorOnMainThread:@selector(showError) withObject:nil waitUntilDone:YES];
            }
        }

        [self performSelectorOnMainThread:@selector(setStopIndicator) withObject:nil waitUntilDone:YES];
    }
}

///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
-(void)setStartIndicator
{
    if([delegate isKindOfClass:[UIViewController class]])
    {
            coverView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, ((UIViewController*)delegate).view.bounds.size.width, ((UIViewController*)delegate).view.bounds.size.height)];
            coverView.backgroundColor = [UIColor blackColor];
            coverView.alpha = 0.5;
            
            UIActivityIndicatorView *activityWheel = [[UIActivityIndicatorView alloc] initWithFrame: CGRectMake(((UIViewController*)delegate).view.bounds.size.width / 2 - 12, ((UIViewController*)delegate).view.bounds.size.height / 2 - 12, 24, 24)];
            activityWheel.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
            
            [coverView addSubview:activityWheel];
    
            [((UIViewController*)delegate).view addSubview: coverView];
    
            [[[coverView subviews] objectAtIndex:0] startAnimating];
    }
    else
    {
        /*coverView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, (delegate. .bounds.size.width, ((UIViewController*)delegate).view.bounds.size.height)];
        coverView.backgroundColor = [UIColor blackColor];
        coverView.alpha = 0.5;
        
        UIActivityIndicatorView *activityWheel = [[UIActivityIndicatorView alloc] initWithFrame: CGRectMake(((UIViewController*)delegate).view.bounds.size.width / 2 - 12, ((UIViewController*)delegate).view.bounds.size.height / 2 - 12, 24, 24)];
        activityWheel.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        
        [coverView addSubview:activityWheel];
        
        [((UIViewController*)delegate).view addSubview: coverView];
        
        [[[coverView subviews] objectAtIndex:0] startAnimating];*/
    }
    
}

-(void)setStopIndicator
{
    if([delegate isKindOfClass:[UIViewController class]])
    {
            [[[coverView subviews] objectAtIndex:0] stopAnimating];
            [coverView removeFromSuperview];
            coverView = nil;
    }
}
///////////////////////////////////////////////////////////

- (void) showError
{
    if([SharedErrorMessage sharedInstance].title.length>0 && [SharedErrorMessage sharedInstance].message.length>0)
    {
    
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[SharedErrorMessage sharedInstance].title message:[SharedErrorMessage sharedInstance].message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
    }
}

- (void) Do
{
    [self performSelectorInBackground:@selector(doOperation) withObject:nil];
}

- (void) DoByWay:(int) _way
{
    way=_way;
    [self performSelectorInBackground:@selector(doOperation) withObject:nil];
}

- (int) getWay
{
    return way;
}

@end
