//
//  AsyncOperation.h
//  FuckinRotation
//
//  Created by Instream on 17.07.13.
//  Copyright (c) 2013 Instream. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol AsyncOperationDelegate <NSObject>
@required
- (BOOL) operationChain1;
@optional
- (BOOL) operationChain2;
- (BOOL) operationChain3;
- (void) operationFail1;
- (void) operationFail2;
- (void) operationFail3;
- (void) postActions;
@end

@interface AsyncOperation : NSObject
{
    id<AsyncOperationDelegate> delegate;
    UIView* coverView;
    UIActivityIndicatorView* bigIndy;
    int way;
}


@property(nonatomic, retain) id<AsyncOperationDelegate> delegate;
- (void) Do;
- (void) DoByWay:(int) _way;
- (int) getWay;

@end
