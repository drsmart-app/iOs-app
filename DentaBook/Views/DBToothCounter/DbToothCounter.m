//
//  DbToothCounter.m
//  DentaBook
//
//  Created by Denis Dubov on 26.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "DbToothCounter.h"
#import "UIView+Helpers.h"

@interface DbToothCounter ()

@end

@implementation DbToothCounter
@synthesize need0;
- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return 100;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 22;
}

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    if (view != nil) {
        ((UILabel*)view).text = [NSString stringWithFormat:@"%ld", (long)row];
        return view;
    }
    UILabel *columnView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 20)];
    columnView.text = [NSString stringWithFormat:@"%ld", (long)(need0?row:row+1)];
    columnView.textAlignment = NSTextAlignmentCenter;
    columnView.font = [UIFont systemFontOfSize:14];
    
    return columnView;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    self.toothCount = (need0?row:row+1);
}

@end
