//
//  DBMonthYearController.h
//  DentaBook
//
//  Created by Denis Dubov on 19.12.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NTMonthYearPicker;

@interface DBMonthYearController : UIViewController
@property (nonatomic, strong) NTMonthYearPicker *datePicker;
@end
