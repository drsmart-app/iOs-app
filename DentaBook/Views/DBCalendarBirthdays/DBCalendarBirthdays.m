//
//  DBCalendarBirthdays.m
//  DentaBook
//
//  Created by Denis Dubov on 10.03.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "DBCalendarBirthdays.h"
#import "Patient+Custom.h"
#import "DataBase.h"

@interface DBCalendarBirthdays ()

@property (nonatomic, strong) UIButton *button;
@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) NSMutableArray *labelArray;

@end

@implementation DBCalendarBirthdays

-(id) init  {
    self = [super initWithFrame:CGRectZero];
    if(self)    {
        [self setup];
    }
    return self;
}

-(void) setup   {
    self.backgroundColor = [UIColor colorWithRed:236.0/255.0 green:240.0/255.0 blue:241.0/255.0 alpha:1.0];
    
    self.iconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"calendar-birthday-icon"]];
    [self addSubview:self.iconImageView];
    
    [self.iconImageView makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.left).with.offset(20.0);
        make.top.equalTo(self.top).with.offset(10.0);
        make.width.equalTo(18.0);
        make.height.equalTo(18.0);
    }];
    
    self.titleLabel = UILabel.new;
    self.titleLabel.numberOfLines = 1;
    self.titleLabel.textColor = [UIColor colorWithRed:41.0/255.0 green:127/255.0 blue:184.0/255.0 alpha:1.0];
    self.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:10.0];
    self.titleLabel.text = @"Дни рождения:";
    [self addSubview:self.titleLabel];
    
    [self.titleLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.iconImageView.right).with.offset(12.0);
        make.centerY.equalTo(self.iconImageView.centerY);
        make.width.equalTo(72);
        make.height.equalTo(12);
    }];
    
    self.button = UIButton.new;
    [self.button addTarget:self action:@selector(onClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.button];
    
    [self.button makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.left);
        make.top.equalTo(self.top);
        make.right.equalTo(self.right);
        make.bottom.equalTo(self.bottom);
    }];
}

-(void) onClick:(id)sender  {
    NSLog(@"Hello!");
}

-(void) updatePatients:(NSArray*)patients   {
    [self clearContent];
    
    self.labelArray = [NSMutableArray new];
    int loop = 0;
    for(NSDictionary *patient in patients)
    {
        NSArray* costs = [DataBase getDBArray:@"visits" withKey:@"patientid" andValue:patient[@"patientid"]];
        float allCredit=0;
        for(NSDictionary* dc in costs)
        {
            allCredit+= [dc[@"total_cost_with_discount"] floatValue];
        }
        
        float allDebit=0;
        NSArray* pments = [DataBase getDBArray:@"payment" withKey:@"patientid" andValue:patient[@"patientid"]];
        for(NSDictionary* dc in pments)
        {
            allDebit += [dc[@"amount"] floatValue];
        }
        
        [self.labelArray addObject:[self labelViewWithTitle:[NSString stringWithFormat:@"%@ %@ %@", patient[@"firstname"], patient[@"middlename"], patient[@"lastname"]] isIllness:(((NSArray*)patient[@"illness"]).count != 0) isDebt:(allDebit - allCredit != 0) tag:loop++]];
    }
    
    // tell constraints they need updating
    [self setNeedsUpdateConstraints];
    
    // update constraints now so we can animate the change
    [self updateConstraintsIfNeeded];
    
    [self layoutIfNeeded];
}

-(UIView*) labelViewWithTitle:(NSString*)title
                    isIllness:(BOOL)isIllness
                       isDebt:(BOOL)isDebt
                          tag:(NSInteger)tagIndex   {
    UIView *subView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    subView.tag = tagIndex;
    subView.backgroundColor = [UIColor clearColor];
    
    __block UILabel *titleLabel = UILabel.new;
    titleLabel.numberOfLines = 1;
    titleLabel.textColor = [UIColor colorWithRed:74.0/255.0 green:74/255.0 blue:74.0/255.0 alpha:1.0];
    titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:10.0];
    titleLabel.text = title;
    titleLabel.backgroundColor = [UIColor clearColor];
    [subView addSubview:titleLabel];
    
    [titleLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(subView.left);
        make.top.equalTo(subView.top);
        make.width.equalTo(subView.width).with.offset(-54.0);
        make.height.equalTo(subView.height);
    }];
    
    __block UIImageView *iconIllnessImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"patient-illness-cell-icon"]];
    iconIllnessImageView.alpha = isIllness;
    [subView addSubview:iconIllnessImageView];
    
    [iconIllnessImageView makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(titleLabel.right).with.offset(5.0);
        make.centerY.equalTo(titleLabel.centerY);
        make.width.equalTo(12.0);
        make.height.equalTo(12.0);
    }];

    __block UIImageView *iconDebtImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"patient-debtor-cell-icon"]];
    iconDebtImageView.alpha = isDebt;
    [subView addSubview:iconDebtImageView];
    
    [iconDebtImageView makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(iconIllnessImageView.right).with.offset(5.0);
        make.centerY.equalTo(titleLabel.centerY);
        make.width.equalTo(12.0);
        make.height.equalTo(12.0);
    }];
    
    [self insertSubview:subView belowSubview:self.button];
    
    return subView;
}

-(void) clearContent    {
    [[self viewWithTag:1] removeFromSuperview];
    [[self viewWithTag:2] removeFromSuperview];
    [[self viewWithTag:3] removeFromSuperview];
}

- (void)updateConstraints {
    [self.iconImageView updateConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.left).with.offset(20.0);
        make.top.equalTo(self.top).with.offset(10.0);
        make.width.equalTo(18.0);
        make.height.equalTo(18.0);
    }];
    
    [self.titleLabel updateConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.iconImageView.right).with.offset(12.0);
        make.centerY.equalTo(self.iconImageView.centerY);
        make.width.equalTo(72);
        make.height.equalTo(12);
    }];
    
    int index = 0;
    for (UIView *subview in self.labelArray) {
        if(index == 0)  {
            [subview makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.titleLabel.right).with.offset(15.0);
                make.centerY.equalTo(self.iconImageView.centerY);
                make.right.equalTo(self.right);
                make.height.equalTo(12);
            }];
        } else  {
            [subview makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.titleLabel.right).with.offset(15.0);
                make.top.equalTo( ((UIView*)(self.labelArray[index-1])).bottom).with.offset(7.0);
                make.right.equalTo(self.right);
                make.height.equalTo(12);
            }];
        }
        index++;
    }
    
    [self.button updateConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.left);
        make.top.equalTo(self.top);
        make.right.equalTo(self.right);
        make.bottom.equalTo(self.bottom);
    }];
    
    [super updateConstraints];
}

@end
