//
//  DBCalendarBirthdays.h
//  DentaBook
//
//  Created by Denis Dubov on 10.03.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DBCalendarBirthdays : UIView

-(void) updatePatients:(NSArray*)patients;

@end
