//
//  UIFlatSearchBar.m
//  DentaBook
//
//  Created by Mac Mini on 02.12.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "UIFlatSearchBar.h"

@implementation UIFlatSearchBar

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void) layoutSubviews
{
    
    CGRect frame = self.frame;
    if (frame.size.height != 22) {
        frame.size.height = 22;
        self.frame = frame;
    }
    
    for (UIView *subView in self.subviews)
    {
        for (UIView *subView1 in subView.subviews)
        {
            if([NSStringFromClass([subView1 class]) isEqualToString:@"UISearchBarTextField"])
            {
                for (UIView *subView2 in subView1.subviews)
                {
                    if([NSStringFromClass([subView2 class]) isEqualToString:@"UISearchBarTextFieldLabel"])
                    {
                        subView2.frame = CGRectOffset(subView2.frame, 0, 8);
                    }
                }
            }
        }
    }
    
    [super layoutSubviews];
}

@end
