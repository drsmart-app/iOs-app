//
//  UITableView+Convertible.h
//  DentaBook
//
//  Created by Mac Mini on 17.12.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView (Convertible)
- (UIImage *) imageFromTableView;
@end
