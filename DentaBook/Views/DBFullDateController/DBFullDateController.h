//
//  DBFullDateController.h
//  DentaBook
//
//  Created by Denis Dubov on 19.12.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DBFullDateController : UIViewController
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@end
