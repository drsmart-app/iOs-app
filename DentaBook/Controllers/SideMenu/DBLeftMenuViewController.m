//
//  DBLeftMenuViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 04.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "DBLeftMenuViewController.h"
#import "AppDelegate.h"
#import "DBPatientsViewController.h"
#import "ExpensesViewController.h"
#import "FinancialReportViewController.h"
#import "AffairsListViewController.h"
#import "SettingsViewController.h"
#import "CalendarViewController.h"

@interface DBLeftMenuViewController ()

@end

@implementation DBLeftMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)onPacients:(id)sender {
    
    DBPatientsViewController *patientsViewController = [DBPatientsViewController new];
    [kNavigationController pushViewController:patientsViewController animated:NO];
    
    [kMainViewController hideLeftViewAnimated:YES completionHandler:nil];
}

- (IBAction)onCalendar:(id)sender {
    CalendarViewController *calendarViewController = [CalendarViewController new];
    [kNavigationController pushViewController:calendarViewController animated:NO];
    
    [kMainViewController hideLeftViewAnimated:YES completionHandler:nil];
}

- (IBAction)onCosts:(id)sender {
    ExpensesViewController *expensesViewController = [ExpensesViewController new];
    [kNavigationController pushViewController:expensesViewController animated:NO];
    
    [kMainViewController hideLeftViewAnimated:YES completionHandler:nil];
}

- (IBAction)onFinancialReport:(id)sender {
    FinancialReportViewController *financialReportViewController = [FinancialReportViewController new];
    [kNavigationController pushViewController:financialReportViewController animated:NO];
    
    [kMainViewController hideLeftViewAnimated:YES completionHandler:nil];
}

- (IBAction)onDeal:(id)sender {
    AffairsListViewController *affairsViewController = [AffairsListViewController new];
    [kNavigationController pushViewController:affairsViewController animated:NO];
    
    [kMainViewController hideLeftViewAnimated:YES completionHandler:nil];
}

- (IBAction)onSettings:(id)sender {
    SettingsViewController *settingsViewController = [SettingsViewController new];
    [kNavigationController pushViewController:settingsViewController animated:NO];
    
    [kMainViewController hideLeftViewAnimated:YES completionHandler:nil];
}

@end
