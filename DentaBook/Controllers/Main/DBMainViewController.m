//
//  DBMainViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 04.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "DBMainViewController.h"
#import "AppDelegate.h"
#import "DBPatientsViewController.h"

@interface DBMainViewController ()

@end

@implementation DBMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIImage *image = [UIImage imageNamed:@"sidemenu-sidemenu-icon"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(openLeftView)forControlEvents:UIControlEventTouchUpInside];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, 15, 14);
    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButton;
    
    //self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStylePlain target:self action:@selector(openLeftView)];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(sideMenuWillShowLeftView:)
                                                 name:kLGSideMenuControllerWillShowLeftViewNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(sideMenuWillHideLeftView:)
                                                 name:kLGSideMenuControllerWillDismissLeftViewNotification
                                               object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark -

- (void)openLeftView
{
    [kMainViewController showLeftViewAnimated:YES completionHandler:nil];
}

- (void) sideMenuWillShowLeftView:(NSNotification *) notification
{
    AppDelegate* appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    [UIView animateWithDuration:0.5 animations:^{
        appDelegate.statusBg.alpha = 0;
    }];
}

- (void) sideMenuWillHideLeftView:(NSNotification *) notification
{
    AppDelegate* appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    [UIView animateWithDuration:0.5 animations:^{
        appDelegate.statusBg.alpha = 1;
    }];
}

@end
