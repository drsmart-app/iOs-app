//
//  TherapyPlanChangeNameViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 15.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "TherapyPlanChangeNameViewController.h"

#import <MagicalRecord/MagicalRecord.h>

#import "Therapy.h"
#import "HttpUtil.h"

@interface TherapyPlanChangeNameViewController ()
@property (nonatomic, strong) Therapy *currentTherapy;
@property (weak, nonatomic) IBOutlet UITextField *nameLabel;
@end

@implementation TherapyPlanChangeNameViewController

-(id) initWithTherapy:(Therapy*)therapy {
    self = [super initWithNibName:@"TherapyPlanChangeNameViewController" bundle:[NSBundle mainBundle]];
    if(self)    {
        self.currentTherapy = therapy;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(self.currentTherapy.name.length > 0)
        self.nameLabel.text = self.currentTherapy.name;
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onSave:(id)sender {
    
    if(self.nameLabel.text.length == 0)
        return;
    
    self.currentTherapy.name = self.nameLabel.text;
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
