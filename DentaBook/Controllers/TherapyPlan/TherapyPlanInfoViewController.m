//
//  TherapyPlanInfoViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 15.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "TherapyPlanInfoViewController.h"
#import "UIView+Helpers.h"
#import "ChoosedServiceViewCell.h"
#import "TherapyViewController.h"
#import "TherapyPlanChangeNameViewController.h"

#import <MagicalRecord/MagicalRecord.h>

#import "Patient.h"
#import "CategoryTherapy.h"
#import "Service.h"
#import "Therapy.h"
#import "TherapyItem.h"
#import "HttpUtil.h"

@interface TherapyPlanInfoViewController () <NSFetchedResultsControllerDelegate>
@property (nonatomic, strong) NSDictionary *currentTherapy;
@property (nonatomic, strong) NSString *autoName;

@property (strong, nonatomic) IBOutlet UITableView *optionTableView;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (weak, nonatomic) IBOutlet UILabel *therapyPlanName;

@property (weak, nonatomic) IBOutlet UILabel *sumWithoutDiscountLabel;
@property (weak, nonatomic) IBOutlet UILabel *discountLabel;
@property (weak, nonatomic) IBOutlet UILabel *sumWithDiscountLabel;

@end

@implementation TherapyPlanInfoViewController
@synthesize sections;
-(id) initWithTherapy:(NSDictionary*)therapy withAutoName:(NSString*)autoName {
    self = [super initWithNibName:@"TherapyPlanInfoViewController" bundle:[NSBundle mainBundle]];
    if(self)    {
        self.currentTherapy = therapy;
        self.autoName = autoName;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(onEdit:)forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"Изменить" forState:UIControlStateNormal];
    [button setTitle:@"Изменить" forState:UIControlStateSelected];
    button.frame = CGRectMake(0, 0, 60, 25);
    button.titleLabel.font = [UIFont systemFontOfSize:12];
    
    [self.optionTableView registerNib:[UINib nibWithNibName:NSStringFromClass([ChoosedServiceViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ChoosedServiceViewCell class])];
    
    [self fetchTableData];
    [self updateBuyInfo];
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = getColorSchemeStatus();
    }
}

-(void) viewWillAppear:(BOOL)animated   {
    if(self.currentTherapy[@"name"] == 0)
        self.therapyPlanName.text = self.autoName;
    else
        self.therapyPlanName.text = self.currentTherapy[@"name"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)onEdit:(id)sender
{
    TherapyViewController *therapyViewController = [[TherapyViewController alloc] initWithTherapy:self.currentTherapy andPatient:nil];
    [self.navigationController pushViewController:therapyViewController animated:YES];
}

- (IBAction)onChangeTherapyName:(id)sender {
    TherapyPlanChangeNameViewController *changeNameViewController = [[TherapyPlanChangeNameViewController alloc] initWithTherapy:self.currentTherapy];
    [self.navigationController pushViewController:changeNameViewController animated:YES];
}

-(void)fetchTableData {
    NSArray* tmp = self.currentTherapy[@"items"];//[DataBase getDBArray:@"photo" withKey:@"patientid" andValue:self.currentPatient[@"patientid"]];
    
    NSMutableArray* tmp_sections = [[NSMutableArray alloc] init];
    
    for(NSDictionary* pat in tmp)
    {
        
        //nmd[@"index"] = [[(NSString*)pat[@"firstname"] substringWithRange:NSMakeRange(0, 1)] uppercaseString];
        [tmp_sections addObject:pat[@"categoryid"]];
    }
    
    sections = [[NSMutableArray alloc] init];
    for(NSString* hd in [tmp_sections valueForKeyPath:@"@distinctUnionOfObjects.self"])
    {
        NSMutableDictionary* ndms = [[NSMutableDictionary alloc] init];
        ndms[@"section"] = hd;
        NSMutableArray* mna=[[NSMutableArray alloc] init];
        for(NSDictionary* dict in tmp)
        {
            if([[(NSString*)dict[@"categoryid"] uppercaseString] isEqualToString:[hd uppercaseString]])
            {
                [mna addObject:dict];
            }
        }
        ndms[@"content"]=mna;
        [sections addObject:ndms];
    }
    
    [self updateBuyInfo];

}

- (NSFetchedResultsController *)fetchedResultsController {
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"TherapyItem"];
    [fetchRequest setFetchBatchSize:20];
    
    // Predicate
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"therapy == %@", self.currentTherapy];
    [fetchRequest setPredicate:predicate];
    
    // Sort
    NSSortDescriptor* dateSortDescripror;
    dateSortDescripror = [NSSortDescriptor sortDescriptorWithKey:@"serviceId"
                                                       ascending:NO];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObjects:dateSortDescripror, nil]];
    
    NSFetchedResultsController *theFetchedResultsController;
    theFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                      managedObjectContext:[NSManagedObjectContext MR_defaultContext]
                                                                        sectionNameKeyPath:@"categoryId"
                                                                                 cacheName:nil];
    theFetchedResultsController.delegate = self;
    self.fetchedResultsController = theFetchedResultsController;
    
    return _fetchedResultsController;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.optionTableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id )sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.optionTableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.optionTableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.optionTableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationBottom];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.optionTableView endUpdates];
    
    [self updateBuyInfo];
}

-(void) updateBuyInfo  {
    NSInteger cost = 0;
    
    for(TherapyItem *buyRef in [TherapyItem MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"therapy == %@", self.currentTherapy]])   {
        Service *serviceRef = [Service MR_findFirstByAttribute:@"remoteId" withValue:buyRef.serviceId];
        cost += [serviceRef.cost integerValue] * [buyRef.count integerValue];
    }
    
    /*self.sumWithoutDiscountLabel.text = [NSString stringWithFormat:@"%d руб.", cost];
    self.discountLabel.text = [NSString stringWithFormat:@"%d%%", [self.currentTherapy.discount integerValue]];
    self.sumWithDiscountLabel.text = [NSString stringWithFormat:@"%.2f руб.", cost - ((cost * [self.currentTherapy.discount floatValue])/100)];*/
}

#pragma mark -
#pragma mark Option Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger count = [[self.fetchedResultsController sections] count];
    return count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    NSInteger count2 = [sectionInfo numberOfObjects];
    return count2;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section    {
    
    id <NSFetchedResultsSectionInfo> theSection = [[self.fetchedResultsController sections] objectAtIndex:section];
    TherapyItem *buyRef = theSection.objects[0];
    CategoryTherapy *categoryRef = [CategoryTherapy MR_findFirstByAttribute:@"remoteId" withValue:buyRef.categoryId];
    
    UIView* headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 25)];
    
    headerView.backgroundColor = [UIColor whiteColor];
    
    UILabel* headerLabel = [[UILabel alloc] init];
    headerLabel.frame = CGRectMake(20, 2, tableView.frame.size.width - 5, 18);
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.textColor = [UIColor blackColor];
    headerLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:13];
    headerLabel.text = categoryRef.name;
    headerLabel.textAlignment = NSTextAlignmentLeft;
    
    [headerView addSubview:headerLabel];
    
    return headerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section    {
    
    UIView* headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 25)];
    headerView.backgroundColor = [UIColor whiteColor];
    
    UIImageView *seperateImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"seperate-cell"]];
    [headerView addSubview:seperateImageView];
    seperateImageView.center = headerView.center;
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section   {
    return 25;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section   {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath  {
    
    return 32.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TherapyItem *buyRef = [self.fetchedResultsController objectAtIndexPath:indexPath];
    Service *serviceRef = [Service MR_findFirstByAttribute:@"remoteId" withValue:buyRef.serviceId];
    
    ChoosedServiceViewCell *chooseCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ChoosedServiceViewCell class])];
    chooseCell.countTooth.text = [NSString stringWithFormat:@"%ld", [buyRef.toothId integerValue]];
    chooseCell.nameLabel.text = serviceRef.name;
    chooseCell.countService.text = [NSString stringWithFormat:@"%ld", [buyRef.count integerValue]];
    chooseCell.costService.text = [serviceRef.cost stringValue];
    
    return chooseCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
