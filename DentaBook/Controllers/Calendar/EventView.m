//
//  EventView.m
//  CalendarDemo_objc
//
//  Created by Dominik Pich on 11/10/15.
//  Copyright © 2015 Dominik Pich. All rights reserved.
//

#import "EventView.h"
#import "DDCalendarEvent.h"

@implementation EventView

CGFloat kResizeThumbSize = 75.0f;

- (instancetype)initWithEvent:(DDCalendarEvent*)event
{
    
    self = [super initWithEvent:event];
    if(self) {
        UILongPressGestureRecognizer *longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
        longTap.cancelsTouchesInView = YES;
        longTap.delegate = self;
        [self addGestureRecognizer:longTap];
        changeMode=NO;
        
        /*UIPanGestureRecognizer* gtap =  [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(scaleWatermark:)];
        longTap.cancelsTouchesInView = YES;
        longTap.delegate = self;
        [self addGestureRecognizer:gtap];
        changeMode=NO;*/
        
        self.myEvent = event;
        
        /*UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
        singleTap.cancelsTouchesInView = TRUE;
        singleTap.delegate = self;
        [self addGestureRecognizer:singleTap];
        
        UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
        swipe.cancelsTouchesInView = YES;
        swipe.delegate = self;
        [self addGestureRecognizer:swipe];
        
        [self setUserInteractionEnabled:YES];
        
        swipe.numberOfTouchesRequired = 1;
        swipe.direction = UISwipeGestureRecognizerDirectionUp;*/
        
    }
    return self;
}

- (void)setActive:(BOOL)active {
   
}

- (void)scaleWatermark:(UIPanGestureRecognizer *)gesture
{
    if (gesture.state == UIGestureRecognizerStateChanged
        || gesture.state == UIGestureRecognizerStateEnded)
    {
        
        CGPoint movePoint = [gesture translationInView:self];
        CGPoint endPoint = [gesture locationInView:self];
        CGPoint startPoint = CGPointMake(endPoint.x-movePoint.x, endPoint.y-movePoint.y);
        
        isResizingTop = (startPoint.y<75);
        isResizingBottom = (self.bounds.size.height - 75 < startPoint.y );
        
        CGFloat deltaWidth = endPoint.x - startPoint.x;
        CGFloat deltaHeight = (endPoint.y - startPoint.y);
        
        // get the frame values so we can calculate changes below
        CGFloat x = self.frame.origin.x;
        CGFloat y = self.frame.origin.y;
        CGFloat width = self.frame.size.width;
        CGFloat height = self.frame.size.height;
        
        //CGFloat true_step = deltaHeight - ((int)kResizeThumbSize)%((int)deltaHeight);
        //NSLog(@"Delta height: %f, true_step: %f", deltaHeight, true_step);
        
        if(isResizingTop)
        {
            self.frame = CGRectMake(x, y+deltaHeight, width, height-deltaHeight);
        }
        
        if(isResizingBottom)
        {
            self.frame = CGRectMake(x, y, width, height+deltaHeight);
        }
        
        [self adjustMarkers];
        [gesture setTranslation:CGPointZero inView: self];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"eventChangedShow" object:@{@"top":@(self.frame.origin.y),@"bottom":@(self.frame.origin.y+self.frame.size.height), @"event":self.myEvent}];
    }
    
    if(gesture.state == UIGestureRecognizerStateEnded)
        [[NSNotificationCenter defaultCenter] postNotificationName:@"eventChanged" object:@{@"top":@(self.frame.origin.y),@"bottom":@(self.frame.origin.y+self.frame.size.height), @"event":self.myEvent}];
}

 - (void) adjustMarkers
{
    /*[top_marker removeFromSuperview];
    [bottom_marker removeFromSuperview];
    
    top_marker = [[UILabel alloc] initWithFrame:CGRectMake(self.bounds.size.width-5,-5,10,10)];
    top_marker.font = [UIFont systemFontOfSize:7];
    top_marker.text=@"⚪️";
    bottom_marker = [[UILabel alloc] initWithFrame:CGRectMake(-5,self.bounds.size.height-5,10,10)];
    bottom_marker.font = [UIFont systemFontOfSize:7];
    bottom_marker.text=@"⚪️";
    [self addSubview:top_marker];
    [self addSubview:bottom_marker];*/
    
   /* UILongPressGestureRecognizer *longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    longTap.cancelsTouchesInView = TRUE;
    longTap.delegate = self;
    [self addGestureRecognizer:longTap];
    changeMode=NO;
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    singleTap.cancelsTouchesInView = TRUE;
    singleTap.delegate = self;
    [self addGestureRecognizer:singleTap];
    
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    swipe.cancelsTouchesInView = TRUE;
    swipe.delegate = self;
    [self addGestureRecognizer:swipe];*/
    
    /*UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    swipe.cancelsTouchesInView = YES;
    swipe.delegate = self;
    [self addGestureRecognizer:swipe];
    
    [self setUserInteractionEnabled:YES];
    
    swipe.numberOfTouchesRequired = 1;
    swipe.direction = UISwipeGestureRecognizerDirectionUp;*/
    
    
}

- (void) handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    /*if (gestureRecognizer.state != UIGestureRecognizerStateEnded) {
        return;
    }
    CGPoint p = [gestureRecognizer locationInView:self];
    NSLog(@"%@ %@ %@ %@",@(p.y),@(self.contentSize.height),@((p.y)/self.contentSize.height*24),@((p.y - (p.y)/self.contentSize.height*24*self.contentSize.height/24)*60));
    int hours = (p.y)/self.contentSize.height*24;
    int minutes = 60 * ((p.y)/self.contentSize.height*24.0f - (float)hours);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"needNewEvent" object:@{@"hour":@(hours),@"minute":@(minutes)}];*/
    [[NSNotificationCenter defaultCenter] postNotificationName:@"dsfgsdfgsdfg" object:nil];
    
}

/*CGFloat kResizeThumbSize = 75.0f;

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    touchStart = [touch locationInView:self];
    
    isResizingTop = (touchStart.y<kResizeThumbSize);
    isResizingBottom = (self.bounds.size.height - kResizeThumbSize < touchStart.y );
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchPoint = [touch locationInView:self];
    CGPoint previous = [touch previousLocationInView:self];
    
    CGFloat deltaWidth = touchPoint.x - previous.x;
    CGFloat deltaHeight = (touchPoint.y - previous.y)*2.5f;
    
    // get the frame values so we can calculate changes below
    CGFloat x = self.frame.origin.x;
    CGFloat y = self.frame.origin.y;
    CGFloat width = self.frame.size.width;
    CGFloat height = self.frame.size.height;
    
    if(isResizingTop)
    {
        self.frame = CGRectMake(x, y+deltaHeight, width, height-deltaHeight);
    }
    
    if(isResizingBottom)
    {
        self.frame = CGRectMake(x, y, width, height+deltaHeight);
    }
    
    [self adjustMarkers];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"noScroll" object:nil];
    
}

- (void) touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"yesScroll" object:nil];
}*/

@end
