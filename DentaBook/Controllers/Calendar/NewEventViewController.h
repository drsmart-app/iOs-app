//
//  NewEventViewController.h
//  DentaBook
//
//  Created by Denis Dubov on 10.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum    {
    kCalendarAffairMode = 0,        //доходы
    kCalendarPatientMode       //расходы
} CalendarEventMode;

@interface NewEventViewController : UIViewController
-(id) initWithPatient:(NSDictionary*)patient;
-(id) initWithEvent:(NSDictionary*)event andPatient:(NSDictionary*)patient;
@property (weak, nonatomic) IBOutlet UISegmentedControl *menuSelector;
- (IBAction)tapMenu:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *patLabel;
@property (weak, nonatomic) IBOutlet UIView *dealView;
@property (weak, nonatomic) IBOutlet UIView *patientView;
@property (weak, nonatomic) IBOutlet UIButton *dealActiveBtn;
@property (weak, nonatomic) IBOutlet UIButton *patientInactiveBtn;
@property (weak, nonatomic) IBOutlet UIButton *dealInactiveBtn;
@property (weak, nonatomic) IBOutlet UIButton *patientActiveBtn;

- (IBAction)toggleMenu:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *delimeterView;


@end
