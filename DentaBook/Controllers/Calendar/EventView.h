//
//  EventView.h
//  CalendarDemo_objc
//
//  Created by Dominik Pich on 11/10/15.
//  Copyright © 2015 Dominik Pich. All rights reserved.
//

#import "DDCalendarEventView.h"

@class Event;

@interface EventView : DDCalendarEventView<UIGestureRecognizerDelegate>
{
    BOOL isResizingLR;
    BOOL isResizingUL;
    BOOL isResizingUR;
    BOOL isResizingLL;
    CGPoint touchStart;
    
    BOOL isResizingTop;
    BOOL isResizingBottom;
    BOOL changeMode;
    
    UILabel* top_marker;
    UILabel* bottom_marker;
}

@property (nonatomic, strong) DDCalendarEvent *myEvent;

- (void) adjustMarkers;

@end
