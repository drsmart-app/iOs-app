//
//  CalendarViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 09.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "CalendarViewController.h"
#import "DDCalendarEvent.h"
#import "NSDate+DDCalendar.h"
#import "DDCalendarView.h"
#import "EventView.h"
#import "UIView+Helpers.h"
#import "NewEventViewController.h"
#import "EventPreviewViewController.h"
#import "DBCalendarBirthdays.h"
#import "FSCalendar.h"

#import <NSDate+Calendar/NSDate+Calendar.h>
#import <MagicalRecord/MagicalRecord.h>

#import "Event.h"
#import "Patient+Custom.h"

#import "DataBase.h"
#import "HttpUtil.h"

@interface CalendarViewController () <FSCalendarDataSource,FSCalendarDelegate,FSCalendarDelegateAppearance, DDCalendarViewDataSource, DDCalendarViewDelegate>

@property (weak, nonatomic) IBOutlet FSCalendar *calendar;
@property (weak, nonatomic) IBOutlet FSCalendar *calendar2;
@property (strong, nonatomic) IBOutlet NSArray *cached_events;
@property (strong, nonatomic) IBOutlet NSArray *cached_patients;
@property (nonatomic, strong) DBCalendarBirthdays *birthdaysView;
@property (nonatomic) BOOL isBackToFullCalendar;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sheduleViewTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sheduleViewHeightConstraint;
@property (nonatomic, strong) DDCalendarView *sheduleView;
@property (nonatomic, strong) NSDictionary *currentPatient;

@end

@implementation CalendarViewController  {
    NSDate *_dateSelected;
}

@synthesize mondayLabel, tuesdayLabel, wednesdayLabel, thursdayLabel, fridayLabel, saturdayLabel, sundayLabel, todayBtn;
-(id) initWithPatient:(NSDictionary*)patient
{
    self = [super initWithNibName:@"CalendarViewController" bundle:[NSBundle mainBundle]];
    if(self)    {
        self.currentPatient = patient;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    
    if(self.currentPatient)
    {
        self.title= [NSString stringWithFormat:@"%@ %@", self.currentPatient[@"lastname"], self.currentPatient[@"firstname"]];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.4 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self.sheduleView scrollDateToVisible:[NSDate date] animated:NO];
        });
        
    }
    else
        self.title = @"Календарь";
    
    self.calendar.tag = 1;
    self.calendar.pagingEnabled = NO;
    self.calendar.firstWeekday = 2;
    self.calendar.appearance.caseOptions = FSCalendarCaseOptionsWeekdayUsesSingleUpperCase|FSCalendarCaseOptionsHeaderUsesUpperCase;
    self.calendar.appearance.borderDefaultColor = [UIColor whiteColor];

    self.calendar2.tag = 2;
    self.calendar2.appearance.caseOptions = FSCalendarCaseOptionsWeekdayUsesSingleUpperCase|FSCalendarCaseOptionsHeaderUsesUpperCase;
    [self.calendar2 setScope:FSCalendarScopeWeek];
    self.calendar2.firstWeekday = 2;
    
    self.sheduleView = [[DDCalendarView alloc] initWithFrame:CGRectMake(0, 113.0, applicationFrame.size.width, applicationFrame.size.height - 113)];
    self.sheduleView.delegate = self;
    self.sheduleView.dataSource = self;
    [self.view insertSubview:self.sheduleView belowSubview:self.calendar];
    
    self.birthdaysView = [DBCalendarBirthdays new];
    [self.view insertSubview:self.birthdaysView belowSubview:self.calendar];
    
    UIImage *image2 = [UIImage imageNamed:@"back-button"];
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 addTarget:self action:@selector(onBack:)forControlEvents:UIControlEventTouchUpInside];
    [button2 setBackgroundImage:image2 forState:UIControlStateNormal];
    button2.frame = CGRectMake(0, 0, 11, 18);
    UIBarButtonItem *barButton2 = [[UIBarButtonItem alloc] initWithCustomView:button2];
    self.navigationItem.leftBarButtonItem = barButton2;
    
    UIImage *image = [UIImage imageNamed:@"patient-add-icon"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(onAdd:)forControlEvents:UIControlEventTouchUpInside];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, 18, 18);
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = barButton;
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    
    self.birthdaysView.hidden = YES;
    self.calendar2.hidden=YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(needNewEvent:) name:@"needNewEvent" object:nil];
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = getColorSchemeStatus();
    }
}

-(void)onBack:(id)sender
{
    if(self.isBackToFullCalendar)   {
        self.calendar.hidden = NO;
        self.calendar2.hidden = YES;
        self.isBackToFullCalendar = NO;
        todayBtn.hidden = YES;
        self.title = @"Календарь";
    } else  {
        [self.navigationController popViewControllerAnimated:YES];
    }
}



-(void)needNewEvent :(NSNotification*) n
{
    //your code
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd.MM.yyyy"];
    
    NSMutableDictionary *eventEntity = [[NSMutableDictionary alloc] init];
    eventEntity[@"type"] = @(0);
    eventEntity[@"title"] = @"";
    eventEntity[@"place"] = @"";
    eventEntity[@"startdate"] = [dateFormat stringFromDate:self.calendar2.selectedDate];
    eventEntity[@"enddate"] = [dateFormat stringFromDate:self.calendar2.selectedDate];
    eventEntity[@"cleardate"] = [dateFormat stringFromDate:self.calendar2.selectedDate];
    [dateFormat setDateFormat:@"HH"];
    eventEntity[@"starthours"] = [n.object[@"hour"] stringValue];
    int end_hours = [n.object[@"hour"] integerValue]+1;
    int end_minutes = [n.object[@"minute"] integerValue];
    if(end_hours >=24)
    {
        end_hours = 23;
        end_minutes= 59;
    }
    eventEntity[@"endhours"] = [@(end_hours) stringValue];
    [dateFormat setDateFormat:@"mm"];
    eventEntity[@"startminutes"] = [n.object[@"minute"] stringValue];
    eventEntity[@"endminutes"] = [@(end_minutes) stringValue];
    
    eventEntity[@"isallday"] = @(NO);
    eventEntity[@"commentt"] = @"";
    eventEntity[@"id"] = GUIDString();
    
    NewEventViewController *newEventViewController = [[NewEventViewController alloc] initWithEvent:eventEntity andPatient:self.currentPatient];
    [self.navigationController pushViewController:newEventViewController animated:YES];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void) viewWillAppear:(BOOL)animated   {
    
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    self.cached_events = [DataBase getDBArray:@"events" withKey:@"" andValue:@""];
    self.cached_patients = [DataBase getDBArray:@"patient" withKey:@"" andValue:@""];

    [self.calendar reloadData];
    [self.calendar2 reloadData];
    [self.sheduleView reloadData];
    [self switchImmediate];
}


-(void)onAdd:(id)sender
{
    NewEventViewController *newEventViewController = [NewEventViewController new];
    [self.navigationController pushViewController:newEventViewController animated:YES];
}

- (NSInteger) haveEventForDay:(NSDate *)date
{
    NSArray* patients = self.cached_events;
    int allFoundEvent = 0;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd.MM.yyyy"];
    NSArray* for_check = [[dateFormat stringFromDate:date] componentsSeparatedByString:@"."];
    for(NSDictionary* r in patients)
    {
        NSArray* a = [r[@"startdate"] componentsSeparatedByString:@"."];
        if(a!=nil)
        {
            if(a.count==3)
            {
                if([a[0] isEqualToString:for_check[0]]&&[a[1] isEqualToString:for_check[1]]&&[a[2] isEqualToString:for_check[2]])
                {
                    allFoundEvent++;
                }
            }
        }
    }
    
    NSArray* bthdays = [self haveBirthDayForDate:date];
    
    allFoundEvent+=bthdays?bthdays.count:0;
    
    return (allFoundEvent > 1) ? 1 : allFoundEvent;//(allFoundEvent > 3) ? 3 : allFoundEvent;
    
}

-(NSArray*) haveBirthDayForDate:(NSDate*)date
{
    NSMutableArray* result = [[NSMutableArray alloc] init];
    NSArray* patients = self.cached_patients;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd.MM.yyyy"];
    NSArray* for_check = [[dateFormat stringFromDate:date] componentsSeparatedByString:@"."];
    for(NSDictionary* r in patients)
    {
        NSArray* a = [r[@"birthdate"] componentsSeparatedByString:@"."];
        if(a!=nil)
        {
            if(a.count==3)
            {
                if([a[0] isEqualToString:for_check[0]]&&[a[1] isEqualToString:for_check[1]])
                {
                    [result addObject:r];
                }
            }
        }
    }
    
    return result;
}

- (NSInteger)calendar:(FSCalendar *)calendar numberOfEventsForDate:(NSDate *)date
{
    return [self haveEventForDay:date];
}

- (UIColor *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance borderDefaultColorForDate:(NSDate *)date
{
    if ([self haveBirthDayForDate:date]) {
        return [UIColor colorWithRed:1.0 green:111.0/255.0 blue:100.0/255.0 alpha:1.0];
    }
    
    return appearance.borderDefaultColor;
}

- (UIColor *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance borderSelectionColorForDate:(NSDate *)date
{
    if ([self haveBirthDayForDate:date]) {
        return appearance.borderSelectionColor;
    }
    return appearance.borderSelectionColor;
}

- (FSCalendarCellShape)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance cellShapeForDate:(NSDate *)date
{
//    if ([self haveBirthDayForDate:date]) {
//        return FSCalendarCellShapeCircle;
//    }
    
    return FSCalendarCellShapeCircle;
}

- (void) switchImmediate
{
    
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    
    [self.calendar selectDate:[NSDate date]];
    [self.calendar2 selectDate:[NSDate date]];
    self.calendar.hidden = YES;
    self.calendar2.hidden = NO;
    self.isBackToFullCalendar = YES;
    todayBtn.hidden = NO;
    
    NSArray* pats_birthay = [self haveBirthDayForDate:[NSDate date]];
    
    if(pats_birthay.count > 0)
    {
        self.birthdaysView.hidden = NO;
        
        [self.birthdaysView updatePatients:pats_birthay];
        
        [self.birthdaysView updateConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(0);
            make.top.equalTo(110);
            make.width.equalTo(applicationFrame.size.width);
            make.height.equalTo(15 + (pats_birthay.count)*12 + ((pats_birthay.count)-1)*7 + 15);
        }];
        
        [self.sheduleView makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(0);
            make.top.equalTo((113 + (15 + (pats_birthay.count)*12 + ((pats_birthay.count)-1)*7 + 15)));
            make.width.equalTo(applicationFrame.size.width);
            make.bottom.equalTo(self.view.bottom);
            //make.bottom.equalTo(applicationFrame.size.height);
        }];
    }
    
    else
    {
        self.birthdaysView.hidden = YES;
        
    }
    
    [self.sheduleView scrollDateToVisible:[NSDate date] animated:NO];
    [self.sheduleView reloadData];
}

- (void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date
{
    NSLog(@"did select %@", [calendar stringFromDate:date format:@"yyyy/MM/dd"]);
    
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    
    if(calendar.tag == 1)
    {
        [self.calendar2 selectDate:date];
        self.calendar.hidden = YES;
        self.calendar2.hidden = NO;
        self.isBackToFullCalendar = YES;
        todayBtn.hidden = NO;
    }
    else
    {
        [self.calendar selectDate:date];
    }
    
    NSArray* pats_birthay = [self haveBirthDayForDate:date];
    
    if(pats_birthay.count > 0)
    {
        self.birthdaysView.hidden = NO;
        
        [self.birthdaysView updatePatients:pats_birthay];
        
        [self.birthdaysView updateConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(0);
            make.top.equalTo(110);
            make.width.equalTo(applicationFrame.size.width);
            make.height.equalTo(15 + (pats_birthay.count)*12 + ((pats_birthay.count)-1)*7 + 15);
        }];
        
        [self.sheduleView makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(0);
            make.top.equalTo((113 + (15 + (pats_birthay.count)*12 + ((pats_birthay.count)-1)*7 + 15)));
            make.width.equalTo(applicationFrame.size.width);
            make.bottom.equalTo(self.view.bottom);
            //make.bottom.equalTo(applicationFrame.size.height);
        }];
    }
    
    else
    {
        self.birthdaysView.hidden = YES;

    }
    
    NSDate *currdate = [NSDate date];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSGregorianCalendar];
    NSDateComponents *components = [gregorian components: NSUIntegerMax fromDate: date];
    [components setHour: currdate.hour];
    [components setMinute: currdate.minute];
    
    NSDate *newDate = [gregorian dateFromComponents: components];
    
    [self.sheduleView scrollDateToVisible:newDate animated:NO];
    [self.sheduleView reloadData];
}

- (void)calendarCurrentPageDidChange:(FSCalendar *)calendar
{
    NSLog(@"did change page %@",[calendar stringFromDate:calendar.currentPage format:@"yyyy-MM"]);
}

#pragma mark DDCalendarViewDelegate

- (void)calendarView:(DDCalendarView* _Nonnull)view focussedOnDay:(NSDate* _Nonnull)date {
    
}

- (void)calendarView:(DDCalendarView* _Nonnull)view didSelectEvent:(DDCalendarEvent* _Nonnull)event {
    NSDictionary *myEvent = [event.userInfo objectForKey:@"myEvent"];
    EventPreviewViewController *previewEventViewController = [[EventPreviewViewController alloc] initWithEvent:myEvent];
    [self.navigationController pushViewController:previewEventViewController animated:YES];
}

- (BOOL)calendarView:(DDCalendarView* _Nonnull)view allowEditingEvent:(DDCalendarEvent* _Nonnull)event {
    return YES;
}

- (void)calendarView:(DDCalendarView* _Nonnull)view commitEditEvent:(DDCalendarEvent* _Nonnull)event {
    NSLog(@"%@", event);
}

#pragma mark DDCalendarViewDataSource

- (NSArray *)calendarView:(DDCalendarView *)view eventsForDay:(NSDate *)date
{
    NSArray* events = [DataBase getDBArray:@"events" withKey:@"" andValue:@""];
    //int allFoundEvent = 0;
    NSMutableArray *dates = [NSMutableArray array];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd.MM.yyyy"];
    NSArray* for_check = [[dateFormat stringFromDate:date] componentsSeparatedByString:@"."];
    for(NSDictionary* r in events)
    {
        NSArray* a = [r[@"startdate"] componentsSeparatedByString:@"."];
        if(a!=nil)
        {
            if(a.count==3)
            {
                if([a[0] isEqualToString:for_check[0]]&&[a[1] isEqualToString:for_check[1]]&&[a[2] isEqualToString:for_check[2]])
                {
                    DDCalendarEvent *calendarEvent = [DDCalendarEvent new];
                    calendarEvent.title = r[@"title"];
                    calendarEvent.dateBegin =  [[dateFormat dateFromString:r[@"startdate"]] dateBySettingHour:[r[@"starthours"] integerValue] minute:[r[@"startminutes"] integerValue] second:0];//[dateFormat dateFromString:r[@"startdate"]];
                    if([r[@"type"] integerValue] == kCalendarAffairMode)
                    {
                        calendarEvent.dateEnd = [[dateFormat dateFromString:r[@"startdate"]] dateBySettingHour:[r[@"endhours"] integerValue] minute:[r[@"endminutes"] integerValue] second:0];
                    }
                    else
                    {
                        calendarEvent.dateEnd = [[dateFormat dateFromString:r[@"startdate"]] dateBySettingHour:[r[@"endhours"] integerValue] minute:[r[@"endminutes"] integerValue] second:0];
                        //calendarEvent.dateEnd = [[dateFormat dateFromString:r[@"startdate"]] dateByAddingHour:1];
                    }
                    
                    [calendarEvent setUserInfo:@{@"myEvent":r}];
                    
                    [dates addObject:calendarEvent];

                }
            }
        }
    }
    
    return dates;
    
}

//optionally provide a view
- (EventView *)calendarView:(DDCalendarView *)view viewForEvent:(DDCalendarEvent *)event {
    
    NSDictionary *myEvent = [event.userInfo objectForKey:@"myEvent"];
    EventView *eventView = [[EventView alloc] initWithEvent:event];
    if([myEvent[@"type"] integerValue] == kCalendarAffairMode)   {
        eventView.backgroundColor = [UIColor colorWithRed:249.0/255.0 green:186.0/255.0 blue:186.0/255.0 alpha:1.0];
    }
    else    {
        eventView.backgroundColor = [UIColor colorWithRed:236.0/255.0 green:240.0/255.0 blue:241.0/255.0 alpha:1.0];
    }
    //eventView.
    return eventView;
}

- (IBAction)tapToday:(id)sender
{
    [_calendar2 selectDate:[NSDate date]];
}
@end
