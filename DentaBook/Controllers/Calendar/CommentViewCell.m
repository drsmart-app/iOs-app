//
//  CommentViewCell.m
//  DentaBook
//
//  Created by Denis Dubov on 29.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "CommentViewCell.h"
#import "UIView+Helpers.h"
#import <SAMTextView/SAMTextView.h>

@interface CommentViewCell()
//@property (weak, nonatomic) IBOutlet UITextField *commentTextField;
@end

@implementation CommentViewCell
@synthesize commentTextView, commentPlaceholder;
- (void)awakeFromNib {
    self.commentTextView.font = [UIFont systemFontOfSize:12];
    //self.commentTextView.placeholder = @"Комментарий";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    //self.commentTextField.delegate=self;
    // Configure the view for the selected state
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    commentTextView.delegate = self;

}

-(void) updateCell:(NSString*)commentText    {
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:commentText];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:3];
    [paragraphStyle setLineBreakMode:NSLineBreakByWordWrapping];
    [paragraphStyle setAlignment:NSTextAlignmentLeft];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, commentText.length)];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12] range:NSMakeRange(0, [commentText length])];
    [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithWhite:74.0f/255.0f alpha:1.0f] range:NSMakeRange(0, [commentText length])];
    self.commentTextView.attributedText = attributedString;
    
    if(commentText.length>0)
        commentPlaceholder.hidden=YES;
}

+ (CGFloat) heightOfCellInText:(NSString *)text cellWidth:(CGFloat)width {
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:text];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:3];
    [paragraphStyle setLineBreakMode:NSLineBreakByWordWrapping];
    [paragraphStyle setAlignment:NSTextAlignmentLeft];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, text.length)];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(0, [text length])];
    
    CGRect rect = [attributedString boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    
    return rect.size.height;
}

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"commentChanged" object:@{@"text":[[textField text] stringByReplacingCharactersInRange:range withString:string]}];
    
    return true;
}

- (void)textViewDidChange:(UITextView *)textView
{
    //Access the textView Content
    //From here you can get the last text entered by the user
    [[NSNotificationCenter defaultCenter] postNotificationName:@"commentChanged" object:@{@"text":[textView text]}];
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    commentPlaceholder.hidden=YES;
}


- (void)textViewDidEndEditing:(UITextView *)textView
{
    if([textView.text length]>0)
    {
    }
    else
    {
        commentPlaceholder.hidden=NO;
    }
}

@end
