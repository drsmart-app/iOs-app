//
//  AllDayViewCell.m
//  DentaBook
//
//  Created by Denis Dubov on 29.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "AllDayViewCell.h"
#import "HttpUtil.h"

@interface AllDayViewCell()
@property (nonatomic, strong) UISwitch *geoSwitch;
@end

@implementation AllDayViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        [self setup];
    }
    
    return self;
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
    
    //self.separator.backgroundColor = [UIColor colorWithWhite:0.902f alpha:1.0f];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    //self.separator.backgroundColor = [UIColor colorWithWhite:0.902f alpha:1.0f];
}

#pragma mark - Private Methods

- (void)setup {
    self.geoSwitch = [UISwitch new];
    [self.geoSwitch addTarget:self
                       action:@selector(stateChanged:) forControlEvents:UIControlEventValueChanged];
    
    self.accessoryView = self.geoSwitch;
    self.selectionStyle = UITableViewCellSelectionStyleDefault;
    
    self.textLabel.font = [UIFont fontWithName:@"GothamPro-Bold" size:12.0f];
    
    self.geoSwitch.onTintColor = getColorSchemeBar();//[UIColor colorWithRed:14.0f/255.0f green:127.0f/255.0f blue:184.0f/255.0f alpha:1.0f];
}

- (void)stateChanged:(UISwitch *)switchState
{
    if([self.delegate respondsToSelector:@selector(switchAllDayCell:stateChanged:)])    {
        [self.delegate switchAllDayCell:self stateChanged:switchState.isOn];
    }
}

-(void) setState:(BOOL)state    {
    [self.geoSwitch setOn:state];
}

@end
