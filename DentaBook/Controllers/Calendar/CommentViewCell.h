//
//  CommentViewCell.h
//  DentaBook
//
//  Created by Denis Dubov on 29.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentViewCell : UITableViewCell<UITextFieldDelegate, UITextViewDelegate>

-(void) updateCell:(NSString*)commentText;
+ (CGFloat) heightOfCellInText:(NSString *)text cellWidth:(CGFloat)width;
@property (weak, nonatomic) IBOutlet UITextView *commentTextView;
@property (weak, nonatomic) IBOutlet UILabel *commentPlaceholder;

@end
