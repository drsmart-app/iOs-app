//
//  AllDayViewCell.h
//  DentaBook
//
//  Created by Denis Dubov on 29.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AllDayViewCellDelegate;

@interface AllDayViewCell : UITableViewCell

@property (nonatomic, weak) id<AllDayViewCellDelegate> delegate;

-(void) setState:(BOOL)state;

@end

@protocol AllDayViewCellDelegate <NSObject>
-(void) switchAllDayCell:(AllDayViewCell*)cell stateChanged:(BOOL)state;
@end