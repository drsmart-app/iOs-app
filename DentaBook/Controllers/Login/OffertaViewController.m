//
//  OffertaViewController.m
//  DentaBook
//
//  Created by Mac Mini on 01.11.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "OffertaViewController.h"
#import "HTTPUtil.h"

@interface OffertaViewController ()

@end

@implementation OffertaViewController
@synthesize webView;
- (void)viewDidLoad {
    [super viewDidLoad];
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"terms_of_use_white" withExtension:@"rtf"];
    
    NSError *error = nil;
    NSAttributedString *string = [[NSAttributedString alloc] initWithFileURL:url
                                                                     options:@{NSDocumentTypeDocumentAttribute:NSRTFTextDocumentType}
                                                          documentAttributes:NULL
                                                                       error:&error];
    
    webView.attributedText = string;
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/




- (IBAction)onAcceptOfferta:(id)sender
{
    setOffertaAccepted();
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
