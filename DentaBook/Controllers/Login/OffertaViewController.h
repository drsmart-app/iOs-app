//
//  OffertaViewController.h
//  DentaBook
//
//  Created by Mac Mini on 01.11.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OffertaViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *acceptBtn;
- (IBAction)onAcceptOfferta:(id)sender;
@property (weak, nonatomic) IBOutlet UITextView *webView;

@end
