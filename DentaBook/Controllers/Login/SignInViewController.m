//
//  SignInViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 04.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "SignInViewController.h"
#import <UIAlertView+Blocks/UIAlertView+Blocks.h>
#import "UserManager.h"
#import "HttpUtil.h"

@interface SignInViewController ()

@property (weak, nonatomic) IBOutlet UITextField *loginTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField1;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField2;
@property (weak, nonatomic) IBOutlet UIButton *signInButton;
@end

@implementation SignInViewController
@synthesize backgroundImg;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Регистрация";
    self.navigationController.navigationBarHidden = YES;
    backgroundImg.image = [UIImage imageNamed:getColorSchemeImg()];
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = getColorSchemeStatus();
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)onSignIn:(id)sender {
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    
    if ([emailTest evaluateWithObject:self.loginTextField.text] == NO)
    {
        [UIAlertView showWithTitle:@"Ошибка" message:@"Проверте формат логина" cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
        return;
    }
    
    if(self.passwordTextField1.text.length < 6 || self.passwordTextField2.text.length < 6)  {
        [UIAlertView showWithTitle:@"Ошибка" message:@"Пароль должен быть минимум 6 символов" cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
        return;
    }
    
    if(![self.passwordTextField1.text isEqualToString:self.passwordTextField2.text])    {
        [UIAlertView showWithTitle:@"Ошибка" message:@"Пароли не совпадают" cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
        return;
    }
    
    [[UserManager shared]  registerWithEmail:self.loginTextField.text
                                    password:self.passwordTextField1.text success:^{
                                        [[[UIApplication sharedApplication] delegate] performSelector:@selector(switchToApp)];
                                    } failure:^{
                                        [UIAlertView showWithTitle:@"Ошибка регистрации" message:@"Повторите снова" cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
                                    }];
}

-(void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

@end
