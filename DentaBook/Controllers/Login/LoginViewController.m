//
//  LoginViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 04.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "LoginViewController.h"
#import <UIAlertView+Blocks/UIAlertView+Blocks.h>
#import "UserManager.h"
#import "UITextField+Insets.h"
#import "OffertaViewController.h"
#import "HttpUtil.h"

#import "SignInViewController.h"

@interface LoginViewController ()
@property (weak, nonatomic) IBOutlet UITextField *loginTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField2;
@property (weak, nonatomic) IBOutlet UIButton *registerButton;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *registerScreenButton;
@end

@implementation LoginViewController
@synthesize backgroundImg;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Войти";
    self.navigationController.navigationBarHidden = YES;
    backgroundImg.image = [UIImage imageNamed:getColorSchemeImg()];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void) viewDidAppear:(BOOL)animated
{
    if([getOffertaAccepted() length]==0)
    {
        OffertaViewController *offertaViewController = [OffertaViewController new];
        [self.navigationController presentViewController:offertaViewController animated:YES completion:nil];
    }
    self.loginTextField.text = getLogin();
    self.passwordTextField.text = getPwd();
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = getColorSchemeStatus();
    }
}

- (IBAction)onRegister:(id)sender {
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    
    if ([emailTest evaluateWithObject:self.loginTextField.text] == NO)
    {
        [UIAlertView showWithTitle:@"Ошибка" message:@"Проверте формат логина" cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
        return;
    }
    
    if(self.passwordTextField.text.length < 6 || self.passwordTextField2.text.length < 6)  {
        [UIAlertView showWithTitle:@"Ошибка" message:@"Пароль должен быть минимум 6 символов" cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
        return;
    }
    
    if(![self.passwordTextField.text isEqualToString:self.passwordTextField2.text])    {
        [UIAlertView showWithTitle:@"Ошибка" message:@"Пароли не совпадают" cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
        return;
    }
    
    [[UserManager shared]  registerWithEmail:self.loginTextField.text
                                    password:self.passwordTextField.text success:^{
                                        [[[UIApplication sharedApplication] delegate] performSelector:@selector(switchToApp)];
                                    } failure:^{
                                        [UIAlertView showWithTitle:@"Ошибка регистрации" message:@"Повторите снова" cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
                                    }];
}

- (IBAction)onLogin:(id)sender {
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    
    if ([emailTest evaluateWithObject:self.loginTextField.text] == NO)
    {
        [UIAlertView showWithTitle:@"Ошибка" message:@"Проверьте формат логина" cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
        return;
    }
    
    if(self.passwordTextField.text.length < 6)  {
        [UIAlertView showWithTitle:@"Ошибка" message:@"Пароль должен быть минимум 6 символов" cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
        return;
    }
    
    [[[UIApplication sharedApplication] delegate] performSelector:@selector(switchToApp)];
    setLoginPassword(self.loginTextField.text, self.passwordTextField.text);

    /*[[UserManager shared] loginWithEmail:self.loginTextField.text
                                password:self.passwordTextField.text success:^{
                                    setLoginPassword(self.loginTextField.text, self.passwordTextField.text);
        [[[UIApplication sharedApplication] delegate] performSelector:@selector(switchToApp)];
    } failure:^{
        setLoginPassword(self.loginTextField.text, self.passwordTextField.text);
        [[[UIApplication sharedApplication] delegate] performSelector:@selector(switchToApp)];
        //[UIAlertView showWithTitle:@"Ошибка" message:@"Повторите снова" cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
    }];*/
}

-(void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

- (IBAction)onRegisterScreen:(id)sender {
    self.registerScreenButton.hidden = YES;
    self.cancelButton.hidden = NO;
    self.loginButton.hidden = YES;
    self.passwordTextField2.hidden = NO;
    self.registerButton.hidden = NO;
}

- (IBAction)onCancel:(id)sender {
    self.registerScreenButton.hidden = NO;
    self.cancelButton.hidden = YES;
    self.loginButton.hidden = NO;
    self.passwordTextField2.hidden = YES;
    self.registerButton.hidden = YES;
}

@end
