//
//  ExpChooseCategoryViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 27.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "ExpChooseCategoryViewController.h"
#import "ExpAddCategoryViewController.h"

#import <MagicalRecord/MagicalRecord.h>
#import <SWTableViewCell/SWTableViewCell.h>

#import "ExpenseCategory.h"
#import "ExpenseSubcategory.h"
#import "ExpenseManager.h"
#import "DataBase.h"
#import "PercUtils.h"
#import "HttpUtil.h"

@interface ExpChooseCategoryViewController () <NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) NSDictionary *currentCategory;

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@end

@implementation ExpChooseCategoryViewController
@synthesize categories;
-(id) initWithExpenseCategory:(NSDictionary*)expenseCategory {
    self = [super initWithNibName:@"ExpChooseCategoryViewController" bundle:[NSBundle mainBundle]];
    if(self)    {
        self.currentCategory = expenseCategory;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Внести расход";
    UIImage *image2 = [UIImage imageNamed:@"back-button"];
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 addTarget:self action:@selector(onBack:)forControlEvents:UIControlEventTouchUpInside];
    [button2 setBackgroundImage:image2 forState:UIControlStateNormal];
    button2.frame = CGRectMake(0, 0, 11, 18);
    UIBarButtonItem *barButton2 = [[UIBarButtonItem alloc] initWithCustomView:button2];
    self.navigationItem.leftBarButtonItem = barButton2;
    
    [self fetchTableData];
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = getColorSchemeStatus();
    }
}

- (void) viewDidAppear:(BOOL)animated
{
    [self fetchTableData];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)fetchTableData
{
    if(self.currentCategory)
    {
        categories = [DataBase getDBArray:@"expence_subcategory" withKey:@"categoryid" andValue:self.currentCategory[@"id"]];
    }
    else
    {
        categories = [DataBase getDBArray:@"expence_category" withKey:@"" andValue:@""];
    }
}

#pragma mark -
#pragma mark Option Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    //NSInteger count = [self.fetchedResultsController.fetchedObjects count];
    return categories.count + 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UILabel* headerLabel = [[UILabel alloc] init];
    headerLabel.frame = CGRectMake(0, 0, self.tableView.frame.size.width, 50.0);
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.textColor = [UIColor colorWithRed:74.0f/255.0f green:74.0f/255.0f blue:74.0f/255.0f alpha:1.0f];

    headerLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
    headerLabel.text = (self.currentCategory == nil)?@"Выбор категории":self.currentCategory[@"title"];
    headerLabel.textAlignment = NSTextAlignmentCenter;
    
    return headerLabel;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section    {
    
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section   {
    
    return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section   {
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath  {
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger count = categories.count;//[self.fetchedResultsController.fetchedObjects count];
    
    UITableViewCell *cell = nil;
    
    if(indexPath.row != count)
    {
        SWTableViewCell *titleCell = [tableView dequeueReusableCellWithIdentifier:@"titleCell"];
        if (titleCell == nil) {
            titleCell = [[SWTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"titleCell"];
            titleCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            titleCell.delegate = self;
            titleCell.rightUtilityButtons = [self rightButtons];
            titleCell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
        }
        
        NSDictionary *expenseCategory = categories[indexPath.row];//[self.fetchedResultsController objectAtIndexPath:indexPath];
        titleCell.textLabel.text = expenseCategory[@"title"];
        
        cell = titleCell;
    }
    else
    {
        UITableViewCell *titleCell = [tableView dequeueReusableCellWithIdentifier:@"titleCell2"];
        if (titleCell == nil) {
            titleCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"titleCell2"];
            titleCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        titleCell.textLabel.text = (self.currentCategory == nil)?@"Добавить категорию":@"Добавить подкатегорию";
        titleCell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
        titleCell.textLabel.textColor = [UIColor colorWithRed:20.0/255.0 green:162.0/255.0 blue:243.0/255.0 alpha:1.0];
        cell = titleCell;
    }
    
    return cell;
}

- (NSArray *)rightButtons
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:231.0f/255.0f green:76.0f/255.0f blue:60.0f/255.0f alpha:1.0f] icon:[PercUtils imageWithImage:[UIImage imageNamed:@"delete_white.png"] scaledToSize:CGSizeMake(20, 25)]];
    
    return rightUtilityButtons;
}

- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell   {
    return YES;
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    switch (index) {
        case 0: {
            NSIndexPath *cellIndexPath = [self.tableView indexPathForCell:cell];
            
            if(self.currentCategory == nil)
            {
                [[ExpenseManager shared] deleteExpenseCategoryById:categories[cellIndexPath.row] success:^{
                    //
                } failure:^{
                    //
                }];
            }
            else
            {
                [[ExpenseManager shared] deleteExpenseSubcategoryById:categories[cellIndexPath.row] success:^{
                    //
                } failure:^{
                    //
                }];
            }
        }
            break;
        default:
            break;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSInteger count = categories.count;//[self.fetchedResultsController.fetchedObjects count];
    
    if(indexPath.row == count)
    {
        ExpAddCategoryViewController *addCategoryVC = [[ExpAddCategoryViewController alloc] initWithExpenseCategory:self.currentCategory];
        [self.navigationController pushViewController:addCategoryVC animated:YES];
    }
    else
    {
        if(self.currentCategory == nil)
        {
            NSDictionary *expenseCategory = categories[indexPath.row];//[self.fetchedResultsController objectAtIndexPath:indexPath];
            ExpChooseCategoryViewController *expChooseSubcategoryVC = [[ExpChooseCategoryViewController alloc] initWithExpenseCategory:expenseCategory];
            [self.navigationController pushViewController:expChooseSubcategoryVC animated:YES];
            expChooseSubcategoryVC.didChooseCategory = ^(NSString *remoteId, NSString *subcategoryId)    {
                if(self.didChooseCategory)  {
                    self.didChooseCategory(remoteId, subcategoryId);
                }
                [self.navigationController popViewControllerAnimated:YES];
            };
        }
        else
        {
            NSDictionary *expenseCategory = categories[indexPath.row];//[self.fetchedResultsController objectAtIndexPath:indexPath];
            if(self.didChooseCategory)
            {
                self.didChooseCategory(self.currentCategory[@"id"], expenseCategory[@"id"]);
            }
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}


-(void)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
