//
//  PutExpenseViewController.h
//  DentaBook
//
//  Created by Denis Dubov on 27.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IBActionSheet.h"
#import "CZPicker.h"
@class Expense;

@interface PutExpenseViewController : UIViewController <UIImagePickerControllerDelegate, CZPickerViewDataSource, CZPickerViewDelegate>

-(id) initWithExpense:(NSDictionary*)expense;
@property (weak, nonatomic) IBOutlet UIButton *dateBtn;
- (IBAction)tapDate:(id)sender;
@property (retain, nonatomic) NSDate *selectedDate;
- (IBAction)tapPhoto:(id)sender;
@property (nonatomic, strong) IBActionSheet *photoActionSheet;
@property (weak, nonatomic) IBOutlet UIImageView *hasImgImg;

@end
