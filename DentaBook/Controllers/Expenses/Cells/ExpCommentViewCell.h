//
//  ExpCommentViewCell.h
//  DentaBook
//
//  Created by Denis Dubov on 27.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExpCommentViewCell : UITableViewCell<UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UITextView *commentTextView;
@property (weak, nonatomic) IBOutlet UILabel *commentPlaceholder;
@end
