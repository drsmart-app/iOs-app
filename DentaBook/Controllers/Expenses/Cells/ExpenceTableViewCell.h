//
//  ExpenceTableViewCell.h
//  DentaBook
//
//  Created by Mac Mini on 02.12.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SWTableViewCell/SWTableViewCell.h>

@interface ExpenceTableViewCell :  SWTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *expenceTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *hasImageBtn;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
- (IBAction)tapImg:(id)sender;
@property (retain, nonatomic) IBOutlet UIImageView *imgToZoom;
@property (weak, nonatomic) UIViewController *owner;
@property (weak, nonatomic) IBOutlet UIImageView *hasImageImage;

@end
