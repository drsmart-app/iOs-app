//
//  ExpenceTableViewCell.m
//  DentaBook
//
//  Created by Mac Mini on 02.12.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "ExpenceTableViewCell.h"
#import <JTSImageViewController/JTSImageViewController.h>

@implementation ExpenceTableViewCell
@synthesize owner, imgToZoom;
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)tapImg:(id)sender
{
    JTSImageInfo *imageInfo = [[JTSImageInfo alloc] init];
    imageInfo.image = imgToZoom.image;
    imageInfo.referenceRect = self.imageView.frame;
    imageInfo.referenceView = self.imageView.superview;
    
    JTSImageViewController *imageViewer = [[JTSImageViewController alloc]
                                           initWithImageInfo:imageInfo
                                           mode: JTSImageViewControllerMode_Image
                                           backgroundStyle: JTSImageViewControllerBackgroundOption_Scaled];
    
    // Present the view controller.
    [imageViewer showFromViewController:owner transition:JTSImageViewControllerTransition_FromOriginalPosition];
}
@end
