//
//  PutExpenseViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 27.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "PutExpenseViewController.h"
#import "ExpAmountViewCell.h"
#import "ExpCommentViewCell.h"
#import "ExpChooseCategoryViewController.h"

#import "WLPActionSheet.h"
#import "DBFullDateController.h"

#import "Expense.h"
#import "ExpenseCategory.h"
#import "ExpenseSubcategory.h"
#import "ExpenseManager.h"

#import <MagicalRecord/MagicalRecord.h>
#import <UIAlertView+Blocks/UIAlertView+Blocks.h>
#import <JGProgressHUD/JGProgressHUD.h>
#import "DataBase.h"

#import "ELCImagePickerController.h"
#import <UIAlertView+Blocks/UIAlertView+Blocks.h>
#import "PhotoEditViewController.h"
#import "GalleryManager.h"
#import "HttpUtil.h"

@interface PutExpenseViewController ()
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) ExpAmountViewCell *amountCell;
@property (nonatomic, strong) ExpCommentViewCell *commentCell;

@property (nonatomic, strong) NSString *selectedCategoryId;
@property (nonatomic, strong) NSString *selectedSubCategoryId;

@property (nonatomic, strong) NSMutableDictionary *currentExpense;

@property (nonatomic, strong) WLPActionSheet *dateActionSheet;
@property (nonatomic, strong) DBFullDateController *dateController;
@end

@implementation PutExpenseViewController
@synthesize selectedDate, dateBtn, photoActionSheet, hasImgImg;
-(id) initWithExpense:(NSDictionary*)expense {
    self = [super initWithNibName:@"PutExpenseViewController" bundle:[NSBundle mainBundle]];
    if(self)    {
        self.currentExpense = [NSMutableDictionary dictionaryWithDictionary:expense];
        self.title = @"Редактирование расхода";
    }
    return self;
}

-(id) init
{
    self = [super initWithNibName:@"PutExpenseViewController" bundle:[NSBundle mainBundle]];
    if(self)    {
        self.currentExpense = [[NSMutableDictionary alloc] init];
        self.currentExpense[@"id"] = GUIDString();
        self.title = @"Внести расход";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.selectedCategoryId = nil;
    self.selectedSubCategoryId = nil;
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ExpAmountViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ExpAmountViewCell class])];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ExpCommentViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ExpCommentViewCell class])];
    
    self.amountCell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ExpAmountViewCell class])];
    self.commentCell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ExpCommentViewCell class])];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(photoUploaded) name:@"photoUploaded" object:nil];
    
    if(self.currentExpense)
    {
        if(self.currentExpense[@"amount"])
            self.amountCell.amountTextField.text = [NSString stringWithFormat:@"%ld", [self.currentExpense[@"amount"] integerValue]];
        if(self.currentExpense[@"commentt"])
        {
            self.commentCell.commentTextView.text = [NSString stringWithFormat:@"%@", self.currentExpense[@"commentt"]];
            self.commentCell.commentPlaceholder.hidden = YES;
        }
        if(self.currentExpense[@"categoryid"])
            self.selectedCategoryId = self.currentExpense[@"categoryid"];
        if(self.currentExpense[@"subcategoryid"])
            self.selectedSubCategoryId = self.currentExpense[@"subcategoryid"];
        
        if(self.currentExpense[@"createat"])
           {
               [self.dateBtn setTitle:self.currentExpense[@"createat"] forState:UIControlStateNormal];
               [self.dateBtn setTitle:self.currentExpense[@"createat"] forState:UIControlStateSelected];
           }
    }
        
    self.tableView.layoutMargins = UIEdgeInsetsZero;
    self.tableView.separatorInset = UIEdgeInsetsZero;
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.dateController = [DBFullDateController new];
    selectedDate = [NSDate date];
    
    NSArray* arr = [DataBase getDBArray:@"photo" withKey:@"expenceid" andValue:self.currentExpense[@"id"]];
    if(arr.count>0)
        hasImgImg.hidden = NO;
    else
        hasImgImg.hidden = YES;
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = getColorSchemeStatus();
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void) photoUploaded
{
    NSArray* arr = [DataBase getDBArray:@"photo" withKey:@"expenceid" andValue:self.currentExpense[@"id"]];
    if(arr.count>0)
        hasImgImg.hidden = NO;
    else
        hasImgImg.hidden = YES;
}

#pragma mark -
#pragma mark Option Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return (self.currentExpense) ? 3 : 3;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section    {
    
    return [UIView new];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section    {
    
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section   {
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section   {
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath  {
    if(indexPath.row == 2)  {
        return 120.0;
    }
    
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    
    if(indexPath.row == 0)
    {
        UITableViewCell *titleCell = [tableView dequeueReusableCellWithIdentifier:@"titleCell"];
        if (titleCell == nil)
        {
            titleCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"titleCell"];
            titleCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        titleCell.textLabel.text = @"Категория";
        
        if([self.selectedCategoryId length] > 0)
        {
            NSDictionary *expenseCategory = (NSDictionary*)[DataBase getDB:@"expence_category" withKey:@"id" andValue:self.selectedCategoryId];//[ExpenseCategory MR_findFirstByAttribute:@"remoteId" withValue:self.selectedCategoryId];
            if(expenseCategory)
            {
                NSDictionary *expenseSubCategory = (NSDictionary*)[DataBase getDB:@"expence_subcategory" withKey:@"id" andValue:self.selectedSubCategoryId];//[ExpenseSubcategory MR_findFirstByAttribute:@"remoteId" withValue:self.selectedSubCategoryId];
                titleCell.detailTextLabel.text = [NSString stringWithFormat:@"%@:%@", expenseCategory[@"title"], expenseSubCategory[@"title"]];
            }
        }
        titleCell.textLabel.font = [UIFont systemFontOfSize:12.0f];
        titleCell.detailTextLabel.font = [UIFont systemFontOfSize:12.0f];
        titleCell.textLabel.textColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
        cell = titleCell;
    }
    else if(indexPath.row == 1)
    {
        cell = self.amountCell;
    }
    else if(indexPath.row == 2)
    {
        cell = self.commentCell;
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleDefault];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(indexPath.row == 0)
    {
        ExpChooseCategoryViewController *chooseCategoryVC = [[ExpChooseCategoryViewController alloc] initWithExpenseCategory:nil];
        chooseCategoryVC.didChooseCategory = ^(NSString *categoryId, NSString *subcategoryId)
        {
            self.selectedCategoryId = categoryId;
            self.selectedSubCategoryId = subcategoryId;
            [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        };
        [self.navigationController pushViewController:chooseCategoryVC animated:YES];
        [self.view endEditing:YES];
    } else if(indexPath.row == 3)   {
        [self onSave:nil];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView  {
    [self.view endEditing:YES];
}

-(IBAction) onSave:(id)sender
{
    if(self.amountCell.amountTextField.text.length == 0)
    {
        [UIAlertView showWithTitle:@"Ошибка" message:@"Введите сумму" cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
        return;
    }
    else if(!([self.selectedCategoryId length] > 0))
    {
        [UIAlertView showWithTitle:@"Ошибка" message:@"Выберите категорию" cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
        return;
    }
    
    selectedDate = selectedDate==nil?[NSDate date]:selectedDate;
    
    NSDateComponents *components = [[NSCalendar currentCalendar]
                                    components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay
                                    fromDate:selectedDate];
    NSDate *startDate = [[NSCalendar currentCalendar]
                         dateFromComponents:components];
    
    //NSMutableDictionary *expenseEntity = [[NSMutableDictionary alloc] init];//[Expense MR_createEntity];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd.MM.yyyy"];
    self.currentExpense[@"createat"] = [dateFormatter stringFromDate:startDate];
    self.currentExpense[@"categoryid"] = self.selectedCategoryId;
    self.currentExpense[@"subcategoryid"] = self.selectedSubCategoryId;
    self.currentExpense[@"amount"] = @([self.amountCell.amountTextField.text doubleValue]);
    self.currentExpense[@"commentt"] = self.commentCell.commentTextView.text;
    //self.currentExpense[@"id"] = GUIDString();
    
    JGProgressHUD *HUD = [JGProgressHUD progressHUDWithStyle:JGProgressHUDStyleDark];
    HUD.textLabel.text = @"Сохранение";
    [HUD showInView:self.view];
    [[ExpenseManager shared] createExpense:self.currentExpense success:^
    {
        [HUD dismissAnimated:YES];
        [self.navigationController popViewControllerAnimated:YES];
    } failure:^{
        [HUD dismissAnimated:YES];
    }];
}

- (IBAction)tapDate:(id)sender
{
    [self.view endEditing:YES];
    
    self.dateActionSheet = [[WLPActionSheet alloc] initWithTitle:@"Дата" cancelButtonTitle:@"ОК" destructiveButtonTitle:nil withViewController:self.dateController];
    self.dateActionSheet.tag = 1;
    self.dateActionSheet.delegate = self;
    [self.dateActionSheet showInView:self.view];
}


-(void)actionSheetDidDone:(WLPActionSheet*)actionSheet  {
    selectedDate = self.dateController.datePicker.date;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MMMM yyyy"];
    NSString *strDate = [dateFormatter stringFromDate:selectedDate];
    [dateBtn setTitle:strDate forState:UIControlStateNormal];
    [dateBtn setTitle:strDate forState:UIControlStateSelected];
}

- (IBAction)tapPhoto:(id)sender {
    
    [self.view endEditing:YES];
    self.photoActionSheet = [[IBActionSheet alloc] initWithTitle:NSLocalizedString(@"Сделать фотографию", nil)
                                                        delegate:self
                                               cancelButtonTitle:NSLocalizedString(@"Отмена", nil)
                                          destructiveButtonTitle:nil
                                          otherButtonTitlesArray:@[@"Камера", NSLocalizedString(@"Выбрать из галереи", nil)]];
    self.photoActionSheet.tag = 1;
    [self.photoActionSheet showInViewX:self.view];
}

- (void)actionSheet:(IBActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(actionSheet.tag == 1)    {
        if(buttonIndex == 0){
            
            if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                
                [UIAlertView showWithTitle:@"Ошибка" message:@"Камера не обнаружена" cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
                
                return;
            }
            
            if (/* DISABLES CODE */ (YES)) {
                [self showImagePickerUsingSourceType:UIImagePickerControllerSourceTypeCamera];
            }else{
                [UIAlertView showWithTitle:@"Ошибка" message:@"This app does not have access to your photos or videos" cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
            }
            
        } else if(buttonIndex == 1) {
            
            if (/* DISABLES CODE */ (YES)) {
                [self showImagePickerUsingSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
            }else{
                [UIAlertView showWithTitle:@"Ошибка" message:@"This app does not have access to your photos or videos" cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
            }
        }
    }
}

- (void)showImagePickerUsingSourceType:(UIImagePickerControllerSourceType)type
{
    
    UIImagePickerController *imagePickerController = [UIImagePickerController new];
    imagePickerController.allowsEditing = NO;
    imagePickerController.delegate = self;
    imagePickerController.sourceType = type;
    
    [self presentViewController:imagePickerController animated:YES completion:^{
        
    }];
    
}

- (void)elcImagePickerController:(ELCImagePickerController *)picker didFinishPickingMediaWithInfo:(NSArray *)info
{
    NSLog(@"Info about images: %@",info);
    if(info.count==1)
    {
        NSString *mediaType = info[0][UIImagePickerControllerMediaType];
        UIImage *photo = nil;
        if ([mediaType isEqualToString:@"ALAssetTypePhoto"])
        {
            photo = info[0][UIImagePickerControllerOriginalImage];//info[UIImagePickerControllerEditedImage];
        }
        
        [self dismissViewControllerAnimated:YES completion:^{
            
            PhotoEditViewController *editViewController = [[PhotoEditViewController alloc] initWithPhoto:photo forExpence:self.currentExpense];
            [self.navigationController pushViewController:editViewController animated:YES];
        }];
    }
    else
    {
        for(NSDictionary* dd in info)
        {
            NSMutableDictionary* galleryEntity = [[NSMutableDictionary alloc] init];//Gallery *galleryEntity = [Gallery MR_createEntity];
            NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
            [dateformate setDateFormat:@"dd.MM.yyyy"];
            NSString *dateString = [dateformate stringFromDate:[NSDate date]];
            galleryEntity[@"createat"] = dateString;
            galleryEntity[@"createdat"] = dateString;
            galleryEntity[@"comment"] = @"";
            galleryEntity[@"expenceid"] = self.currentExpense[@"id"];
            galleryEntity[@"id"] = GUIDString();
            UIImage *photo = nil;
            photo = dd[UIImagePickerControllerOriginalImage];
            [[GalleryManager shared] createGallery:galleryEntity success:^{
                [[GalleryManager shared] uploadPhoto:photo toExpence:galleryEntity completion:^(NSError *error)
                {
                    hasImgImg.hidden = NO;
                }];
            } failure:^{
                //
            }];
        }
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
}

- (void)elcImagePickerControllerDidCancel:(ELCImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = info[UIImagePickerControllerMediaType];
    
    UIImage *photo = nil;
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]){
        photo = info[UIImagePickerControllerOriginalImage];//info[UIImagePickerControllerEditedImage];
    }
    
    [self dismissViewControllerAnimated:YES completion:^{
        
        PhotoEditViewController *editViewController = [[PhotoEditViewController alloc] initWithPhoto:photo forExpence:self.currentExpense];
        [self.navigationController pushViewController:editViewController animated:YES];
    }];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
