//
//  ExpChooseCategoryViewController.h
//  DentaBook
//
//  Created by Denis Dubov on 27.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ExpenseCategory;

@interface ExpChooseCategoryViewController : UIViewController

-(id) initWithExpenseCategory:(NSDictionary*)expenseCategory;

@property (copy, nonatomic) void (^didChooseCategory)(NSString *categoryId, NSString *subcategoryId);
@property (retain, nonatomic) NSArray* categories;

@end
