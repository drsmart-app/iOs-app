//
//  ExpensesViewController.h
//  DentaBook
//
//  Created by Denis Dubov on 27.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExpensesViewController : UIViewController
@property (nonatomic, retain) NSMutableArray* expences;
@property (weak, nonatomic) IBOutlet UILabel *totalMonthExpencesLabel;
@property (weak, nonatomic) IBOutlet UIView *addExpenceView;
@end
