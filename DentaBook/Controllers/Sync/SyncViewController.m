//
//  SyncViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 06.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "SyncViewController.h"
#import "NSOperationQueue+Completion.h"

#import <MagicalRecord/MagicalRecord.h>
#import <JGProgressHUD/JGProgressHUD.h>

#import "PatientManager.h"
#import "IllnessManager.h"
#import "DiagnosisManager.h"
#import "TherapyManager.h"
#import "DataManager.h"
#import "DBLayoutManager.h"
#import "AffairManager.h"
#import "ExpenseManager.h"

@interface SyncViewController ()
@property (nonatomic, strong) NSOperationQueue *operationQueue;
@end

@implementation SyncViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    JGProgressHUD *HUD = [JGProgressHUD progressHUDWithStyle:JGProgressHUDStyleDark];
    HUD.textLabel.text = @"Синхронизация";
    [HUD showInView:self.view];
    
    self.operationQueue = [[NSOperationQueue alloc] init];
    [self.operationQueue setMaxConcurrentOperationCount:1];
    
    AFHTTPRequestOperation *illnessOperation = [[IllnessManager shared] syncIllness:^{
        NSLog(@"Illness synced");
    } failure:^{
        NSLog(@"ERROR: Illness don't synced");
    }];
    
    AFHTTPRequestOperation *diagnosisOperation = [[DiagnosisManager shared] syncDiagnosis:^{
        //
    } failure:^{
        //
    }];
    
    AFHTTPRequestOperation *therapyCategoryOperation = [[TherapyManager shared] syncTherapyCategory:^{
        AFHTTPRequestOperation *therapyServiceOperation = [[TherapyManager shared] syncTherapyServices:^{
            //
        } failure:^{
            //
        }];
        [self.operationQueue addOperation:therapyServiceOperation];
    } failure:^{
        //
    }];
    
    AFHTTPRequestOperation *patientOperation = [[PatientManager shared] syncPatients:^{
        //
    } failure:^{
        //
    }];
    
    /*AFHTTPRequestOperation *affairOperation = [[AffairManager shared] syncAffairs:^{
        //
    } failure:^{
        //
    }];*/
    
    /*AFHTTPRequestOperation *expenseFixtureOperation = [[ExpenseManager shared] syncExpenseFixtures:^{
        //
    } failure:^{
        //
    }];
    
    AFHTTPRequestOperation *expenseOperation = [[ExpenseManager shared] syncExpense:^{
        //
    } failure:^{
        //
    }];*/
    
    [diagnosisOperation addDependency:illnessOperation];
    [therapyCategoryOperation addDependency:diagnosisOperation];
    [patientOperation addDependency:therapyCategoryOperation];
    /*[affairOperation addDependency:patientOperation];
    [expenseFixtureOperation addDependency:affairOperation];
    [expenseOperation addDependency:expenseFixtureOperation];
    
    [self.operationQueue addOperations:@[illnessOperation, diagnosisOperation, therapyCategoryOperation, patientOperation, affairOperation, expenseFixtureOperation, expenseOperation] waitUntilFinished:NO];
    [self.operationQueue setCompletion:^{
        [HUD dismissAnimated:YES];
        [[DBLayoutManager shared] closeSyncScreen];
    }];*/
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
