//
//  ToothCardListViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 04.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "ToothCardListViewController.h"
#import "IBActionSheet.h"
#import "ToothCardViewController.h"
#import "DBToothMapScroller.h"
#import "TestViewController.h"
#import "ToothViewCell.h"
#import "UIView+Helpers.h"
#import "UIView+GestureBlocks.h"
#import <SWTableViewCell/SWTableViewCell.h>

#import "Patient+Custom.h"
#import "ToothList.h"
#import "Tooth.h"
#import "DentalDiagnosis.h"
#import "DataBase.h"
#import "ToothManager.h"
#import "PercUtils.h"
#import "CZPicker.h"
#import "HttpUtil.h"

#define BUTTON_LINE     50.0f
#define BUTTON_MARGIN   10.0f
#define CENTER_MARGIN   40.0f

#define BUTTON_ID_DELTA  100

@interface ToothCardListViewController ()<NSFetchedResultsControllerDelegate, SWTableViewCellDelegate, CZPickerViewDelegate, CZPickerViewDataSource>
@property (nonatomic, strong) NSDictionary *currentPatient;

@property (nonatomic, strong) IBActionSheet *toothCategoryActionSheet;

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (nonatomic, strong) NSArray *leftTopCombineArray;
@property (nonatomic, strong) NSArray *leftTopArray;
@property (nonatomic, strong) NSArray *rightTopCombineArray;
@property (nonatomic, strong) NSArray *rightTopArray;
@property (nonatomic, strong) NSArray *leftBottomCombineArray;
@property (nonatomic, strong) NSArray *leftBottomArray;
@property (nonatomic, strong) NSArray *rightBottomCombineArray;
@property (nonatomic, strong) NSArray *rightBottomArray;

@end

@implementation ToothCardListViewController
@synthesize tooths, diagnoses, nameLabel, illnessImg;
-(id) initWithPatient:(NSDictionary*)patient {
    self = [super initWithNibName:@"ToothCardListViewController" bundle:[NSBundle mainBundle]];
    if(self)    {
        self.currentPatient = patient;
        diagnoses = [DataBase getDBArray:@"diagnosis" withKey:@"" andValue:@""];
    }
    return self;
}

- (void) viewDidAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(copyToothCard:) name:@"toothcardCopied" object:nil];
    [self fetchTableData];
    [self.tableView reloadData];
}

- (NSMutableDictionary*) copyToothCard:(NSDictionary*) card
{
    NSMutableDictionary* copied = [NSMutableDictionary dictionaryWithDictionary:card];
    copied[@"id"]=GUIDString();
    copied[@"immutable"] = @"false";
    
    [[ToothManager shared] updateToothMap:copied success:^{
        [self fetchTableData];
        [self.tableView reloadData];
    } failure:^{
    }];
    
    return copied;
}

- (void) viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Зубные карты";
    
    UIImage *image2 = [UIImage imageNamed:@"back-button"];
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 addTarget:self action:@selector(onBack:)forControlEvents:UIControlEventTouchUpInside];
    [button2 setBackgroundImage:image2 forState:UIControlStateNormal];
    button2.frame = CGRectMake(0, 0, 11, 18);
    UIBarButtonItem *barButton2 = [[UIBarButtonItem alloc] initWithCustomView:button2];
    self.navigationItem.leftBarButtonItem = barButton2;
    
    UIImage *image = [UIImage imageNamed:@"patient-add-icon"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(onAdd:)forControlEvents:UIControlEventTouchUpInside];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, 18, 18);
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = barButton;
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ToothViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ToothViewCell class])];
    
    [self fetchTableData];
    
    self.tableView.opaque = NO;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.backgroundView  = nil;
    
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]&&getNeedName())
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    
    if(!getNeedName())
    {
        CGRect tempRect;
        for(UIView *sub in [[self view] subviews])
        {
            tempRect = [sub frame];
            tempRect.origin.y += 20.0f; //Height of status bar
            [sub setFrame:tempRect];
        }
        
        self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, self.tableView.frame.size.width, self.tableView.frame.size.height -20.0f);
    }
    
    if(((NSArray*)self.currentPatient[@"illness"]).count>0)
    {
        
        NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
        attachment.image =  [PercUtils imageWithImage:[UIImage imageNamed:@"patient-illness-icon.png"] scaledToSize:CGSizeMake(15, 15)];
        attachment.bounds = CGRectMake(0, -3, 18, 18);
        NSMutableAttributedString *attachmentString = [[NSAttributedString attributedStringWithAttachment:attachment] mutableCopy];
        
        NSMutableAttributedString *myString= [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"  %@ %@ %@", self.currentPatient[@"lastname"], self.currentPatient[@"firstname"], self.currentPatient[@"middlename"]]];
        [attachmentString appendAttributedString:myString];
        
        nameLabel.attributedText = attachmentString;
    }
    else
    {
        nameLabel.text = [NSString stringWithFormat:@"  %@ %@ %@", self.currentPatient[@"lastname"], self.currentPatient[@"firstname"], self.currentPatient[@"middlename"]];
    }
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = getColorSchemeStatus();
    }
    
    
    self.extendedLayoutIncludesOpaqueBars = YES;
    self.automaticallyAdjustsScrollViewInsets = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)onAdd:(id)sender
{
    [self.view endEditing:YES];
    self.toothCategoryActionSheet = [[IBActionSheet alloc] initWithTitle:NSLocalizedString(@"Выберите зубную карту", nil)
                                                        delegate:self
                                               cancelButtonTitle:NSLocalizedString(@"Отмена", nil)
                                          destructiveButtonTitle:nil
                                          otherButtonTitlesArray:@[@"Взрослая", NSLocalizedString(@"Детская", nil),@"Комбинированная"]];
    self.toothCategoryActionSheet.tag = 1;
    [self.toothCategoryActionSheet showInView:self.view];
}

- (void)actionSheet:(IBActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(actionSheet.tag == 1 && buttonIndex != 3)    {
        ToothMapMode toothMode;
        if(buttonIndex == 0)    {
            toothMode = kToothMapModeHight;
        } else if(buttonIndex == 1) {
            toothMode = kToothMapModeLow;
        }
        else
            toothMode = kToothMapModeCombine;
        
        ToothCardViewController *toothCardVC = [[ToothCardViewController alloc] initWithToothMode:toothMode withPatient:self.currentPatient];
        toothCardVC.isCreationMode = YES;
        [self.navigationController pushViewController:toothCardVC animated:YES];
    }
}

-(void)fetchTableData
{
    tooths = [NSMutableArray arrayWithArray:[DataBase getDBArray:@"toothmap" withKey:@"patientid" andValue:self.currentPatient[@"patientid"]]];
}


#pragma mark -
#pragma mark Option Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    //NSInteger count = [self.fetchedResultsController.fetchedObjects count];
    return tooths.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section    {
    return [UIView new];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section    {
    
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section   {
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section   {
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *toothList = tooths[indexPath.row];
    ToothMapMode toothMode = (ToothMapMode)[toothList[@"type"] integerValue];
    if(toothMode == kToothMapModeCombine)
    {
        return 175.0;
    }
    else
    {
        return 125.0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *toothList = tooths[indexPath.row];//[self.fetchedResultsController objectAtIndexPath:indexPath];
    
    ToothViewCell *toothCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ToothViewCell class])];
    //toothCell.delegate = self;

    toothCell.dateLabel.text = toothList[@"createat"];
    
    ToothMapMode toothMode = (ToothMapMode)[toothList[@"type"] integerValue];
    
    UIView *contentToothView;
    if(toothMode == kToothMapModeHight)
    {
        self.leftTopArray = @[@(18),@(17),@(16),@(15),@(14),@(13),@(12),@(11)];
        self.rightTopArray = @[@(21),@(22),@(23),@(24),@(25),@(26),@(27),@(28)];
        self.leftBottomArray = @[@(48),@(47),@(46),@(45),@(44),@(43),@(42),@(41)];
        self.rightBottomArray = @[@(31),@(32),@(33),@(34),@(35),@(36),@(37),@(38)];
        self.leftTopCombineArray = nil;
        self.rightTopCombineArray = nil;
        self.leftBottomCombineArray = nil;
        self.rightBottomCombineArray = nil;
        contentToothView = [self generateView:12.0 buttonMargin:5.0 centerMargin:10.0 withSmallText:YES];
        contentToothView.top = 35;
    }
    else if (toothMode == kToothMapModeLow)
    {
        self.leftTopArray = @[@(55),@(54),@(53),@(52),@(51)];
        self.rightTopArray = @[@(61),@(62),@(63),@(64),@(65)];
        self.leftBottomArray = @[@(85),@(84),@(83),@(82),@(81)];
        self.rightBottomArray = @[@(71),@(72),@(73),@(74),@(75)];
        self.leftTopCombineArray = nil;
        self.rightTopCombineArray = nil;
        self.leftBottomCombineArray = nil;
        self.rightBottomCombineArray = nil;
        contentToothView = [self generateView:12.0 buttonMargin:5.0 centerMargin:10.0 withSmallText:YES];
        contentToothView.top = 35;
    }
    else
    {
        self.leftTopArray = @[@(18),@(17),@(16),@(15),@(14),@(13),@(12),@(11)];
        self.rightTopArray = @[@(21),@(22),@(23),@(24),@(25),@(26),@(27),@(28)];
        self.leftBottomArray = @[@(48),@(47),@(46),@(45),@(44),@(43),@(42),@(41)];
        self.rightBottomArray = @[@(31),@(32),@(33),@(34),@(35),@(36),@(37),@(38)];
        
        self.leftTopCombineArray = @[@(55),@(54),@(53),@(52),@(51)];
        self.rightTopCombineArray = @[@(61),@(62),@(63),@(64),@(65)];
        self.leftBottomCombineArray = @[@(85),@(84),@(83),@(82),@(81)];
        self.rightBottomCombineArray = @[@(71),@(72),@(73),@(74),@(75)];
        contentToothView = [self generateView:10.0 buttonMargin:5.0 centerMargin:8.0 withSmallText:YES];
        contentToothView.top = 25;
    }
    
    for(UIView *v in toothCell.toothBoxView.subviews)
    {
        if(![v isKindOfClass:[UILabel class]])
            [v removeFromSuperview];
    }

    //NSLog(@"%f, %lu", [[UIScreen mainScreen] bounds].size.width/2, (((10 * self.leftTopArray.count) + (self.leftTopArray.count * 5)) * 2 + 10)/2);

    contentToothView.left = (toothMode == kToothMapModeCombine)?[[UIScreen mainScreen] bounds].size.width/2 - (((10 * self.leftTopArray.count) + (self.leftTopArray.count * 5)) * 2 + 8*2)/2:[[UIScreen mainScreen] bounds].size.width/2 - (((12 * self.leftTopArray.count) + (self.leftTopArray.count * 5)) * 2 + 10*2)/2;
    //contentToothView.top = toothCell.frame.size.height/2 - contentToothView.frame.size.height/2;
    //CGFloat height = (buttonMargin + buttonLine) * 2 + centerMargin;

    [toothCell.toothBoxView addSubview:contentToothView];
    
    [self updateToothMap:toothList inView:contentToothView];
    
    toothCell.toothmap = toothList;
    toothCell.delegate = self;
    toothCell.rightUtilityButtons = [self rightButtons: indexPath];
    return toothCell;
}

- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell   {
    return YES;
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    switch (index) {
        case 0: {
            CZPickerView *picker = [[CZPickerView alloc] initWithHeaderTitle:@"Удалить?" cancelButtonTitle:@"Отмена" confirmButtonTitle:@"Да"];
            picker.headerBackgroundColor = getColorScheme();
            picker.tag = [self.tableView indexPathForCell:cell].row;
            picker.delegate = self;
            picker.dataSource = self;
            picker.allowMultipleSelection = YES;
            [picker show];
        }

            break;
        case 1: {
            NSIndexPath *cellIndexPath = [self.tableView indexPathForCell:cell];
            NSDictionary* toothList = tooths[cellIndexPath.row];
            [self copyToothCard:toothList];
            [self fetchTableData];
            [self.tableView reloadData];
        }
            
            break;
        default:
            break;
    }
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    //ToothList *toothList = [self.fetchedResultsController objectAtIndexPath:indexPath];

    NSDictionary* toothList = tooths[indexPath.row];
    ToothCardViewController *toothCardVC = [[ToothCardViewController alloc] initShowToothList:toothList withPatient:self.currentPatient];
    //toothCardVC.isCreationMode = YES;
    [self.navigationController pushViewController:toothCardVC animated:YES];
}

-(UIView*) generateView:(CGFloat)buttonLine buttonMargin:(CGFloat)buttonMargin centerMargin:(CGFloat)centerMargin withSmallText:(BOOL)withSmallText {
    CGFloat width = ((buttonLine * self.leftTopArray.count) + (self.leftTopArray.count * buttonMargin)) * 2 + centerMargin;
    CGFloat height;
    
    if(self.leftTopCombineArray == nil&&self.rightTopCombineArray == nil&&self.leftBottomCombineArray == nil&&self.rightBottomCombineArray == nil)
        height = (buttonMargin + buttonLine) * 2 + centerMargin;
    else
        height = (buttonMargin*2 + buttonLine) * 4 + centerMargin;
    
    UIView *toothMapView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    toothMapView.backgroundColor = [UIColor whiteColor];
    
    NSInteger index = 1;
    if(self.leftTopCombineArray == nil&&self.rightTopCombineArray == nil&&self.leftBottomCombineArray == nil&&self.rightBottomCombineArray == nil)
    {
        for(NSNumber *num in self.leftTopArray)
        {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.left = (index * buttonMargin) + ((index-1) * buttonLine);
            button.top = buttonMargin;
            button.width = buttonLine;
            button.height = buttonLine;
            CGFloat left = (index * buttonMargin) + ((index-1) * buttonLine);
            CGFloat top = buttonMargin;
            CGFloat width = buttonLine;

            [button setTitle:[NSString stringWithFormat:@"%d", [num integerValue]] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            if(withSmallText)    {
                button.titleLabel.numberOfLines = 1;
                button.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:5.0];
                button.titleLabel.lineBreakMode = NSLineBreakByClipping;
            }
            button.tag = [num integerValue];
            button.layer.cornerRadius = (buttonLine/2);
            button.layer.borderWidth = 1;
            button.layer.borderColor = [UIColor lightGrayColor].CGColor;

            [toothMapView addSubview:button];
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(left, top-15.0f, width, 15.0f)];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0];
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:withSmallText?5.0:12.0];
            titleLabel.adjustsFontSizeToFitWidth = YES;
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.tag = [num integerValue] + BUTTON_ID_DELTA;
            [toothMapView addSubview:titleLabel];
            
            index++;
        }
        
        index = 1;
        for(NSNumber *num in self.leftBottomArray) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.left = (index * buttonMargin) + ((index-1) * buttonLine);
            button.top = buttonMargin + buttonLine + centerMargin;
            button.width = buttonLine;
            button.height = buttonLine;
            CGFloat left = (index * buttonMargin) + ((index-1) * buttonLine);
            CGFloat top = buttonMargin + buttonLine + centerMargin;
            CGFloat width = buttonLine;

            [button setTitle:[NSString stringWithFormat:@"%d", [num integerValue]] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            if(withSmallText)    {
                button.titleLabel.numberOfLines = 1;
                button.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:5.0];
                button.titleLabel.lineBreakMode = NSLineBreakByClipping;
            }
            button.tag = [num integerValue];
            button.layer.cornerRadius = (buttonLine/2);
            button.layer.borderWidth = 1;
            button.layer.borderColor = [UIColor lightGrayColor].CGColor;

            [toothMapView addSubview:button];
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(left, top+buttonLine, width, 15.0f)];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0];
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:withSmallText?5.0:12.0];
            titleLabel.adjustsFontSizeToFitWidth = YES;
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.tag = [num integerValue] + BUTTON_ID_DELTA;
            [toothMapView addSubview:titleLabel];
            
            index++;
        }
        
        index = 1;
        for(NSNumber *num in [[self.rightTopArray reverseObjectEnumerator] allObjects]) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.left = width - (index * (buttonMargin + buttonLine));
            button.top = buttonMargin;
            button.width = buttonLine;
            button.height = buttonLine;
            CGFloat left = width - (index * (buttonMargin + buttonLine));
            CGFloat top = buttonMargin;
            CGFloat width = buttonLine;

            [button setTitle:[NSString stringWithFormat:@"%d", [num integerValue]] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            if(withSmallText)    {
                button.titleLabel.numberOfLines = 1;
                button.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:5.0];
                button.titleLabel.lineBreakMode = NSLineBreakByClipping;
            }
            button.tag = [num integerValue];
            button.layer.cornerRadius = (buttonLine/2);
            button.layer.borderWidth = 1;
            button.layer.borderColor = [UIColor lightGrayColor].CGColor;

            [toothMapView addSubview:button];
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(left, top-15.0f, width, 15.0f)];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0];
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:withSmallText?5.0:12.0];
            titleLabel.adjustsFontSizeToFitWidth = YES;
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.tag = [num integerValue] + BUTTON_ID_DELTA;
            [toothMapView addSubview:titleLabel];
            
            index++;
        }
        
        index = 1;
        for(NSNumber *num in [[self.rightBottomArray reverseObjectEnumerator] allObjects]) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.left = width - (index * (buttonMargin + buttonLine));
            button.top = buttonMargin + buttonLine + centerMargin;
            button.width = buttonLine;
            button.height = buttonLine;
            CGFloat left = width - (index * (buttonMargin + buttonLine));
            CGFloat top = buttonMargin + buttonLine + centerMargin;;
            CGFloat width = buttonLine;
 
            [button setTitle:[NSString stringWithFormat:@"%d", [num integerValue]] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            if(withSmallText)    {
                button.titleLabel.numberOfLines = 1;
                button.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:5.0];
                button.titleLabel.lineBreakMode = NSLineBreakByClipping;
            }
            NSLog(@"%f %f %f",left, top, width);
            button.tag = [num integerValue];
            button.layer.cornerRadius = (buttonLine/2);
            button.layer.borderWidth = 1;
            button.layer.borderColor = [UIColor lightGrayColor].CGColor;

            [toothMapView addSubview:button];
            NSLog(@"%f %f %f",left, top, width);
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(left, top+buttonLine, width, 15.0f)];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0];
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:withSmallText?5.0:12.0];
            titleLabel.adjustsFontSizeToFitWidth = YES;
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.tag = [num integerValue] + BUTTON_ID_DELTA;
            //titleLabel.text = @"SHEISSE";
            [toothMapView addSubview:titleLabel];
            
            index++;
        }
    }
    else
    {
        index=4;
        for(NSNumber *num in self.leftTopCombineArray)
        {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.left = (index * buttonMargin) + ((index-1) * buttonLine);
            button.top = buttonMargin*2;
            button.width = buttonLine;
            button.height = buttonLine;
            CGFloat left = (index * buttonMargin) + ((index-1) * buttonLine);
            CGFloat top = buttonMargin*2;
            CGFloat width = buttonLine;

            [button setTitle:[NSString stringWithFormat:@"%d", [num integerValue]] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            if(withSmallText)    {
                button.titleLabel.numberOfLines = 1;
                button.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:5.0];
                button.titleLabel.lineBreakMode = NSLineBreakByClipping;
            }
            button.tag = [num integerValue];
            button.layer.cornerRadius = (buttonLine/2);
            button.layer.borderWidth = 1;
            button.layer.borderColor = [UIColor lightGrayColor].CGColor;

            [toothMapView addSubview:button];
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(left, top-10.0f, width, 10.0f)];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0];
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:withSmallText?5.0:10.0];
            titleLabel.adjustsFontSizeToFitWidth = YES;
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.tag = [num integerValue] + BUTTON_ID_DELTA;
            [toothMapView addSubview:titleLabel];
            
            index++;
        }
        
        index = 1;
        
        for(NSNumber *num in self.leftTopArray)
        {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.left = (index * buttonMargin) + ((index-1) * buttonLine);
            button.top = buttonLine+buttonMargin*2*2;
            button.width = buttonLine;
            button.height = buttonLine;
            CGFloat left = (index * buttonMargin) + ((index-1) * buttonLine);
            CGFloat top = buttonLine+buttonMargin*2*2;
            CGFloat width = buttonLine;

            [button setTitle:[NSString stringWithFormat:@"%d", [num integerValue]] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            if(withSmallText)    {
                button.titleLabel.numberOfLines = 1;
                button.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:5.0];
                button.titleLabel.lineBreakMode = NSLineBreakByClipping;
            }
            button.tag = [num integerValue];
            button.layer.cornerRadius = (buttonLine/2);
            button.layer.borderWidth = 1;
            button.layer.borderColor = [UIColor lightGrayColor].CGColor;

            [toothMapView addSubview:button];
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(left, top-10.0f, width, 10.0f)];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0];
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:withSmallText?5.0:10.0];
            titleLabel.adjustsFontSizeToFitWidth = YES;
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.tag = [num integerValue] + BUTTON_ID_DELTA;
            [toothMapView addSubview:titleLabel];
            
            index++;
        }
        
        index = 1;
        
        for(NSNumber *num in self.leftBottomArray) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.left = (index * buttonMargin) + ((index-1) * buttonLine);
            button.top = buttonMargin*2*2 + buttonLine*2 + centerMargin;
            button.width = buttonLine;
            button.height = buttonLine;
            CGFloat left = (index * buttonMargin) + ((index-1) * buttonLine);
            CGFloat top = buttonMargin*2*2 + buttonLine*2 + centerMargin;
            CGFloat width = buttonLine;

            [button setTitle:[NSString stringWithFormat:@"%d", [num integerValue]] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            if(withSmallText)    {
                button.titleLabel.numberOfLines = 1;
                button.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:5.0];
                button.titleLabel.lineBreakMode = NSLineBreakByClipping;
            }
            button.tag = [num integerValue];
            button.layer.cornerRadius = (buttonLine/2);
            button.layer.borderWidth = 1;
            button.layer.borderColor = [UIColor lightGrayColor].CGColor;

            [toothMapView addSubview:button];
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(left, top+buttonLine, width, 10.0f)];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0];
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:withSmallText?5.0:10.0];
            titleLabel.adjustsFontSizeToFitWidth = YES;
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.tag = [num integerValue] + BUTTON_ID_DELTA;
            [toothMapView addSubview:titleLabel];
            
            index++;
        }
        
        index=4;
        
        for(NSNumber *num in self.leftBottomCombineArray) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.left = (index * buttonMargin) + ((index-1) * buttonLine);
            button.top = buttonMargin*3*2 + buttonLine*3 + centerMargin;
            button.width = buttonLine;
            button.height = buttonLine;
            CGFloat left = (index * buttonMargin) + ((index-1) * buttonLine);
            CGFloat top = buttonMargin*3*2 + buttonLine*3 + centerMargin;
            CGFloat width = buttonLine;

            [button setTitle:[NSString stringWithFormat:@"%d", [num integerValue]] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            if(withSmallText)    {
                button.titleLabel.numberOfLines = 1;
                button.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:5.0];
                button.titleLabel.lineBreakMode = NSLineBreakByClipping;
            }
            button.tag = [num integerValue];
            button.layer.cornerRadius = (buttonLine/2);
            button.layer.borderWidth = 1;
            button.layer.borderColor = [UIColor lightGrayColor].CGColor;

            [toothMapView addSubview:button];
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(left, top+buttonLine, width, 10.0f)];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0];
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:withSmallText?5.0:10.0];
            titleLabel.adjustsFontSizeToFitWidth = YES;
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.tag = [num integerValue] + BUTTON_ID_DELTA;
            [toothMapView addSubview:titleLabel];
            
            index++;
        }
        
        index = 4;
        
        for(NSNumber *num in [[self.rightTopCombineArray reverseObjectEnumerator] allObjects])
        {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.left = width - (index * (buttonMargin + buttonLine));
            button.top = buttonMargin*2;
            button.width = buttonLine;
            button.height = buttonLine;
            CGFloat left = width - (index * (buttonMargin + buttonLine));
            CGFloat top = buttonMargin*2;
            CGFloat width = buttonLine;

            [button setTitle:[NSString stringWithFormat:@"%d", [num integerValue]] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            if(withSmallText)    {
                button.titleLabel.numberOfLines = 1;
                button.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:5.0];
                button.titleLabel.lineBreakMode = NSLineBreakByClipping;
            }
            button.tag = [num integerValue];
            button.layer.cornerRadius = (buttonLine/2);
            button.layer.borderWidth = 1;
            button.layer.borderColor = [UIColor lightGrayColor].CGColor;

            [toothMapView addSubview:button];
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(left, top-10.0f, width, 10.0f)];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0];
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:withSmallText?5.0:10.0];
            titleLabel.adjustsFontSizeToFitWidth = YES;
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.tag = [num integerValue] + BUTTON_ID_DELTA;
            [toothMapView addSubview:titleLabel];
            
            index++;
        }
        
        index = 1;
        
        for(NSNumber *num in [[self.rightTopArray reverseObjectEnumerator] allObjects])
        {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.left = width - (index * (buttonMargin + buttonLine));
            button.top = buttonLine+buttonMargin*2*2;
            button.width = buttonLine;
            button.height = buttonLine;
            CGFloat left = width - (index * (buttonMargin + buttonLine));
            CGFloat top = buttonLine+buttonMargin*2*2;
            CGFloat width = buttonLine;

            [button setTitle:[NSString stringWithFormat:@"%d", [num integerValue]] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            if(withSmallText)    {
                button.titleLabel.numberOfLines = 1;
                button.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:5.0];
                button.titleLabel.lineBreakMode = NSLineBreakByClipping;
            }
            button.tag = [num integerValue];
            button.layer.cornerRadius = (buttonLine/2);
            button.layer.borderWidth = 1;
            button.layer.borderColor = [UIColor lightGrayColor].CGColor;

            [toothMapView addSubview:button];
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(left, top-10.0f, width, 10.0f)];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0];
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:withSmallText?5.0:10.0];
            titleLabel.adjustsFontSizeToFitWidth = YES;
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.tag = [num integerValue] + BUTTON_ID_DELTA;
            [toothMapView addSubview:titleLabel];
            
            index++;
        }
        
        index = 1;
        
        for(NSNumber *num in [[self.rightBottomArray reverseObjectEnumerator] allObjects]) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.left = width - (index * (buttonMargin + buttonLine));
            button.top = buttonMargin*2*2 + buttonLine*2 + centerMargin;
            button.width = buttonLine;
            button.height = buttonLine;
            CGFloat left = width - (index * (buttonMargin + buttonLine));
            CGFloat top = buttonMargin*2*2 + buttonLine*2 + centerMargin;
            CGFloat width = buttonLine;

            [button setTitle:[NSString stringWithFormat:@"%d", [num integerValue]] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            if(withSmallText)    {
                button.titleLabel.numberOfLines = 1;
                button.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:5.0];
                button.titleLabel.lineBreakMode = NSLineBreakByClipping;
            }
            NSLog(@"%f %f %f",left, top, width);
            button.tag = [num integerValue];
            button.layer.cornerRadius = (buttonLine/2);
            button.layer.borderWidth = 1;
            button.layer.borderColor = [UIColor lightGrayColor].CGColor;

            [toothMapView addSubview:button];
            NSLog(@"%f %f %f",left, top, width);
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(left, top+buttonLine, width, 10.0f)];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0];
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:withSmallText?5.0:10.0];
            titleLabel.adjustsFontSizeToFitWidth = YES;
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.tag = [num integerValue] + BUTTON_ID_DELTA;
            //titleLabel.text = @"SHEISSE";
            [toothMapView addSubview:titleLabel];
            
            index++;
        }
        
        
        index = 4;
        
        for(NSNumber *num in [[self.rightBottomCombineArray reverseObjectEnumerator] allObjects]) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.left = width - (index * (buttonMargin + buttonLine));
            button.top = buttonMargin*3*2 + buttonLine*3 + centerMargin;
            button.width = buttonLine;
            button.height = buttonLine;
            CGFloat left = width - (index * (buttonMargin + buttonLine));
            CGFloat top = buttonMargin*3*2 + buttonLine*3 + centerMargin;
            CGFloat width = buttonLine;

            [button setTitle:[NSString stringWithFormat:@"%d", [num integerValue]] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            if(withSmallText)    {
                button.titleLabel.numberOfLines = 1;
                button.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:5.0];
                button.titleLabel.lineBreakMode = NSLineBreakByClipping;
            }
            NSLog(@"%f %f %f",left, top, width);
            button.tag = [num integerValue];
            button.layer.cornerRadius = (buttonLine/2);
            button.layer.borderWidth = 1;
            button.layer.borderColor = [UIColor lightGrayColor].CGColor;
            [toothMapView addSubview:button];
            NSLog(@"%f %f %f",left, top, width);
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(left, top+buttonLine, width, 10.0f)];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0];
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:withSmallText?5.0:10.0];
            titleLabel.adjustsFontSizeToFitWidth = YES;
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.tag = [num integerValue] + BUTTON_ID_DELTA;
            //titleLabel.text = @"SHEISSE";
            [toothMapView addSubview:titleLabel];
            
            index++;
        }
    }
    
    UIView *vertLineView = [[UIView alloc] initWithFrame:CGRectMake(width/2, 0, 1, height)];
    vertLineView.backgroundColor = [UIColor blackColor];
    [toothMapView addSubview:vertLineView];
    
    UIView *horizLineView = [[UIView alloc] initWithFrame:CGRectMake(0, height/2, width, 1)];
    horizLineView.backgroundColor = [UIColor grayColor];
    [toothMapView addSubview:horizLineView];
    
    return toothMapView;
}


-(void) updateToothMap:(NSDictionary*)toothList inView:(UIView*)contentView  {
    NSArray *toothMapArray = [toothList[@"tooth_list"] allKeys];//[Tooth MR_findByAttribute:@"tooth_list" withValue:toothList];
    
    if(toothMapArray.count > 0) {
        for(NSString *tooth_index in toothMapArray)
        {
            NSDictionary* tooth = toothList[@"tooth_list"][tooth_index];
            NSInteger tag = [tooth_index integerValue];
            
            UIButton *btn = (UIButton*)[contentView viewWithTag:tag];
            if([tooth[@"diagnosisid"] length] > 0 || [tooth[@"comment"] length] > 0 )
            {
                btn.backgroundColor = getColorSchemeTooth();//[UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0];
                btn.layer.borderColor = getColorSchemeTooth().CGColor;//[UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0].CGColor;
                [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                
                //DentalDiagnosis *dentaDiagnosis = [DentalDiagnosis MR_findFirstByAttribute:@"remoteId" withValue:tooth.dentalDiagnosisId];
                NSDictionary *dentaDiagnosis;
                
                for(NSDictionary* diag in diagnoses)
                {
                    if([diag[@"id"] isEqualToString:tooth[@"diagnosisid"]])
                    {
                        dentaDiagnosis=diag;
                        break;
                    }
                }
                
                UILabel *titleLabel = (UILabel*)[contentView viewWithTag:tag + BUTTON_ID_DELTA];
                titleLabel.text = dentaDiagnosis[@"subtitle"];
            }
        }
    }
}

- (void)customIOS7dialogButtonTouchUpInside: (CustomIOS7AlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    NSLog(@"Button at position %ld is clicked on alertView %ld.", (long)buttonIndex, (long)[alertView tag]);
    switch(buttonIndex)
    {
        case 0:
        {
            NSDictionary* toothList = tooths[[alertView tag]];
            [DataBase killDB:@"toothmap" withKey:@"id" andValue:toothList[@"id"]];
            break;
        }
        case 1:
        {
            NSDictionary* toothList = tooths[[alertView tag]];
            [self copyToothCard:toothList];
            
            break;
        }
    }
}

- (NSArray *)rightButtons:(NSIndexPath*) atIndex
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:231.0f/255.0f green:76.0f/255.0f blue:60.0f/255.0f alpha:1.0f] icon:[PercUtils imageWithImage:[UIImage imageNamed:@"delete_white.png"] scaledToSize:CGSizeMake(20, 25)]];
    
    if((tooths.count-1 == atIndex.row))
    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:20.0f/255.0f green:162.0f/255.0f blue:243.0f/255.0f alpha:1.0f] icon:[PercUtils imageWithImage:[UIImage imageNamed:@"copy_white.png"] scaledToSize:CGSizeMake(20, 25)]];
    
    
    return rightUtilityButtons;
}

- (int) getSWButtonsOffset:(SWTableViewCell *)cell
{
    return 15;
}

- (NSString *)czpickerView:(CZPickerView *)pickerView titleForRow:(NSInteger)row
{
    return @"Да";
    /*DentalDiagnosis *denta = [DentalDiagnosis MR_findAllSortedBy:@"remoteId" ascending:YES][row];
     return denta.name;*/
}

- (NSInteger)numberOfRowsInPickerView:(CZPickerView *)pickerView
{
    return 0;
}

- (void)czpickerView:(CZPickerView *)pickerView didConfirmWithItemAtRow:(NSInteger)row{
    
    NSDictionary *toothcard = tooths[pickerView.tag];//[self.fetchedResultsController objectAtIndexPath:cellIndexPath];
    [DataBase killDB:@"toothmap" withKey:@"id" andValue:toothcard[@"id"]];
    [self fetchTableData];
    [self.tableView reloadData];
}

- (void)czpickerView:(CZPickerView *)pickerView didConfirmWithItemsAtRows:(NSArray *)rows   {
 
    NSDictionary *toothcard = tooths[pickerView.tag];//[self.fetchedResultsController objectAtIndexPath:cellIndexPath];
    [DataBase killDB:@"toothmap" withKey:@"id" andValue:toothcard[@"id"]];
    [self fetchTableData];
    [self.tableView reloadData];
}

- (void)czpickerViewDidClickCancelButton:(CZPickerView *)pickerView{
    NSLog(@"Canceled.");
}


@end
