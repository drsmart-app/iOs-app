//
//  ToothCardViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 04.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "ToothCardViewController.h"
#import "UIView+Helpers.h"
#import "UIView+GestureBlocks.h"

#import <MagicalRecord/MagicalRecord.h>
#import "JCTagListView.h"
#import "CZPicker.h"
#import <JGProgressHUD/JGProgressHUD.h>

#import "DentalDiagnosis.h"
#import "AdditionDentalDiagnosis.h"
#import "ToothList.h"
#import "Tooth.h"
#import "Patient.h"
#import "ToothManager.h"
#import "DataBase.h"
#import "PercUtils.h"
#import "DiagnosisAddGroupViewController.h"
#import "DiagnosisManager.h"
#import "HttpUtil.h"


#define BUTTON_LINE     50.0f
#define BUTTON_MARGIN   10.0f
#define CENTER_MARGIN   40.0f

#define BUTTON_ID_DELTA  100

@interface ToothCardViewController ()<CZPickerViewDataSource, CZPickerViewDelegate>
@property (nonatomic) ToothMapMode toothMode;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (nonatomic, strong) NSArray *leftTopCombineArray;
@property (nonatomic, strong) NSArray *leftTopArray;
@property (nonatomic, strong) NSArray *rightTopCombineArray;
@property (nonatomic, strong) NSArray *rightTopArray;
@property (nonatomic, strong) NSArray *leftBottomCombineArray;
@property (nonatomic, strong) NSArray *leftBottomArray;
@property (nonatomic, strong) NSArray *rightBottomCombineArray;
@property (nonatomic, strong) NSArray *rightBottomArray;
@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UIView *contentViewSmallZoom;
@property (weak, nonatomic) IBOutlet UIButton *buttonZoom;
@property (nonatomic) BOOL cardCopied;

@property (weak, nonatomic) IBOutlet UITextField *mainDiagnosisTextView;
@property (nonatomic, weak) IBOutlet JCTagListView *tagListView;
@property (weak, nonatomic) IBOutlet UITextView *commentTextView;
@property (weak, nonatomic) IBOutlet UITextView *commonCommentTextView;

@property (weak, nonatomic) IBOutlet UIView *toothInfoCard;
@property (weak, nonatomic) IBOutlet UIView *toothCommonInfoCard;

@property (nonatomic, strong) NSMutableDictionary *currentToothList;

@property (nonatomic, strong) UITextView *textView;
@property (nonatomic, strong) UITextView *commonTextView;
@property (nonatomic) BOOL keyboardIsShown;
@property (nonatomic, strong) NSDictionary* currentPatient;
@end

@implementation ToothCardViewController {
    NSInteger lastSelectedTag;
    NSString* lastSelectedDentaDiagnosis;
    NSInteger lastSelectedAdditionDentaDiagnosis;
    
    BOOL isPreviewToothMode;
    BOOL isViewMode;
    BOOL isReadOnly;
    BOOL need_forward;
    
}

@synthesize diagnoses, edit_btn, save_btn, isCreationMode, callMenuBtn, nameLabel, illnessImg;

-(id) initWithToothMode:(ToothMapMode)mode withPatient:(NSDictionary*)patient  {
    self = [super initWithNibName:@"ToothCardViewController" bundle:[NSBundle mainBundle]];
    if(self)
    {
        self.toothMode = mode;
        self.currentToothList = [[NSMutableDictionary alloc] init];
        self.currentToothList[@"id"] = GUIDString();
        NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
        [dateformate setDateFormat:@"dd.MM.yyyy"];
        self.currentToothList[@"createat"] = [dateformate stringFromDate:[NSDate date]];
        self.currentToothList[@"type"] = @(self.toothMode);
        self.currentToothList[@"patientid"] = patient[@"patientid"];
        self.currentToothList[@"tooth_list"] = [[NSMutableDictionary alloc] init];
        self.currentToothList[@"immutable"] = @"true";
        diagnoses = [DataBase getDBArray:@"diagnosis" withKey:@"" andValue:@""];
        isReadOnly = false;
        isCreationMode = false;
        
        isViewMode = NO;
        self.currentPatient = patient;
        need_forward=NO;
        self.cardCopied = NO;
    }
    return self;
}

- (BOOL) checkWideScreen
{
    return [UIScreen mainScreen].bounds.size.width>321;
}

-(id) initShowToothList:(NSDictionary*)toothList withPatient:(NSDictionary*)patient  {
    self = [super initWithNibName:@"ToothCardViewController" bundle:[NSBundle mainBundle]];
    if(self)    {
        self.toothMode = [toothList[@"type"] integerValue];
        self.currentToothList = [NSMutableDictionary dictionaryWithDictionary:toothList];
        diagnoses = [DataBase getDBArray:@"diagnosis" withKey:@"" andValue:@""];
        if([self.currentToothList[@"immutable"] isEqualToString:@"true"])
            isReadOnly = false;
        else
            isReadOnly = false;
        isViewMode = YES;
        isCreationMode = false;
        self.currentPatient = patient;
        need_forward=NO;
        self.cardCopied = NO;
    }
    return self;
}

-(id) initShowToothListForce:(NSDictionary*)toothList withPatient:(NSDictionary*)patient  {
    self = [super initWithNibName:@"ToothCardViewController" bundle:[NSBundle mainBundle]];
    if(self)    {
        self.toothMode = [toothList[@"type"] integerValue];
        self.currentToothList = [NSMutableDictionary dictionaryWithDictionary:toothList];
        diagnoses = [DataBase getDBArray:@"diagnosis" withKey:@"" andValue:@""];
        if([self.currentToothList[@"immutable"] isEqualToString:@"true"])
            isReadOnly = false;
        else
            isReadOnly = false;
        isViewMode = YES;
        isCreationMode = false;
        self.currentPatient = patient;
        need_forward=YES;
        self.cardCopied = NO;
    }
    return self;
}

- (void)viewDidLoad {
    
    self.commonCommentTextView.delegate = self;

    [super viewDidLoad];
    
    UIImage *image2 = [UIImage imageNamed:@"back-button"];
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 addTarget:self action:@selector(onBack:)forControlEvents:UIControlEventTouchUpInside];
    [button2 setBackgroundImage:image2 forState:UIControlStateNormal];
    button2.frame = CGRectMake(0, 0, 11, 18);
    UIBarButtonItem *barButton2 = [[UIBarButtonItem alloc] initWithCustomView:button2];
    self.navigationItem.leftBarButtonItem = barButton2;
    
    UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button1 addTarget:self action:@selector(onSave:)forControlEvents:UIControlEventTouchUpInside];
    [button1 setTitle:@"Сохранить" forState:UIControlStateNormal];
    [button1 setTitle:@"Сохранить" forState:UIControlStateSelected];
    button1.frame = CGRectMake(0, 0, 65, 18);
    button1.titleLabel.font = [UIFont systemFontOfSize:12];
    button1.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    button1.contentVerticalAlignment = UIControlContentVerticalAlignmentBottom;
    save_btn = [[UIBarButtonItem alloc] initWithCustomView:button1];
    //self.navigationItem.rightBarButtonItems = @[edit_btn];

    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(onZoom:)forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"Изменить" forState:UIControlStateNormal];
    [button setTitle:@"Изменить" forState:UIControlStateSelected];
    button.frame = CGRectMake(0, 0, 60, 18);
    button.titleLabel.font = [UIFont systemFontOfSize:12];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    button.contentVerticalAlignment = UIControlContentVerticalAlignmentBottom;
    edit_btn = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItems = @[edit_btn];
    
    isPreviewToothMode = YES;
    
    lastSelectedTag = -1;
    lastSelectedDentaDiagnosis = nil;
    lastSelectedAdditionDentaDiagnosis = -1;
    
    [self setupTooth];
    
    self.tagListView.canSeletedTags = YES;
    self.tagListView.tagColor = [UIColor darkGrayColor];
    self.tagListView.tagCornerRadius = 5.0f;
    self.tagListView.layer.borderWidth = 1;
    self.tagListView.layer.borderColor = [UIColor colorWithRed:72.0/255.0 green:100.0/255.0 blue:124.0/255.0 alpha:1.0].CGColor;
    if(!isReadOnly)
    {
        [self.tagListView initialiseTapHandler:^(UIGestureRecognizer *sender) {
            CZPickerView *picker = [[CZPickerView alloc] initWithHeaderTitle:@"Основной диагноз" cancelButtonTitle:@"Отмена" confirmButtonTitle:@"Готово"];
            picker.headerBackgroundColor = getColorScheme();//[UIColor colorWithRed:72.0/255.0 green:100.0/255.0 blue:124.0/255.0 alpha:1.0];
            picker.tag = 1;
            picker.delegate = self;
            picker.dataSource = self;
            picker.allowMultipleSelection = YES;
            [picker show];
        } forTaps:1];
    }
    
    [self updateToothMap:YES];
    
    self.commentTextView.layer.borderWidth = 1;
    self.commentTextView.layer.borderColor = [UIColor colorWithWhite:0.0f alpha:0.5f].CGColor;//[UIColor colorWithRed:72.0/255.0 green:100.0/255.0 blue:124.0/255.0 alpha:1.0].CGColor;
    self.commonCommentTextView.layer.borderWidth = 1;
    self.commonCommentTextView.layer.borderColor = [UIColor colorWithWhite:0.0f alpha:0.5f].CGColor;//[UIColor colorWithRed:72.0/255.0 green:100.0/255.0 blue:124.0/255.0 alpha:1.0].CGColor;
    
    //self.mainDiagnosisTextView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    self.textView = [[UITextView alloc] initWithFrame:CGRectMake(0, 64, self.view.width, self.view.height - 50)];
    self.textView.textColor = [UIColor blackColor];
    
    self.commonTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, 64, self.view.width, self.view.height - 50)];
    self.commonTextView.textColor = [UIColor blackColor];
    self.commonCommentTextView.text = self.currentToothList[@"comment"];
    NSLog(@"==>%@", self.currentToothList[@"comment"]);
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]&&getNeedName())
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    if(!getNeedName())
    {
        CGRect tempRect;
        for(UIView *sub in [[self view] subviews])
        {
            tempRect = [sub frame];
            tempRect.origin.y += 20.0f; //Height of status bar
            [sub setFrame:tempRect];
        }
    }
    
    self.title = @"Зубная карта";
    
    if(((NSArray*)self.currentPatient[@"illness"]).count>0)
    {
        
        NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
        attachment.image =  [PercUtils imageWithImage:[UIImage imageNamed:@"patient-illness-icon.png"] scaledToSize:CGSizeMake(15, 15)];
        attachment.bounds = CGRectMake(0, -3, 18, 18);
        NSMutableAttributedString *attachmentString = [[NSAttributedString attributedStringWithAttachment:attachment] mutableCopy];
        
        NSMutableAttributedString *myString= [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"  %@ %@ %@", self.currentPatient[@"lastname"], self.currentPatient[@"firstname"], self.currentPatient[@"middlename"]]];
        [attachmentString appendAttributedString:myString];
        
        nameLabel.attributedText = attachmentString;
    }
    else
    {
        nameLabel.text = [NSString stringWithFormat:@"  %@ %@ %@", self.currentPatient[@"lastname"], self.currentPatient[@"firstname"], self.currentPatient[@"middlename"]];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.scrollView addGestureRecognizer:gestureRecognizer];
    gestureRecognizer.cancelsTouchesInView = NO;  // this prevents the gesture recognizers to 'block' touches
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = getColorSchemeStatus();
    }
    
    if(need_forward)
    {
        [self onZoom:nil];
    }
    
    
    self.extendedLayoutIncludesOpaqueBars = YES;
    self.automaticallyAdjustsScrollViewInsets = NO;
}

- (void)hideKeyboard
{
    [self.commonTextView resignFirstResponder];
    [self.commentTextView resignFirstResponder];
    [self.commonCommentTextView resignFirstResponder];
}

- (void) viewDidAppear:(BOOL)animated
{
    if(isCreationMode)
    {
        [self onZoom:nil];
    }
    diagnoses = [DataBase getDBArray:@"diagnosis" withKey:@"" andValue:@""];
}

-(void)onBack:(id)sender
{
    if(isPreviewToothMode)
    {
        if(!isViewMode)
        {
            //[self.currentToothList MR_deleteEntity];
            //[[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        isPreviewToothMode = YES;
        [self.contentView removeFromSuperview];
        
        int toothSize=12;
        int button_margin=5;
        int centerMargin = 10;
        
        if([self checkWideScreen])
        {
            toothSize=14;
            button_margin=6;
            centerMargin = 11;
        }
        
        self.contentView = [self generateView:toothSize buttonMargin:button_margin centerMargin:centerMargin withSmallText:YES];
        
        self.scrollView.contentSize = CGSizeMake(self.contentView.frame.size.width, self.contentView.frame.size.height);
        [self.scrollView addSubview:self.contentView];
        self.contentView.left = [[UIScreen mainScreen] bounds].size.width/2 - (((toothSize * self.leftTopArray.count) + (self.leftTopArray.count * button_margin)) * 2 + centerMargin)/2;
        self.contentView.top = 60.0f;
        
        [self updateToothMap:YES];
        
        self.buttonZoom.hidden = NO;
        self.toothCommonInfoCard.hidden = NO;
        self.toothInfoCard.hidden = YES;
        
        self.navigationItem.rightBarButtonItems = @[edit_btn];
        
    }
}

-(void)onSave:(id)sender
{
    if(lastSelectedTag > 0) {
        [self saveToothInfoCardByIndex:lastSelectedTag];
    }
    
    if(!isViewMode) {
        JGProgressHUD *HUD = [JGProgressHUD progressHUDWithStyle:JGProgressHUDStyleDark];
        HUD.textLabel.text = @"Сохранение";
        [HUD showInView:self.view];
        [[ToothManager shared] createToothMap:self.currentToothList success:^{
            [HUD dismissAnimated:YES];
            self.navigationItem.rightBarButtonItems = @[edit_btn];
            [self.navigationController popViewControllerAnimated:YES];
        } failure:^{
            [HUD dismissAnimated:YES];
        }];
    } else  {
        JGProgressHUD *HUD = [JGProgressHUD progressHUDWithStyle:JGProgressHUDStyleDark];
        HUD.textLabel.text = @"Сохранение";
        [HUD showInView:self.view];
        if(need_forward && !self.cardCopied)
        {
            self.cardCopied = YES;
            self.currentToothList = [self copyToothCard:self.currentToothList];
            
        }
        [[ToothManager shared] updateToothMap:self.currentToothList success:^{
            [HUD dismissAnimated:YES];
            self.navigationItem.rightBarButtonItems = @[edit_btn];
            [self.navigationController popViewControllerAnimated:YES];
        } failure:^{
            [HUD dismissAnimated:YES];
        }];
    }
    
}

- (IBAction)onZoom:(UIButton*)sender {
    isPreviewToothMode = NO;
    
    [self.contentView removeFromSuperview];
    self.buttonZoom.hidden = YES;
    
    if(self.toothMode == kToothMapModeCombine)
        self.contentView = [self generateView:30.0 buttonMargin:6.0 centerMargin:12.0 withSmallText:NO];
    else
        self.contentView = [self generateView:50.0 buttonMargin:10.0 centerMargin:20.0 withSmallText:NO];
    
    self.scrollView.contentSize = CGSizeMake(self.contentView.frame.size.width, self.scrollView.frame.size.height);
    self.contentView.frame = CGRectOffset(self.contentView.frame, 0, self.scrollView.frame.size.height/2-self.contentView.frame.size.height/2);
    [self.scrollView addSubview:self.contentView];
    //self.scrollView.contentOffset = CGPointMake(self.scrollView.frame.size.width/2, 0);
    CGFloat newContentOffsetX = (self.scrollView.contentSize.width - self.scrollView.frame.size.width) / 2;
    self.scrollView.contentOffset = CGPointMake(newContentOffsetX, 0);
    
    [self updateToothMap:NO];
    
    self.toothCommonInfoCard.hidden = YES;
    self.navigationItem.rightBarButtonItems = @[save_btn];
    
    self.commentTextView.editable = YES;
    self.callMenuBtn.enabled = YES;
}

- (IBAction)onZoomPreview:(UIButton*)sender {
    isPreviewToothMode = NO;
    
    [self.contentView removeFromSuperview];
    self.buttonZoom.hidden = YES;
    
    if(self.toothMode == kToothMapModeCombine)
        self.contentView = [self generateView:30.0 buttonMargin:6.0 centerMargin:12.0 withSmallText:NO];
    else
        self.contentView = [self generateView:50.0 buttonMargin:10.0 centerMargin:20.0 withSmallText:NO];
    
    self.scrollView.contentSize = CGSizeMake(self.contentView.frame.size.width, self.scrollView.frame.size.height);
    self.contentView.frame = CGRectOffset(self.contentView.frame, 0, self.scrollView.frame.size.height/2-self.contentView.frame.size.height/2);
    [self.scrollView addSubview:self.contentView];
    //self.scrollView.contentOffset = CGPointMake(self.contentView.width/2 - self.scrollView.width/2, -64);
    CGFloat newContentOffsetX = (self.scrollView.contentSize.width - self.scrollView.frame.size.width) / 2;
    self.scrollView.contentOffset = CGPointMake(newContentOffsetX, 0);
    
    [self updateToothMap:NO];
    
    self.toothCommonInfoCard.hidden = YES;
    self.navigationItem.rightBarButtonItems = nil;//@[save_btn];
    
    self.commentTextView.editable = NO;
    self.callMenuBtn.enabled = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

- (IBAction)onMainDiagnosis:(id)sender {
    if(!isReadOnly)
    {
        CZPickerView *picker = [[CZPickerView alloc] initWithHeaderTitle:@"Основной диагноз" cancelButtonTitle:@"Отмена" confirmButtonTitle:@"Готово"];
        picker.headerBackgroundColor = getColorScheme();//[UIColor colorWithRed:72.0/255.0 green:100.0/255.0 blue:124.0/255.0 alpha:1.0];
        picker.tag = 2;
        picker.delegate = self;
        picker.dataSource = self;
        picker.allowMultipleSelection = NO;
        [picker show];
    }
}

- (NSString *)czpickerView:(CZPickerView *)pickerView titleForRow:(NSInteger)row
{
    if(pickerView.tag == 1) {
        if(row == diagnoses.count)
            return @"Добавить диагноз";
        else
            return diagnoses[row][@"title"];
        /*AdditionDentalDiagnosis *addDenta = [AdditionDentalDiagnosis MR_findAllSortedBy:@"remoteId" ascending:YES][row];
        return addDenta.name;*/
    }
    
    if(row == diagnoses.count)
        return @"Добавить диагноз";
    else
        return diagnoses[row][@"title"];
    /*DentalDiagnosis *denta = [DentalDiagnosis MR_findAllSortedBy:@"remoteId" ascending:YES][row];
    return denta.name;*/
}

- (NSInteger)numberOfRowsInPickerView:(CZPickerView *)pickerView
{
    if(pickerView.tag == 1)
    {
        return diagnoses.count+1;
        //return [AdditionDentalDiagnosis MR_findAll].count;
    }

    //return [DentalDiagnosis MR_findAll].count;
    return diagnoses.count+1;
}

- (void)czpickerView:(CZPickerView *)pickerView didConfirmWithItemAtRow:(NSInteger)row{
    
    //DentalDiagnosis *denta = [DentalDiagnosis MR_findAllSortedBy:@"remoteId" ascending:YES][row];
    if(row == diagnoses.count)
    {
        DiagnosisAddGroupViewController *dnt = [DiagnosisAddGroupViewController new];
        dnt.didClickCreateName = ^(NSString *exName, NSString *name)  {
            
            JGProgressHUD *HUD = [JGProgressHUD progressHUDWithStyle:JGProgressHUDStyleDark];
            HUD.textLabel.text = @"Сохранение";
            [HUD showInView:self.view];
            [[DiagnosisManager shared] createDiagnosisByTitle:name subTitle:exName success:^{
                [HUD dismissAnimated:YES];
            } failure:^{
                [HUD dismissAnimated:YES];
            }];
        };
        [self.navigationController pushViewController:dnt animated:YES];
    }
    else
    {
        NSDictionary *denta = diagnoses[row];
        self.mainDiagnosisTextView.text = denta[@"title"];
        lastSelectedDentaDiagnosis = denta[@"id"];

        
        if(lastSelectedTag > 0) {
            [self saveToothInfoCardByIndex:lastSelectedTag];
        }
        
        [self updateToothMap:NO];
    }
}

- (NSMutableDictionary*) copyToothCard:(NSDictionary*) card
{
    NSMutableDictionary* copied = [NSMutableDictionary dictionaryWithDictionary:card];
    copied[@"id"]=GUIDString();
    copied[@"immutable"] = @"false";
    
    [[ToothManager shared] updateToothMap:copied success:^{
        
    } failure:^{
    }];
    
    return copied;
}

- (void)czpickerView:(CZPickerView *)pickerView didConfirmWithItemsAtRows:(NSArray *)rows   {
    [self.tagListView.tags removeAllObjects];
    
    for(NSNumber *n in rows){
        NSInteger row = [n integerValue];
        //AdditionDentalDiagnosis *addDenta = [AdditionDentalDiagnosis MR_findAllSortedBy:@"remoteId" ascending:YES][row];
        NSDictionary* addDenta = diagnoses[row];
        [self.tagListView.tags addObjectsFromArray:@[addDenta[@"title"]]];
    }
    
    [self.tagListView.collectionView reloadData];
}

- (void)czpickerViewDidClickCancelButton:(CZPickerView *)pickerView{
    NSLog(@"Canceled.");
}

-(void) setupTooth  {
    
    int toothSize=12;
    int button_margin=5;
    int centerMargin = 10;
    
    if(self.toothMode == kToothMapModeHight)
    {
        self.leftTopArray = @[@(18),@(17),@(16),@(15),@(14),@(13),@(12),@(11)];
        self.rightTopArray = @[@(21),@(22),@(23),@(24),@(25),@(26),@(27),@(28)];
        self.leftBottomArray = @[@(48),@(47),@(46),@(45),@(44),@(43),@(42),@(41)];
        self.rightBottomArray = @[@(31),@(32),@(33),@(34),@(35),@(36),@(37),@(38)];
        self.leftTopCombineArray = nil;
        self.rightTopCombineArray = nil;
        self.leftBottomCombineArray = nil;
        self.rightBottomCombineArray = nil;
        //self.contentView = [self generateView:12.0 buttonMargin:5.0 centerMargin:10.0 withSmallText:YES];
        
        
        if([self checkWideScreen])
        {
            toothSize=14;
            button_margin=6;
            centerMargin = 11;
        }
        
        self.contentView = [self generateView:toothSize buttonMargin:button_margin centerMargin:centerMargin withSmallText:YES];
        
        /*self.scrollView.contentSize = CGSizeMake(self.contentView.frame.size.width, self.contentView.frame.size.height);
        [self.scrollView addSubview:self.contentView];
        self.contentView.left = [[UIScreen mainScreen] bounds].size.width/2 - (((toothSize * self.leftTopArray.count) + (self.leftTopArray.count * button_margin)) * 2 + centerMargin)/2;*/
    }
    else if (self.toothMode == kToothMapModeLow)
    {
        self.leftTopArray = @[@(55),@(54),@(53),@(52),@(51)];
        self.rightTopArray = @[@(61),@(62),@(63),@(64),@(65)];
        self.leftBottomArray = @[@(85),@(84),@(83),@(82),@(81)];
        self.rightBottomArray = @[@(71),@(72),@(73),@(74),@(75)];
        self.leftTopCombineArray = nil;
        self.rightTopCombineArray = nil;
        self.leftBottomCombineArray = nil;
        self.rightBottomCombineArray = nil;
        //self.contentView = [self generateView:12.0 buttonMargin:5.0 centerMargin:10.0 withSmallText:YES];
        
        if([self checkWideScreen])
        {
            toothSize=14;
            button_margin=6;
            centerMargin = 11;
        }
        
        self.contentView = [self generateView:toothSize buttonMargin:button_margin centerMargin:centerMargin withSmallText:YES];
    }
    else
    {
        self.leftTopArray = @[@(18),@(17),@(16),@(15),@(14),@(13),@(12),@(11)];
        self.rightTopArray = @[@(21),@(22),@(23),@(24),@(25),@(26),@(27),@(28)];
        self.leftBottomArray = @[@(48),@(47),@(46),@(45),@(44),@(43),@(42),@(41)];
        self.rightBottomArray = @[@(31),@(32),@(33),@(34),@(35),@(36),@(37),@(38)];
        
        self.leftTopCombineArray = @[@(55),@(54),@(53),@(52),@(51)];
        self.rightTopCombineArray = @[@(61),@(62),@(63),@(64),@(65)];
        self.leftBottomCombineArray = @[@(85),@(84),@(83),@(82),@(81)];
        self.rightBottomCombineArray = @[@(71),@(72),@(73),@(74),@(75)];
        
        //self.contentView = [self generateView:12.0 buttonMargin:4.0 centerMargin:10.0 withSmallText:YES];
        int button_margin=4;
        
        if([self checkWideScreen])
        {
            toothSize=14;
            button_margin=6;
            centerMargin = 11;
        }
        
        self.contentView = [self generateView:toothSize buttonMargin:button_margin centerMargin:centerMargin withSmallText:YES];
    }
    
    
    
    /*self.scrollView.contentSize = CGSizeMake(self.contentView.frame.size.width, self.contentView.frame.size.height);
    [self.scrollView addSubview:self.contentView];
    self.contentView.left = [[UIScreen mainScreen] bounds].size.width/2 - (((12 * self.leftTopArray.count) + (self.leftTopArray.count * 5)) * 2 + 10)/2;
    self.contentView.top = 60.0f;
    self.scrollView.contentOffset = CGPointMake(-(self.scrollView.width/2 - self.contentView.width/2), -(self.scrollView.height/2 - self.contentView.height/2 - 44.0));*/
    
    self.contentView = [self generateView:toothSize buttonMargin:button_margin centerMargin:centerMargin withSmallText:YES];
    
    self.scrollView.contentSize = CGSizeMake(self.contentView.frame.size.width, self.contentView.frame.size.height);
    [self.scrollView addSubview:self.contentView];
    self.contentView.left = [[UIScreen mainScreen] bounds].size.width/2 - (((toothSize * self.leftTopArray.count) + (self.leftTopArray.count * button_margin)) * 2 + centerMargin)/2;
    self.contentView.top = 60.0f;
    
    if([self.currentToothList[@"tooth_list"] allKeys].count==0)
    {
        for(NSNumber* num in self.leftTopArray)
        {
            self.currentToothList[@"tooth_list"][[num stringValue]] = [NSMutableDictionary dictionaryWithDictionary:@{@"id":GUIDString(),@"indexx":[num stringValue], @"diagnosisid":@"", @"comment":@""}];
        }
        for(NSNumber* num in self.rightTopArray)
        {
            self.currentToothList[@"tooth_list"][[num stringValue]] =[NSMutableDictionary dictionaryWithDictionary:@{@"id":GUIDString(),@"indexx":[num stringValue], @"diagnosisid":@"", @"comment":@""}];
        }
        for(NSNumber* num in self.leftBottomArray)
        {
            self.currentToothList[@"tooth_list"][[num stringValue]] =[NSMutableDictionary dictionaryWithDictionary:@{@"id":GUIDString(),@"indexx":[num stringValue], @"diagnosisid":@"", @"comment":@""}];
        }
        for(NSNumber* num in self.rightBottomArray)
        {
            self.currentToothList[@"tooth_list"][[num stringValue]] =[NSMutableDictionary dictionaryWithDictionary:@{@"id":GUIDString(),@"indexx":[num stringValue], @"diagnosisid":@"", @"comment":@""}];
        }
        for(NSNumber* num in self.leftTopCombineArray)
        {
            self.currentToothList[@"tooth_list"][[num stringValue]] = [NSMutableDictionary dictionaryWithDictionary:@{@"id":GUIDString(),@"indexx":[num stringValue], @"diagnosisid":@"", @"comment":@""}];
        }
        for(NSNumber* num in self.rightTopCombineArray)
        {
            self.currentToothList[@"tooth_list"][[num stringValue]] =[NSMutableDictionary dictionaryWithDictionary:@{@"id":GUIDString(),@"indexx":[num stringValue], @"diagnosisid":@"", @"comment":@""}];
        }
        for(NSNumber* num in self.leftBottomCombineArray)
        {
            self.currentToothList[@"tooth_list"][[num stringValue]] =[NSMutableDictionary dictionaryWithDictionary:@{@"id":GUIDString(),@"indexx":[num stringValue], @"diagnosisid":@"", @"comment":@""}];
        }
        for(NSNumber* num in self.rightBottomCombineArray)
        {
            self.currentToothList[@"tooth_list"][[num stringValue]] =[NSMutableDictionary dictionaryWithDictionary:@{@"id":GUIDString(),@"indexx":[num stringValue], @"diagnosisid":@"", @"comment":@""}];
        }
    }

}

-(UIView*) generateView:(CGFloat)buttonLine buttonMargin:(CGFloat)buttonMargin centerMargin:(CGFloat)centerMargin withSmallText:(BOOL)withSmallText {
    CGFloat width = ((buttonLine * self.leftTopArray.count) + (self.leftTopArray.count * buttonMargin)) * 2 + centerMargin;
    CGFloat height;
    
    if(self.leftTopCombineArray == nil&&self.rightTopCombineArray == nil&&self.leftBottomCombineArray == nil&&self.rightBottomCombineArray == nil)
        height = (buttonMargin + buttonLine) * 2 + centerMargin;
    else
        height = (buttonMargin*2 + buttonLine) * 4 + centerMargin;
    
    UIView *toothMapView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    toothMapView.backgroundColor = [UIColor whiteColor];
    
    NSInteger index = 1;
    if(self.leftTopCombineArray == nil&&self.rightTopCombineArray == nil&&self.leftBottomCombineArray == nil&&self.rightBottomCombineArray == nil)
    {
        for(NSNumber *num in self.leftTopArray)
        {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.left = (index * buttonMargin) + ((index-1) * buttonLine);
            button.top = buttonMargin;
            button.width = buttonLine;
            button.height = buttonLine;
            CGFloat left = (index * buttonMargin) + ((index-1) * buttonLine);
            CGFloat top = buttonMargin;
            CGFloat width = buttonLine;
            CGFloat height = buttonLine;
            [button setTitle:[NSString stringWithFormat:@"%d", [num integerValue]] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            if(withSmallText)    {
                button.titleLabel.numberOfLines = 1;
                button.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:5.0];
                button.titleLabel.lineBreakMode = NSLineBreakByClipping;
            }
            button.tag = [num integerValue];
            button.layer.cornerRadius = (buttonLine/2);
            button.layer.borderWidth = 1;
            button.layer.borderColor = [UIColor lightGrayColor].CGColor;
            [button addTarget:self action:@selector(onTooth:) forControlEvents:UIControlEventTouchUpInside];
            [toothMapView addSubview:button];
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(left, top-15.0f, width, 15.0f)];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0];
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:withSmallText?5.0:12.0];
            titleLabel.adjustsFontSizeToFitWidth = YES;
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.tag = [num integerValue] + BUTTON_ID_DELTA;
            [toothMapView addSubview:titleLabel];
            
            index++;
        }
        
        index = 1;
        for(NSNumber *num in self.leftBottomArray) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.left = (index * buttonMargin) + ((index-1) * buttonLine);
            button.top = buttonMargin + buttonLine + centerMargin;
            button.width = buttonLine;
            button.height = buttonLine;
            CGFloat left = (index * buttonMargin) + ((index-1) * buttonLine);
            CGFloat top = buttonMargin + buttonLine + centerMargin;
            CGFloat width = buttonLine;
            CGFloat height = buttonLine;
            [button setTitle:[NSString stringWithFormat:@"%d", [num integerValue]] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            if(withSmallText)    {
                button.titleLabel.numberOfLines = 1;
                button.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:5.0];
                button.titleLabel.lineBreakMode = NSLineBreakByClipping;
            }
            button.tag = [num integerValue];
            button.layer.cornerRadius = (buttonLine/2);
            button.layer.borderWidth = 1;
            button.layer.borderColor = [UIColor lightGrayColor].CGColor;
            [button addTarget:self action:@selector(onTooth:) forControlEvents:UIControlEventTouchUpInside];
            [toothMapView addSubview:button];
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(left, top+buttonLine, width, 15.0f)];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0];
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:withSmallText?5.0:12.0];
            titleLabel.adjustsFontSizeToFitWidth = YES;
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.tag = [num integerValue] + BUTTON_ID_DELTA;
            [toothMapView addSubview:titleLabel];
            
            index++;
        }
        
        index = 1;
        for(NSNumber *num in [[self.rightTopArray reverseObjectEnumerator] allObjects]) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.left = width - (index * (buttonMargin + buttonLine));
            button.top = buttonMargin;
            button.width = buttonLine;
            button.height = buttonLine;
            CGFloat left = width - (index * (buttonMargin + buttonLine));
            CGFloat top = buttonMargin;
            CGFloat width = buttonLine;
            CGFloat height = buttonLine;
            [button setTitle:[NSString stringWithFormat:@"%d", [num integerValue]] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            if(withSmallText)    {
                button.titleLabel.numberOfLines = 1;
                button.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:5.0];
                button.titleLabel.lineBreakMode = NSLineBreakByClipping;
            }
            button.tag = [num integerValue];
            button.layer.cornerRadius = (buttonLine/2);
            button.layer.borderWidth = 1;
            button.layer.borderColor = [UIColor lightGrayColor].CGColor;
            [button addTarget:self action:@selector(onTooth:) forControlEvents:UIControlEventTouchUpInside];
            [toothMapView addSubview:button];
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(left, top-15.0f, width, 15.0f)];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0];
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:withSmallText?5.0:12.0];
            titleLabel.adjustsFontSizeToFitWidth = YES;
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.tag = [num integerValue] + BUTTON_ID_DELTA;
            [toothMapView addSubview:titleLabel];
            
            index++;
        }
        
        index = 1;
        for(NSNumber *num in [[self.rightBottomArray reverseObjectEnumerator] allObjects]) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.left = width - (index * (buttonMargin + buttonLine));
            button.top = buttonMargin + buttonLine + centerMargin;
            button.width = buttonLine;
            button.height = buttonLine;
            CGFloat left = width - (index * (buttonMargin + buttonLine));
            CGFloat top = buttonMargin + buttonLine + centerMargin;;
            CGFloat width = buttonLine;
            CGFloat height = buttonLine;
            [button setTitle:[NSString stringWithFormat:@"%d", [num integerValue]] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            if(withSmallText)    {
                button.titleLabel.numberOfLines = 1;
                button.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:5.0];
                button.titleLabel.lineBreakMode = NSLineBreakByClipping;
            }
            NSLog(@"%f %f %f",left, top, width);
            button.tag = [num integerValue];
            button.layer.cornerRadius = (buttonLine/2);
            button.layer.borderWidth = 1;
            button.layer.borderColor = [UIColor lightGrayColor].CGColor;
            [button addTarget:self action:@selector(onTooth:) forControlEvents:UIControlEventTouchUpInside];
            [toothMapView addSubview:button];
            NSLog(@"%f %f %f",left, top, width);
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(left, top+buttonLine, width, 15.0f)];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0];
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:withSmallText?5.0:12.0];
            titleLabel.adjustsFontSizeToFitWidth = YES;
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.tag = [num integerValue] + BUTTON_ID_DELTA;
            //titleLabel.text = @"SHEISSE";
            [toothMapView addSubview:titleLabel];
            
            index++;
        }
    }
    else
    {
        index=4;
        for(NSNumber *num in self.leftTopCombineArray)
        {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.left = (index * buttonMargin) + ((index-1) * buttonLine);
            button.top = buttonMargin*2;
            button.width = buttonLine;
            button.height = buttonLine;
            CGFloat left = (index * buttonMargin) + ((index-1) * buttonLine);
            CGFloat top = buttonMargin*2;
            CGFloat width = buttonLine;
            CGFloat height = buttonLine;
            [button setTitle:[NSString stringWithFormat:@"%d", [num integerValue]] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            if(withSmallText)    {
                button.titleLabel.numberOfLines = 1;
                button.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:5.0];
                button.titleLabel.lineBreakMode = NSLineBreakByClipping;
            }
            button.tag = [num integerValue];
            button.layer.cornerRadius = (buttonLine/2);
            button.layer.borderWidth = 1;
            button.layer.borderColor = [UIColor lightGrayColor].CGColor;
            [button addTarget:self action:@selector(onTooth:) forControlEvents:UIControlEventTouchUpInside];
            [toothMapView addSubview:button];
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(left, top-10.0f, width, 10.0f)];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0];
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:withSmallText?5.0:10.0];
            titleLabel.adjustsFontSizeToFitWidth = YES;
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.tag = [num integerValue] + BUTTON_ID_DELTA;
            [toothMapView addSubview:titleLabel];
            
            index++;
        }
        
        index = 1;
        
        for(NSNumber *num in self.leftTopArray)
        {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.left = (index * buttonMargin) + ((index-1) * buttonLine);
            button.top = buttonLine+buttonMargin*2*2;
            button.width = buttonLine;
            button.height = buttonLine;
            CGFloat left = (index * buttonMargin) + ((index-1) * buttonLine);
            CGFloat top = buttonLine+buttonMargin*2*2;
            CGFloat width = buttonLine;
            CGFloat height = buttonLine;
            [button setTitle:[NSString stringWithFormat:@"%d", [num integerValue]] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            if(withSmallText)    {
                button.titleLabel.numberOfLines = 1;
                button.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:5.0];
                button.titleLabel.lineBreakMode = NSLineBreakByClipping;
            }
            button.tag = [num integerValue];
            button.layer.cornerRadius = (buttonLine/2);
            button.layer.borderWidth = 1;
            button.layer.borderColor = [UIColor lightGrayColor].CGColor;
            [button addTarget:self action:@selector(onTooth:) forControlEvents:UIControlEventTouchUpInside];
            [toothMapView addSubview:button];
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(left, top-10.0f, width, 10.0f)];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0];
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:withSmallText?5.0:10.0];
            titleLabel.adjustsFontSizeToFitWidth = YES;
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.tag = [num integerValue] + BUTTON_ID_DELTA;
            [toothMapView addSubview:titleLabel];
            
            index++;
        }
        
        index = 1;
        
        for(NSNumber *num in self.leftBottomArray) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.left = (index * buttonMargin) + ((index-1) * buttonLine);
            button.top = buttonMargin*2*2 + buttonLine*2 + centerMargin;
            button.width = buttonLine;
            button.height = buttonLine;
            CGFloat left = (index * buttonMargin) + ((index-1) * buttonLine);
            CGFloat top = buttonMargin*2*2 + buttonLine*2 + centerMargin;
            CGFloat width = buttonLine;
            CGFloat height = buttonLine;
            [button setTitle:[NSString stringWithFormat:@"%d", [num integerValue]] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            if(withSmallText)    {
                button.titleLabel.numberOfLines = 1;
                button.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:5.0];
                button.titleLabel.lineBreakMode = NSLineBreakByClipping;
            }
            button.tag = [num integerValue];
            button.layer.cornerRadius = (buttonLine/2);
            button.layer.borderWidth = 1;
            button.layer.borderColor = [UIColor lightGrayColor].CGColor;
            [button addTarget:self action:@selector(onTooth:) forControlEvents:UIControlEventTouchUpInside];
            [toothMapView addSubview:button];
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(left, top+buttonLine, width, 10.0f)];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0];
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:withSmallText?5.0:10.0];
            titleLabel.adjustsFontSizeToFitWidth = YES;
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.tag = [num integerValue] + BUTTON_ID_DELTA;
            [toothMapView addSubview:titleLabel];
            
            index++;
        }
        
        index=4;
        
        for(NSNumber *num in self.leftBottomCombineArray) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.left = (index * buttonMargin) + ((index-1) * buttonLine);
            button.top = buttonMargin*3*2 + buttonLine*3 + centerMargin;
            button.width = buttonLine;
            button.height = buttonLine;
            CGFloat left = (index * buttonMargin) + ((index-1) * buttonLine);
            CGFloat top = buttonMargin*3*2 + buttonLine*3 + centerMargin;
            CGFloat width = buttonLine;
            CGFloat height = buttonLine;
            [button setTitle:[NSString stringWithFormat:@"%d", [num integerValue]] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            if(withSmallText)    {
                button.titleLabel.numberOfLines = 1;
                button.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:5.0];
                button.titleLabel.lineBreakMode = NSLineBreakByClipping;
            }
            button.tag = [num integerValue];
            button.layer.cornerRadius = (buttonLine/2);
            button.layer.borderWidth = 1;
            button.layer.borderColor = [UIColor lightGrayColor].CGColor;
            [button addTarget:self action:@selector(onTooth:) forControlEvents:UIControlEventTouchUpInside];
            [toothMapView addSubview:button];
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(left, top+buttonLine, width, 10.0f)];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0];
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:withSmallText?5.0:10.0];
            titleLabel.adjustsFontSizeToFitWidth = YES;
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.tag = [num integerValue] + BUTTON_ID_DELTA;
            [toothMapView addSubview:titleLabel];
            
            index++;
        }
        
        index = 4;
        
        for(NSNumber *num in [[self.rightTopCombineArray reverseObjectEnumerator] allObjects])
        {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.left = width - (index * (buttonMargin + buttonLine));
            button.top = buttonMargin*2;
            button.width = buttonLine;
            button.height = buttonLine;
            CGFloat left = width - (index * (buttonMargin + buttonLine));
            CGFloat top = buttonMargin*2;
            CGFloat width = buttonLine;
            CGFloat height = buttonLine;
            [button setTitle:[NSString stringWithFormat:@"%d", [num integerValue]] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            if(withSmallText)    {
                button.titleLabel.numberOfLines = 1;
                button.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:5.0];
                button.titleLabel.lineBreakMode = NSLineBreakByClipping;
            }
            button.tag = [num integerValue];
            button.layer.cornerRadius = (buttonLine/2);
            button.layer.borderWidth = 1;
            button.layer.borderColor = [UIColor lightGrayColor].CGColor;
            [button addTarget:self action:@selector(onTooth:) forControlEvents:UIControlEventTouchUpInside];
            [toothMapView addSubview:button];
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(left, top-10.0f, width, 10.0f)];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0];
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:withSmallText?5.0:10.0];
            titleLabel.adjustsFontSizeToFitWidth = YES;
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.tag = [num integerValue] + BUTTON_ID_DELTA;
            [toothMapView addSubview:titleLabel];
            
            index++;
        }
        
        index = 1;
        
        for(NSNumber *num in [[self.rightTopArray reverseObjectEnumerator] allObjects])
        {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.left = width - (index * (buttonMargin + buttonLine));
            button.top = buttonLine+buttonMargin*2*2;
            button.width = buttonLine;
            button.height = buttonLine;
            CGFloat left = width - (index * (buttonMargin + buttonLine));
            CGFloat top = buttonLine+buttonMargin*2*2;
            CGFloat width = buttonLine;
            CGFloat height = buttonLine;
            [button setTitle:[NSString stringWithFormat:@"%d", [num integerValue]] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            if(withSmallText)    {
                button.titleLabel.numberOfLines = 1;
                button.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:5.0];
                button.titleLabel.lineBreakMode = NSLineBreakByClipping;
            }
            button.tag = [num integerValue];
            button.layer.cornerRadius = (buttonLine/2);
            button.layer.borderWidth = 1;
            button.layer.borderColor = [UIColor lightGrayColor].CGColor;
            [button addTarget:self action:@selector(onTooth:) forControlEvents:UIControlEventTouchUpInside];
            [toothMapView addSubview:button];
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(left, top-10.0f, width, 10.0f)];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0];
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:withSmallText?5.0:10.0];
            titleLabel.adjustsFontSizeToFitWidth = YES;
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.tag = [num integerValue] + BUTTON_ID_DELTA;
            [toothMapView addSubview:titleLabel];
            
            index++;
        }
        
        index = 1;
        
        for(NSNumber *num in [[self.rightBottomArray reverseObjectEnumerator] allObjects]) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.left = width - (index * (buttonMargin + buttonLine));
            button.top = buttonMargin*2*2 + buttonLine*2 + centerMargin;
            button.width = buttonLine;
            button.height = buttonLine;
            CGFloat left = width - (index * (buttonMargin + buttonLine));
            CGFloat top = buttonMargin*2*2 + buttonLine*2 + centerMargin;
            CGFloat width = buttonLine;
            CGFloat height = buttonLine;
            [button setTitle:[NSString stringWithFormat:@"%d", [num integerValue]] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            if(withSmallText)    {
                button.titleLabel.numberOfLines = 1;
                button.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:5.0];
                button.titleLabel.lineBreakMode = NSLineBreakByClipping;
            }
            NSLog(@"%f %f %f",left, top, width);
            button.tag = [num integerValue];
            button.layer.cornerRadius = (buttonLine/2);
            button.layer.borderWidth = 1;
            button.layer.borderColor = [UIColor lightGrayColor].CGColor;
            [button addTarget:self action:@selector(onTooth:) forControlEvents:UIControlEventTouchUpInside];
            [toothMapView addSubview:button];
            NSLog(@"%f %f %f",left, top, width);
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(left, top+buttonLine, width, 10.0f)];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0];
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:withSmallText?5.0:10.0];
            titleLabel.adjustsFontSizeToFitWidth = YES;
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.tag = [num integerValue] + BUTTON_ID_DELTA;
            //titleLabel.text = @"SHEISSE";
            [toothMapView addSubview:titleLabel];
            
            index++;
        }


        index = 4;
        
        for(NSNumber *num in [[self.rightBottomCombineArray reverseObjectEnumerator] allObjects]) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.left = width - (index * (buttonMargin + buttonLine));
            button.top = buttonMargin*3*2 + buttonLine*3 + centerMargin;
            button.width = buttonLine;
            button.height = buttonLine;
            CGFloat left = width - (index * (buttonMargin + buttonLine));
            CGFloat top = buttonMargin*3*2 + buttonLine*3 + centerMargin;
            CGFloat width = buttonLine;
            CGFloat height = buttonLine;
            [button setTitle:[NSString stringWithFormat:@"%d", [num integerValue]] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            if(withSmallText)    {
                button.titleLabel.numberOfLines = 1;
                button.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:5.0];
                button.titleLabel.lineBreakMode = NSLineBreakByClipping;
            }
            NSLog(@"%f %f %f",left, top, width);
            button.tag = [num integerValue];
            button.layer.cornerRadius = (buttonLine/2);
            button.layer.borderWidth = 1;
            button.layer.borderColor = [UIColor lightGrayColor].CGColor;
            [button addTarget:self action:@selector(onTooth:) forControlEvents:UIControlEventTouchUpInside];
            [toothMapView addSubview:button];
            NSLog(@"%f %f %f",left, top, width);
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(left, top+buttonLine, width, 10.0f)];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0];
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:withSmallText?5.0:10.0];
            titleLabel.adjustsFontSizeToFitWidth = YES;
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.tag = [num integerValue] + BUTTON_ID_DELTA;
            //titleLabel.text = @"SHEISSE";
            [toothMapView addSubview:titleLabel];
            
            index++;
        }
    }
    
    UIView *vertLineView = [[UIView alloc] initWithFrame:CGRectMake(width/2, 0, 1, height)];
    vertLineView.backgroundColor = [UIColor blackColor];
    [toothMapView addSubview:vertLineView];
    
    UIView *horizLineView = [[UIView alloc] initWithFrame:CGRectMake(0, height/2, width, 1)];
    horizLineView.backgroundColor = [UIColor grayColor];
    [toothMapView addSubview:horizLineView];
    
    return toothMapView;
}

-(void) onTooth:(UIButton*)button   {
    NSInteger tag = button.tag;
    
    if(lastSelectedTag > 0) {
        [self saveToothInfoCardByIndex:lastSelectedTag];
    }
    
    lastSelectedTag = tag;
    lastSelectedDentaDiagnosis = nil;
    lastSelectedAdditionDentaDiagnosis = -1;
    
    [self updateToothMap:NO];
    
    button.backgroundColor = getColorSchemeTooth();//[UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0];
    button.layer.borderColor = getColorSchemeTooth().CGColor;//[UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0].CGColor;
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self showToothInfoCardByIndex:tag];
}

-(void) updateToothMap:(BOOL)isBlue  {
    NSMutableDictionary *toothMapArray = self.currentToothList[@"tooth_list"];//[Tooth MR_findByAttribute:@"tooth_list" withValue:self.currentToothList];
    
    for(UIView *subView in self.contentView.subviews)   {
        if([subView isKindOfClass:[UIButton class]])   {
            UIButton *btn = (UIButton*)subView;
            
            btn.backgroundColor = [UIColor clearColor];
            btn.layer.borderColor = [UIColor lightGrayColor].CGColor;
            [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        } else if([subView isKindOfClass:[UILabel class]])   {
            UILabel *btn = (UILabel*)subView;
            btn.text = @"";
        }
    }
    
    if(toothMapArray.count > 0) {
        for(NSString *tooth_index in [toothMapArray allKeys])
        {
            NSDictionary* tooth = toothMapArray[tooth_index];
            NSInteger tag = [tooth_index integerValue];
            
            UIButton *btn = (UIButton*)[self.contentView viewWithTag:tag];
            if([tooth[@"diagnosisid"] length]>0 || [tooth[@"comment"] length]>0 )
            {
                if(isBlue) {
                    btn.backgroundColor = getColorSchemeTooth();//[UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0];
                    btn.layer.borderColor = getColorSchemeTooth().CGColor;//[UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0].CGColor;
                } else  {
                    btn.backgroundColor = [UIColor colorWithRed:151.0/255.0 green:151.0/255.0 blue:151.0/255.0 alpha:1.0];
                    btn.layer.borderColor = [UIColor colorWithRed:151.0/255.0 green:151.0/255.0 blue:151.0/255.0 alpha:1.0].CGColor;
                }
                [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

                NSDictionary *dentaDiagnosis;
                
                for(NSDictionary* diag in diagnoses)
                {
                    if([diag[@"id"] isEqualToString:tooth[@"diagnosisid"]])
                    {
                        dentaDiagnosis=diag;
                        break;
                    }
                }
                UILabel *titleLabel = (UILabel*)[self.contentView viewWithTag:tag + BUTTON_ID_DELTA];
                titleLabel.text = dentaDiagnosis[@"subtitle"];
            }
        }
    }
}

-(void) showToothInfoCardByIndex:(NSInteger)index   {
    
    NSDictionary *foundToothArray = self.currentToothList[@"tooth_list"][[@(index) stringValue]];/*= [Tooth MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"tooth_list == %@ AND index == %@", self.currentToothList, @(index)]];*/
    if(foundToothArray)
    {
        NSDictionary *selectedToothEntity = foundToothArray;
        
        lastSelectedDentaDiagnosis = selectedToothEntity[@"diagnosisid"];
        
        NSDictionary *dentaDiagnosis;
        
        for(NSDictionary* diag in diagnoses)
        {
            if([diag[@"id"] isEqualToString:selectedToothEntity[@"diagnosisid"]])
            {
                dentaDiagnosis=diag;
                break;
            }
        }
        //[selectedToothEntity[@"diagnosisid"]];//[DentalDiagnosis MR_findFirstByAttribute:@"remoteId" withValue:selectedToothEntity.dentalDiagnosisId];
        self.mainDiagnosisTextView.text = dentaDiagnosis[@"title"];
        
        [self.tagListView.tags removeAllObjects];
        [self.tagListView.collectionView reloadData];
        
        self.commentTextView.text = selectedToothEntity[@"comment"];
        
    } else  {
        self.mainDiagnosisTextView.text = @"";
        [self.tagListView.tags removeAllObjects];
        [self.tagListView.collectionView reloadData];
        self.commentTextView.text = @"";
    }
    
    self.toothInfoCard.hidden = NO;
}

-(void) saveToothInfoCardByIndex:(NSInteger)index   {
    if(lastSelectedDentaDiagnosis != nil)  {
        NSMutableDictionary *saveTooth = nil;
        /*if([self.currentToothList[@"tooth_list"] length]>index)
        {
            saveTooth
        }*/
        saveTooth = self.currentToothList[@"tooth_list"][[@(index) stringValue]];//[Tooth MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"index == %@ AND tooth_list == %@", @(index), self.currentToothList]];
        if(!saveTooth)
        {
            saveTooth=[NSMutableDictionary dictionaryWithDictionary:@{@"id":GUIDString(),@"indexx":[@(index) stringValue]}];
            //saveTooth = [Tooth MR_createEntity];
            //saveTooth.index = @(index);
        }
        saveTooth[@"diagnosisid"] = lastSelectedDentaDiagnosis;
        saveTooth[@"comment"] = self.commentTextView.text;
        //[[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    }
}

//-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
//    return NO;  // Hide both keyboard and blinking cursor.
//}
- (IBAction)onCommnet:(id)sender {
    /*if(!isReadOnly)
    {
        self.textView.text = self.commentTextView.text;
        
        self.textView.width = self.view.width;
        self.textView.height = self.view.height - 253.0 - 50.0;
        [self.view addSubview:self.textView];
        
        [self.commentTextView resignFirstResponder];
        [self.textView becomeFirstResponder];
        
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                                 style:UIBarButtonItemStylePlain
                                                                                target:self
                                                                                action:@selector(noProcess)];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Готово"
                                                                                  style:UIBarButtonItemStylePlain
                                                                                 target:self
                                                                                 action:@selector(donePad:)];
    }*/
}

/*-(void)donePad:(id)sender
{
    self.commentTextView.text = self.textView.text;
    [self.view endEditing:YES];
    [self.commentTextView resignFirstResponder];
    
    [self.textView removeFromSuperview];
    
    self.keyboardIsShown = NO;
    
    UIImage *image2 = [UIImage imageNamed:@"back-button"];
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 addTarget:self action:@selector(onBack:)forControlEvents:UIControlEventTouchUpInside];
    [button2 setBackgroundImage:image2 forState:UIControlStateNormal];
    button2.frame = CGRectMake(0, 0, 11, 18);
    UIBarButtonItem *barButton2 = [[UIBarButtonItem alloc] initWithCustomView:button2];
    self.navigationItem.leftBarButtonItem = barButton2;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Сохранить"
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(onSave:)];
}*/

-(void) noProcess   {
    //
}

- (IBAction)onCommonComment:(id)sender {
/*    self.commonTextView.text = self.commonCommentTextView.text;
    
    self.commonTextView.width = self.view.width;
    self.commonTextView.height = self.view.height - 253.0 - 50.0;
    [self.view addSubview:self.commonTextView];
    
    [self.commonCommentTextView resignFirstResponder];
    [self.commonTextView becomeFirstResponder];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:self
                                                                            action:@selector(noProcess)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Готово"
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(doneCommonPad:)];*/
}

-(void)doneCommonPad:(id)sender
{
    /*self.commonCommentTextView.text = self.commonTextView.text;
    [self.view endEditing:YES];
    [self.commonCommentTextView resignFirstResponder];
    
    [self.commonTextView removeFromSuperview];
    
    self.currentToothList[@"comment"] = self.commonCommentTextView.text;
    //[[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    
    self.keyboardIsShown = NO;
    
    UIImage *image2 = [UIImage imageNamed:@"back-button"];
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 addTarget:self action:@selector(onBack:)forControlEvents:UIControlEventTouchUpInside];
    [button2 setBackgroundImage:image2 forState:UIControlStateNormal];
    button2.frame = CGRectMake(0, 0, 11, 18);
    UIBarButtonItem *barButton2 = [[UIBarButtonItem alloc] initWithCustomView:button2];
    self.navigationItem.leftBarButtonItem = barButton2;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Сохранить"
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(onSave:)];*/
}

/*- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    self.currentToothList[@"comment"] = [[textField text] stringByReplacingCharactersInRange:range withString:string];
    [[ToothManager shared] updateToothMap:self.currentToothList success:^{
    } failure:^{
    }];

    
    return true;
}*/

 - (void) textViewDidChange:(UITextView *)textView
{
    self.currentToothList[@"comment"] = textView.text;//[[textView text] stringByReplacingCharactersInRange:range withString:string];
    [[ToothManager shared] updateToothMap:self.currentToothList success:^{
    } failure:^{
    }];

}

- (CGFloat) AACStatusBarHeight
{
    CGSize statusBarSize = [[UIApplication sharedApplication] statusBarFrame].size;
    return MIN(statusBarSize.width, statusBarSize.height);
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = -keyboardSize.height+self.navigationController.navigationBar.frame.size.height+[self AACStatusBarHeight];
        self.view.frame = f;
    }];
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = 0;//f.origin.y-(-keyboardSize.height+self.navigationController.navigationBar.frame.size.height+[self AACStatusBarHeight]);
        self.view.frame = f;
    }];

}

@end
