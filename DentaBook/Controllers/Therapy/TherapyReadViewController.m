//
//  TherapyReadViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 27.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "TherapyReadViewController.h"
#import "UIView+Helpers.h"
#import "ChoosedServiceViewCell.h"

#import <MagicalRecord/MagicalRecord.h>

#import "Patient.h"
#import "CategoryTherapy.h"
#import "Service.h"
#import "Therapy.h"
#import "TherapyItem.h"
#import "DataBase.h"
#import "HttpUtil.h"
#import "PercUtils.h"

@interface TherapyReadViewController () <NSFetchedResultsControllerDelegate>
@property (nonatomic, strong) NSDictionary *currentTherapy;

@property (strong, nonatomic) IBOutlet UITableView *optionTableView;

@property (strong, nonatomic) IBOutlet UIView *optionInfoBoxView;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (weak, nonatomic) IBOutlet UILabel *sumWithoutDiscountLabel;
@property (weak, nonatomic) IBOutlet UILabel *discountLabel;
@property (weak, nonatomic) IBOutlet UILabel *sumWithDiscountLabel;

@end

@implementation TherapyReadViewController
@synthesize sections, patientLabel;
-(id) initWithTherapy:(NSDictionary*)therapy {
    self = [super initWithNibName:@"TherapyReadViewController" bundle:[NSBundle mainBundle]];
    if(self)    {
        self.currentTherapy = therapy;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSDictionary* pat = (NSDictionary*)[DataBase getDB:@"patient" withKey:@"patientid" andValue:self.currentTherapy[@"patientid"]];
    
    self.title = self.currentTherapy[@"name"];
    
    [self.optionTableView registerNib:[UINib nibWithNibName:NSStringFromClass([ChoosedServiceViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ChoosedServiceViewCell class])];
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    
    if(((NSArray*)pat[@"illness"]).count>0)
    {
        
        NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
        attachment.image =  [PercUtils imageWithImage:[UIImage imageNamed:@"patient-illness-icon.png"] scaledToSize:CGSizeMake(15, 15)];
        attachment.bounds = CGRectMake(0, -3, 18, 18);
        NSMutableAttributedString *attachmentString = [[NSAttributedString attributedStringWithAttachment:attachment] mutableCopy];
        
        NSMutableAttributedString *myString= [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"  %@ %@ %@", pat[@"firstname"], pat[@"middlename"], pat[@"lastname"]]];
        [attachmentString appendAttributedString:myString];
        
        patientLabel.attributedText = attachmentString;
    }
    else
    {
        patientLabel.text = [NSString stringWithFormat:@"  %@ %@ %@", pat[@"firstname"], pat[@"middlename"], pat[@"lastname"]];
    }
    //self.optionInfoBoxView.top = 0;
    //self.optionInfoBoxView.left = 0;
    //self.optionInfoBoxView.width = screenRect.size.width;
    //[self.infoCell.contentView addSubview:self.optionInfoBoxView];
    
    [self fetchTableData];
    [self updateBuyInfo];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)] && getNeedName())
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    if(!getNeedName())
    {
        [[UINavigationBar appearance] setFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width , 44)];
        //[[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    }
    else
    {
        [[UINavigationBar appearance] setFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width , 64)];
        //[[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    }
    
    UIImage *image2 = [UIImage imageNamed:@"back-button"];
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 addTarget:self action:@selector(onBack:)forControlEvents:UIControlEventTouchUpInside];
    [button2 setBackgroundImage:image2 forState:UIControlStateNormal];
    button2.frame = CGRectMake(0, 0, 11, 18);
    UIBarButtonItem *barButton2 = [[UIBarButtonItem alloc] initWithCustomView:button2];
    self.navigationItem.leftBarButtonItem = barButton2;
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)fetchTableData
{
    NSArray* tmp = self.currentTherapy[@"items"];
    NSMutableArray* tmp_sections = [[NSMutableArray alloc] init];
    
    for(NSDictionary* pat in tmp)
    {
        
        //nmd[@"index"] = [[(NSString*)pat[@"firstname"] substringWithRange:NSMakeRange(0, 1)] uppercaseString];
        [tmp_sections addObject:pat[@"categoryid"]];
    }
    
    sections = [[NSMutableArray alloc] init];
    for(NSString* hd in [tmp_sections valueForKeyPath:@"@distinctUnionOfObjects.self"])
    {
        NSMutableDictionary* ndms = [[NSMutableDictionary alloc] init];
        ndms[@"section"] = hd;
        NSMutableArray* mna=[[NSMutableArray alloc] init];
        for(NSDictionary* dict in tmp)
        {
            if([[(NSString*)dict[@"categoryid"] uppercaseString] isEqualToString:[hd uppercaseString]])
            {
                [mna addObject:dict];
            }
        }
        ndms[@"content"]=mna;
        [sections addObject:ndms];
    }

}

-(void) updateBuyInfo
{
    
    NSInteger cost = 0;
    
    for(NSDictionary *buyRef in self.currentTherapy[@"items"] /*[TherapyItem MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"therapy == %@", self.currentTherapy]]*/)
    {
        NSDictionary *serviceRef = [DataBase getDBArray:@"services" withKey:@"id" andValue:buyRef[@"serviceid"]][0];//[Service MR_findFirstByAttribute:@"remoteId" withValue:buyRef.serviceId];
        cost += [serviceRef[@"cost"] integerValue] * [buyRef[@"countt"] integerValue];
    }
    
    float realCostWithDiscount = cost - ((cost * [self.currentTherapy[@"discount"] integerValue])/100);
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setGroupingSeparator:@" "];
    [numberFormatter setGroupingSize:3];
    [numberFormatter setUsesGroupingSeparator:YES];
    [numberFormatter setDecimalSeparator:@"."];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [numberFormatter setMaximumFractionDigits:2];
    
    NSString *theString = [numberFormatter stringFromNumber:@(cost)];
    
    self.sumWithoutDiscountLabel.text = [NSString stringWithFormat:@"%@ ₽", theString];
    theString = [numberFormatter stringFromNumber:@(realCostWithDiscount)];
    self.sumWithDiscountLabel.text = /*[self.currentTherapy[@"total_cost_with_discount"];*/[NSString stringWithFormat:@"%@ ₽", theString];
    self.discountLabel.text = [NSString stringWithFormat:@"%@%%", self.currentTherapy[@"discount"]];

    /*for(TherapyItem *buyRef in [TherapyItem MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"therapy == %@", self.currentTherapy]])   {
        Service *serviceRef = [Service MR_findFirstByAttribute:@"remoteId" withValue:buyRef.serviceId];
        cost += [serviceRef.cost integerValue] * [buyRef.count integerValue];
    }
    
    self.sumWithoutDiscountLabel.text = [NSString stringWithFormat:@"%d руб.", cost];
    self.discountLabel.text = [NSString stringWithFormat:@"%d%%", [self.currentTherapy.discount integerValue]];
    self.sumWithDiscountLabel.text = [NSString stringWithFormat:@"%.2f руб.", cost - ((cost * [self.currentTherapy.discount floatValue])/100)];*/
}

#pragma mark -
#pragma mark Option Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //NSInteger count = [[self.fetchedResultsController sections] count];
    return sections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    //id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    //NSInteger count2 = [sectionInfo numberOfObjects];
    return ((NSArray*)sections[section][@"content"]).count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    NSString *buyRef = sections[section][@"section"];
    NSDictionary *categoryRef = (NSDictionary*)[DataBase getDB:@"services_group" withKey:@"id" andValue:buyRef];
    
    UIView* headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 35)];
    
    headerView.backgroundColor = [UIColor whiteColor];
    
    UILabel* headerLabel = [[UILabel alloc] init];
    headerLabel.frame = CGRectMake(10, 10, tableView.frame.size.width - 10, 18);
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.textColor = [UIColor blackColor];
    headerLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:13];
    headerLabel.text = categoryRef[@"title"];
    headerLabel.textAlignment = NSTextAlignmentLeft;
    
    [headerView addSubview:headerLabel];
    
    return headerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section    {
    
    UIView* headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 1)];
    UIView* subheaderView = [[UIView alloc] initWithFrame:CGRectMake(10, 0, tableView.frame.size.width-10, 1)];
    [headerView addSubview:subheaderView];
    subheaderView.backgroundColor = [UIColor colorWithRed:239.0f/255.0f green:239.0f/255.0f blue:244.0f/255.0f alpha:1];
    
    /*UIImageView *seperateImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"seperate-cell"]];
    [headerView addSubview:seperateImageView];
    seperateImageView.center = headerView.center;*/
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section   {
    return 35;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section   {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath  {
    
    return 32.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //TherapyItem *buyRef = [self.fetchedResultsController objectAtIndexPath:indexPath];
    //Service *serviceRef = [Service MR_findFirstByAttribute:@"remoteId" withValue:buyRef.serviceId];
    NSDictionary *categoryRef = (NSDictionary*)[DataBase getDB:@"services_group" withKey:@"id" andValue:sections[indexPath.section][@"section"]];
    NSDictionary *serviceRef = (NSDictionary*)[DataBase getDB:@"services" withKey:@"id" andValue:sections[indexPath.section][@"content"][indexPath.row][@"serviceid"]];//(NSDictionary*)[DataBase getDB:@"services_wrap" withKey:@"id" andValue:buyRef];
    
    ChoosedServiceViewCell *chooseCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ChoosedServiceViewCell class])];
    chooseCell.countTooth.text = [NSString stringWithFormat:@"%ld", [sections[indexPath.section][@"content"][indexPath.row][@"toothid"] integerValue]];
    chooseCell.nameLabel.text = serviceRef[@"title"];
    chooseCell.countService.text = [NSString stringWithFormat:@"%ld", [sections[indexPath.section][@"content"][indexPath.row][@"countt"] integerValue]];
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setGroupingSeparator:@" "];
    [numberFormatter setGroupingSize:3];
    [numberFormatter setUsesGroupingSeparator:YES];
    [numberFormatter setDecimalSeparator:@"."];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [numberFormatter setMaximumFractionDigits:2];
    
    NSString *theString = [numberFormatter stringFromNumber:@([serviceRef[@"cost"] floatValue])];
    
    chooseCell.costService.text = theString;
    chooseCell.deleteBtn.hidden = YES;
    
    return chooseCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
