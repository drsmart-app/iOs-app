//
//  TherapyViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 25.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "TherapyViewController.h"
#import "ChoosedServiceViewCell.h"
#import "ServiceDataTableViewCell.h"
#import "CategoryViewCell.h"
#import "UIView+Helpers.h"
#import "TherapyEditViewController.h"
#import "TherapyAcceptViewController.h"
#import "WLPActionSheet.h"
#import "DBToothMapScroller.h"
#import "DbToothCounter.h"
#import "MoneyDepositViewController.h"

#import <MagicalRecord/MagicalRecord.h>
#import <KISObserver/KISObserver.h>
#import <JGProgressHUD/JGProgressHUD.h>

#import "Patient.h"
#import "CategoryTherapy.h"
#import "Service.h"
#import "Therapy.h"
#import "TherapyItem.h"
#import "RADataObject.h"
#import "PatientTherapyManager.h"
#import "DataBase.h"
#import "NormalItemsViewController.h"

#import "ToothCardViewController.h"
#import "UIView+FindFirstResponder.h"
#import <MessageUI/MessageUI.h>
#import "UIView+Toast.h"
#import "ShareViewController.h"
#import "UIViewController+PresentedOver.h"
#import "HttpUtil.h"
#import "UITableView+Convertible.h"
#import "ToothManager.h"
#import "DataManager.h"
#import "UIImageView+ImageDownloader.h"
#import "SharedImage.h"
#import "PercUtils.h"
#import <MessageUI/MessageUI.h>
#import "URBAlertView.h"


//- (void)presentTransparentViewController:(UIViewController *)viewControllerToPresent animated:(BOOL)flag completion:(void (^)(void)) completion
typedef enum {
    kOptionTableNoneMode = 0,
    kOptionTableShortMode,
    kOptionTableExMode
} OptionTableMode;

@interface TherapyViewController () <NSFetchedResultsControllerDelegate, WLPActionSheetDelegate, MFMailComposeViewControllerDelegate>
@property (nonatomic) OptionTableMode optionTableMode;

@property (strong, nonatomic) IBOutlet UIView *optionBoxView;
@property (strong, nonatomic) IBOutlet UITableView *optionTableView;

@property (strong, nonatomic) IBOutlet UIView *optionInfoBoxView;
@property (strong, nonatomic) IBOutlet UITableViewCell *infoCell;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *saveButtonWidthConstraints;
@property (weak, nonatomic) IBOutlet UIButton *saveAndPayButton;

@property (nonatomic, strong) NSMutableArray *data;

@property (weak, nonatomic) IBOutlet UIButton *switchButton;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (nonatomic, strong) NSMutableDictionary *currentPatient;
@property (nonatomic, strong) NSMutableDictionary *currentTherapy;
@property (nonatomic, strong) NSArray *therapiesToConvert;

@property (nonatomic) BOOL isPlan;
@property (nonatomic) BOOL isTherapyCreateMode;

@property (nonatomic) BOOL cardCopied;

@property (weak, nonatomic) IBOutlet UILabel *sumWithoutDiscountLabel;
@property (weak, nonatomic) IBOutlet UILabel *sumWithDiscountLabel;
@property (weak, nonatomic) IBOutlet UILabel *sumWithoutDiscountTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *discountTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *sumWithDiscountTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *sumWithDiscountTitleLabel2;
@property (weak, nonatomic) IBOutlet UILabel *sumWithDiscountLabel2;
@property (weak, nonatomic) IBOutlet UILabel *discountLabel;

@property (nonatomic, strong) WLPActionSheet *toothMapActionSheet;
@property (nonatomic, strong) DBToothMapScroller *toothMapScroller;

@property (nonatomic, strong) WLPActionSheet *toothCounterActionSheet;
@property (nonatomic, strong) DbToothCounter *toothCounter;

@property (nonatomic, strong) WLPActionSheet *discountCounterActionSheet;
@property (nonatomic, strong) DbToothCounter *discountCounter;
@property (nonatomic) NSInteger local_counter;
@property (nonatomic) BOOL stop_autoscroll;
@property (nonatomic) int direction;
@property (nonatomic) CGRect remembered_frame;
@property (nonatomic) BOOL convert_mode;
@end

@implementation TherapyViewController   {
    CGFloat discountValue;
    NSInteger realCost;
    CGFloat realCostWithDiscount;
    
}
@synthesize optionTableMode, sections, extendedView, commentTextView, commentPlaceholder, planNameLabel,bigSaveBtn, saveTherapyView, applyTherapyView, payButton, extButton, saveButton, saveAndPayButton, selectedRow, firstVisitView, secondVisitView, undergroundView, edit_btn, applyButton, commentTextView1, commentPlaceholder1, delimeterFirstVisitView, extendedDelimeterView, showPriceBtn, changeNameButton, commentLabel, discountLabelLabel;


@synthesize headerView, logoImageView, fioLabel, positionLabel, phoneLabel, emailLabel, addressLabel, licenseLabel, titleLabel, patientLabel, footerView, totalLabel, footerDiscountLabel, totalWithDiscountLabel, clinicLabel, tableHeadView, planNameView, convertMode;

-(id) initWithPatient:(NSDictionary*)patient placeToPlan:(BOOL)isPlan andNumber:(int) counter {
    self = [super initWithNibName:@"TherapyViewController" bundle:[NSBundle mainBundle]];
    if(self)    {
        self.currentPatient = [NSMutableDictionary dictionaryWithDictionary:patient];
        self.isPlan = isPlan;
        self.isTherapyCreateMode = YES;
        self.title = [NSString stringWithFormat:@"%@ %@",patient[@"firstname"],patient[@"lastname"]];
        self.cardCopied=false;
        self.direction = 0;
        //
        if(counter!=-1)
            self.local_counter = counter;
        else
            self.local_counter = [DataBase getDBArray:@"patient_therapy" withKey:@"patientid" andValue:self.currentPatient[@"patientid"]].count;
        
        self.stop_autoscroll=NO;
        convertMode=-1;
    }
    return self;
}

-(id) initWithTherapy:(NSDictionary*)therapy  andPatient:(NSDictionary*)patient{
    self = [super initWithNibName:@"TherapyViewController" bundle:[NSBundle mainBundle]];
    if(self)    {
        self.currentPatient = [NSMutableDictionary dictionaryWithDictionary:patient];
        self.currentTherapy = [NSMutableDictionary dictionaryWithDictionary:therapy];
        self.title = [NSString stringWithFormat:@"%@ %@",patient[@"firstname"],patient[@"lastname"]];
        self.isTherapyCreateMode = NO;
        self.isPlan = YES;
        self.cardCopied=false;
        convertMode=-1;
    }
    return self;
}

-(id) initWithTherapies:(NSArray*)therapies andPatient:(NSDictionary*)patient
{
    self = [super initWithNibName:@"TherapyViewController" bundle:[NSBundle mainBundle]];
    if(self)    {
        self.currentPatient = [NSMutableDictionary dictionaryWithDictionary:patient];
        self.currentTherapy = [NSMutableDictionary dictionaryWithDictionary:therapies[0]];
        self.title = [NSString stringWithFormat:@"%@ %@",patient[@"firstname"],patient[@"lastname"]];
        self.isTherapyCreateMode = NO;
        self.isPlan = YES;
        self.cardCopied=false;
        self.therapiesToConvert = therapies;
    }
    return self;
}

- (void) viewDidAppear:(BOOL)animated
{
    [self fetchTableData];
    [self.optionTableView reloadData];
    
    if([self isLastRowVisible])
    {
        applyTherapyView.hidden = NO;
        saveTherapyView.hidden = NO;
        bigSaveBtn.hidden = NO;
    }
    else
    {
        applyTherapyView.hidden = YES;
        saveTherapyView.hidden = YES;
        bigSaveBtn.hidden = YES;
    }
    [self adjustVisibility:YES];
    
    ///////////////////////////
    if(self.therapiesToConvert)
    {
        [self.view makeToast:[NSString stringWithFormat:@"Подождите, идет конвертация"]];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.3 * NSEC_PER_SEC), dispatch_get_main_queue(),
        ^{
            headerView.hidden = NO;
            footerView.hidden = NO;
            fioLabel.text = getProfileFio();
            positionLabel.text = getProfilePosition();
            phoneLabel.text =  [NSString stringWithFormat:@"Тел: %@", getProfilePhone()];
            emailLabel.text =  [NSString stringWithFormat:@"E-mail: %@", getProfileEmail()];
            addressLabel.text =  [NSString stringWithFormat:@"Адрес: %@", getProfileAddress()];
            clinicLabel.text =  getProfileClinic();
            licenseLabel.text =  [NSString stringWithFormat:@"Лицензия: %@", getProfileLicense()];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"dd.MM.yyyy"];
            titleLabel.text =  [NSString stringWithFormat:@"Отчет по планам лечения от %@", [dateFormatter stringFromDate:[NSDate date]] ];
            patientLabel.text =  [NSString stringWithFormat:@"%@ %@ %@ дата рождения: %@", self.currentPatient[@"firstname"],self.currentPatient[@"middlename"],self.currentPatient[@"lastname"], self.currentPatient[@"birthdate"]];
            
            
            NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
            [numberFormatter setGroupingSeparator:@" "];
            [numberFormatter setGroupingSize:3];
            [numberFormatter setUsesGroupingSeparator:YES];
            [numberFormatter setDecimalSeparator:@"."];
            [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
            [numberFormatter setMaximumFractionDigits:2];
            
            
            UIImage* return_img = nil;
            UIImage* together_img;
            
            NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
            //NSString *imageSubdirectory = [documentsDirectory stringByAppendingPathComponent:@"logo"];
            
            NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"logo.png"];
            
            // Convert UIImage object into NSData (a wrapper for a stream of bytes) formatted according to PNG spec
            //NSData *imageData = [NSData dataWithContentsOfFile:filePath]; //UIImagePNGRepresentation(photo);
            //[imageData writeToFile:filePath atomically:YES];
            UIImage* zoip = [UIImage imageWithContentsOfFile:filePath];
            logoImageView.image = zoip;
            
            //planNameView.hidden = YES;
            
            discountLabelLabel.hidden = YES;
            footerDiscountLabel.hidden = YES;
            tableHeadView.hidden = NO;
            tableHeadView.backgroundColor = [UIColor whiteColor];
            return_img = [self joinImages:[self captureView:headerView] secondImage:[self captureView:tableHeadView]];
            tableHeadView.hidden = YES;
            //UIImageWriteToSavedPhotosAlbum(return_img, nil, nil, nil);
            
            tableHeadView.hidden = NO;
            //planNameView.hidden = NO;
            
            
            self.convert_mode = YES;
            float real_cost = 0;
            float real_cost_with_discount = 0;
            for(NSDictionary* pat in self.therapiesToConvert)
            {
                float current_cost=0;
                self.currentTherapy = [NSMutableDictionary dictionaryWithDictionary:pat];
                [self fetchTableData];
                [self.optionTableView reloadData];
                //together_img = [self prepareTableImage];
                
                self.planNameLabel.text = self.currentTherapy[@"name"];
                
                together_img = [self joinImages:[self captureView:planNameView] secondImage:[self prepareTableImage]];
                
                if(together_img)
                    return_img = [self joinImages:return_img secondImage:together_img];
                
                for(NSDictionary *buyRef in self.currentTherapy[@"items"])
                {
                    NSDictionary *serviceRef = [DataBase getDBArray:@"services" withKey:@"id" andValue:buyRef[@"serviceid"]][0];
                    current_cost += [serviceRef[@"cost"] integerValue] * [buyRef[@"countt"] integerValue];
                }
                float discount_value = [self.currentTherapy[@"discount"] floatValue];
                real_cost_with_discount+= current_cost - ((current_cost * discount_value)/100);
                real_cost+=current_cost;

            }
            self.convert_mode = NO;

            
            
            totalLabel.text = [NSString stringWithFormat:@"%@ ₽", [numberFormatter stringFromNumber:@(real_cost)]];
            //footerDiscountLabel.text = [NSString stringWithFormat:@"%d", (int)discountValue];
            totalWithDiscountLabel.text = [NSString stringWithFormat:@"%@ ₽", [numberFormatter stringFromNumber:@(real_cost_with_discount)]];

            
            return_img = [self joinImages:return_img secondImage:[self captureView:footerView]];
            
            //UIImageWriteToSavedPhotosAlbum(return_img, nil, nil, nil);
            
            //UIImageWriteToSavedPhotosAlbum([self prepareImage], nil, nil, nil);
            
            //[self.view makeToast:[NSString stringWithFormat:@"Изображение сохранено в галерее"]];
            
            //[SharedImage sharedInstance].img = return_img;
            
            [self.navigationController popViewControllerAnimated:NO];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"imageReady" object:@{@"mode":@(convertMode), @"img":return_img}];

        });
    }
}

-(void)scrollViewWillEndDragging:(UIScrollView *)scrollView
                    withVelocity:(CGPoint)velocity
             targetContentOffset:(inout CGPoint *)targetContentOffset{
    
    if (velocity.y > 0){
        NSLog(@"up");
        self.direction = 0;
        self.stop_autoscroll=NO;
    }
    if (velocity.y < 0){
        NSLog(@"down");
        self.direction = 1;
        self.stop_autoscroll=NO;
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    /*if (!decelerate) {
        [self scrollingFinish];
    }*/
    [self scrollingFinish];
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self scrollingFinish];
}
- (void)scrollingFinish {
    if([self isLastRowVisible])
    {
        [UIView animateWithDuration:1
                              delay:0
                            options:UIViewAnimationOptionCurveLinear
                         animations:^{
                                        //view.frame = CGRectMake(200, 200, 10, 10);
                                        applyTherapyView.hidden = NO;
                                        saveTherapyView.hidden = NO;
                                        bigSaveBtn.hidden = NO;
                                        delimeterFirstVisitView.hidden = NO;
                                        extendedDelimeterView.hidden = NO;
                                     }
                         completion:^(BOOL finished) {
                             //completion();
                         }];
        /*applyTherapyView.hidden = NO;
        saveTherapyView.hidden = NO;
        bigSaveBtn.hidden = NO;
        delimeterFirstVisitView.hidden = NO;
        extendedDelimeterView.hidden = NO;*/
    }
    else
    {
        [UIView animateWithDuration:1
                              delay:0
                            options:UIViewAnimationOptionCurveLinear
                         animations:^{
                             //view.frame = CGRectMake(200, 200, 10, 10);
                             applyTherapyView.hidden = YES;
                             saveTherapyView.hidden = YES;
                             bigSaveBtn.hidden = YES;
                             delimeterFirstVisitView.hidden = YES;
                             extendedDelimeterView.hidden = YES;
                         }
                         completion:^(BOOL finished) {
                             //completion();
                         }];
        /*applyTherapyView.hidden = YES;
        saveTherapyView.hidden = YES;
        bigSaveBtn.hidden = YES;
        delimeterFirstVisitView.hidden = YES;
        extendedDelimeterView.hidden = YES;*/
    }
    [self adjustVisibility:YES];
    //enter code here
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self scrollingFinish];;
}

- (void) updateList
{
    [self fetchTableData];
    [self.optionTableView reloadData];
    planNameLabel.text=self.currentTherapy[@"name"];
    
    if([self isLastRowVisible])
    {
        applyTherapyView.hidden = NO;
        saveTherapyView.hidden = NO;
        bigSaveBtn.hidden = NO;
        delimeterFirstVisitView.hidden = NO;
        extendedDelimeterView.hidden = NO;
    }
    else
    {
        applyTherapyView.hidden = YES;
        saveTherapyView.hidden = YES;
        bigSaveBtn.hidden = YES;
        delimeterFirstVisitView.hidden = YES;
        extendedDelimeterView.hidden = YES;
    }
    
    [self adjustVisibility:NO];
}

- (void) updateListDelay
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        /*if([self isLastRowVisible])
        {
            applyTherapyView.hidden = NO;
            saveTherapyView.hidden = NO;
            bigSaveBtn.hidden = NO;
            delimeterFirstVisitView.hidden = NO;
            extendedDelimeterView.hidden = NO;
        }
        else
        {
            applyTherapyView.hidden = YES;
            saveTherapyView.hidden = YES;
            bigSaveBtn.hidden = YES;
            delimeterFirstVisitView.hidden = YES;
            extendedDelimeterView.hidden = YES;
        }*/
        if([self isLastRowVisible])
        {
            [UIView animateWithDuration:1
                                  delay:0
                                options:UIViewAnimationOptionCurveLinear
                             animations:^{
                                 //view.frame = CGRectMake(200, 200, 10, 10);
                                 applyTherapyView.hidden = NO;
                                 saveTherapyView.hidden = NO;
                                 bigSaveBtn.hidden = NO;
                                 delimeterFirstVisitView.hidden = NO;
                                 extendedDelimeterView.hidden = NO;
                             }
                             completion:^(BOOL finished) {
                                 //completion();
                             }];
            /*applyTherapyView.hidden = NO;
             saveTherapyView.hidden = NO;
             bigSaveBtn.hidden = NO;
             delimeterFirstVisitView.hidden = NO;
             extendedDelimeterView.hidden = NO;*/
        }
        else
        {
            [UIView animateWithDuration:1
                                  delay:0
                                options:UIViewAnimationOptionCurveLinear
                             animations:^{
                                 //view.frame = CGRectMake(200, 200, 10, 10);
                                 applyTherapyView.hidden = YES;
                                 saveTherapyView.hidden = YES;
                                 bigSaveBtn.hidden = YES;
                                 delimeterFirstVisitView.hidden = YES;
                                 extendedDelimeterView.hidden = YES;
                             }
                             completion:^(BOOL finished) {
                                 //completion();
                             }];
            /*applyTherapyView.hidden = YES;
             saveTherapyView.hidden = YES;
             bigSaveBtn.hidden = YES;
             delimeterFirstVisitView.hidden = YES;
             extendedDelimeterView.hidden = YES;*/
        }
        
        [self adjustVisibility:NO];
    });
    
}


-(BOOL)isLastRowVisible {
    if(sections.count>0)
    {
        NSIndexPath* last_row = [NSIndexPath indexPathForRow:((NSArray*)sections[sections.count-1][@"content"]).count-1 inSection:sections.count-1];
        
        NSArray *indexes = [self.optionTableView visibleCells];//[self.optionTableView indexPathsForVisibleRows];
        for (UITableViewCell *index in indexes)
        {
            NSIndexPath* ind = [self.optionTableView indexPathForCell:index];
            //NSLog(@"Test: %ld:%ld against %ld:%ld",(long)ind.row, (long)last_row.row, (long)ind.section, (long)last_row.section);
            if (ind.row == last_row.row && ind.section == last_row.section) {
                return YES;
            }
        }
        
        return NO;
    }
    
    return NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self adjustVisibility:YES];
    
    commentTextView.delegate = self;
    commentTextView1.delegate = self;
    self.convert_mode = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateList) name:@"needBuyUpdate" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateListDelay) name:@"needBuyUpdateDelay" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callToothMap) name:@"needCallToothMap" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onBack:) name:@"needClose" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onSwitchViewMode:) name:@"becomeActive" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    if(self.isTherapyCreateMode)
    {
        NSDateComponents *components = [[NSCalendar currentCalendar]
                                        components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay
                                        fromDate:[NSDate date]];
        NSDate *startDate = [[NSCalendar currentCalendar]
                             dateFromComponents:components];
        
        /*NSInteger uniqueId = [Therapy MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"patient == %@", self.currentPatient]].count;*/
        NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
        [dateformate setDateFormat:@"dd.MM.yyyy"];
        //NSDateFormatter df=
        self.currentTherapy = [[NSMutableDictionary alloc] init];// [Therapy MR_createEntity];
        self.currentTherapy[@"patientid"] = self.currentPatient[@"patientid"];
        self.currentTherapy[@"createat"] = [dateformate stringFromDate:startDate];
        self.currentTherapy[@"id"] = GUIDString();
        self.currentTherapy[@"isplan"] = @(self.isPlan);
        self.currentTherapy[@"discount"] = @(0);
        self.currentTherapy[@"items"] = [[NSMutableArray alloc] init];
        //[[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    }
    
    discountValue = [self.currentTherapy[@"discount"] floatValue];
    
    UIImage *image2 = [UIImage imageNamed:@"back-button"];
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 addTarget:self action:@selector(onBack:)forControlEvents:UIControlEventTouchUpInside];
    [button2 setBackgroundImage:image2 forState:UIControlStateNormal];
    button2.frame = CGRectMake(0, 0, 11, 18);
    UIBarButtonItem *barButton2 = [[UIBarButtonItem alloc] initWithCustomView:button2];
    self.navigationItem.leftBarButtonItem = barButton2;
    
   
    [self loadData];
    
    [self.optionTableView registerNib:[UINib nibWithNibName:NSStringFromClass([ChoosedServiceViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ChoosedServiceViewCell class])];
    [self.optionTableView registerNib:[UINib nibWithNibName:NSStringFromClass([ServiceDataTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ServiceDataTableViewCell class])];
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    self.optionBoxView.top = 64.0f;
    self.optionBoxView.width = screenRect.size.width;
    self.optionBoxView.height = screenRect.size.height;
    self.optionInfoBoxView.width = screenRect.size.width;

    
    self.optionTableMode = kOptionTableExMode;//kOptionTableShortMode;
    [self fetchTableData];
    [self.optionTableView reloadData];
    [self updateBuyInfo];
    
    
    self.toothCounter = [DbToothCounter new];
    self.discountCounter = [DbToothCounter new];
    self.discountCounter.need0 = YES;
    
    
    NSArray* toothcards = [NSMutableArray arrayWithArray:[DataBase getDBArray:@"toothmap" withKey:@"patientid" andValue:self.currentPatient[@"patientid"]]];
    
    if(self.isPlan)
    {
        if(toothcards.count==0)
        {
            self.toothMapScroller = [[DBToothMapScroller alloc] initWithToothMapMode:kToothMapModeCombine];
        }
        else
        {
            self.toothMapScroller = [[DBToothMapScroller alloc] initWithToothCard:toothcards[toothcards.count-1]];
        }
        //self.toothMapScroller = [[DBToothMapScroller alloc] initWithToothMapMode:kToothMapModeCombine];
        
    }
    else
    {
        if(toothcards.count==0)
        {
            self.toothMapScroller = [[DBToothMapScroller alloc] initWithPatient:self.currentPatient];
        }
        else
        {
            self.toothMapScroller = [[DBToothMapScroller alloc] initWithToothCard:toothcards[toothcards.count-1]];
        }
    }
    
    self.toothMapScroller.delegate = self;
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.optionTableView addGestureRecognizer:gestureRecognizer];
    gestureRecognizer.cancelsTouchesInView = NO;  // this prevents the gesture recognizers to 'block' touches
    
    if(!self.isTherapyCreateMode&&self.isPlan)
    {
    }
    else
    {
        NormalItemsViewController *editTherapyViewController = [[NormalItemsViewController alloc] initWithTherapy:self.currentTherapy];
        editTherapyViewController.isPlan = self.isPlan;
        editTherapyViewController.isTherapyCreateMode = self.isTherapyCreateMode;
        //[self.navigationController pushViewController:editTherapyViewController animated:NO];
        [self presentTransparentViewController:editTherapyViewController animated:NO completion:nil];
    }
    
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
    
    applyTherapyView.backgroundColor = getColorScheme();
    applyButton.backgroundColor = getColorScheme();
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = getColorSchemeStatus();
    }
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.extendedLayoutIncludesOpaqueBars = NO;
    //self.optionTableView.contentInset = UIEdgeInsetsMake(-36, 0, 0, 0);
    commentLabel.text = self.currentTherapy[@"comment"];
}

- (void)hideKeyboard
{
    [self.commentTextView resignFirstResponder];
}

-(void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

- (void) adjustVisibility:(BOOL) needRename
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    
    self.optionBoxView.frame=CGRectMake(0, 64, screenRect.size.width, screenRect.size.height);
    
    if(self.isTherapyCreateMode)
    {
        changeNameButton.enabled = YES;
        
        if(self.isPlan)
        {
            firstVisitView.hidden = NO;
            secondVisitView.hidden = YES;
            extendedView.hidden = YES;
            NSString* currname = self.currentTherapy[@"name"];
            
            if(self.currentTherapy[@"comment"]) commentPlaceholder1.hidden = YES; else commentPlaceholder1.hidden = NO;
            
            if(needRename&&!currname)
            {
                planNameLabel.text=[NSString stringWithFormat:@"План лечения №%d",self.local_counter+1];
                self.currentTherapy[@"name"]=[NSString stringWithFormat:@"План лечения №%d",self.local_counter+1];
            }
            
            CGFloat movement=0;
            if(![self isLastRowVisible])
            {
                movement=152;
                //self.stop_autoscroll=NO;
            }
            else
            {
                
            }
            
            self.optionBoxView.frame = CGRectMake(0,0,screenRect.size.width,screenRect.size.height);
            
            self.optionTableView.frame = CGRectMake(0,self.optionTableView.frame.origin.y,screenRect.size.width, screenRect.size.height - self.optionTableView.frame.origin.y - 314 +movement);// = screenRect.size.height - self.optionTableView.frame.origin.y - self.optionInfoBoxView.frame.size.height;
            //NSLog(@"Whatis  screenRect.size.height = %f, self.optionTableView.frame.origin.y = %f, self.optionInfoBoxView.frame.size.height = %f", screenRect.size.height, self.optionTableView.frame.origin.y, self.optionInfoBoxView.frame.size.height);
            self.optionInfoBoxView.frame = CGRectMake(0, self.optionTableView.frame.origin.y+self.optionTableView.frame.size.height, screenRect.size.width, 314-movement);
            self.navigationItem.rightBarButtonItems=nil;
            if(movement==0 && sections.count>0 && !self.stop_autoscroll)
            {
                [self.optionTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:((NSArray*)[self.sections lastObject][@"content"]).count-1 inSection:sections.count-1] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
                self.stop_autoscroll=YES;
            }
            
            showPriceBtn.hidden = NO;
            
        }
        else
        {
            firstVisitView.hidden = YES;
            secondVisitView.hidden = YES;
            extendedView.hidden = NO;
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"dd.MM.yyyy"];
            planNameLabel.text=[NSString stringWithFormat:@"Лечение %@",[dateFormatter stringFromDate:[NSDate date]]];
            self.currentTherapy[@"name"]=[NSString stringWithFormat:@"Лечение %@",[dateFormatter stringFromDate:[NSDate date]]];
            
            CGFloat movement=0;
            if(![self isLastRowVisible])
            {
                movement=152;
                //self.stop_autoscroll=NO;
            }
            else
            {
                
            }
            
            self.optionBoxView.frame = CGRectMake(0,0,screenRect.size.width,screenRect.size.height);
            self.optionTableView.frame = CGRectMake(0,self.optionTableView.frame.origin.y,screenRect.size.width, screenRect.size.height - self.optionTableView.frame.origin.y - 314 + movement);// = screenRect.size.height - self.optionTableView.frame.origin.y - self.optionInfoBoxView.frame.size.height;
            NSLog(@"Whatis  screenRect.size.height = %f, self.optionTableView.frame.origin.y = %f, self.optionInfoBoxView.frame.size.height = %f", screenRect.size.height, self.optionTableView.frame.origin.y, self.optionInfoBoxView.frame.size.height);
            self.optionInfoBoxView.frame = CGRectMake(0, self.optionTableView.frame.origin.y+self.optionTableView.frame.size.height, screenRect.size.width, 314+movement);
            self.navigationItem.rightBarButtonItems=nil;
            
            if(movement==0 && sections.count>0 && !self.stop_autoscroll)
            {
                [self.optionTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:((NSArray*)[self.sections lastObject][@"content"]).count-1 inSection:sections.count-1] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
                self.stop_autoscroll=YES;
            }
            
            showPriceBtn.hidden = NO;
        }
    }
    else
    {
        changeNameButton.enabled = NO;
        
        if(self.isPlan)
        {
            firstVisitView.hidden = YES;
            secondVisitView.hidden = NO;
            extendedView.hidden = YES;

            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button addTarget:self action:@selector(onPlanEdit:)forControlEvents:UIControlEventTouchUpInside];
            [button setTitle:@"Изменить" forState:UIControlStateNormal];
            [button setTitle:@"Изменить" forState:UIControlStateSelected];
            button.frame = CGRectMake(0, 0, 60, 18);
            button.titleLabel.font = [UIFont systemFontOfSize:12];
            edit_btn = [[UIBarButtonItem alloc] initWithCustomView:button];
            self.navigationItem.rightBarButtonItems = @[edit_btn];
            
            button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            button.contentVerticalAlignment = UIControlContentVerticalAlignmentBottom;
            
            planNameLabel.text=self.currentTherapy[@"name"];
            self.optionBoxView.frame = CGRectMake(0,0,screenRect.size.width,screenRect.size.height);
            
            CGFloat movement=0;
            if(![self isLastRowVisible])
            {
                movement=105;
                //self.stop_autoscroll=NO;
            }
            else
            {
                
            }
            
            self.optionTableView.frame = CGRectMake(0,self.optionTableView.frame.origin.y,screenRect.size.width, screenRect.size.height - self.optionTableView.frame.origin.y - 219 + movement);
            NSLog(@"Whatis  screenRect.size.height = %f, self.optionTableView.frame.origin.y = %f, self.optionInfoBoxView.frame.size.height = %f", screenRect.size.height, self.optionTableView.frame.origin.y, self.optionInfoBoxView.frame.size.height);
            self.optionInfoBoxView.frame = CGRectMake(0, self.optionTableView.frame.origin.y+self.optionTableView.frame.size.height, screenRect.size.width, 219 + movement);
            //self.navigationItem.rightBarButtonItems=nil;
            
            if(movement==0 && sections.count>0 && !self.stop_autoscroll)
            {
                [self.optionTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:((NSArray*)[self.sections lastObject][@"content"]).count-1 inSection:sections.count-1] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
                self.stop_autoscroll=YES;
            }
            
            showPriceBtn.hidden = YES;
        }
        else
        {
            firstVisitView.hidden = YES;
            secondVisitView.hidden = YES;
            extendedView.hidden = YES;
            planNameLabel.text=self.currentTherapy[@"name"];
            self.optionBoxView.frame = CGRectMake(0,0,screenRect.size.width,screenRect.size.height);
            
            CGFloat movement=0;
            if(![self isLastRowVisible])
            {
                movement=152;
                //self.stop_autoscroll=NO;
            }
            else
            {
                
            }
            
            self.optionTableView.frame = CGRectMake(0,self.optionTableView.frame.origin.y,screenRect.size.width, screenRect.size.height - self.optionTableView.frame.origin.y - 314 + movement);
            NSLog(@"Whatis  screenRect.size.height = %f, self.optionTableView.frame.origin.y = %f, self.optionInfoBoxView.frame.size.height = %f", screenRect.size.height, self.optionTableView.frame.origin.y, self.optionInfoBoxView.frame.size.height);
            self.optionInfoBoxView.frame = CGRectMake(0, self.optionTableView.frame.origin.y+self.optionTableView.frame.size.height, screenRect.size.width, 314 + movement);
            self.navigationItem.rightBarButtonItems=nil;
            
            if(movement==0 && sections.count>0 && !self.stop_autoscroll)
            {
                [self.optionTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:((NSArray*)[self.sections lastObject][@"content"]).count-1 inSection:sections.count-1] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
                self.stop_autoscroll=YES;
            }
            
            showPriceBtn.hidden = NO;
        }
    }

}

-(void) onPlanEdit:(id) sender
{
    self.isTherapyCreateMode = YES;
    [self.optionTableView reloadData];
    [self adjustVisibility:NO];
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    commentPlaceholder.hidden = YES;
    commentPlaceholder1.hidden = YES;
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if(textView.text.length>0)
    {
    }
    else
    {
        commentPlaceholder.hidden = NO;
        commentPlaceholder1.hidden = NO;
    }
}

-(void) viewWillAppear:(BOOL)animated
{
    [self.optionTableView reloadData];
    [self updateBuyInfo];
    
    selectedRow = nil;
    
    NSArray* toothcards = [NSMutableArray arrayWithArray:[DataBase getDBArray:@"toothmap" withKey:@"patientid" andValue:self.currentPatient[@"patientid"]]];
    
    if(self.isPlan)
    {
        if(toothcards.count==0)
        {
            self.toothMapScroller = [[DBToothMapScroller alloc] initWithToothMapMode:kToothMapModeCombine];
        }
        else
        {
            self.toothMapScroller = [[DBToothMapScroller alloc] initWithToothCard:toothcards[toothcards.count-1]];
        }

        
    }
    else
    {
        if(toothcards.count==0)
        {
            self.toothMapScroller = [[DBToothMapScroller alloc] initWithPatient:self.currentPatient];
        }
        else
        {
            self.toothMapScroller = [[DBToothMapScroller alloc] initWithToothCard:toothcards[toothcards.count-1]];
        }
    }
    
    self.toothMapScroller.delegate = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onPrint) name:@"needPrint" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onConvert) name:@"needConvert" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onEmail) name:@"needEmail" object:nil];
   
}

-(void) viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"needConvert" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"needPrint" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"needEmail" object:nil];
    
}

-(void)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)onEdit:(id)sender
{
    //[self onSwitchViewMode:self];
    TherapyEditViewController *therapyViewController = [[TherapyEditViewController alloc] initWithTherapy:self.currentTherapy];
    
    [self.navigationController pushViewController:therapyViewController animated:YES];
}

- (void)loadData
{
    self.data = [NSMutableArray new];
    
    for(NSDictionary *category in [DataBase getDBArray:@"services_group" withKey:@"" andValue:@""])
    {

        RADataObject *categoryData = [RADataObject dataObjectWithName:category[@"title"] withCost:0 withId:category[@"id"]  children:nil];
        NSArray* arr=[DataBase getDBArray:@"services" withKey:@"category_id" andValue:category[@"id"]];
        for(NSDictionary *service in arr)   {
            RADataObject *serviceData = [RADataObject dataObjectWithName:service[@"title"] withCost:[service[@"cost"] integerValue] withId:service[@"id"] children:nil];
            [categoryData addChild:serviceData];
        }
        
        [self.data addObject:categoryData];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void) setOptionTableMode:(OptionTableMode)mode {
    optionTableMode = mode;
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    
    self.navigationItem.rightBarButtonItem.enabled = YES;
    
    [self adjustVisibility:YES];
    self.sumWithoutDiscountTitleLabel.hidden = NO;
    self.sumWithoutDiscountLabel.hidden = NO;
    self.discountTitleLabel.hidden = NO;
    self.sumWithDiscountTitleLabel.hidden = NO;
    self.sumWithDiscountLabel.hidden = NO;
    
    self.optionBoxView.hidden = NO;
    //self.optionBoxView.height = screenRect.size.height - self.optionBoxView.top;
    self.optionTableView.scrollEnabled = YES;
    
    if(self.isPlan && !self.isTherapyCreateMode)
    {
        [self.saveAndPayButton setTitle:@"Исполнить и Оплатить" forState:UIControlStateNormal];
    }
    else
    {
        [self.saveAndPayButton setTitle:@"Сохранить и Оплатить" forState:UIControlStateNormal];
    }
}

-(OptionTableMode) optionTableMode  {
    return optionTableMode;
}

- (IBAction)onSwitchViewMode:(id)sender
{
    NormalItemsViewController *editTherapyViewController = [[NormalItemsViewController alloc] initWithTherapy:self.currentTherapy];
    editTherapyViewController.isPlan = self.isPlan;
    editTherapyViewController.isTherapyCreateMode = self.isTherapyCreateMode;
    [self presentTransparentViewController:editTherapyViewController animated:YES completion:nil];
    //[self.navigationController pushViewController:editTherapyViewController animated:YES];
}

- (IBAction)onSaveTherapy:(id)sender
{
    NSArray *arr = self.currentTherapy[@"items"];//[TherapyItem MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"therapy == %@", self.currentTherapy]];
    if(arr.count == 0)
        return;
    
    if(self.isTherapyCreateMode)
    {
        if(self.isPlan)
        {
            if(!self.currentTherapy[@"name"])
            {
                self.currentTherapy[@"name"] = planNameLabel.text;
            }
            
            self.currentTherapy[@"comment"]=commentTextView1.text;
            
            [[PatientTherapyManager shared] createTherapyMap:self.currentTherapy success:^
            {
                [self.navigationController popViewControllerAnimated:YES];
            }
            failure:^
            {

            }];
        }
        else
        {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"dd.MM.yyyy"];
            self.currentTherapy[@"startdate"] = [dateFormatter stringFromDate:[NSDate date]];
            
            if(!self.currentTherapy[@"name"])
            {
                self.currentTherapy[@"name"] = planNameLabel.text;
            }
            
            self.currentTherapy[@"comment"]=commentTextView.text;
            
            for(NSMutableDictionary *buyRef in self.currentTherapy[@"items"])
            {
                NSDictionary *serviceRef = [DataBase getDBArray:@"services" withKey:@"id" andValue:buyRef[@"serviceid"]][0];
                buyRef[@"cost"] = @([serviceRef[@"cost"] integerValue] * [buyRef[@"countt"] integerValue]);
            }
            [DataBase putDB:self.currentTherapy toTable:@"visits" withKey:@"id"];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    else
    {
        if(self.isPlan)
        {
            JGProgressHUD *HUD = [JGProgressHUD progressHUDWithStyle:JGProgressHUDStyleDark];
            HUD.textLabel.text = @"Сохранение";
            [HUD showInView:self.view];
            [[PatientTherapyManager shared] updateTherapyMap:self.currentTherapy success:^{
                [HUD dismissAnimated:YES];
                [self.navigationController popViewControllerAnimated:YES];
            } failure:^{
                [HUD dismissAnimated:YES];
            }];
        }
        else
        {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"dd.MM.yyyy"];
            self.currentTherapy[@"startdate"] = [dateFormatter stringFromDate:[NSDate date]];
            [DataBase putDB:self.currentTherapy toTable:@"visits" withKey:@"id"];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

- (IBAction)onSaveTherapyAndPay:(id)sender {
    NSArray *arr = self.currentTherapy[@"items"];//[TherapyItem MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"therapy == %@", self.currentTherapy]];
    if(arr.count == 0)
        return;
    
    if(self.isPlan && !self.isTherapyCreateMode)
    {
        self.currentTherapy[@"isplan"] = @(NO);
        //[[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd.MM.yyyy"];
    self.currentTherapy[@"startdate"] = [dateFormatter stringFromDate:[NSDate date]];
    
    if(!self.currentTherapy[@"name"])
    {
        self.currentTherapy[@"name"] = planNameLabel.text;
    }
    
    self.currentTherapy[@"comment"]=commentTextView.text;
    
    for(NSMutableDictionary *buyRef in self.currentTherapy[@"items"])
    {
        NSDictionary *serviceRef = [DataBase getDBArray:@"services" withKey:@"id" andValue:buyRef[@"serviceid"]][0];
        buyRef[@"cost"] = @([serviceRef[@"cost"] integerValue] * [buyRef[@"countt"] integerValue]);
    }
    [DataBase putDB:self.currentTherapy toTable:@"visits" withKey:@"id"];
    MoneyDepositViewController *moneyViewController = [[MoneyDepositViewController alloc] initWithPatient:self.currentPatient withMoney:realCostWithDiscount andVisit:self.currentTherapy[@"id"]];
    [self.navigationController pushViewController:moneyViewController animated:YES];
    
}

-(void)fetchTableData {
    NSArray* tmp = self.currentTherapy[@"items"];//[DataBase getDBArray:@"photo" withKey:@"patientid" andValue:self.currentPatient[@"patientid"]];
    
    NSMutableArray* tmp_sections = [[NSMutableArray alloc] init];
    
    for(NSDictionary* pat in tmp)
    {
        
        //nmd[@"index"] = [[(NSString*)pat[@"firstname"] substringWithRange:NSMakeRange(0, 1)] uppercaseString];
        [tmp_sections addObject:pat[@"categoryid"]];
    }
    
    sections = [[NSMutableArray alloc] init];
    for(NSString* hd in [tmp_sections valueForKeyPath:@"@distinctUnionOfObjects.self"])
    {
        NSMutableDictionary* ndms = [[NSMutableDictionary alloc] init];
        ndms[@"section"] = hd;
        NSMutableArray* mna=[[NSMutableArray alloc] init];
        for(NSDictionary* dict in tmp)
        {
            if([[(NSString*)dict[@"categoryid"] uppercaseString] isEqualToString:[hd uppercaseString]])
            {
                [mna addObject:dict];
            }
        }
        ndms[@"content"]=mna;
        [sections addObject:ndms];
    }
    
    __block UITableView *curTableViewBlock = self.optionTableView;
    __block NSMutableArray *ars = sections;
    __block NSMutableArray *self_items = self.currentTherapy[@"items"];
    
    
    self.toothMapScroller.didChooseToothId = ^(NSIndexPath *indexPath, NSInteger toothId)
    {
        if(self.optionTableMode == kOptionTableShortMode)
        {
            NSMutableArray *array = ars;
            NSMutableDictionary *buyRef = array[indexPath.section][@"content"][indexPath.row];
            buyRef[@"toothid"] = @(toothId);
            
            for(NSMutableDictionary* d in self_items)
            {
                if([d[@"id"] isEqualToString:buyRef[@"id"]])
                {
                    d[@"toothid"] = @(toothId);
                }
            }
            
            [curTableViewBlock reloadData];
        }
        else
        {
            NSMutableArray *array = ars;
            NSMutableDictionary *buyRef = array[indexPath.section][@"content"][indexPath.row];
            buyRef[@"toothid"] = @(toothId);
            
            for(NSMutableDictionary* d in self_items)
            {
                if([d[@"id"] isEqualToString:buyRef[@"id"]])
                {
                    d[@"toothid"] = @(toothId);
                }
            }
            
            [curTableViewBlock reloadData];
        }
        
        [self.toothMapActionSheet removeFromView];
    };
    
    [self updateBuyInfo];
    
    self.commentTextView1.text = self.currentTherapy[@"comment"];
    //self.commentTextView.text = self.currentTherapy[@"comment"];

}

#pragma mark -
#pragma mark Option Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(self.optionTableMode == kOptionTableShortMode)   {
        return 2;
    }
    
    NSInteger count = sections.count;//[[self.fetchedResultsController sections] count];
    return count;// + 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.optionTableMode == kOptionTableShortMode)   {
        if(section == 0)    {
            NSInteger cnt = 0;
            NSArray *array = self.currentTherapy[@"items"];//sections[section][@"content"];//[TherapyItem MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"therapy == %@", self.currentTherapy]];
            cnt = array.count;
            if(array.count > 1) cnt = 1;
            
            return cnt;
        }
        return 1;
    }
    
    NSInteger count = sections.count;
    if(section == count)    {
        return 1;
    }
    
    //id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    //NSInteger count2 = [sectionInfo numberOfObjects];
    //return count2;
    NSArray *array = sections[section][@"content"];
    return array.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section    {
    
    if(self.optionTableMode == kOptionTableShortMode)   {
        return [UIView new];
    }
    
    NSInteger count = sections.count;//[[self.fetchedResultsController sections] count];
    if(section == count)    {
        return [UIView new];
    }
    
    //id <NSFetchedResultsSectionInfo> theSection = [[self.fetchedResultsController sections] objectAtIndex:section];
    //NSDictionary *buyRef = sections[section][@"content"][0];
    NSDictionary *categoryRef = (NSDictionary*)[DataBase getDB:@"services_group" withKey:@"id" andValue:sections[section][@"section"]];//[CategoryTherapy MR_findFirstByAttribute:@"remoteId" withValue:buyRef.categoryId];
    
    UIView* hderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 25)];
    
    hderView.backgroundColor = [UIColor whiteColor];
    
    UILabel* headerLabel = [[UILabel alloc] init];
    headerLabel.frame = CGRectMake(10, 2, tableView.frame.size.width - 10, 18);
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.textColor = [UIColor colorWithWhite:74.0f/255.0f alpha:1.0f];
    headerLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:13];
    headerLabel.text = categoryRef[@"title"];
    headerLabel.textAlignment = NSTextAlignmentLeft;
    
    [hderView addSubview:headerLabel];
    
    return hderView;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section   {
    if(self.optionTableMode == kOptionTableShortMode)   {
        return 1;
    }
    
    return 25;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section   {
    if(self.optionTableMode == kOptionTableShortMode)   {
        return 1;
    }
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath  {
    if(self.optionTableMode == kOptionTableShortMode)   {
        if(indexPath.section == 0)  {
            return 32.0;
        }
        
        return 88.0;
    }
    
    NSInteger count = sections.count;//((NSArray*)self.currentTherapy[@"items"]).count;
    if(indexPath.section == count)
    {
        if(self.isTherapyCreateMode)
        {
            if(self.isPlan)
            {
                return 314;
            }
            else
            {
                return 314;
            }
        }
        else
        {
            if(self.isPlan)
            {
                return 164;
            }
            else
            {
                return 314;
            }
        }
    }
    
    return 32.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        
    NSInteger count = sections.count;//[[self.fetchedResultsController sections] count];
    if(indexPath.section == count)    {
        return self.infoCell;
    }
    
    NSDictionary *buyRef = sections[indexPath.section][@"content"][indexPath.row];//self.currentTherapy[@"items"][indexPath.row];
    NSArray* ser_arr = [DataBase getDBArray:@"services" withKey:@"id" andValue:buyRef[@"serviceid"]];
    NSDictionary *serviceRef = ser_arr==nil?nil:ser_arr[0];
    
    ChoosedServiceViewCell *chooseCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ChoosedServiceViewCell class])];
    chooseCell.countTooth.text = [NSString stringWithFormat:@"%ld", [buyRef[@"toothid"] integerValue]];
    chooseCell.nameLabel.text = serviceRef[@"title"];
    chooseCell.countService.text = [NSString stringWithFormat:@"%ld", [buyRef[@"countt"] integerValue]];
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setGroupingSeparator:@" "];
    [numberFormatter setGroupingSize:3];
    [numberFormatter setUsesGroupingSeparator:YES];
    [numberFormatter setDecimalSeparator:@"."];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [numberFormatter setMaximumFractionDigits:2];
    
    //cell.costLabel.text = [numberFormatter stringFromNumber:@((long)dataObject.cost)];
    
    chooseCell.costService.text = [numberFormatter stringFromNumber:@([serviceRef[@"cost"] floatValue])];//[serviceRef[@"cost"] stringValue];
    chooseCell.indexPath = indexPath;
    
    if(self.convert_mode&&indexPath.row%2==0)
    {
        chooseCell.contentView.backgroundColor = [UIColor colorWithWhite:239.0f/255.0f alpha:1];
    }
    else
    {
        chooseCell.contentView.backgroundColor = [UIColor colorWithWhite:255.0f/255.0f alpha:1];
    }
    
    if(!self.isTherapyCreateMode&&self.isPlan)
    {
    }
    else
    {
        chooseCell.didClickToothMap = ^(NSIndexPath *indexPath)
        {
            self.toothMapScroller.indexPath = indexPath;
            self.toothMapScroller.willSelectToothIndex = [buyRef[@"toothid"] integerValue];
            self.toothMapActionSheet = [[WLPActionSheet alloc] initWithTitle:@"Выбор зуба" cancelButtonTitle:@"Отмена" destructiveButtonTitle:(self.toothMapScroller.willSelectToothIndex > 0)?@"Удалить":nil andMoreButton:!self.isPlan?@"Изменить основной диагноз":nil withViewController:self.toothMapScroller];
            //[[WLPActionSheet alloc] initWithTitle:@"Выбор зуба" cancelButtonTitle:@"Отмена" destructiveButtonTitle:(self.toothMapScroller.willSelectToothIndex > 0)?@"Удалить":nil withViewController:self.toothMapScroller];
            self.toothMapActionSheet.tag = 2;
            self.toothMapActionSheet.delegate = self.toothMapScroller;
            [self.toothMapActionSheet showInView:self.view];
        };
        
        chooseCell.didClickCounter = ^(NSIndexPath *indexPath)
        {
            self.toothCounter.indexPath = indexPath;
            self.toothCounterActionSheet = [[WLPActionSheet alloc] initWithTitle:@"Количество" cancelButtonTitle:@"OK" destructiveButtonTitle:nil withViewController:self.toothCounter];
            self.toothCounterActionSheet.tag = 3;
            self.toothCounterActionSheet.delegate = self;
            [self.toothCounterActionSheet showInView:self.view];
        };
        
        chooseCell.didClickDelete = ^(NSIndexPath *indexPath)
        {
            NSString* idi=((NSMutableArray*)sections[indexPath.section][@"content"])[indexPath.row][@"id"];
            id to_remove=nil;
            for(NSDictionary *d in self.currentTherapy[@"items"])
            {
                if([d[@"id"] isEqualToString:idi])
                {
                    to_remove=d;
                    break;
                }
            }
            
            if(to_remove!=nil)
            {
                [self.currentTherapy[@"items"] removeObject:to_remove];
                [self fetchTableData];
                [self updateBuyInfo];
                [self.optionTableView reloadData];
                selectedRow = nil;
            }
            
        };
    }
    
    if(selectedRow!=nil)
    {
        if(selectedRow.section == indexPath.section && selectedRow.row == indexPath.row)
        {
            chooseCell.deleteBtn.hidden=NO;
            chooseCell.delImg.hidden=NO;
        }
        else
        {
            chooseCell.deleteBtn.hidden=YES;
            chooseCell.delImg.hidden=YES;
        }
    }
    else
    {
        chooseCell.deleteBtn.hidden=YES;
        chooseCell.delImg.hidden=YES;
    }
    return chooseCell;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*if(!self.isTherapyCreateMode&&self.isPlan)
    {
    }
    else
    {*/
    if((self.isTherapyCreateMode&&self.isPlan)||(!self.isPlan))
    {
        selectedRow = indexPath;
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
        [self.optionTableView reloadData];
    }
    /*}*/
    
    [commentTextView resignFirstResponder];
}

-(void) updateBuyInfo  {
    float cost = 0;
    
    for(NSDictionary *buyRef in self.currentTherapy[@"items"])
    {
        NSDictionary *serviceRef = [DataBase getDBArray:@"services" withKey:@"id" andValue:buyRef[@"serviceid"]][0];
        cost += [serviceRef[@"cost"] integerValue] * [buyRef[@"countt"] integerValue];
    }
    
    realCost = cost;
    realCostWithDiscount = cost - ((cost * discountValue)/100);
    
    self.currentTherapy[@"total_cost"] = [NSString stringWithFormat:@"%.2f", cost];
    self.currentTherapy[@"total_cost_with_discount"] = [NSString stringWithFormat:@"%.2f", realCostWithDiscount];
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setGroupingSeparator:@" "];
    [numberFormatter setGroupingSize:3];
    [numberFormatter setUsesGroupingSeparator:YES];
    [numberFormatter setDecimalSeparator:@"."];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [numberFormatter setMaximumFractionDigits:2];
    
    self.sumWithoutDiscountLabel.text = [NSString stringWithFormat:@"%@ ₽", [numberFormatter stringFromNumber:@(cost)]];
    self.sumWithDiscountLabel.text =  [NSString stringWithFormat:@"%@ ₽", [numberFormatter stringFromNumber:@(realCostWithDiscount)]];
    self.sumWithDiscountLabel2.text = self.sumWithDiscountLabel.text;
    
    //self.currentTherapy[@"discount"] = @(discountValue);
    
    self.discountLabel.text = [NSString stringWithFormat:@"%ld%%", (long)discountValue];

}

- (IBAction)onDiscount:(id)sender {
    self.discountCounterActionSheet = [[WLPActionSheet alloc] initWithTitle:@"Скидка (%)" cancelButtonTitle:@"ОК" destructiveButtonTitle:nil withViewController:self.discountCounter];
    self.discountCounterActionSheet.tag = 1;
    self.discountCounterActionSheet.delegate = self;
    [self.discountCounterActionSheet showInView:self.view];
}

-(void)actionSheetDidDone:(WLPActionSheet*)actionSheet
{
    if(actionSheet.tag == 1)
    {   //Скидка
        discountValue = self.discountCounter.toothCount;
        self.currentTherapy[@"discount"] = @(discountValue);
        
        self.discountLabel.text = [NSString stringWithFormat:@"%ld%%", (long)discountValue];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"needUpdateBuyInfo" object:nil];
        
        [self updateBuyInfo];
    }
    else if(actionSheet.tag == 2)
    {   //Выбор зуба
        
    }
    else if(actionSheet.tag == 3)
    {   //Кол-во
        if(self.optionTableMode == kOptionTableShortMode)
        {
            NSMutableArray *array = self.currentTherapy[@"items"];
            NSMutableDictionary *buyRef = array[0];
            buyRef[@"countt"] = @(self.toothCounter.toothCount);
            [self.optionTableView reloadData];
        }
        else
        {
            
            NSMutableDictionary *buyRef = sections[self.toothCounter.indexPath.section][@"content"][self.toothCounter.indexPath.row];
            
            for(NSMutableDictionary* md in self.currentTherapy[@"items"])
            {
                if([md[@"id"] isEqualToString:buyRef[@"id"]])
                {
                    md[@"countt"] = @(self.toothCounter.toothCount);
                }
            }
            
            buyRef[@"countt"] = @(self.toothCounter.toothCount);
            [self fetchTableData];
            [self.optionTableView reloadData];
        }
    }
}

-(void)actionSheetDidMore:(WLPActionSheet*)actionSheet
{
    if(actionSheet.tag == 2)
    {   //Выбор зуба
        NSMutableArray* tooths = [NSMutableArray arrayWithArray:[DataBase getDBArray:@"toothmap" withKey:@"patientid" andValue:self.currentPatient[@"patientid"]]];
        if(tooths.count>0)
        {
            /*NSDictionary* toothList;
            if(self.cardCopied)
                toothList = tooths[tooths.count-1];
            else
            {
                toothList = [self copyToothCard:tooths[tooths.count-1]];
                self.cardCopied = YES;
            }*/
            ToothCardViewController *toothCardVC = [[ToothCardViewController alloc] initShowToothListForce:tooths[tooths.count-1] withPatient:self.currentPatient];
            [self.navigationController pushViewController:toothCardVC animated:YES];
        }
    }
}

- (NSMutableDictionary*) copyToothCard:(NSDictionary*) card
{
    NSMutableDictionary* copied = [NSMutableDictionary dictionaryWithDictionary:card];
    copied[@"id"]=GUIDString();
    copied[@"immutable"] = @"false";
    
    [[ToothManager shared] updateToothMap:copied success:^{

    } failure:^{
    }];
    
    return copied;
}

-(void)actionSheetDidDissapear:(WLPActionSheet*)actionSheet   {
    NSLog(@"DESTRUCTIVE");
}

- (IBAction)tapApply:(id)sender
{
    NSArray *arr = self.currentTherapy[@"items"];//[TherapyItem MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"therapy == %@", self.currentTherapy]];
    if(arr.count == 0)
        return;
    
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd.MM.yyyy"];
    self.currentTherapy[@"id"] = GUIDString();
    self.currentTherapy[@"startdate"] = [dateFormatter stringFromDate:[NSDate date]];
    [DataBase putDB:self.currentTherapy toTable:@"visits" withKey:@"id"];
    self.isPlan = NO;
    self.isTherapyCreateMode = YES;
    
    NSArray* toothcards = [NSMutableArray arrayWithArray:[DataBase getDBArray:@"toothmap" withKey:@"patientid" andValue:self.currentPatient[@"patientid"]]];
    
    if(self.isPlan)
    {
        self.toothMapScroller = [[DBToothMapScroller alloc] initWithToothMapMode:kToothMapModeCombine];
        
    }
    else
    {
        if(toothcards.count==0)
        {
            self.toothMapScroller = [[DBToothMapScroller alloc] initWithPatient:self.currentPatient];
        }
        else
        {
            self.toothMapScroller = [[DBToothMapScroller alloc] initWithToothCard:toothcards[toothcards.count-1]];
        }
    }
    
    self.toothMapScroller.delegate = self;
    
    __block UITableView *curTableViewBlock = self.optionTableView;
    __block NSMutableArray *ars = sections;
    __block NSMutableArray *self_items = self.currentTherapy[@"items"];
    
    self.toothMapScroller.didChooseToothId = ^(NSIndexPath *indexPath, NSInteger toothId)
    {
        if(self.optionTableMode == kOptionTableShortMode)
        {
            NSMutableArray *array = ars;
            NSMutableDictionary *buyRef = array[indexPath.section][@"content"][indexPath.row];
            buyRef[@"toothid"] = @(toothId);
            
            for(NSMutableDictionary* d in self_items)
            {
                if([d[@"id"] isEqualToString:buyRef[@"id"]])
                {
                    d[@"toothid"] = @(toothId);
                }
            }
            
            [curTableViewBlock reloadData];
        }
        else
        {
            NSMutableArray *array = ars;
            NSMutableDictionary *buyRef = array[indexPath.section][@"content"][indexPath.row];
            buyRef[@"toothid"] = @(toothId);
            
            for(NSMutableDictionary* d in self_items)
            {
                if([d[@"id"] isEqualToString:buyRef[@"id"]])
                {
                    d[@"toothid"] = @(toothId);
                }
            }
            
            [curTableViewBlock reloadData];
        }
        
        [self.toothMapActionSheet removeFromView];
    };

    
    [self adjustVisibility:YES];
}

- (IBAction)tabPriceList:(id)sender {
    NormalItemsViewController *editTherapyViewController = [[NormalItemsViewController alloc] initWithTherapy:self.currentTherapy];
    editTherapyViewController.isPlan = self.isPlan;
    editTherapyViewController.isTherapyCreateMode = self.isTherapyCreateMode;
    [self presentTransparentViewController:editTherapyViewController animated:YES completion:nil];
}

- (IBAction)tapEditPlanName:(id)sender
{
    if(self.isPlan == YES)
    {
        /*UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Введите название плана"
                                                        message:@"fgsdfgrsd"
                                                       delegate:self
                                              cancelButtonTitle:@"Готово"
                                              otherButtonTitles:nil];
        alert.alertViewStyle = UIAlertViewStylePlainTextInput;
        [alert textFieldAtIndex:0].layer.sublayerTransform = CATransform3DMakeTranslation(-10, 0, 0);
        [alert show];*/
        URBAlertView *textAlertView = [URBAlertView dialogWithTitle:@"Введите название плана" message:nil];
        [textAlertView addButtonWithTitle:@"Отмена"];
        [textAlertView addButtonWithTitle:@"OK"];
        [textAlertView setTitleFont:[UIFont boldSystemFontOfSize:14.0f]];
        [textAlertView setTitleColor:[UIColor colorWithWhite:74.0f/255.0f alpha:1.0f]];
        [textAlertView setStrokeColor:[UIColor whiteColor]];
        [textAlertView setBackgroundColor:[UIColor whiteColor]];
        [textAlertView setBackgroundGradation:0];
        [textAlertView setTitleShadowColor:[UIColor clearColor]];
        [textAlertView setButtonStrokeColor:[UIColor whiteColor]];
        [textAlertView setButtonBackgroundColor:[UIColor whiteColor]];
        [textAlertView setButtonTextAttributes:@{NSForegroundColorAttributeName : [UIColor colorWithRed:20.0f/255.0f green:162.0f/255.0f blue:243.0f/255.0f alpha:1.0f], NSFontAttributeName: [UIFont systemFontOfSize:12.0f], UITextAttributeTextColor: [UIColor colorWithRed:20.0f/255.0f green:162.0f/255.0f blue:243.0f/255.0f alpha:1.0f]} forState:UIControlStateNormal];
        [textAlertView setButtonTextAttributes:@{NSForegroundColorAttributeName : [UIColor colorWithRed:20.0f/255.0f green:162.0f/255.0f blue:243.0f/255.0f alpha:1.0f], NSFontAttributeName: [UIFont systemFontOfSize:12.0f], UITextAttributeTextColor: [UIColor colorWithRed:20.0f/255.0f green:162.0f/255.0f blue:243.0f/255.0f alpha:1.0f]} forState:UIControlStateAll];
        [textAlertView setCancelButtonTextAttributes:@{NSForegroundColorAttributeName : [UIColor colorWithRed:20.0f/255.0f green:162.0f/255.0f blue:243.0f/255.0f alpha:1.0f], NSFontAttributeName: [UIFont systemFontOfSize:12.0f], UITextAttributeTextColor: [UIColor colorWithRed:20.0f/255.0f green:162.0f/255.0f blue:243.0f/255.0f alpha:1.0f]} forState:UIControlStateAll];
        
        
        [textAlertView addTextFieldWithPlaceholder:@"Название плана" secure:NO];
        [textAlertView setHandlerBlock:^(NSInteger buttonIndex, URBAlertView *alertView) {
            NSLog(@"button tapped: index=%i, text=%@", buttonIndex, [alertView textForTextFieldAtIndex:0]);
            if(buttonIndex == 1)
            {
                self.currentTherapy[@"name"] = [alertView textForTextFieldAtIndex:0];
                planNameLabel.text=self.currentTherapy[@"name"];
            }
            [alertView hideWithCompletionBlock:^{
                // stub
            }];
        }];
        [textAlertView showWithAnimation:URBAlertAnimationDefault];
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"%@", [alertView textFieldAtIndex:0].text);
    
    self.currentTherapy[@"name"] = [alertView textFieldAtIndex:0].text;
    planNameLabel.text=self.currentTherapy[@"name"];
}

-(void)willPresentAlertView:(UIAlertView *)alertView{
    UILabel *theTitle = [alertView valueForKey:@"_titleLabel"];
    theTitle.font = [UIFont boldSystemFontOfSize:14];
    [theTitle setTextColor:[UIColor colorWithWhite:74.0f/255.0f alpha:1]];
    
    UITextField* tf = [alertView textFieldAtIndex:0];
    tf.textColor =[UIColor colorWithWhite:74.0f/255.0f alpha:1];
}

- (IBAction)tapPay:(id)sender
{
    MoneyDepositViewController *moneyViewController = [[MoneyDepositViewController alloc] initWithPatient:self.currentPatient withMoney:realCostWithDiscount];
    [self.navigationController pushViewController:moneyViewController animated:YES];
}

- (CGFloat) AACStatusBarHeight
{
    CGSize statusBarSize = [[UIApplication sharedApplication] statusBarFrame].size;
    return MIN(statusBarSize.width, statusBarSize.height);
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.remembered_frame = self.optionInfoBoxView.frame;
        CGRect f = self.optionInfoBoxView.frame;
        f.origin.y -= keyboardSize.height-self.navigationController.navigationBar.frame.size.height-[self AACStatusBarHeight];
        self.optionInfoBoxView.frame = f;
    }];
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    
    [UIView animateWithDuration:0.3 animations:^{
        //CGRect f = self.optionInfoBoxView.frame;
        //f.origin.y = 0.0f+self.navigationController.navigationBar.frame.size.height+[self AACStatusBarHeight];
        self.optionInfoBoxView.frame = self.remembered_frame;
    }];
    
}

- (void) callToothMap
{
    NSMutableArray* tooths = [NSMutableArray arrayWithArray:[DataBase getDBArray:@"toothmap" withKey:@"patientid" andValue:self.currentPatient[@"patientid"]]];
    if(tooths.count>0)
    {
        NSMutableArray* tooths = [NSMutableArray arrayWithArray:[DataBase getDBArray:@"toothmap" withKey:@"patientid" andValue:self.currentPatient[@"patientid"]]];
        if(tooths.count>0)
        {
            /*NSDictionary* toothList;
            if(self.cardCopied)
                toothList = tooths[tooths.count-1];
            else
            {
                toothList = [self copyToothCard:tooths[tooths.count-1]];
                self.cardCopied = YES;
            }*/
            ToothCardViewController *toothCardVC = [[ToothCardViewController alloc] initShowToothListForce:tooths[tooths.count-1] withPatient:self.currentPatient];
            [self.navigationController pushViewController:toothCardVC animated:YES];
        }
    }
}
- (IBAction)tapExport:(id)sender {
    ShareViewController *editTherapyViewController = [[ShareViewController alloc] init];
    
    [self presentTransparentViewController:editTherapyViewController animated:NO completion:nil];
}

//////
/////////////////////////////////////
/////////////////////////////////////
- (UIImage *)captureView:(UIView*) view {
    
    //hide controls if needed
    UIGraphicsBeginImageContext(CGSizeMake(view.frame.size.width, view.frame.size.height));
    [view drawViewHierarchyInRect:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height) afterScreenUpdates:YES];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
    
}

- (UIImage *)joinImages:(UIImage *)im1 secondImage:(UIImage *)im2
{
    //Joins 3 UIImages together, stitching them vertically
    CGSize size = CGSizeMake(im1.size.width, im1.size.height+im2.size.height);
    UIImage* im3 = [im2 copy];
    
    CGFloat sz = im1.size.width;
    sz = im2.size.width;
    sz = [PercUtils imageWithImage:im2 scaledToSize:CGSizeMake(im1.size.width, im2.size.height)].size.width;
    
    if(im1.size.width!=im2.size.width)
    {
        im3 = [PercUtils imageWithImage:im2 scaledToSize:CGSizeMake(im1.size.width, im2.size.height*im1.size.width/im2.size.width)];
    }
    
    UIGraphicsBeginImageContext(size);
    
    CGPoint image1Point = CGPointMake(0, 0);
    [im1 drawAtPoint:image1Point];
    
    CGPoint image2Point = CGPointMake(0, im1.size.height);
    [im3 drawAtPoint:image2Point];
    
    UIImage* finalImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return finalImage;
}

- (void) printItem
{
   
    UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];
    //pic.delegate = self;
    
    
    UIPrintInfo *printInfo = [UIPrintInfo printInfo];
    printInfo.outputType = UIPrintInfoOutputGeneral;
    printInfo.jobName = @"PrintJob";
    pic.printInfo = printInfo;
    
    
    pic.showsPageRange = YES;
    pic.printingItem = [self prepareImage];
    
    
    void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) = ^(UIPrintInteractionController *printController, BOOL completed, NSError *error) {
        if (!completed && error) {
            NSLog(@"Printing could not complete because of error: %@", error);
        }
    };
    
    
    [pic presentAnimated:YES completionHandler:completionHandler];
}

- (void) onPrint
{
    if(!self.therapiesToConvert)
    {
    [self printItem];
    }
}

- (UIImage*) prepareImageX
{
    
    UIImage* img = [self prepareImage];
    [self dismissViewControllerAnimated:NO completion:^{}];
    return img;
}

- (UIImage*) prepareTableImage
{
    self.convert_mode = YES;
    UIImage* img = [self.optionTableView imageFromTableView];//[self joinImages:[self captureView: planNameView] secondImage:[self.optionTableView imageFromTableView]];
    
    //[self dismissViewControllerAnimated:NO completion:^{}];
    return img;
}

/*- (UIImage*) prepareHeadImage
{
    self.convert_mode = YES;
    headerView.hidden = NO;
    fioLabel.text = getProfileFio();
    positionLabel.text = getProfilePosition();
    phoneLabel.text =  [NSString stringWithFormat:@"Тел: %@", getProfilePhone()];
    emailLabel.text =  [NSString stringWithFormat:@"E-mail: %@", getProfileEmail()];
    addressLabel.text =  [NSString stringWithFormat:@"Адрес: %@", getProfileAddress()];
    clinicLabel.text =  getProfileClinic();
    licenseLabel.text =  [NSString stringWithFormat:@"Лицензия: %@", getProfileLicense()];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd.MM.yyyy"];
    titleLabel.text =  [NSString stringWithFormat:@"Акт по выполненным работам от %@", [dateFormatter stringFromDate:[NSDate date]] ];
    patientLabel.text =  [NSString stringWithFormat:@"%@ %@ %@ дата рождения: %@", self.currentPatient[@"firstname"],self.currentPatient[@"middlename"],self.currentPatient[@"lastname"], self.currentPatient[@"birthdate"]];
    
    return [self captureView:headerView];
}*/

- (UIImage*) prepareImage
{
    headerView.hidden = NO;
    footerView.hidden = NO;
    self.convert_mode = YES;
    fioLabel.text = getProfileFio();
    positionLabel.text = getProfilePosition();
    phoneLabel.text =  [NSString stringWithFormat:@"Тел: %@", getProfilePhone()];
    emailLabel.text =  [NSString stringWithFormat:@"E-mail: %@", getProfileEmail()];
    addressLabel.text =  [NSString stringWithFormat:@"Адрес: %@", getProfileAddress()];
    clinicLabel.text =  getProfileClinic();
    licenseLabel.text =  [NSString stringWithFormat:@"Лицензия: %@", getProfileLicense()];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd.MM.yyyy"];
    titleLabel.text =  [NSString stringWithFormat:@"Акт по выполненным работам от %@", [dateFormatter stringFromDate:[NSDate date]] ];
    patientLabel.text =  [NSString stringWithFormat:@"%@ %@ %@ дата рождения: %@", self.currentPatient[@"firstname"],self.currentPatient[@"middlename"],self.currentPatient[@"lastname"], self.currentPatient[@"birthdate"]];
    
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setGroupingSeparator:@" "];
    [numberFormatter setGroupingSize:3];
    [numberFormatter setUsesGroupingSeparator:YES];
    [numberFormatter setDecimalSeparator:@"."];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [numberFormatter setMaximumFractionDigits:2];
    
    
    totalLabel.text = [NSString stringWithFormat:@"%@ ₽", [numberFormatter stringFromNumber:@(realCost)]];
    footerDiscountLabel.text = [NSString stringWithFormat:@"%d", (int)discountValue];
    totalWithDiscountLabel.text = [NSString stringWithFormat:@"%@ ₽", [numberFormatter stringFromNumber:@(realCostWithDiscount)]];
    
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    //NSString *imageSubdirectory = [documentsDirectory stringByAppendingPathComponent:@"logo"];
    
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"logo.png"];
    
    // Convert UIImage object into NSData (a wrapper for a stream of bytes) formatted according to PNG spec
    //NSData *imageData = [NSData dataWithContentsOfFile:filePath]; //UIImagePNGRepresentation(photo);
    //[imageData writeToFile:filePath atomically:YES];
    UIImage* zoip = [UIImage imageWithContentsOfFile:filePath];
    logoImageView.image = zoip;
    //headerView.hidden = YES;
    //footerView.hidden = NO;
    
    tableHeadView.backgroundColor = [UIColor whiteColor];
    UIImage* img = [self joinImages:[self joinImages:[self joinImages:[self captureView:headerView] secondImage:[self captureView:tableHeadView]] secondImage:[self.optionTableView imageFromTableView]] secondImage:[self captureView:footerView]];
    headerView.hidden = YES;
    footerView.hidden = YES;
    tableHeadView.backgroundColor = [UIColor colorWithWhite:247 alpha:1];
    self.convert_mode = NO;
    
    return img;
}

- (void) onConvert
{
    if(!self.therapiesToConvert)
    {
    UIImageWriteToSavedPhotosAlbum([self prepareImage], nil, nil, nil);
    
    [self.view makeToast:[NSString stringWithFormat:@"Изображение сохранено в галерее"]];
    }
}

- (void)showEmail {
    
    NSString *emailTitle = [NSString stringWithFormat:@"%@ %@ %@", self.currentPatient[@"firstname"], self.currentPatient[@"middlename"], self.currentPatient[@"lastname"]];
    NSString *messageBody = @"";
    NSArray *toRecipents = [[NSArray alloc] init];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    if([MFMailComposeViewController canSendMail])
    {
        mc.mailComposeDelegate = self;
        [mc setSubject:emailTitle];
        [mc setMessageBody:messageBody isHTML:NO];
        [mc setToRecipients:toRecipents];
        [mc.navigationBar setTintColor:getColorSchemeBar()];
        
        
        
        UIImage* img = [self prepareImage];
        
        // Get the resource path and read the file using NSData
        NSData *fileData = UIImagePNGRepresentation(img);// = [NSData dataWithContentsOfFile:filePath];
        
        // Determine the MIME type
        NSString *mimeType = @"image/png";
        
        // Add attachment
        [mc addAttachmentData:fileData mimeType:mimeType fileName:@"test"];
        // Present mail view controller on screen
        [self presentViewController:mc animated:YES completion:NULL];
    }
    
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

- (void) onEmail
{
    if(!self.therapiesToConvert)
    {
    [self showEmail];
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



@end
