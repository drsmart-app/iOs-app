//
//  TherapyReadViewController.h
//  DentaBook
//
//  Created by Denis Dubov on 27.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Therapy;

@interface TherapyReadViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *patientLabel;

-(id) initWithTherapy:(NSDictionary*)therapy;
@property (nonatomic, retain) NSMutableArray* sections;
@end
