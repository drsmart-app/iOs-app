//
//  NormalItemsViewController.h
//  DentaBook
//
//  Created by Mac Mini on 27.10.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RATreeView.h"
@interface NormalItemsViewController : UIViewController<RATreeViewDelegate, RATreeViewDataSource, UITableViewDelegate, UITableViewDataSource>
-(id) initWithTherapy:(NSDictionary*)therapy;
@property (nonatomic, retain) IBOutlet RATreeView* treeView;
- (IBAction)onTapPriceList:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *sumWithDiscountLabel;
- (IBAction)tapChangeName:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *optionView;
@property (weak, nonatomic) NSIndexPath* selectedRow;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (nonatomic) BOOL isPlan;
@property (nonatomic) BOOL isTherapyCreateMode;
@property (weak, nonatomic) IBOutlet UIView *subView;

@end
