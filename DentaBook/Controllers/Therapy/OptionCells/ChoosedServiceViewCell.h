//
//  ChoosedServiceViewCell.h
//  DentaBook
//
//  Created by Denis Dubov on 25.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"

@interface ChoosedServiceViewCell : UITableViewCell

@property (nonatomic, strong) NSIndexPath *indexPath;

@property (weak, nonatomic) IBOutlet UILabel *countTooth;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *countService;
@property (weak, nonatomic) IBOutlet UILabel *costService;

@property (copy, nonatomic) void (^didClickToothMap)(NSIndexPath *indexPath);
@property (copy, nonatomic) void (^didClickCounter)(NSIndexPath *indexPath);
@property (copy, nonatomic) void (^didClickDelete)(NSIndexPath *indexPath);
- (IBAction)opTapDelete:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (weak, nonatomic) IBOutlet UIImageView *delImg;

@end
