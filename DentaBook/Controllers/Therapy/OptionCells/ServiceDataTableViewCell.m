//
//  ServiceDataTableViewCell.m
//  DentaBook
//
//  Created by Denis Dubov on 25.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "ServiceDataTableViewCell.h"

@implementation ServiceDataTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) setDetailMode:(NSString*)leftText rightText:(NSString*)rightText    {
    self.leftLabel.hidden = NO;
    self.rightLabel.hidden = NO;
    self.saveButton.hidden = YES;
    
    self.leftLabel.text = leftText;
    self.rightLabel.text = rightText;
}

-(void) setButtonMode   {
    self.leftLabel.hidden = YES;
    self.rightLabel.hidden = YES;
    self.saveButton.hidden = NO;
}

@end
