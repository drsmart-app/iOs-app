//
//  ChoosedServiceViewCell.m
//  DentaBook
//
//  Created by Denis Dubov on 25.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "ChoosedServiceViewCell.h"

@implementation ChoosedServiceViewCell

- (void)awakeFromNib {
    // Initialization code
    
}

-(void) layoutSubviews
{
    /*if(!self.didClickDelete)
    {
        self.deleteBtn.hidden = YES;
    }
    else
    {
        self.deleteBtn.hidden = NO;
    }*/
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (IBAction)onToothMap:(id)sender {
    if(self.didClickToothMap)   {
        self.didClickToothMap(self.indexPath);
    }
}

- (IBAction)onCounter:(id)sender {
    if(self.didClickCounter)    {
        self.didClickCounter(self.indexPath);
    }
}

- (IBAction)opTapDelete:(id)sender {
    if(self.didClickDelete)    {
        self.didClickDelete(self.indexPath);
    }
}
@end
