//
//  CategoryViewCell.h
//  DentaTest
//
//  Created by Denis Dubov on 04.08.15.
//  Copyright (c) 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *costLabel;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImageView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftTitleConstraint;
@end
