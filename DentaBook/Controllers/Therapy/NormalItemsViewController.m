//
//  NormalItemsViewController.m
//  DentaBook
//
//  Created by Mac Mini on 27.10.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "NormalItemsViewController.h"
#import "CategoryViewCell.h"
#import "DataBase.h"
#import "RADataObject.h"
#import "ChoosedServiceViewCell.h"
#import "DBToothMapScroller.h"
#import "DbToothCounter.h"
#import "ToothCardViewController.h"
#import "HttpUtil.h"

@interface NormalItemsViewController ()
@property (nonatomic, strong) NSMutableDictionary *currentTherapy;
@property (nonatomic, strong) NSMutableArray *data;
@property (nonatomic, strong) WLPActionSheet *toothMapActionSheet;
@property (nonatomic, strong) DBToothMapScroller *toothMapScroller;

@property (nonatomic, strong) WLPActionSheet *toothCounterActionSheet;
@property (nonatomic, strong) DbToothCounter *toothCounter;

@property (nonatomic, strong) WLPActionSheet *discountCounterActionSheet;
@property (nonatomic, strong) DbToothCounter *discountCounter;
@property (nonatomic, strong) NSDictionary* currentPatient;
@property (nonatomic) int selectedTooth;
@end

@implementation NormalItemsViewController
@synthesize optionView, selectedRow, nameLabel, subView;
-(id) initWithTherapy:(NSMutableDictionary*)therapy {
    self = [super initWithNibName:@"NormalItemsViewController" bundle:[NSBundle mainBundle]];
    if(self)    {
        self.currentTherapy = therapy;
        
        self.currentPatient = [DataBase getDB:@"patient" withKey:@"patientid" andValue:self.currentTherapy[@"patientid"]];
        self.selectedTooth=0;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateBuyInfo) name:@"needUpdateBuyInfo" object:nil];
    self.treeView.delegate = self;
    self.treeView.dataSource = self;
    self.treeView.separatorStyle = RATreeViewCellSeparatorStyleSingleLine;
    [self.treeView reloadData];
    [self.treeView setBackgroundColor:[UIColor colorWithRed:232.0/255.0 green:236.0/255.0 blue:240.0/255.0 alpha:1.0]];
    [self.treeView registerNib:[UINib nibWithNibName:NSStringFromClass([CategoryViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([CategoryViewCell class])];
    [self.optionView registerNib:[UINib nibWithNibName:NSStringFromClass([ChoosedServiceViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ChoosedServiceViewCell class])];
    self.optionView.delegate = self;
    self.optionView.dataSource = self;
    nameLabel.text=self.currentTherapy[@"name"];
    // Do any additional setup after loading the view from its nib.
    
    NSArray* toothcards = [NSMutableArray arrayWithArray:[DataBase getDBArray:@"toothmap" withKey:@"patientid" andValue:self.currentPatient[@"patientid"]]];
    
    if(self.isPlan)
    {
        if(toothcards.count==0)
        {
            self.toothMapScroller = [[DBToothMapScroller alloc] initWithToothMapMode:kToothMapModeCombine];
        }
        else
        {
            self.toothMapScroller = [[DBToothMapScroller alloc] initWithToothCard:toothcards[toothcards.count-1]];
        }
        
    }
    else
    {
        if(toothcards.count==0)
        {
            self.toothMapScroller = [[DBToothMapScroller alloc] initWithPatient:self.currentPatient];
        }
        else
        {
            self.toothMapScroller = [[DBToothMapScroller alloc] initWithToothCard:toothcards[toothcards.count-1]];
        }
    }
    
    self.toothMapScroller.delegate = self;
    
    [self loadData];
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
    
    /*UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = getColorSchemeStatus();
    }*/
    
}

- (void) viewWillAppear:(BOOL)animated
{
    int count = (getRowsCount());
    
    if(count==2)
    {
        /*subView.frame = CGRectOffset(subView.frame, 0, -44.0);
        self.treeView.frame = CGRectMake(self.treeView.frame.origin.x, self.treeView.frame.origin.y-44.0, self.treeView.frame.size.width, self.treeView.frame.size.height+44.0);*/
    }
    else if(count==1)
    {
        subView.frame = CGRectOffset(subView.frame, 0, -44.0);
        self.treeView.frame = /*CGRectInset(self.treeView.frame, 0, -44);*/CGRectMake(self.treeView.frame.origin.x, self.treeView.frame.origin.y-44.0, self.treeView.frame.size.width, self.treeView.frame.size.height+44.0);
    }
    else
    {
        subView.frame = CGRectOffset(subView.frame, 0, -80.0);
        self.treeView.frame = /*CGRectInset(self.treeView.frame, 0, -88);*/ CGRectMake(self.treeView.frame.origin.x, self.treeView.frame.origin.y-80.0, self.treeView.frame.size.width, self.treeView.frame.size.height+80.0);
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadData
{
    self.data = [NSMutableArray new];
    
    for(NSDictionary *category in [DataBase getDBArray:@"services_group" withKey:@"" andValue:@""])
    {
        
        RADataObject *categoryData = [RADataObject dataObjectWithName:category[@"title"] withCost:0 withId:category[@"id"]  children:nil];
        NSArray* arr=[DataBase getDBArray:@"services" withKey:@"category_id" andValue:category[@"id"]];
        for(NSDictionary *service in arr)   {
            RADataObject *serviceData = [RADataObject dataObjectWithName:service[@"title"] withCost:[service[@"cost"] integerValue] withId:service[@"id"] children:nil];
            [categoryData addChild:serviceData];
        }
        
        [self.data addObject:categoryData];
    }
    
    __block UITableView *curTableViewBlock = self.optionView;
    __block NSMutableArray *ars = self.currentTherapy[@"items"];
    __block NSMutableArray *self_items = self.currentTherapy[@"items"];

    self.toothMapScroller.didChooseToothId = ^(NSIndexPath *indexPath, NSInteger toothId)
    {
        /*int indx = ((NSArray*)self.currentTherapy[@"items"]).count - (((NSArray*)self.currentTherapy[@"items"]).count>=3?3:((NSArray*)self.currentTherapy[@"items"]).count)+indexPath.row;*/
        int indx = ((NSArray*)self.currentTherapy[@"items"]).count - (((NSArray*)self.currentTherapy[@"items"]).count>=(getRowsCount()+1)?(getRowsCount()+1):((NSArray*)self.currentTherapy[@"items"]).count)+indexPath.row;
        NSMutableArray *array = ars;
        NSMutableDictionary *buyRef = array[indx];
        buyRef[@"toothid"] = @(toothId);
        self.selectedTooth = toothId;
        
        [curTableViewBlock reloadData];
        
        [self.toothMapActionSheet removeFromView];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"needBuyUpdate" object:nil];
    };
    
    self.toothCounter = [DbToothCounter new];
    self.discountCounter = [DbToothCounter new];
    
}

- (void) viewDidAppear:(BOOL)animated
{
    [self loadData];
    [self.treeView reloadData];
    [self updateBuyInfo];
    [self.optionView reloadData];
}

#pragma mark -
#pragma mark Option Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int count = ((NSArray*)self.currentTherapy[@"items"]).count>=(getRowsCount()+1)?(getRowsCount()+1):((NSArray*)self.currentTherapy[@"items"]).count;
    
    return ((NSArray*)self.currentTherapy[@"items"]).count>=(getRowsCount()+1)?(getRowsCount()+1):((NSArray*)self.currentTherapy[@"items"]).count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 32.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int indx = ((NSArray*)self.currentTherapy[@"items"]).count - (((NSArray*)self.currentTherapy[@"items"]).count>=(getRowsCount()+1)?(getRowsCount()+1):((NSArray*)self.currentTherapy[@"items"]).count)+indexPath.row;
    NSDictionary *buyRef = self.currentTherapy[@"items"][indx];
    NSArray* ser_arr = [DataBase getDBArray:@"services" withKey:@"id" andValue:buyRef[@"serviceid"]];
    NSDictionary *serviceRef = ser_arr==nil?nil:ser_arr[0];
    
    ChoosedServiceViewCell *chooseCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ChoosedServiceViewCell class])];
    chooseCell.countTooth.text = [NSString stringWithFormat:@"%ld", [buyRef[@"toothid"] integerValue]];
    chooseCell.nameLabel.text = serviceRef[@"title"];
    chooseCell.countService.text = [NSString stringWithFormat:@"%ld", [buyRef[@"countt"] integerValue]];
    chooseCell.costService.text = [serviceRef[@"cost"] stringValue];
    chooseCell.indexPath = indexPath;
    chooseCell.didClickToothMap = ^(NSIndexPath *indexPath)
    {
        self.toothMapScroller.indexPath = indexPath;
        self.toothMapScroller.willSelectToothIndex = [buyRef[@"toothid"] integerValue];
        self.toothMapActionSheet = [[WLPActionSheet alloc] initWithTitle:@"Выбор зуба" cancelButtonTitle:@"Отмена" destructiveButtonTitle:(self.toothMapScroller.willSelectToothIndex > 0)?@"Удалить":nil andMoreButton:!self.isPlan?@"Изменить основной диагноз":nil withViewController:self.toothMapScroller];
        self.toothMapActionSheet.tag = 2;
        self.toothMapActionSheet.delegate = self.toothMapScroller;
        [self.toothMapActionSheet showInView:self.view];
    };
    
    chooseCell.didClickCounter = ^(NSIndexPath *indexPath)
    {
        self.toothCounter.indexPath = indexPath;
        self.toothCounterActionSheet = [[WLPActionSheet alloc] initWithTitle:@"Количество" cancelButtonTitle:@"ОК" destructiveButtonTitle:nil withViewController:self.toothCounter];
        self.toothCounterActionSheet.tag = 3;
        self.toothCounterActionSheet.delegate = self;
        [self.toothCounterActionSheet showInView:self.view];
    };
    
    chooseCell.didClickDelete = ^(NSIndexPath *indexPath)
    {
        NSString* idi=self.currentTherapy[@"items"][indx][@"id"];
        id to_remove=nil;
        for(NSDictionary *d in self.currentTherapy[@"items"])
        {
            if([d[@"id"] isEqualToString:idi])
            {
                to_remove=d;
                break;
            }
        }
        
        if(to_remove!=nil)
        {
            [self.currentTherapy[@"items"] removeObject:to_remove];
            [self.optionView reloadData];
            [self updateBuyInfo];
        }
        
    };
    

    if(selectedRow!=nil)
    {
        if(selectedRow.section == indexPath.section && selectedRow.row == indexPath.row)
        {
            chooseCell.deleteBtn.hidden=NO;
        }
        else
        {
            chooseCell.deleteBtn.hidden=YES;
        }
    }
    else
    {
        chooseCell.deleteBtn.hidden=YES;
    }
    return chooseCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedRow = indexPath;
    NSLog(@"Row: %ld, section: %ld", (long)indexPath.row, (long)indexPath.section);
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    [self.optionView reloadData];
}

#pragma mark TreeView Delegate methods

- (CGFloat)treeView:(RATreeView *)treeView heightForRowForItem:(id)item
{
    return 44;
}

- (BOOL)treeView:(RATreeView *)treeView canEditRowForItem:(id)item
{
    return NO;
}

- (void)treeView:(RATreeView *)treeView willExpandRowForItem:(id)item
{
    //Если нажали на категорию то сворачиваем все категории
    NSInteger level = [self.treeView levelForCellForItem:item];
    if(level == 0)  {
        for(RADataObject *dataObject in self.data)  {
            [treeView collapseRowForItem:dataObject];
            CategoryViewCell *cell = (CategoryViewCell *)[treeView cellForItem:dataObject];
            [UIView animateWithDuration:0.2 animations:^{
                cell.arrowImageView.transform = CGAffineTransformIdentity;
            }];
        }
    }
    
    CategoryViewCell *cell = (CategoryViewCell *)[treeView cellForItem:item];
    [UIView animateWithDuration:0.2 animations:^{
        cell.arrowImageView.transform = CGAffineTransformMakeRotation(M_PI_2);
    }];
}

- (void)treeView:(RATreeView *)treeView willCollapseRowForItem:(id)item
{
    CategoryViewCell *cell = (CategoryViewCell *)[treeView cellForItem:item];
    [UIView animateWithDuration:0.2 animations:^{
        cell.arrowImageView.transform = CGAffineTransformIdentity;
    }];
}

#pragma mark TreeView Data Source
- (BOOL)treeView:(RATreeView *)treeView shouldExpandRowForItem:(id)item
{
    RADataObject *dataObject = item;
    RADataObject* last_str = [self.data lastObject];
    
    /*if([last_str.objectId isEqualToString:dataObject.objectId])
    {*/
    if(dataObject.children.count>0)
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [treeView scrollToRowForItem:[last_str.children firstObject] atScrollPosition:RATreeViewScrollPositionTop animated:YES];
        });
        
    /*}*/
    
    return true;
}




- (UITableViewCell *)treeView:(RATreeView *)treeView cellForItem:(id)item
{
    RADataObject *dataObject = item;
    //NSDictionary* dataObject = item;
    
    NSInteger level = [self.treeView levelForCellForItem:item];
    //NSInteger numberOfChildren = [dataObject.children count];
    //NSString *detailText = [NSString localizedStringWithFormat:@"Number of children %@", [@(numberOfChildren) stringValue]];
    BOOL expanded = [self.treeView isCellForItemExpanded:item];
    
    CategoryViewCell *cell = [self.treeView dequeueReusableCellWithIdentifier:NSStringFromClass([CategoryViewCell class])];
    cell.titleLabel.text = dataObject.name;
    
    if(level == 0)    {
        cell.arrowImageView.hidden = NO;
        cell.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:13.0];
        cell.titleLabel.textColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
        cell.costLabel.text = @"";
        //cell.leftTitleConstraint.constant = 8.0f;
        [cell layoutIfNeeded];
        
        cell.arrowImageView.transform = CGAffineTransformIdentity;
        if(expanded)    {
            cell.arrowImageView.transform = CGAffineTransformMakeRotation(M_PI_2);
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.separatorInset = UIEdgeInsetsZero;
        
    } else  {
        cell.arrowImageView.hidden = YES;
        cell.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:11.0];
        cell.titleLabel.textColor = [UIColor grayColor];
        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
        [numberFormatter setGroupingSeparator:@" "];
        [numberFormatter setGroupingSize:3];
        [numberFormatter setUsesGroupingSeparator:YES];
        [numberFormatter setDecimalSeparator:@"."];
        [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
        [numberFormatter setMaximumFractionDigits:2];
        
        cell.costLabel.text = [numberFormatter stringFromNumber:@((long)dataObject.cost)];
        cell.costLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:11.0];
        cell.costLabel.textColor = [UIColor grayColor];
        //cell.leftTitleConstraint.constant = 10.0;
        //[cell layoutIfNeeded];
    }
    
    return cell;
}


- (void)treeView:(RATreeView *)treeView willDisplayCell:(UITableViewCell *)cell forItem:(id)item
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (NSInteger)treeView:(RATreeView *)treeView numberOfChildrenOfItem:(id)item
{
    if (item == nil) {
        return [self.data count];
    }
    
    RADataObject *data = item;
    return [data.children count];
}

- (id)treeView:(RATreeView *)treeView child:(NSInteger)index ofItem:(id)item
{
    RADataObject *data = item;
    if (item == nil) {
        return [self.data objectAtIndex:index];
    }
    
    return data.children[index];
}

- (void)treeView:(RATreeView *)treeView didSelectRowForItem:(id)item    {
    [treeView deselectRowForItem:item animated:YES];
    
    RADataObject *dataObject = item;
    NSInteger level = [self.treeView levelForCellForItem:item];
    if(level != 0)
    {
        RADataObject *dataObject2 = [self.treeView parentForItem:item];
        
        
        NSMutableDictionary* therapyItem = [[NSMutableDictionary alloc] init];
        therapyItem[@"id"]=GUIDString();
        therapyItem[@"therapy_id"]=self.currentTherapy[@"id"];
        therapyItem[@"serviceid"] = dataObject.objectId;
        therapyItem[@"categoryid"] = dataObject2.objectId;
        therapyItem[@"countt"] = @(1);
        therapyItem[@"toothid"] = @(self.selectedTooth);
        NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
        [dateformate setDateFormat:@"dd.MM.yyyy"];
        therapyItem[@"createat"] = [dateformate stringFromDate:[NSDate date]];
        [self.currentTherapy[@"items"] addObject:therapyItem];
        [self updateBuyInfo];
        [self.optionView reloadData];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"needBuyUpdate" object:nil];
    }
    
}

-(void) updateBuyInfo
{
    NSInteger cost = 0;
    
    for(NSDictionary *buyRef in self.currentTherapy[@"items"] /*[TherapyItem MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"therapy == %@", self.currentTherapy]]*/)
    {
        NSDictionary *serviceRef = [DataBase getDBArray:@"services" withKey:@"id" andValue:buyRef[@"serviceid"]][0];//[Service MR_findFirstByAttribute:@"remoteId" withValue:buyRef.serviceId];
        cost += [serviceRef[@"cost"] integerValue] * [buyRef[@"countt"] integerValue];
    }
    
    //float realCost = cost;
    float realCostWithDiscount = cost - ((cost * [self.currentTherapy[@"discount"] floatValue])/100);
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setGroupingSeparator:@" "];
    [numberFormatter setGroupingSize:3];
    [numberFormatter setUsesGroupingSeparator:YES];
    [numberFormatter setDecimalSeparator:@"."];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [numberFormatter setMaximumFractionDigits:2];
    
    //cell.costLabel.text = [numberFormatter stringFromNumber:@((long)dataObject.cost)];
    
    self.sumWithDiscountLabel.text = self.currentTherapy[@"total_cost_with_discount"] = [NSString stringWithFormat:@"%@ ₽", [numberFormatter stringFromNumber:@(realCostWithDiscount)]];
}

- (IBAction)onTapPriceList:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"needBuyUpdateDelay" object:nil];
}

- (IBAction)tapChangeName:(id)sender
{
    if(self.isPlan == YES)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Название плана"
                                                        message:@"Введите название плана"
                                                       delegate:self
                                              cancelButtonTitle:@"Готово"
                                              otherButtonTitles:nil];
        alert.delegate = self;
        alert.alertViewStyle = UIAlertViewStylePlainTextInput;
        [alert show];
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"%@", [alertView textFieldAtIndex:0].text);
    
    self.currentTherapy[@"name"] = [alertView textFieldAtIndex:0].text;
    nameLabel.text=self.currentTherapy[@"name"];
}

-(void)actionSheetDidDone:(WLPActionSheet*)actionSheet
{

    if(actionSheet.tag == 2)
    {   //Выбор зуба
        
    }
    else if(actionSheet.tag == 3)
    {   //Кол-во
        int indx = ((NSArray*)self.currentTherapy[@"items"]).count - (((NSArray*)self.currentTherapy[@"items"]).count>=(getRowsCount()+1)?(getRowsCount()+1):((NSArray*)self.currentTherapy[@"items"]).count)+self.toothCounter.indexPath.row;
        NSMutableDictionary *buyRef = self.currentTherapy[@"items"][indx];
        
        
        buyRef[@"countt"] = @(self.toothCounter.toothCount);
        [self.optionView reloadData];
        [self updateBuyInfo];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"needBuyUpdate" object:nil];
    }
}

-(void)actionSheetDidDissapear:(WLPActionSheet*)actionSheet   {
    NSLog(@"DESTRUCTIVE");
}


-(void)actionSheetDidMore:(WLPActionSheet*)actionSheet
{
    if(actionSheet.tag == 2)
    {   //Выбор зуба
        [self dismissViewControllerAnimated:YES completion:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"needCallToothMap" object:nil];
    }
}

@end
