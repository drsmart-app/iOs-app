//
//  TherapyAcceptViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 26.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "TherapyAcceptViewController.h"
#import "UIView+Helpers.h"
#import "ChoosedServiceViewCell.h"

#import <MagicalRecord/MagicalRecord.h>

#import "Patient.h"
#import "CategoryTherapy.h"
#import "Service.h"
#import "Therapy.h"
#import "TherapyItem.h"

#import "HttpUtil.h"

@interface TherapyAcceptViewController ()

@property (nonatomic, strong) Therapy *currentTherapy;

@property (strong, nonatomic) IBOutlet UITableView *optionTableView;
@property (strong, nonatomic) IBOutlet UITableViewCell *acceptCell;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@end

@implementation TherapyAcceptViewController

-(id) initWithTherapy:(Therapy*)therapy {
    self = [super initWithNibName:@"TherapyAcceptViewController" bundle:[NSBundle mainBundle]];
    if(self)    {
        self.currentTherapy = therapy;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Подтвердить";
    
    [self.optionTableView registerNib:[UINib nibWithNibName:NSStringFromClass([ChoosedServiceViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ChoosedServiceViewCell class])];

    [self fetchTableData];
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)fetchTableData {
    _fetchedResultsController = nil;
    NSError *error = nil;
    [self.fetchedResultsController performFetch:&error];
}

- (NSFetchedResultsController *)fetchedResultsController {
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"TherapyItem"];
    [fetchRequest setFetchBatchSize:20];
    
    // Predicate
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"therapy == %@", self.currentTherapy];
    [fetchRequest setPredicate:predicate];
    
    // Sort
    NSSortDescriptor* dateSortDescripror;
    dateSortDescripror = [NSSortDescriptor sortDescriptorWithKey:@"serviceId"
                                                       ascending:NO];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObjects:dateSortDescripror, nil]];
    
    NSFetchedResultsController *theFetchedResultsController;
    theFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                      managedObjectContext:[NSManagedObjectContext MR_defaultContext]
                                                                        sectionNameKeyPath:@"categoryId"
                                                                                 cacheName:nil];
    theFetchedResultsController.delegate = self;
    self.fetchedResultsController = theFetchedResultsController;
    
    return _fetchedResultsController;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.optionTableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id )sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.optionTableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.optionTableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.optionTableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationBottom];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.optionTableView endUpdates];
}

#pragma mark -
#pragma mark Option Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger count = [[self.fetchedResultsController sections] count];
    return count + 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger count = [[self.fetchedResultsController sections] count];
    if(section == count)    {
        return 1;
    }
    
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    NSInteger count2 = [sectionInfo numberOfObjects];
    return count2;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section    {
    
    NSInteger count = [[self.fetchedResultsController sections] count];
    if(section == count)    {
        return [UIView new];
    }
    
    id <NSFetchedResultsSectionInfo> theSection = [[self.fetchedResultsController sections] objectAtIndex:section];
    TherapyItem *buyRef = theSection.objects[0];
    CategoryTherapy *categoryRef = [CategoryTherapy MR_findFirstByAttribute:@"remoteId" withValue:buyRef.categoryId];
    
    UIView* headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 25)];
    
    headerView.backgroundColor = [UIColor whiteColor];
    
    UILabel* headerLabel = [[UILabel alloc] init];
    headerLabel.frame = CGRectMake(20, 2, tableView.frame.size.width - 5, 18);
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.textColor = [UIColor blackColor];
    headerLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:13];
    headerLabel.text = categoryRef.name;
    headerLabel.textAlignment = NSTextAlignmentLeft;
    
    [headerView addSubview:headerLabel];
    
    return headerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section    {
    
    NSInteger count = [[self.fetchedResultsController sections] count];
    if(section == count)    {
        return [UIView new];
    }
    
    UIView* headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 25)];
    headerView.backgroundColor = [UIColor whiteColor];
    
    UIImageView *seperateImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"seperate-cell"]];
    [headerView addSubview:seperateImageView];
    seperateImageView.center = headerView.center;
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section   {
    return 25;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section   {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath  {
    
    NSInteger count = [[self.fetchedResultsController sections] count];
    if(indexPath.section == count)    {
        return 44.0;
    }
    
    return 32.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger count = [[self.fetchedResultsController sections] count];
    if(indexPath.section == count)    {
        return self.acceptCell;
    }
    
    TherapyItem *buyRef = [self.fetchedResultsController objectAtIndexPath:indexPath];
    Service *serviceRef = [Service MR_findFirstByAttribute:@"remoteId" withValue:buyRef.serviceId];
    
    ChoosedServiceViewCell *chooseCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ChoosedServiceViewCell class])];
    chooseCell.countTooth.text = @"";
    chooseCell.nameLabel.text = serviceRef.name;
    chooseCell.countService.text = [NSString stringWithFormat:@"%d", [buyRef.count integerValue]];
    chooseCell.costService.text = [serviceRef.cost stringValue];
    
    return chooseCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(indexPath.section == 1)  {
        [self onAccept:nil];
    }
}

- (IBAction)onAccept:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
