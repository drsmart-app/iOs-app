//
//  TherapyAcceptViewController.h
//  DentaBook
//
//  Created by Denis Dubov on 26.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Therapy;

@interface TherapyAcceptViewController : UIViewController

-(id) initWithTherapy:(Therapy*)therapy;

@end
