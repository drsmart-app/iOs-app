//
//  PatientFinanceViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 18.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "PatientFinanceViewController.h"

#import <MagicalRecord/MagicalRecord.h>

#import "Patient+Custom.h"
#import "Payment.h"
#import "DataBase.h"
#import "FinanceTableViewCell.h"
#import "VisitHistoryViewController.h"
#import "PercUtils.h"
#import "HttpUtil.h"
#import "TherapyReadViewController.h"

@interface PatientFinanceViewController () <NSFetchedResultsControllerDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (nonatomic, strong) NSDictionary *currentPatient;
@end

@implementation PatientFinanceViewController
@synthesize payments, sections, commonDebtLabel, commonDebtDescLabel, nameLabel, illnessImg;
-(id) initWithPatient:(NSDictionary*)patient {
    self = [super initWithNibName:@"PatientFinanceViewController" bundle:[NSBundle mainBundle]];
    if(self)    {
        self.currentPatient = patient;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Финансы";
    
    UIImage *image2 = [UIImage imageNamed:@"back-button"];
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 addTarget:self action:@selector(onBack:)forControlEvents:UIControlEventTouchUpInside];
    [button2 setBackgroundImage:image2 forState:UIControlStateNormal];
    button2.frame = CGRectMake(0, 0, 11, 18);
    UIBarButtonItem *barButton2 = [[UIBarButtonItem alloc] initWithCustomView:button2];
    self.navigationItem.leftBarButtonItem = barButton2;
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, 30, 28);
    [button setTitle:@"    " forState:UIControlStateNormal];
    [button setTitle:@"    " forState:UIControlStateSelected];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = barButton;
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([FinanceTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([FinanceTableViewCell class])];
    
    [self fetchTableData];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]&&getNeedName())
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    if(!getNeedName())
    {
        CGRect tempRect;
        for(UIView *sub in [[self view] subviews])
        {
            tempRect = [sub frame];
            tempRect.origin.y += 20.0f; //Height of status bar
            [sub setFrame:tempRect];
        }
        
        self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, self.tableView.frame.size.width, self.tableView.frame.size.height -20.0f);
    }
    
    if(((NSArray*)self.currentPatient[@"illness"]).count>0)
    {
        
        NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
        attachment.image =  [PercUtils imageWithImage:[UIImage imageNamed:@"patient-illness-icon.png"] scaledToSize:CGSizeMake(15, 15)];
        attachment.bounds = CGRectMake(0, -3, 18, 18);
        NSMutableAttributedString *attachmentString = [[NSAttributedString attributedStringWithAttachment:attachment] mutableCopy];
        
        NSMutableAttributedString *myString= [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"  %@ %@ %@", self.currentPatient[@"lastname"], self.currentPatient[@"firstname"], self.currentPatient[@"middlename"]]];
        [attachmentString appendAttributedString:myString];
        
        nameLabel.attributedText = attachmentString;
    }
    else
    {
        nameLabel.text = [NSString stringWithFormat:@"  %@ %@ %@", self.currentPatient[@"lastname"], self.currentPatient[@"firstname"], self.currentPatient[@"middlename"]];
    }
    
    CGFloat dummyViewHeight = 40;
    UIView *dummyView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.bounds.size.width, dummyViewHeight)];
    self.tableView.tableHeaderView = dummyView;
    self.tableView.contentInset = UIEdgeInsetsMake(-dummyViewHeight, 0, 0, 0);
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = getColorSchemeStatus();
    }
    
    self.extendedLayoutIncludesOpaqueBars = YES;
    
    //self.navigationController.navigationBar.translucent = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)fetchTableData
{

    NSArray* aa = [DataBase getDBArray:@"payment" withKey:@"patientid" andValue:self.currentPatient[@"patientid"]];
    NSArray* tmp1 = [DataBase getDBArray:@"visits" withKey:@"patientid" andValue:self.currentPatient[@"patientid"]];
    NSMutableArray* tmp=[NSMutableArray arrayWithArray:aa];
    for(NSDictionary* dd in tmp1)
    {
        NSLog(@"Therapy object: %@",dd);
        [tmp addObject:@{@"therapyid":dd[@"id"],@"comment":dd[@"name"]==nil?dd[@"title"]:dd[@"name"],@"createat":dd[@"startdate"],@"amount":@(-1*[dd[@"total_cost_with_discount"] floatValue])}];
    }
    //total_cost_with_discount;
    //startdate
    NSMutableArray* tmp_sections = [[NSMutableArray alloc] init];
    
    for(NSDictionary* pat in tmp)
    {
        [tmp_sections addObject:pat[@"createat"]];
    }
    
    tmp_sections = [NSMutableArray arrayWithArray:[tmp_sections valueForKeyPath:@"@distinctUnionOfObjects.self"]];
    
    [tmp_sections sortUsingComparator:^NSComparisonResult(id obj1, id obj2)
     {
         if([obj1 isEqualToString:@"Не назначено"] && ![obj2 isEqualToString:@"Не назначено"])
             return NSOrderedDescending;
         NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
         [dateformate setDateFormat:@"dd.MM.yyyy"];
         return [[dateformate dateFromString:obj2] compare:[dateformate dateFromString:obj1]];
     }];
    
    sections = [[NSMutableArray alloc] init];
    for(NSString* hd in tmp_sections)
    {
        NSMutableDictionary* ndms = [[NSMutableDictionary alloc] init];
        ndms[@"section"] = hd;
        NSMutableArray* mna=[[NSMutableArray alloc] init];
        for(NSDictionary* dict in tmp)
        {
            if([[(NSString*)dict[@"createat"] uppercaseString] isEqualToString:[hd uppercaseString]])
            {
                if(dict[@"total_cost_with_discount"])
                {
                    if([dict[@"total_cost_with_discount"] floatValue]!=0)
                    {
                        [mna addObject:dict];
                    }
                }
                if(dict[@"amount"])
                {
                    if([dict[@"amount"] floatValue]!=0)
                    {
                        [mna addObject:dict];
                    }
                }
            }
        }
        ndms[@"content"]=mna;
        [sections addObject:ndms];
    }
    
    NSArray* costs = [DataBase getDBArray:@"visits" withKey:@"patientid" andValue:self.currentPatient[@"patientid"]];
    float allCredit=0;
    for(NSDictionary* dc in costs)
    {
        allCredit+= [dc[@"total_cost_with_discount"] floatValue];
    }
    
    float allDebit=0;
    NSArray* pments = [DataBase getDBArray:@"payment" withKey:@"patientid" andValue:self.currentPatient[@"patientid"]];
    for(NSDictionary* dc in pments)
    {
        allDebit += [dc[@"amount"] floatValue];
    }
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setGroupingSeparator:@" "];
    [numberFormatter setGroupingSize:3];
    [numberFormatter setUsesGroupingSeparator:YES];
    [numberFormatter setDecimalSeparator:@"."];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [numberFormatter setMaximumFractionDigits:2];
    
    NSString *theString = [numberFormatter stringFromNumber:[NSNumber numberWithDouble:allDebit - allCredit]];
    
    if(allDebit - allCredit<0)
    {
        commonDebtDescLabel.text=@"Общий долг";
        commonDebtLabel.textColor=[UIColor colorWithRed:231.0/255.0 green:76.0/255.0 blue:60.0/255.0 alpha:1.0];
        commonDebtLabel.text = [NSString stringWithFormat:@"%@ ₽", theString];
    }
    else
    {
        commonDebtDescLabel.text=@"Аванс";
        commonDebtLabel.textColor=[UIColor colorWithRed:46.0/255.0 green:204.0/255.0 blue:113.0/255.0 alpha:1.0];
        commonDebtLabel.text = [NSString stringWithFormat:@"+ %@ ₽", theString];
    }

    
}


#pragma mark -
#pragma mark Option Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return sections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    //NSInteger count = [self.fetchedResultsController.fetchedObjects count];
    return ((NSArray*)sections[section][@"content"]).count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(((NSArray*)sections[section][@"content"]).count>0)
        return 30;
    else
        return 0;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return sections[section][@"section"];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *myLabel = [[UILabel alloc] init];
    myLabel.frame = CGRectMake(20, 12, 290, 20);
    myLabel.font = [UIFont systemFontOfSize:12];
    myLabel.textColor = [UIColor colorWithRed:72.0f/255.0f green:72.0f/255.0f blue:72.0f/255.0f alpha:1];
    myLabel.text = [self tableView:tableView titleForHeaderInSection:section];
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 44, tableView.frame.size.width, 1)];
    [headerView addSubview:myLabel];
    
    CGRect sepFrame = CGRectMake(0, headerView.frame.size.height-1, headerView.frame.size.width, 1);
    //UIView* seperatorView = [[UIView alloc] initWithFrame:sepFrame];
    //seperatorView.backgroundColor = [UIColor colorWithRed:224.0f/255.0f green:223.0f/255.0f blue:224.0f/255.0f alpha:1];
    //[headerView addSubview:seperatorView];
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath  {
    return 70;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FinanceTableViewCell *cell = nil;
    NSDictionary *payment = sections[indexPath.section][@"content"][indexPath.row];//[self.fetchedResultsController objectAtIndexPath:indexPath];
    
    FinanceTableViewCell *titleCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([FinanceTableViewCell class])];

    NSString *dateString = payment[@"comment"];

    titleCell.commentLabel.text = dateString;
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setGroupingSeparator:@" "];
    [numberFormatter setGroupingSize:3];
    [numberFormatter setUsesGroupingSeparator:YES];
    [numberFormatter setDecimalSeparator:@"."];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [numberFormatter setMaximumFractionDigits:2];
    
    
    titleCell.commentLabel.font = [UIFont fontWithName:@"Helvetica" size:14.0];
    titleCell.commentLabel.textColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
    titleCell.sumLabel.font = [UIFont fontWithName:@"Helvetica" size:14.0];
    if([payment[@"amount"] doubleValue]>0)
    {
        NSString *theString = [numberFormatter stringFromNumber:[NSNumber numberWithDouble:[payment[@"amount"] floatValue]]];
        titleCell.sumLabel.text = [NSString stringWithFormat:@"+ %@ ₽", theString];
        //titleCell.sumLabel.textColor = [UIColor colorWithRed:46.0/255.0 green:204.0/255.0 blue:113.0/255.0 alpha:1.0];
        //titleCell.sumLabel.frame = CGRectOffset(20, 20, 0)
        titleCell.discLabel.hidden=YES;
    }
    else
    {
        NSString *theString = [numberFormatter stringFromNumber:[NSNumber numberWithDouble:-[payment[@"amount"] floatValue]]];
        titleCell.sumLabel.text = [NSString stringWithFormat:@"- %@ ₽", theString];
        //titleCell.sumLabel.textColor = [UIColor colorWithRed:231.0/255.0 green:76.0/255.0 blue:60.0/255.0 alpha:1.0];
        titleCell.discLabel.hidden=NO;
    }
    
    cell = titleCell;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *payment = sections[indexPath.section][@"content"][indexPath.row];
    
    if([payment[@"amount"] doubleValue]<0)
    {
        NSLog(@"%@",sections);
        NSDictionary* therapyEntity = (NSDictionary*)[DataBase getDB:@"visits" withKey:@"id" andValue:((NSArray*)sections[indexPath.section][@"content"])[indexPath.row][@"therapyid"]];
        //Therapy *therapyEntity = [self.fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:button.tag inSection:0]];
        /*NSDictionary* therapyEntity*/
        
        TherapyReadViewController *therapyReadOnlyVC = [[TherapyReadViewController alloc] initWithTherapy:therapyEntity];
        [self.navigationController pushViewController:therapyReadOnlyVC animated:YES];
        //VisitHistoryViewController *visitHistoryViewController = [[VisitHistoryViewController alloc] initWithPatient:self.currentPatient];
        //[self.navigationController pushViewController:visitHistoryViewController animated:YES];
    }
    
}

-(NSString*) genStr {
    NSString *alphabet  = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXZY0123456789";
    NSMutableString *s = [NSMutableString stringWithCapacity:10];
    for (NSUInteger i = 0U; i < 10; i++) {
        u_int32_t r = arc4random() % [alphabet length];
        unichar c = [alphabet characterAtIndex:r];
        [s appendFormat:@"%C", c];
    }
    return s;
}


-(void)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
