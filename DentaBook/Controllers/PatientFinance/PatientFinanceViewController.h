//
//  PatientFinanceViewController.h
//  DentaBook
//
//  Created by Denis Dubov on 18.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Patient;

@interface PatientFinanceViewController : UIViewController

-(id) initWithPatient:(NSDictionary*)patient;
@property (nonatomic,retain) NSArray* payments;
@property (nonatomic,retain) NSMutableArray* sections;
@property (weak, nonatomic) IBOutlet UILabel *commonDebtLabel;
@property (weak, nonatomic) IBOutlet UILabel *commonDebtDescLabel;
@property (weak, nonatomic) IBOutlet UIImageView *illnessImg;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@end
