//
//  VisitHistoryViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 27.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "VisitHistoryViewController.h"
#import "TherapyPlanViewCell.h"
#import "VisitHistoryViewCell.h"
#import "UIView+Helpers.h"
#import "TherapyReadViewController.h"
#import "VisitHistoryDateViewCell.h"

#import <MagicalRecord/MagicalRecord.h>

#import "Patient.h"
#import "CategoryTherapy.h"
#import "Service.h"
#import "Therapy+Custom.h"
#import "TherapyItem.h"
#import "DataBase.h"
#import "PercUtils.h"
#import <MessageUI/MessageUI.h>
#import "UIView+Toast.h"
#import "ShareViewController.h"
#import "UIViewController+PresentedOver.h"
#import "HttpUtil.h"
#import "UITableView+Convertible.h"

@interface VisitHistoryViewController () <NSFetchedResultsControllerDelegate, MFMailComposeViewControllerDelegate>
@property (nonatomic, strong) NSDictionary *currentPatient;

@property (strong, nonatomic) IBOutlet UITableView *dataTableView;
@property (strong, nonatomic) IBOutlet UITableView *dateTableView;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (nonatomic, strong) NSIndexPath *selectedIndexPath;
@property (nonatomic) BOOL convertMode;

@end

@implementation VisitHistoryViewController
@synthesize visits, needLast, lastIndexPath, lastVisitDate, lastVisitDateString, illnessImg, nameLabel, selected_items, optionView;
-(id) initWithPatient:(NSDictionary*)patient {
    self = [super initWithNibName:@"VisitHistoryViewController" bundle:[NSBundle mainBundle]];
    if(self)    {
        self.currentPatient = patient;
        self.convertMode = NO;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.convertMode = NO;
    selected_items = [[NSMutableArray alloc] init];
    
    self.title=@"История посещений";
    
    //self.navigationItem.titleView = label;
    
    if(((NSArray*)self.currentPatient[@"illness"]).count>0)
    {
        
        NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
        attachment.image =  [PercUtils imageWithImage:[UIImage imageNamed:@"patient-illness-icon.png"] scaledToSize:CGSizeMake(15, 15)];
        attachment.bounds = CGRectMake(0, -3, 18, 18);
        NSMutableAttributedString *attachmentString = [[NSAttributedString attributedStringWithAttachment:attachment] mutableCopy];
        
        NSMutableAttributedString *myString= [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"  %@ %@ %@", self.currentPatient[@"lastname"], self.currentPatient[@"firstname"], self.currentPatient[@"middlename"]]];
        [attachmentString appendAttributedString:myString];
        
        nameLabel.attributedText = attachmentString;
    }
    else
    {
        nameLabel.text = [NSString stringWithFormat:@"  %@ %@ %@", self.currentPatient[@"lastname"], self.currentPatient[@"firstname"], self.currentPatient[@"middlename"]];
    }
    
    UIImage *image2 = [UIImage imageNamed:@"back-button"];
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 addTarget:self action:@selector(onBack:)forControlEvents:UIControlEventTouchUpInside];
    [button2 setBackgroundImage:image2 forState:UIControlStateNormal];
    button2.frame = CGRectMake(0, 0, 11, 18);
    UIBarButtonItem *barButton2 = [[UIBarButtonItem alloc] initWithCustomView:button2];
    self.navigationItem.leftBarButtonItem = barButton2;
    
    [self.dataTableView registerNib:[UINib nibWithNibName:NSStringFromClass([VisitHistoryViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([VisitHistoryViewCell class])];
    [self.dateTableView registerNib:[UINib nibWithNibName:NSStringFromClass([VisitHistoryDateViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([VisitHistoryDateViewCell class])];
    
    [self fetchTableData];
    [self.dataTableView reloadData];
    
    @try
    {
        if(needLast)
        {
            [self.dataTableView layoutIfNeeded];
            //NSIndexPath* indexPath = [NSIndexPath indexPathForRow: ([self.tableView numberOfRowsInSection:([self.tableView numberOfSections]-1)]-1) inSection: ([self.tableView numberOfSections]-1)];
            [self.dataTableView scrollToRowAtIndexPath:lastIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
        }
    }
    @catch(NSException *e)
    {
        NSLog(@"Error scrolling!!!");
    }
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]&&getNeedName())
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    if(!getNeedName())
    {
        CGRect tempRect;
        for(UIView *sub in [[self view] subviews])
        {
            tempRect = [sub frame];
            tempRect.origin.y += 20.0f; //Height of status bar
            [sub setFrame:tempRect];
        }
        
        self.dataTableView.frame = CGRectMake(self.dataTableView.frame.origin.x, self.dataTableView.frame.origin.y, self.dataTableView.frame.size.width, self.dataTableView.frame.size.height -20);
    }
    
    UILongPressGestureRecognizer *lpgr
    = [[UILongPressGestureRecognizer alloc]
       initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.delegate = self;
    lpgr.delaysTouchesBegan = YES;
    [self.dateTableView addGestureRecognizer:lpgr];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onPrint) name:@"needPrint" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onConvert) name:@"needConvert" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onEmail) name:@"needEmail" object:nil];
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
    
    self.optionView.backgroundColor = getColorScheme();
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = getColorSchemeStatus();
    }
    
    self.extendedLayoutIncludesOpaqueBars = YES;
    
    //self.navigationController.navigationBar.translucent = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) viewDidAppear:(BOOL)animated
{
    [self.dataTableView reloadData];
    @try
    {
        if(needLast)
        {
            [self.dataTableView layoutIfNeeded];
            //NSIndexPath* indexPath = [NSIndexPath indexPathForRow: ([self.tableView numberOfRowsInSection:([self.tableView numberOfSections]-1)]-1) inSection: ([self.tableView numberOfSections]-1)];
            [self.dataTableView scrollToRowAtIndexPath:lastIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
        }
    }
    @catch(NSException *e)
    {
        NSLog(@"Error scrolling!!!");
    }
}

-(void)fetchTableData {
    /*_fetchedResultsController = nil;
    NSError *error = nil;
    [self.fetchedResultsController performFetch:&error];*/
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"dd.MM.yyyy"];
    lastVisitDate = [dateformate dateFromString:@"01.01.1970"];
    visits=[DataBase getDBArray:@"visits" withKey:@"patientid" andValue:self.currentPatient[@"patientid"]];
    
    int i=0;
    for(NSDictionary* d in visits)
    {
        if([[dateformate dateFromString:d[@"startdate"]] timeIntervalSince1970]>[lastVisitDate timeIntervalSince1970])
        {
            lastVisitDate = [dateformate dateFromString:d[@"startdate"]];
            lastIndexPath = [NSIndexPath indexPathForRow:0 inSection:i];
        }
        i++;
    }
    
    lastVisitDateString = [dateformate stringFromDate:lastVisitDate];
}

#pragma mark -
#pragma mark Option Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //NSInteger count = [self.fetchedResultsController.fetchedObjects count];
    return visits.count;
}

- (NSArray*) getSectionInfo:(NSInteger) section
{
    NSDictionary* tmp = visits[section];
    NSMutableArray* tmp_sections = [[NSMutableArray alloc] init];
    
    if(tmp[@"items"])
    {
        for(NSDictionary* pat in tmp[@"items"])
        {
            
            //nmd[@"index"] = [[(NSString*)pat[@"firstname"] substringWithRange:NSMakeRange(0, 1)] uppercaseString];
            [tmp_sections addObject:pat[@"categoryid"]];
        }
        
        NSMutableArray* sections = [[NSMutableArray alloc] init];
        for(NSString* hd in [tmp_sections valueForKeyPath:@"@distinctUnionOfObjects.self"])
        {
            NSMutableDictionary* ndms = [[NSMutableDictionary alloc] init];
            ndms[@"section"] = hd;
            NSMutableArray* mna=[[NSMutableArray alloc] init];
            for(NSDictionary* dict in tmp[@"items"])
            {
                if([[(NSString*)dict[@"categoryid"] uppercaseString] isEqualToString:[hd uppercaseString]])
                {
                    [mna addObject:dict];
                }
            }
            ndms[@"content"]=mna;
            [sections addObject:ndms];
        }
        
        return sections;
    }
    else return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(tableView.tag == 2)  {
        return 1;
    }
    
    /*Therapy *therapyEntity = [self.fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:section inSection:0]];
    
    NSArray *therapyItems = [therapyEntity allTherapyItem];
    
    return (therapyItems.count > 3) ? 3 : therapyItems.count;*/
    //NSDictionary* next_visit = visits[section];
    //NSArray *therapyItems = next_visit[@"items"];
    
    return [self getSectionInfo:section].count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section    {
    
    if(tableView.tag == 2)
    {
        /*if(section==0)
            return [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 25)];
        else*/
            return [UIView new];
    }
    
    BOOL is_found = NO;
    
    if(self.convertMode)
    {
        for(NSIndexPath* ind in selected_items)
        {
            if(ind.section == section)
            {
                is_found = YES;
                break;
            }
        }
        if(!is_found)
            return [UIView new];
    }
    
    NSDictionary *therapyEntity = visits[section];//[self.fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:section inSection:0]];

    
    UIView* headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 25)];
    if(therapyEntity[@"items"])
    {
        headerView.backgroundColor = [UIColor clearColor];
        
        NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
        [dateformate setDateFormat:@"dd.MM.yyyy"];
        NSString *dateString = therapyEntity[@"startdate"];//[dateformate stringFromDate:therapyEntity.createAt];
        
        /*if([dateString isEqualToString:lastVisitDateString])
         {
         lastIndexPath = [NSIndexPath indexPathForRow:0 inSection:section];
         }*/
        
        UILabel* headerLabel = [[UILabel alloc] init];
        headerLabel.frame = headerView.bounds;
        headerLabel.backgroundColor = [UIColor clearColor];
        headerLabel.textColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
        headerLabel.font = [UIFont fontWithName:@"Helvetica" size:12];
        headerLabel.text = dateString;
        headerLabel.textAlignment = NSTextAlignmentLeft;
        [headerView addSubview:headerLabel];
    }
    else
    {
        headerView.backgroundColor = [UIColor clearColor];
        
        NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
        [dateformate setDateFormat:@"dd.MM.yyyy"];
        NSString *dateString = therapyEntity[@"startdate"];//[dateformate stringFromDate:therapyEntity.createAt];
        
        /*if([dateString isEqualToString:lastVisitDateString])
         {
         lastIndexPath = [NSIndexPath indexPathForRow:0 inSection:section];
         }*/
        
        UILabel* headerLabel = [[UILabel alloc] init];
        headerLabel.frame = headerView.bounds;
        headerLabel.backgroundColor = [UIColor clearColor];
        headerLabel.textColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
        headerLabel.font = [UIFont fontWithName:@"Helvetica" size:12];
        headerLabel.text = dateString;
        headerLabel.textAlignment = NSTextAlignmentLeft;
        [headerView addSubview:headerLabel];
    }
    
    return headerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section    {
    
    if(tableView.tag == 2)  {
        return [UIView new];
    }
    
    BOOL is_found = NO;
    
    if(self.convertMode)
    {
        for(NSIndexPath* ind in selected_items)
        {
            if(ind.section == section)
            {
                is_found = YES;
                break;
            }
        }
        if(!is_found)
            return [UIView new];
    }

    
    NSDictionary *therapyEntity = visits[section];
    
    UIView* headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 50)];
    if(therapyEntity[@"items"])
    {
        if([therapyEntity[@"comment"] length]>0)
        {
            headerView.backgroundColor = [UIColor clearColor];
            
            UIView* whiteHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 85)];
            whiteHeaderView.backgroundColor = [UIColor whiteColor];
            [headerView addSubview:whiteHeaderView];
            
            UILabel* comment_head=[[UILabel alloc] initWithFrame:CGRectMake(10, 5, tableView.frame.size.width, 20)];
            comment_head.textColor=[UIColor lightGrayColor];
            comment_head.text = @"Комментарий:";
            comment_head.font = [UIFont systemFontOfSize:12];
            [whiteHeaderView addSubview:comment_head];
            
            UILabel* comment_body=[[UILabel alloc] initWithFrame:CGRectMake(10, 25, tableView.frame.size.width, 20)];
            comment_body.textColor=[UIColor lightGrayColor];
            comment_body.text = therapyEntity[@"comment"];
            comment_body.font = [UIFont systemFontOfSize:12];
            [whiteHeaderView addSubview:comment_body];
            
            UIButton *descButton = [UIButton buttonWithType:UIButtonTypeSystem];
            [descButton setTitle:@"Подробнее" forState:UIControlStateNormal];
            [descButton setTitleColor:[UIColor colorWithRed:20.0/255.0 green:162.0/255.0 blue:243.0/255.0 alpha:1.0] forState:UIControlStateNormal];
            [descButton sizeToFit];
            descButton.top = 45;
            descButton.left = 5;
            descButton.height = 40;
            descButton.width = 150;
            descButton.tag = section;
            [descButton setTitleEdgeInsets:UIEdgeInsetsMake(0, -70, 0, 0)];
            
            descButton.titleLabel.font = [UIFont systemFontOfSize:12];
            [descButton addTarget:self action:@selector(onDesc:) forControlEvents:UIControlEventTouchUpInside];
            [whiteHeaderView addSubview:descButton];
        }
        else
        {
            headerView.backgroundColor = [UIColor clearColor];
            
            UIView* whiteHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
            whiteHeaderView.backgroundColor = [UIColor whiteColor];
            [headerView addSubview:whiteHeaderView];
            
            UIButton *descButton = [UIButton buttonWithType:UIButtonTypeSystem];
            [descButton setTitle:@"Подробнее" forState:UIControlStateNormal];
            [descButton setTitleColor:[UIColor colorWithRed:20.0/255.0 green:162.0/255.0 blue:243.0/255.0 alpha:1.0] forState:UIControlStateNormal];
            [descButton sizeToFit];
            descButton.top = 0;
            descButton.left = 5;
            descButton.height = 30;
            descButton.width = 150;
            [descButton setTitleEdgeInsets:UIEdgeInsetsMake(0, -70, 0, 0)];

            descButton.titleLabel.font = [UIFont systemFontOfSize:12];
            descButton.tag = section;
            [descButton addTarget:self action:@selector(onDesc:) forControlEvents:UIControlEventTouchUpInside];
            [whiteHeaderView addSubview:descButton];
        }
    }
    else
    {
        headerView.backgroundColor = [UIColor whiteColor];//[UIColor colorWithRed:249.0/255.0 green:186.0/255.0 blue:186.0/255.0 alpha:1.0];
        
        UIView* whiteHeaderView = [therapyEntity[@"comment"] length]>0?[[UIView alloc] initWithFrame:CGRectMake(10, 0, tableView.frame.size.width-10, 65)]:[[UIView alloc] initWithFrame:CGRectMake(10, 0, tableView.frame.size.width-10, 25)];
        //whiteHeaderView.backgroundColor = [UIColor colorWithRed:249.0/255.0 green:186.0/255.0 blue:186.0/255.0 alpha:1.0];
        [headerView addSubview:whiteHeaderView];
        
        UIButton *descButton = [UIButton buttonWithType:UIButtonTypeSystem];
        descButton.frame = CGRectMake(0, 7, whiteHeaderView.frame.size.width, 15);
        [descButton setTitle:[NSString stringWithFormat:@"Прием в %@:%@",therapyEntity[@"starthours"], therapyEntity[@"startminutes"]] forState:UIControlStateNormal];
        descButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [descButton setTitleColor:[UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0] forState:UIControlStateNormal];

        descButton.titleLabel.font = [UIFont systemFontOfSize:12];
        descButton.tag = section;
        
        if([therapyEntity[@"commentt"] length]>0)
        {
            UILabel* comment_head=[[UILabel alloc] initWithFrame:CGRectMake(10, 25, tableView.frame.size.width-10, 20)];
            comment_head.textColor=[UIColor lightGrayColor];
            comment_head.text = @"Комментарий:";
            comment_head.font = [UIFont systemFontOfSize:12];
            [whiteHeaderView addSubview:comment_head];
            
            CGRect labelSize = [therapyEntity[@"commentt"] boundingRectWithSize:CGSizeMake(tableView.frame.size.width-20, CGFLOAT_MAX)
                                                                        options:NSStringDrawingUsesLineFragmentOrigin
                                                                     attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12]}
                                                                        context:nil];
            
            UILabel* comment_body=[[UILabel alloc] initWithFrame:CGRectMake(10, 45, tableView.frame.size.width-20, labelSize.size.height)];
            comment_body.numberOfLines = 0;
            comment_body.lineBreakMode = UILineBreakModeWordWrap;
            comment_body.textColor=[UIColor lightGrayColor];
            comment_body.text = therapyEntity[@"commentt"];
            comment_body.font = [UIFont systemFontOfSize:12];
            [whiteHeaderView addSubview:comment_body];
        }

        [whiteHeaderView addSubview:descButton];
    }
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    if(tableView.tag == 2)  {
        if(section==0)
            return 25;
        return 5;
    }
    
    if(self.convertMode)
    {
        for(NSIndexPath* ind in selected_items)
        {
            if(ind.section == section)
            {
                return 25;
            }
        }
        return 0;
    }
    else
        return 25;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section   {
    
    if(tableView.tag == 2)  {
        return 10;
    }
    
    NSDictionary *therapyEntity = visits[section];
    
    if(self.convertMode)
    {
        for(NSIndexPath* ind in selected_items)
        {
            if(ind.section == section)
            {
                if(therapyEntity[@"items"])
                {
                    if([therapyEntity[@"comment"] length]>0)
                    {
                        return 100;
                    }
                }
                else
                {
                    if([therapyEntity[@"commentt"] length]>0)
                    {
                        return 70;
                    }
                }
                
                return 50;
            }
        }
        return 0;
    }
    else
    {
        if(therapyEntity[@"items"])
        {
            if([therapyEntity[@"comment"] length]>0)
            {
                return 100;
            }

        }
        else
        {
            if([therapyEntity[@"commentt"] length]>0)
            {
                CGRect labelSize = [therapyEntity[@"commentt"] boundingRectWithSize:CGSizeMake(tableView.frame.size.width-20, CGFLOAT_MAX)
                                                      options:NSStringDrawingUsesLineFragmentOrigin
                                                   attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12]}
                                                      context:nil];/*[therapyEntity[@"commentt"] sizeWithFont:[UIFont systemFontOfSize:12]
                                            constrainedToSize:CGSizeMake(tableView.frame.size.width-20, 20)
                                                lineBreakMode:NSLineBreakByWordWrapping];*/
                return labelSize.size.height + 50;
            }
        }
        
        return 30;
    }
    //return 25;

    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath  {
    
    if(tableView.tag == 2)  {
        return 30;
    }
    
    //NSInteger* sect_info =((NSArray*)[self getSectionInfo:indexPath.section][indexPath.row][@"content"]).count;
    
    if(self.convertMode)
    {
        for(NSIndexPath* ind in selected_items)
        {
            if(ind.section == indexPath.section)
            {
                return (((NSArray*)[self getSectionInfo:indexPath.section][indexPath.row][@"content"]).count > 1) ? 101 : 70;;
            }
        }
        return 0;
    }
    else
        return (((NSArray*)[self getSectionInfo:indexPath.section][indexPath.row][@"content"]).count > 1) ? 101 : 70;
    //return
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *therapyEntity = visits[indexPath.section];//[self.fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:indexPath.section inSection:0]];
    
    
    if(tableView.tag == 2)
    {
        VisitHistoryDateViewCell *dateCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([VisitHistoryDateViewCell class])];
        
        NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
        [dateformate setDateFormat:@"dd.MM.yyyy"];
        NSString *dateString = therapyEntity[@"startdate"];
        dateCell.titleLabel.text = dateString;
        
        if([indexPath isEqual:self.selectedIndexPath])
        {
            if(therapyEntity[@"items"])
            {
                dateCell.backgroundColor = getColorSchemeXZ();//[UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0];
            }
            else
            {
                dateCell.backgroundColor = [UIColor colorWithRed:231.0/255.0 green:76.0/255.0 blue:60.0/255.0 alpha:1.0];
            }
            
        }
        else
        {
            if(therapyEntity[@"items"])
            {
                 dateCell.backgroundColor = [UIColor colorWithRed:185.0/255.0 green:192.0/255.0 blue:197.0/255.0 alpha:1.0];
            }
            else
            {
                dateCell.backgroundColor = [UIColor colorWithRed:249.0/255.0 green:186.0/255.0 blue:186.0/255.0 alpha:1.0];
            }
        }
        
        BOOL found=false;
        
        for(NSIndexPath* pt in selected_items)
        {
            if(pt.section == indexPath.section && pt.row == indexPath.row)
            {
                found=true;
                break;
            }
        }
        
        if(found)
        {
            dateCell.selectView.hidden=NO;
        }
        else
        {
            dateCell.selectView.hidden=YES;
        }
        
        return dateCell;
    }
    else
    {
        BOOL is_found = NO;
        
        if(self.convertMode)
        {
            for(NSIndexPath* ind in selected_items)
            {
                if(ind.section == indexPath.section)
                {
                    is_found = YES;
                    break;
                }
            }
            if(!is_found)
            {
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"empty"];
                if (cell == nil) {
                    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"empty"];
                }
                return cell;
            }
            
        }
        
        NSArray *therapyItems = [self getSectionInfo:indexPath.section];//therapyEntity allTherapyItem];
        
        NSDictionary *therapyItem = therapyItems[indexPath.row];
        NSDictionary *categoryRef = (NSDictionary*)[DataBase getDB:@"services_group" withKey:@"id" andValue:therapyItem[@"section"]];
        VisitHistoryViewCell *chooseCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([VisitHistoryViewCell class])];
        chooseCell.titleLabel.text = categoryRef[@"title"];
        NSDictionary *serviceRef = (NSDictionary*)[DataBase getDB:@"services" withKey:@"id" andValue:therapyItem[@"content"][0][@"serviceid"]];
        chooseCell.toothIdLabel.text = [NSString stringWithFormat:@"%ld", [therapyItem[@"content"][0][@"toothid"] integerValue]];
        chooseCell.serviceNameLabel.text = serviceRef[@"title"];
        if(((NSArray*)therapyItem[@"content"]).count>1)
        {
            chooseCell.toothIdLabel2.hidden=NO;
            chooseCell.serviceNameLabel2.hidden=NO;
            NSDictionary *serviceRef = (NSDictionary*)[DataBase getDB:@"services" withKey:@"id" andValue:therapyItem[@"content"][1][@"serviceid"]];
            chooseCell.toothIdLabel2.text = [NSString stringWithFormat:@"%ld", [therapyItem[@"content"][0][@"toothid"] integerValue]];
            chooseCell.serviceNameLabel2.text = serviceRef[@"title"];

        }
        else
        {
            chooseCell.toothIdLabel2.hidden=YES;
            chooseCell.serviceNameLabel2.hidden=YES;
        }
        return chooseCell;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(tableView.tag == 2)  {
        self.selectedIndexPath = indexPath;
        [self.dateTableView reloadData];
        if([self.dataTableView numberOfRowsInSection:indexPath.section]>0)
        {
            [self.dataTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
        }
        else
        {
            [self.dataTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:NSNotFound inSection:indexPath.section] atScrollPosition:UITableViewScrollPositionTop animated:YES];
            
        }
        
        
    }
}

-(void) onDesc:(UIButton*)button    {
    NSDictionary* therapyEntity = visits[button.tag];
    //Therapy *therapyEntity = [self.fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:button.tag inSection:0]];
    /*NSDictionary* therapyEntity*/
    
    TherapyReadViewController *therapyReadOnlyVC = [[TherapyReadViewController alloc] initWithTherapy:therapyEntity];
    [self.navigationController pushViewController:therapyReadOnlyVC animated:YES];
}

-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state != UIGestureRecognizerStateEnded) {
        return;
    }
    CGPoint p = [gestureRecognizer locationInView:self.dateTableView];
    
    NSIndexPath *indexPath = [self.dateTableView indexPathForRowAtPoint:p];
    if (indexPath == nil)
    {
        NSLog(@"couldn't find index path");
    }
    else
    {
        // get the cell at indexPath (the one you long pressed)
        VisitHistoryViewCell* cell  = [self.dataTableView cellForRowAtIndexPath:indexPath];
        BOOL found = false;
        int i=0;
        for(NSIndexPath* pt in selected_items)
        {
            if(pt.section == indexPath.section && pt.row == indexPath.row)
            {
                found=true;
                break;
            }
            i++;
        }
        
        if(found)
        {
            [selected_items removeObjectAtIndex:i];
        }
        else
        {
            [selected_items addObject:indexPath];
        }
        
        [self.dateTableView reloadData];
        
        if(selected_items.count>0)
        {
            optionView.hidden = NO;
        }
        else
        {
            optionView.hidden = YES;
        }
        // do stuff with the cell
    }
}
/////////////////////////////////////
/////////////////////////////////////

- (UIImage *)joinImages:(UIImage *)im1 secondImage:(UIImage *)im2
{
    //Joins 3 UIImages together, stitching them vertically
    CGSize size = CGSizeMake(320, 480);
    UIGraphicsBeginImageContext(size);
    
    CGPoint image1Point = CGPointMake(0, 0);
    [im1 drawAtPoint:image1Point];
    
    CGPoint image2Point = CGPointMake(0, im1.size.height);
    [im2 drawAtPoint:image2Point];
    
    UIImage* finalImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return finalImage;
}

- (void) printItem:(NSIndexPath*) item
{
    CGSize size = CGSizeMake(1, 1);
    UIGraphicsBeginImageContextWithOptions(size, YES, 0);
    [[UIColor whiteColor] setFill];
    UIRectFill(CGRectMake(0, 0, size.width, size.height));
    UIImage *img = [self.dataTableView imageFromTableView];

    
    UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];
    //pic.delegate = self;
    
    
    UIPrintInfo *printInfo = [UIPrintInfo printInfo];
    printInfo.outputType = UIPrintInfoOutputGeneral;
    printInfo.jobName = @"PrintJob";
    pic.printInfo = printInfo;
    
    
    pic.showsPageRange = YES;
    pic.printingItem = img;
    
    
    void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) = ^(UIPrintInteractionController *printController, BOOL completed, NSError *error) {
        if (!completed && error) {
            NSLog(@"Printing could not complete because of error: %@", error);
        }
    };
    
    
    [pic presentAnimated:YES completionHandler:completionHandler];
}

- (void) onPrint
{
    for(NSIndexPath* d in selected_items)
    {
        [self printItem:d];
    }
}

- (void) onConvert
{
    self.convertMode = YES;
    [self.dataTableView reloadData];
    UIImageWriteToSavedPhotosAlbum([self.dataTableView imageFromTableView], nil, nil, nil);
    [self.view makeToast:[NSString stringWithFormat:@"Изображение сохранено в галерее"]];
    self.convertMode = NO;
    [self.dataTableView reloadData];
}

- (void)showEmail {
    
    NSString *emailTitle = [NSString stringWithFormat:@"%@ %@ %@", self.currentPatient[@"firstname"], self.currentPatient[@"middlename"], self.currentPatient[@"lastname"]];
    NSString *messageBody = @"";
    NSArray *toRecipents = [[NSArray alloc] init];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:NO];
    [mc setToRecipients:toRecipents];
    
    UIImage* img = [self.dataTableView imageFromTableView];
        
    // Get the resource path and read the file using NSData
    NSData *fileData = UIImagePNGRepresentation(img);// = [NSData dataWithContentsOfFile:filePath];
    
    // Determine the MIME type
    NSString *mimeType = @"image/png";
    
    // Add attachment
    [mc addAttachmentData:fileData mimeType:mimeType fileName:@"image"];

    // Present mail view controller on screen
    [self presentViewController:mc animated:YES completion:NULL];
    
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

- (void) onEmail
{
    [self showEmail];
}



- (IBAction)tapOptional:(id)sender {
    ShareViewController *editTherapyViewController = [[ShareViewController alloc] init];
    
    [self presentTransparentViewController:editTherapyViewController animated:NO completion:nil];
}
@end
