//
//  PhotoCollectionViewCell.h
//  DentaBook
//
//  Created by Denis Dubov on 12.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoCollectionViewCell : UICollectionViewCell

+ (CGFloat) heightText:(NSString *)text;

@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UIImageView *selectView;
@property (nonatomic, strong) NSString *commentString;

-(void) setMode:(BOOL)isFullMode;

@property (copy, nonatomic) void (^didClickDeleteButton)(NSIndexPath *indexPath);
@property (copy, nonatomic) void (^didClickMoreButton)(NSIndexPath *indexPath);

@end
