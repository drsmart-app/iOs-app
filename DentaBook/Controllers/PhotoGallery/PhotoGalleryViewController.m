//
//  PhotoGalleryViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 12.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "PhotoGalleryViewController.h"
#import "PhotoCollectionViewCell.h"
#import "HeaderReusableView.h"
#import "IBActionSheet.h"
#import "UIImageView+ImageDownloader.h"
#import "PhotoEditViewController.h"

#import <UIAlertView+Blocks/UIAlertView+Blocks.h>
#import <objc/runtime.h>
#import <MagicalRecord/MagicalRecord.h>
#import <NSDate+Calendar/NSDate+Calendar.h>
#import <JTSImageViewController/JTSImageViewController.h>

#import "Patient+Custom.h"
#import "Gallery+Custom.h"
#import "GalleryManager.h"
#import "DataManager.h"
#import "DataBase.h"
#import "ELCImagePickerController.h"
#import "PercUtils.h"
#import "ShareViewController.h"
#import "UIViewController+PresentedOver.h"
#import <MessageUI/MessageUI.h>
#import "UIView+Toast.h"
#import "HttpUtil.h"

@interface PhotoGalleryViewController () <NSFetchedResultsControllerDelegate, UIImagePickerControllerDelegate, MFMailComposeViewControllerDelegate>
@property (nonatomic, strong) NSDictionary *currentPatient;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSBlockOperation *blockOperation;
@property (nonatomic) BOOL shouldReloadCollectionView;
@property (nonatomic, strong) IBActionSheet *photoActionSheet;
@property (weak, nonatomic) IBOutlet UIButton *changeModeButton;
@end

@implementation PhotoGalleryViewController
@synthesize  photos, sections, illnessImg, nameLabel, selected_items, optionsView;
static NSString * const reuseHeaderId = @"headerId";
static NSString * const reuseFooterId = @"footerId";
static NSString * const reuseCellId = @"cellId";

-(id) initWithPatient:(NSDictionary*)patient {
    self = [super initWithNibName:@"PhotoGalleryViewController" bundle:[NSBundle mainBundle]];
    if(self)    {
        self.currentPatient = patient;
    }
    return self;
}

- (BOOL)prefersStatusBarHidden
{
    return !getNeedName();
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    selected_items = [[NSMutableArray alloc] init];
    
    self.title = @"Фотоальбом";
    
    UIImage *image2 = [UIImage imageNamed:@"back-button"];
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 addTarget:self action:@selector(onBack:)forControlEvents:UIControlEventTouchUpInside];
    [button2 setBackgroundImage:image2 forState:UIControlStateNormal];
    button2.frame = CGRectMake(0, 0, 11, 18);
    UIBarButtonItem *barButton2 = [[UIBarButtonItem alloc] initWithCustomView:button2];
    self.navigationItem.leftBarButtonItem = barButton2;
    
    UIImage *image = [UIImage imageNamed:@"patient-add-icon"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(addPatient)forControlEvents:UIControlEventTouchUpInside];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, 18, 18);
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = barButton;
    
    [self.collectionView registerClass:[HeaderReusableView class]  forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:reuseHeaderId];
    [self.collectionView registerClass:[UICollectionReusableView class]  forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:reuseFooterId];
    [self.collectionView registerClass:[PhotoCollectionViewCell class] forCellWithReuseIdentifier:reuseCellId];
    
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    flow.sectionInset = UIEdgeInsetsMake(0.0, 15.0, 0.0, 15.0);
    flow.headerReferenceSize = CGSizeMake(self.view.frame.size.width, 40.0);
    [self.collectionView setCollectionViewLayout:flow];
    
    self.collectionView.allowsMultipleSelection = YES;
    
    //self.collectionView.contentInset = UIEdgeInsetsMake(-36, 0, 0, 0);
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [self fetchTableData];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)] && getNeedName())
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    if(!getNeedName())
    {
        [[UINavigationBar appearance] setFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width , 44)];
        //[[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    }
    else
    {
        [[UINavigationBar appearance] setFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width , 64)];
        //[[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    }
    
    if(((NSArray*)self.currentPatient[@"illness"]).count>0)
    {
        
        NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
        attachment.image =  [PercUtils imageWithImage:[UIImage imageNamed:@"patient-illness-icon.png"] scaledToSize:CGSizeMake(15, 15)];
        attachment.bounds = CGRectMake(0, -3, 18, 18);
        NSMutableAttributedString *attachmentString = [[NSAttributedString attributedStringWithAttachment:attachment] mutableCopy];
        
        NSMutableAttributedString *myString= [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"  %@ %@ %@", self.currentPatient[@"lastname"], self.currentPatient[@"firstname"], self.currentPatient[@"middlename"]]];
        [attachmentString appendAttributedString:myString];
        
        nameLabel.attributedText = attachmentString;
    }
    else
    {
        nameLabel.text = [NSString stringWithFormat:@"  %@ %@ %@", self.currentPatient[@"lastname"], self.currentPatient[@"firstname"], self.currentPatient[@"middlename"]];
    }
    
    UILongPressGestureRecognizer *lpgr
    = [[UILongPressGestureRecognizer alloc]
       initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.delegate = self;
    lpgr.delaysTouchesBegan = YES;
    [self.collectionView addGestureRecognizer:lpgr];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onPrint) name:@"needPrint" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onConvert) name:@"needConvert" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onEmail) name:@"needEmail" object:nil];
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
    self.optionsView.backgroundColor = getColorScheme();
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = getColorSchemeStatus();
    }
    
    if(!getNeedName())
    {
        CGRect tempRect;
        for(UIView *sub in [[self view] subviews])
        {
            tempRect = [sub frame];
            tempRect.origin.y += 20.0f; //Height of status bar
            [sub setFrame:tempRect];
        }
        
        self.collectionView.frame = CGRectMake(self.collectionView.frame.origin.x, self.collectionView.frame.origin.y, self.collectionView.frame.size.width, self.collectionView.frame.size.height -20.0f);
    }
    
    
    self.extendedLayoutIncludesOpaqueBars = YES;
    self.automaticallyAdjustsScrollViewInsets = NO;
}

- (void) printItem:(NSIndexPath*) item
{
    PhotoCollectionViewCell* c  = [self.collectionView cellForItemAtIndexPath:item];
    
    UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];
    //pic.delegate = self;
    
    
    UIPrintInfo *printInfo = [UIPrintInfo printInfo];
    printInfo.outputType = UIPrintInfoOutputGeneral;
    printInfo.jobName = @"PrintJob";
    pic.printInfo = printInfo;
    
    
    pic.showsPageRange = YES;
    pic.printingItem = c.imageView.image;
    
    
    void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) = ^(UIPrintInteractionController *printController, BOOL completed, NSError *error) {
        if (!completed && error) {
            NSLog(@"Printing could not complete because of error: %@", error);
        }
    };
    
    
    [pic presentAnimated:YES completionHandler:completionHandler];
}

- (void) onPrint
{
    for(NSIndexPath* d in selected_items)
    {
        [self printItem:d];
    }
}

- (void) onConvert
{
    for(NSIndexPath* d in selected_items)
    {
        PhotoCollectionViewCell* c  = [self.collectionView cellForItemAtIndexPath:d];
        
        UIImage* img = [c.imageView.image copy];
        
        UIImageWriteToSavedPhotosAlbum(img, nil, nil, nil);
    }
    [self.view makeToast:[NSString stringWithFormat:@"%lu изображений сохранено в галерее", (unsigned long)selected_items.count]];
}

- (void)showEmail {
    
    NSString *emailTitle = [NSString stringWithFormat:@"%@ %@ %@", self.currentPatient[@"firstname"], self.currentPatient[@"middlename"], self.currentPatient[@"lastname"]];
    NSString *messageBody = @"";
    NSArray *toRecipents = [[NSArray alloc] init];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:NO];
    [mc setToRecipients:toRecipents];
    
    for(NSIndexPath* d in selected_items)
    {
        PhotoCollectionViewCell* c  = [self.collectionView cellForItemAtIndexPath:d];
        
        UIImage* img = [c.imageView.image copy];
        
        // Get the resource path and read the file using NSData
        NSData *fileData = UIImagePNGRepresentation(img);// = [NSData dataWithContentsOfFile:filePath];
        
        // Determine the MIME type
        NSString *mimeType = @"image/png";
        
        // Add attachment
        [mc addAttachmentData:fileData mimeType:mimeType fileName:@"test"];
    }
    
    // Present mail view controller on screen
    [self presentViewController:mc animated:YES completion:NULL];
    
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

- (void) onEmail
{
    [self showEmail];
}

-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state != UIGestureRecognizerStateEnded) {
        return;
    }
    CGPoint p = [gestureRecognizer locationInView:self.collectionView];
    
    NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:p];
    if (indexPath == nil)
    {
        NSLog(@"couldn't find index path");
    }
    else
    {
        // get the cell at indexPath (the one you long pressed)
        UICollectionViewCell* cell = [self.collectionView cellForItemAtIndexPath:indexPath];
        BOOL found = false;
        int i=0;
        for(NSIndexPath* pt in selected_items)
        {
            if(pt.section == indexPath.section && pt.row == indexPath.row)
            {
                found=true;
                break;
            }
            i++;
        }
        
        if(found)
        {
            [selected_items removeObjectAtIndex:i];
        }
        else
        {
            [selected_items addObject:indexPath];
        }
        
        [self.collectionView reloadData];
        
        if(selected_items.count>0)
        {
            optionsView.hidden = NO;
        }
        else
        {
            optionsView.hidden = YES;
        }
        // do stuff with the cell
    }
}

-(void) viewWillAppear:(BOOL)animated   {
    [self.collectionView reloadItemsAtIndexPaths:[self.collectionView indexPathsForVisibleItems]];
}

- (void) viewDidAppear:(BOOL)animated
{
    [self fetchTableData];
    [self.collectionView reloadData];
}

-(void)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) addPatient  {
    [self.view endEditing:YES];
    self.photoActionSheet = [[IBActionSheet alloc] initWithTitle:NSLocalizedString(@"Сделать фотографию", nil)
                                                        delegate:self
                                               cancelButtonTitle:NSLocalizedString(@"Отмена", nil)
                                          destructiveButtonTitle:nil
                                          otherButtonTitlesArray:@[@"Камера", NSLocalizedString(@"Выбрать из галереи", nil)]];
    self.photoActionSheet.tag = 1;
    [self.photoActionSheet showInView:self.view];
}

- (IBAction)onChangeMode:(UIButton*)sender {
    sender.selected = !sender.selected;
    
    [self.collectionView reloadItemsAtIndexPaths:[self.collectionView indexPathsForVisibleItems]];
}

- (void)actionSheet:(IBActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    //NSLog(@"ERROR: Server is not available!");
    UIApplication *app = [UIApplication sharedApplication];
    //[app performSelector:@selector(suspend)];
    
    if(actionSheet.tag == 1)    {
        if(buttonIndex == 0){
            
            if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                
                [UIAlertView showWithTitle:@"Error" message:@"No Camera" cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
                
                return;
            }
            
            if (YES) {
                [self showImagePickerUsingSourceType:UIImagePickerControllerSourceTypeCamera];
            }else{
                [UIAlertView showWithTitle:@"Error" message:@"This app does not have access to your photos or videos" cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
            }
            
        } else if(buttonIndex == 1) {
            
            if (YES) {
                [self showImagePickerUsingSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
            }else{
                [UIAlertView showWithTitle:@"Error" message:@"This app does not have access to your photos or videos" cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
            }
        }
    }
}

- (void)showImagePickerUsingSourceType:(UIImagePickerControllerSourceType)type{
    
    // Create the image picker
    if(type == UIImagePickerControllerSourceTypeCamera)
    {
        UIImagePickerController *imagePickerController = [UIImagePickerController new];
        imagePickerController.allowsEditing = NO;
        imagePickerController.delegate = self;
        imagePickerController.sourceType = type;
        
        [self presentViewController:imagePickerController animated:YES completion:^{
            
        }];
    }
    else
    {
        ELCImagePickerController *elcPicker = [[ELCImagePickerController alloc] initImagePicker];
        elcPicker.delegate = self;
        elcPicker.maximumImagesCount = 40; //Set the maximum number of images to select, defaults to 4
        elcPicker.returnsOriginalImage = YES; //Only return the fullScreenImage, not the fullResolutionImage
        elcPicker.returnsImage = YES; //Return UIimage if YES. If NO, only return asset location information
        elcPicker.onOrder = YES; //For multiple image selection, display and return selected order of images
        elcPicker.imagePickerDelegate = self;
        
        [self presentViewController:elcPicker animated:YES completion:nil];
    }
    
}

- (void)elcImagePickerController:(ELCImagePickerController *)picker didFinishPickingMediaWithInfo:(NSArray *)info
{
    NSLog(@"Info about images: %@",info);
    if(info.count==1)
    {
        NSString *mediaType = info[0][UIImagePickerControllerMediaType];
        UIImage *photo = nil;
        if ([mediaType isEqualToString:@"ALAssetTypePhoto"])
        {
            photo = info[0][UIImagePickerControllerOriginalImage];//info[UIImagePickerControllerEditedImage];
        }
        
        [self dismissViewControllerAnimated:YES completion:^{
            
            PhotoEditViewController *editViewController = [[PhotoEditViewController alloc] initWithPhoto:photo forPatient:self.currentPatient];
            [self.navigationController pushViewController:editViewController animated:YES];
        }];
    }
    else
    {
        for(NSDictionary* dd in info)
        {
            NSMutableDictionary* galleryEntity = [[NSMutableDictionary alloc] init];//Gallery *galleryEntity = [Gallery MR_createEntity];
            NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
            [dateformate setDateFormat:@"dd.MM.yyyy"];
            NSString *dateString = [dateformate stringFromDate:[NSDate date]];
            galleryEntity[@"createat"] = dateString;
            galleryEntity[@"createdat"] = dateString;
            galleryEntity[@"comment"] = @"";
            galleryEntity[@"patientid"] = self.currentPatient[@"patientid"];
            galleryEntity[@"id"] = GUIDString();
            UIImage *photo = nil;
            photo = dd[UIImagePickerControllerOriginalImage];
            [[GalleryManager shared] createGallery:galleryEntity success:^{
                [[GalleryManager shared] uploadPhoto:photo toPatient:galleryEntity completion:^(NSError *error) {
                    
                }];
            } failure:^{
                //
            }];
            [self fetchTableData];
            [self.collectionView reloadData];
        }
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
}

- (void)elcImagePickerControllerDidCancel:(ELCImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = info[UIImagePickerControllerMediaType];
    
    UIImage *photo = nil;
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]){
        photo = info[UIImagePickerControllerOriginalImage];//info[UIImagePickerControllerEditedImage];
    }
    
    [self dismissViewControllerAnimated:YES completion:^{
        
        PhotoEditViewController *editViewController = [[PhotoEditViewController alloc] initWithPhoto:photo forPatient:self.currentPatient];
        [self.navigationController pushViewController:editViewController animated:YES];
    }];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)fetchTableData {
    /*_fetchedResultsController = nil;
    NSError *error = nil;
    [self.fetchedResultsController performFetch:&error];*/
    
    NSArray* tmp = [DataBase getDBArray:@"photo" withKey:@"patientid" andValue:self.currentPatient[@"patientid"]];
    
    NSMutableArray* result = [[NSMutableArray alloc] init];
    NSMutableArray* tmp_sections = [[NSMutableArray alloc] init];
    
    for(NSDictionary* pat in tmp)
    {

            //nmd[@"index"] = [[(NSString*)pat[@"firstname"] substringWithRange:NSMakeRange(0, 1)] uppercaseString];
        [tmp_sections addObject:pat[@"createat"]];
    }
    
    sections = [[NSMutableArray alloc] init];
    for(NSString* hd in [tmp_sections valueForKeyPath:@"@distinctUnionOfObjects.self"])
    {
        NSMutableDictionary* ndms = [[NSMutableDictionary alloc] init];
        ndms[@"section"] = hd;
        NSMutableArray* mna=[[NSMutableArray alloc] init];
        for(NSDictionary* dict in tmp)
        {
            if([[(NSString*)dict[@"createat"] uppercaseString] isEqualToString:[hd uppercaseString]])
            {
                [mna addObject:dict];
            }
        }
        ndms[@"content"]=mna;
        [sections addObject:ndms];
    }
    
    photos = result;
}

#pragma mark - UICollectionViewDelegate & UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return sections.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    /*id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];*/
    return ((NSArray*)sections[section][@"content"]).count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout columnCountForSection:(NSInteger)section
{
    return 3;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    //id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][indexPath.section];
    NSDictionary* sec = sections[indexPath.section];
    NSDictionary *item = sec[@"content"][0];
    
    if([kind isEqual:UICollectionElementKindSectionHeader]) {
        HeaderReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:reuseHeaderId forIndexPath:indexPath];
        
        //NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
        //[dateformate setDateFormat:@"dd.MM.yyyy"];
        //NSString *dateString = [dateformate stringFromDate:item.createdAt];
        headerView.titleLabel.text = sec[@"section"];//dateString;
        
        return headerView;
    }
    else {
        UICollectionReusableView *footerView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:reuseFooterId forIndexPath:indexPath];
        return footerView;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat squareSize; 
    if(self.changeModeButton.selected)  {
        squareSize = ([[UIScreen mainScreen] bounds].size.width-20);
        //Gallery *item = [self.fetchedResultsController objectAtIndexPath:indexPath];
        NSDictionary* item = sections[indexPath.section][@"content"][indexPath.row];
        
        if([(NSString*)item[@"comment"] length] > 0) {
            CGFloat height = [PhotoCollectionViewCell heightText:item[@"comment"]];
            return CGSizeMake(squareSize, squareSize + 7 + 22 + 7 + 1 + 7 + height+7);
        } else  {
            return CGSizeMake(squareSize, squareSize + 7 + 22 + 7);
        }
    }
    else
    {
        squareSize = ([[UIScreen mainScreen] bounds].size.width-40);
    }
    
    return CGSizeMake(squareSize/2, squareSize/2);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *item = sections[indexPath.section][@"content"][indexPath.row];//[self.fetchedResultsController objectAtIndexPath:indexPath];
    
    PhotoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseCellId forIndexPath:indexPath];
    cell.indexPath = indexPath;
    
    [cell.imageView addSpinnerView];
    cell.imageView.spinnerView.color = [UIColor grayColor];
    cell.imageView.imageModel.imageCachePolicy = ImageCachePolicyMemoryAndFileCache;
    cell.imageView.imageModel.queuePriority = NSOperationQueuePriorityHigh;
    cell.imageView.contentMode = UIViewContentModeScaleAspectFit;//UIViewContentModeScaleAspectFit; UIViewContentModeScaleAspectFill
    [cell.imageView setImageWithURLString:[NSString stringWithFormat:[DataManager urlForPath:@"gallery/%@/image"], item[@"id"]]
                                                 success:^(ImageRequestModel *object, UIImage *image) {
                                                     
                                                 } failure:^(ImageRequestModel *object, NSError *error) {
                                                     
                                                 } progress:^(ImageRequestModel *object, CGFloat progress) {
                                                     //
                                                 }];
    [cell setMode:self.changeModeButton.selected];
    [cell setCommentString:item[@"comment"]];
    
    BOOL found=false;
    
    for(NSIndexPath* pt in selected_items)
    {
        if(pt.section == indexPath.section && pt.row == indexPath.row)
        {
            found=true;
            break;
        }
    }
    
    if(found)
    {
        cell.selectView.hidden=NO;
    }
    else
    {
        cell.selectView.hidden=YES;
    }
    
    //cell.selectedBackgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"affair-check-icon.png"]];
    
    cell.didClickDeleteButton = ^(NSIndexPath *indexPath)   {
        [UIAlertView showWithTitle:@"" message:@"Вы уверены что хотите удалить фотографию?"
                 cancelButtonTitle:@"Отмена"
                 otherButtonTitles:@[@"Да"] tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex)   {
                     if(buttonIndex == 1)   {
                         NSDictionary *item = sections[indexPath.section][@"content"][indexPath.row];
                         [DataBase killDB:@"photo" withKey:@"id" andValue:item[@"id"]];
                         [self fetchTableData];
                         [self.collectionView reloadData];
                         //Gallery *item = [self.fetchedResultsController objectAtIndexPath:indexPath];
                         //[item MR_deleteEntity];
                         //[[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                     }
                 }];
    };
    
    cell.didClickMoreButton = ^(NSIndexPath *indexPath)   {
        NSDictionary *item = sections[indexPath.section][@"content"][indexPath.row];
        PhotoCollectionViewCell *cell = (PhotoCollectionViewCell*)[collectionView cellForItemAtIndexPath:indexPath];
        
        NSMutableArray *items = [NSMutableArray new];
        [items addObject:item[@"comment"]];
        [items addObject:cell.imageView.image];
        NSArray *activityItems = [NSArray arrayWithArray:items];
        UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
        [self presentViewController:activityViewController animated:YES completion:nil];
    };
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    PhotoCollectionViewCell *cell = (PhotoCollectionViewCell*)[collectionView cellForItemAtIndexPath:indexPath];
    
    
    JTSImageInfo *imageInfo = [[JTSImageInfo alloc] init];
    imageInfo.image = cell.imageView.image;
    imageInfo.referenceRect = cell.imageView.frame;
    imageInfo.referenceView = cell.imageView.superview;
    
    JTSImageViewController *imageViewer = [[JTSImageViewController alloc]
                                           initWithImageInfo:imageInfo
                                           mode: JTSImageViewControllerMode_Image
                                           backgroundStyle: JTSImageViewControllerBackgroundOption_Scaled];
    
    // Present the view controller.
    [imageViewer showFromViewController:self transition:JTSImageViewControllerTransition_FromOriginalPosition];
}

- (IBAction)tapDelete:(id)sender
{
    
    for(NSIndexPath* dd in selected_items)
    {
        NSDictionary *item = sections[dd.section][@"content"][dd.row];
        [DataBase killDB:@"photo" withKey:@"id" andValue:item[@"id"]];
    }
}

- (IBAction)tapShare:(id)sender
{
    ShareViewController *editTherapyViewController = [[ShareViewController alloc] init];

    [self presentTransparentViewController:editTherapyViewController animated:NO completion:nil];
}


@end
