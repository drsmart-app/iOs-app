//
//  HeaderReusableView.m
//  DentaBook
//
//  Created by Denis Dubov on 12.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "HeaderReusableView.h"

@implementation HeaderReusableView

- (id)initWithFrame:(CGRect)aRect   {
    self = [super initWithFrame:aRect];
    if(self)    {
        [self setup];
    }
    return self;
}

-(void) setup   {
    self.backgroundColor = [UIColor clearColor];
    
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, self.frame.size.width-20, self.frame.size.height)];
    self.titleLabel.textColor = [UIColor lightGrayColor];
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    self.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:14.0];
    [self addSubview:self.titleLabel];
}

@end
