//
//  PhotoGalleryViewController.h
//  DentaBook
//
//  Created by Denis Dubov on 12.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Patient;

@interface PhotoGalleryViewController : UIViewController <UIImagePickerControllerDelegate, UIGestureRecognizerDelegate, UINavigationControllerDelegate>

-(id) initWithPatient:(NSDictionary*)patient;
@property (nonatomic, retain) NSArray* photos;
@property (nonatomic, retain) NSMutableArray* sections;
@property (weak, nonatomic) IBOutlet UIImageView *illnessImg;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (nonatomic, retain) NSMutableArray* selected_items;
@property (weak, nonatomic) IBOutlet UIView *optionsView;

- (IBAction)tapDelete:(id)sender;
- (IBAction)tapShare:(id)sender;

@end
