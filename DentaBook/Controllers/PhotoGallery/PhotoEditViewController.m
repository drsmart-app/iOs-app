//
//  PhotoEditViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 16.03.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "PhotoEditViewController.h"
#import "UIView+GestureBlocks.h"

#import <SAMTextView/SAMTextView.h>
#import <UIAlertView+Blocks/UIAlertView+Blocks.h>
#import <objc/runtime.h>
#import <MagicalRecord/MagicalRecord.h>
#import <NSDate+Calendar/NSDate+Calendar.h>

#import "Patient+Custom.h"
#import "Gallery+Custom.h"
#import "GalleryManager.h"
#import "DataManager.h"
#import "DataBase.h"
#import "HttpUtil.h"

@interface PhotoEditViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;

@property (nonatomic, strong) NSDictionary *currentPatient;
@property (nonatomic, strong) NSDictionary *currentAffair;
@property (nonatomic, strong) NSDictionary *currentExpence;
@property (nonatomic, strong) UIImage *photo;
@property (nonatomic, strong) NSString *commentString;

@end

@implementation PhotoEditViewController
@synthesize commentTextView, placeholderLabel;
-(id) initWithPhoto:(UIImage*)photoImage forPatient:(NSDictionary*)patient
{
    self = [super initWithNibName:@"PhotoEditViewController" bundle:[NSBundle mainBundle]];
    if(self)    {
        self.currentPatient = patient;
        self.photo = photoImage;
    }
    return self;
}

-(id) initWithPhoto:(UIImage*)photoImage forAffair:(NSDictionary*)affair
{
    self = [super initWithNibName:@"PhotoEditViewController" bundle:[NSBundle mainBundle]];
    if(self)    {
        self.currentAffair = affair;
        self.photo = photoImage;
    }
    return self;
}

-(id) initWithPhoto:(UIImage*)photoImage forExpence:(NSDictionary*)expence
{
    self = [super initWithNibName:@"PhotoEditViewController" bundle:[NSBundle mainBundle]];
    if(self)    {
        self.currentExpence = expence;
        self.photo = photoImage;
    }
    return self;
}

-(id)  initWithPhoto:(UIImage*)photo forProfile:(BOOL)forProfile
{
    self = [super initWithNibName:@"PhotoEditViewController" bundle:[NSBundle mainBundle]];
    if(self)    {
        self.photo = photo;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImage *image2 = [UIImage imageNamed:@"back-button"];
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 addTarget:self action:@selector(onBack:)forControlEvents:UIControlEventTouchUpInside];
    [button2 setBackgroundImage:image2 forState:UIControlStateNormal];
    button2.frame = CGRectMake(0, 0, 11, 18);
    UIBarButtonItem *barButton2 = [[UIBarButtonItem alloc] initWithCustomView:button2];
    self.navigationItem.leftBarButtonItem = barButton2;
    
    /*self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Сохранить"
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(onSave:)];*/
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(onSave:)forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"Сохранить" forState:UIControlStateNormal];
    [button setTitle:@"Сохранить" forState:UIControlStateSelected];
    button.frame = CGRectMake(0, 0, 65, 25);
    button.titleLabel.font = [UIFont systemFontOfSize:12];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    self.photoImageView.image = self.photo;
    [self.photoImageView initialiseTapHandler:^(UIGestureRecognizer *sender) {
        [self.view endEditing:YES];
    } forTaps:1];
    
    self.commentString = @"";
    
    self.commentTextView.font = [UIFont fontWithName:@"Helvetica" size:14.0];
    
    //self.commentTextView.placeholder = @"Комментарий";
    commentTextView.delegate = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = getColorSchemeStatus();
    }
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
}

-(void)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) onSave:(id)sender   {
    NSMutableDictionary* galleryEntity = [[NSMutableDictionary alloc] init];//Gallery *galleryEntity = [Gallery MR_createEntity];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"dd.MM.yyyy"];
    NSString *dateString = [dateformate stringFromDate:[NSDate date]];
    galleryEntity[@"createat"] = dateString;
    galleryEntity[@"createdat"] = dateString;
    galleryEntity[@"comment"] = self.commentTextView.text;
    
    
    galleryEntity[@"id"] = GUIDString();
    //[[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];

    if(self.currentAffair&&!self.currentPatient&&!self.currentExpence)
    {
        galleryEntity[@"affairid"] = self.currentAffair[@"id"];
        [[GalleryManager shared] createGallery:galleryEntity success:^{
            [[GalleryManager shared] uploadPhoto:self.photo toAffair:galleryEntity completion:^(NSError *error) {
                
            }];
            [self.navigationController popViewControllerAnimated:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"photoUploaded" object:nil userInfo:nil];
        } failure:^{
            //
        }];
    }
    else if(!self.currentAffair&&self.currentPatient&&!self.currentExpence)
    {
        galleryEntity[@"patientid"] = self.currentPatient[@"patientid"];
        [[GalleryManager shared] createGallery:galleryEntity success:^{
            [[GalleryManager shared] uploadPhoto:self.photo toPatient:galleryEntity completion:^(NSError *error) {
                
            }];
            [self.navigationController popViewControllerAnimated:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"photoUploaded" object:nil userInfo:nil];
        } failure:^{
            //
        }];
    }
    else if(!self.currentAffair&&!self.currentPatient&&self.currentExpence)
    {
        galleryEntity[@"expenceid"] = self.currentExpence[@"id"];
        [[GalleryManager shared] createGallery:galleryEntity success:^{
            [[GalleryManager shared] uploadPhoto:self.photo toExpence:galleryEntity completion:^(NSError *error) {
                
            }];
            [self.navigationController popViewControllerAnimated:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"photoUploaded" object:nil userInfo:nil];
        } failure:^{
            //
        }];
    }
    else
    {
        galleryEntity[@"profile"] = @"yes";
        [[GalleryManager shared] createGallery:galleryEntity success:^{
            [[GalleryManager shared] uploadPhoto:self.photo forProfile:YES completion:^(NSError *error) {
                
            }];
            [self.navigationController popViewControllerAnimated:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"photoUploaded" object:nil userInfo:nil];
        } failure:^{
            //
        }];
    }
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView   {
    //[self onCommonComment];
    return YES;
}

- (CGFloat) AACStatusBarHeight
{
    CGSize statusBarSize = [[UIApplication sharedApplication] statusBarFrame].size;
    return MIN(statusBarSize.width, statusBarSize.height);
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = -keyboardSize.height;//+self.navigationController.navigationBar.frame.size.height+[self AACStatusBarHeight];
        self.view.frame = f;
    }];
}

-(void)keyboardWillHide:(NSNotification *)notification
{

    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = 0.0f;//+self.navigationController.navigationBar.frame.size.height+[self AACStatusBarHeight];
        self.view.frame = f;
    }];
}


- (void)textViewDidBeginEditing:(UITextView *)textView
{
    placeholderLabel.hidden=YES;
}


- (void)textViewDidEndEditing:(UITextView *)textView
{
    if([textView.text length]>0)
    {
    }
    else
    {
        placeholderLabel.hidden=NO;
    }
}

@end
