//
//  ShareViewController.h
//  DentaBook
//
//  Created by Mac Mini on 11.12.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShareViewController : UIViewController
- (IBAction)tapCancel:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *bgView;
- (IBAction)tapPrint:(id)sender;
- (IBAction)tapConvert:(id)sender;
- (IBAction)tapEmail:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *cancelBackView;
@property (nonatomic) BOOL multiple;

@end
