//
//  PhotoEditViewController.h
//  DentaBook
//
//  Created by Denis Dubov on 16.03.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Patient;

@interface PhotoEditViewController : UIViewController<UITextViewDelegate>

-(id) initWithPhoto:(UIImage*)photoImage forPatient:(NSDictionary*)patient;
-(id) initWithPhoto:(UIImage*)photoImage forAffair:(NSDictionary*)affair;
-(id) initWithPhoto:(UIImage*)photoImage forExpence:(NSDictionary*)expence;
-(id)  initWithPhoto:(UIImage*)photo forProfile:(BOOL)forProfile;
@property (weak, nonatomic) IBOutlet UITextView *commentTextView;
@property (weak, nonatomic) IBOutlet UILabel *placeholderLabel;

@end
