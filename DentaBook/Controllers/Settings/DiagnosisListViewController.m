//
//  DiagnosisListViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 15.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "DiagnosisListViewController.h"
#import "DiagnosisAddGroupViewController.h"
#import "IllnessAddGroupViewController.h"

#import <MagicalRecord/MagicalRecord.h>
#import <JGProgressHUD/JGProgressHUD.h>
#import <SWTableViewCell/SWTableViewCell.h>

#import "DentalDiagnosis.h"
#import "PatientIllness.h"
#import "IllnessManager.h"
#import "DiagnosisManager.h"
#import "DataBase.h"
#import "PercUtils.h"
#import "CZPicker.h"
#import "HttpUtil.h"

@interface DiagnosisListViewController ()<NSFetchedResultsControllerDelegate, SWTableViewCellDelegate, CZPickerViewDelegate, CZPickerViewDelegate>
@property (nonatomic) SettingsDataContentMode mode;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@end

@implementation DiagnosisListViewController

@synthesize diagnoses;
-(id) initWithContentMode:(SettingsDataContentMode)mode {
    self = [super initWithNibName:@"DiagnosisListViewController" bundle:[NSBundle mainBundle]];
    if(self)    {
        self.mode = mode;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = (self.mode == kDataContentDiagnosis)?@"Диагнозы":@"Заболевания";
    
    UIImage *image = [UIImage imageNamed:@"patient-add-icon"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(onAdd:)forControlEvents:UIControlEventTouchUpInside];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, 18, 18);
    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = barButton;
    
    UIImage *image2 = [UIImage imageNamed:@"back-button"];
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 addTarget:self action:@selector(onBack:)forControlEvents:UIControlEventTouchUpInside];
    [button2 setBackgroundImage:image2 forState:UIControlStateNormal];
    button2.frame = CGRectMake(0, 0, 11, 18);
    UIBarButtonItem *barButton2 = [[UIBarButtonItem alloc] initWithCustomView:button2];
    self.navigationItem.leftBarButtonItem = barButton2;
    
    [self fetchTableData];
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
    
    /*self.extendedLayoutIncludesOpaqueBars = YES;
    
    //self.navigationController.navigationBar.translucent = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;*/
}

- (void) viewDidAppear:(BOOL)animated
{
    [self fetchTableData];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(IBAction)onAdd:(id)sender
{
    if(self.mode == kDataContentDiagnosis)  {
        DiagnosisAddGroupViewController *dnt = [DiagnosisAddGroupViewController new];
        dnt.didClickCreateName = ^(NSString *exName, NSString *name)  {
            
            JGProgressHUD *HUD = [JGProgressHUD progressHUDWithStyle:JGProgressHUDStyleDark];
            HUD.textLabel.text = @"Сохранение";
            [HUD showInView:self.view];
            [[DiagnosisManager shared] createDiagnosisByTitle:name subTitle:exName success:^{
                [HUD dismissAnimated:YES];
            } failure:^{
                [HUD dismissAnimated:YES];
            }];
        };
        [self.navigationController pushViewController:dnt animated:YES];
    } else  {
        IllnessAddGroupViewController *dnt = [IllnessAddGroupViewController new];
        dnt.didClickCreateName = ^(NSString *name)  {
            
            JGProgressHUD *HUD = [JGProgressHUD progressHUDWithStyle:JGProgressHUDStyleDark];
            HUD.textLabel.text = @"Сохранение";
            [HUD showInView:self.view];
            [[IllnessManager shared] createIllnessByTitle:name success:^{
                [HUD dismissAnimated:YES];
            } failure:^{
                [HUD dismissAnimated:YES];
            }];
        };
        [self.navigationController pushViewController:dnt animated:YES];
    }
}

-(void)fetchTableData {
    if(self.mode == kDataContentDiagnosis)
    {
        diagnoses = [DataBase getDBArray:@"diagnosis" withKey:@"" andValue:@""];
    }
    else
    {
        diagnoses = [DataBase getDBArray:@"illness" withKey:@"" andValue:@""];
    }
    //_fetchedResultsController = nil;
    //NSError *error = nil;
    //[self.fetchedResultsController performFetch:&error];
}

#pragma mark -
#pragma mark Option Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    //NSInteger count = [self.fetchedResultsController.fetchedObjects count];
    return diagnoses.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section    {
    return [UIView new];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section    {
    
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section   {
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section   {
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath  {
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *name = nil;
    NSString *exName = nil;
    UITableViewCell *cell = nil;
    
    if(self.mode == kDataContentDiagnosis)  {
        NSDictionary *categoryTherapy = [diagnoses objectAtIndex:indexPath.row]; //[self.fetchedResultsController objectAtIndexPath:indexPath];
        name = categoryTherapy[@"title"];
        exName = categoryTherapy[@"subtitle"];
    } else  {
        NSDictionary *categoryTherapy = [diagnoses objectAtIndex:indexPath.row];//[self.fetchedResultsController objectAtIndexPath:indexPath];
        name = categoryTherapy[@"title"];
        //name = categoryTherapy.name;
    }
    
    SWTableViewCell *titleCell = (SWTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"titleCell2"];
    if (titleCell == nil) {
        titleCell = [[SWTableViewCell alloc] initWithStyle:((self.mode == kDataContentDiagnosis)?UITableViewCellStyleSubtitle:UITableViewCellStyleDefault) reuseIdentifier:@"titleCell2"];
        titleCell.delegate = self;
        titleCell.rightUtilityButtons = [self rightButtons];
        titleCell.textLabel.textColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
        titleCell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:12];
        titleCell.detailTextLabel.textColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
        titleCell.detailTextLabel.font = [UIFont fontWithName:@"Helvetica" size:12];
    }
    titleCell.textLabel.text = name;
    
    if(self.mode == kDataContentDiagnosis)  {
        titleCell.detailTextLabel.text = exName;
    }
    cell = titleCell;
    
    return cell;
}

- (NSArray *)rightButtons
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    //[rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:231.0f/255.0f green:76.0f/255.0f blue:60.0f/255.0f alpha:1.0f] icon:[UIImage imageNamed:@"gallery-trash-icon.png"]];
    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:231.0f/255.0f green:76.0f/255.0f blue:60.0f/255.0f alpha:1.0f] icon:[PercUtils imageWithImage:[UIImage imageNamed:@"delete_white.png"] scaledToSize:CGSizeMake(20, 25)]];
    
    //[rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:20.0f/255.0f green:162.0f/255.0f blue:243.0f/255.0f alpha:1.0f] icon:[PercUtils imageWithImage:[UIImage imageNamed:@"pencil_white.png"] scaledToSize:CGSizeMake(10, 25)] ];
    
    return rightUtilityButtons;
}

- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell   {
    return YES;
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    switch (index) {
        case 0: {
            NSIndexPath *cellIndexPath = [self.tableView indexPathForCell:cell];
            if(self.mode == kDataContentDiagnosis)  {
                CZPickerView *picker = [[CZPickerView alloc] initWithHeaderTitle:@"Удалить?" cancelButtonTitle:@"Отмена" confirmButtonTitle:@"Да"];
                picker.headerBackgroundColor = getColorScheme();
                picker.tag = [self.tableView indexPathForCell:cell].row;
                picker.delegate = self;
                picker.dataSource = self;
                picker.allowMultipleSelection = YES;
                [picker show];
            } else  {
                NSDictionary *categoryTherapy = [diagnoses objectAtIndex:cellIndexPath.row];
                [[IllnessManager shared] deleteIllnessById:categoryTherapy[@"id"] success:^{
                    //
                    [self fetchTableData];
                    [self.tableView reloadData];
                } failure:^{
                    //
                }];
            }
        }
            break;
        default:
            break;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSString *)czpickerView:(CZPickerView *)pickerView titleForRow:(NSInteger)row
{
    return @"Да";
    /*DentalDiagnosis *denta = [DentalDiagnosis MR_findAllSortedBy:@"remoteId" ascending:YES][row];
     return denta.name;*/
}

- (NSInteger)numberOfRowsInPickerView:(CZPickerView *)pickerView
{
    return 0;
}

- (void)czpickerView:(CZPickerView *)pickerView didConfirmWithItemAtRow:(NSInteger)row{
    
    //DentalDiagnosis *categoryTherapy = [self.fetchedResultsController objectAtIndexPath:cellIndexPath];
    NSDictionary *categoryTherapy = [diagnoses objectAtIndex:pickerView.tag]; //[self.fetchedResultsController objectAtIndexPath:indexPath];
    [[DiagnosisManager shared] deleteDiagnosisById:categoryTherapy[@"id"] success:^{
        [self fetchTableData];
        [self.tableView reloadData];
        //
    } failure:^{
        //
    }];
}

- (void)czpickerView:(CZPickerView *)pickerView didConfirmWithItemsAtRows:(NSArray *)rows   {
    //DentalDiagnosis *categoryTherapy = [self.fetchedResultsController objectAtIndexPath:cellIndexPath];
    NSDictionary *categoryTherapy = [diagnoses objectAtIndex:pickerView.tag]; //[self.fetchedResultsController objectAtIndexPath:indexPath];
    [[DiagnosisManager shared] deleteDiagnosisById:categoryTherapy[@"id"] success:^{
        [self fetchTableData];
        [self.tableView reloadData];
        //
    } failure:^{
        //
    }];
}

- (void)czpickerViewDidClickCancelButton:(CZPickerView *)pickerView{
    NSLog(@"Canceled.");
}

@end
