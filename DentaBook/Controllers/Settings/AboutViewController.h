//
//  AboutViewController.h
//  DentaBook
//
//  Created by Mac Mini on 17.02.17.
//  Copyright © 2017 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *table;

@end
