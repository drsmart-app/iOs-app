//
//  AffairAddGroupViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 04.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "DiagnosisAddGroupViewController.h"

#import <MagicalRecord/MagicalRecord.h>

#import "DentalDiagnosis.h"
#import "HttpUtil.h"

@interface DiagnosisAddGroupViewController ()
@property (weak, nonatomic) IBOutlet UITextField *categoryTitleLabel;
@property (weak, nonatomic) IBOutlet UITextField *extendTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *bottomLine;
@end

@implementation DiagnosisAddGroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImage *image2 = [UIImage imageNamed:@"back-button"];
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 addTarget:self action:@selector(onBack:)forControlEvents:UIControlEventTouchUpInside];
    [button2 setBackgroundImage:image2 forState:UIControlStateNormal];
    button2.frame = CGRectMake(0, 0, 11, 18);
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button2];
    self.navigationItem.leftBarButtonItem = barButton;
    
    self.title = @"Добавить диагноз";
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
    
    self.extendedLayoutIncludesOpaqueBars = YES;
    self.automaticallyAdjustsScrollViewInsets = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated   {
    [self.categoryTitleLabel becomeFirstResponder];
}

- (IBAction)onSave:(id)sender {
    if(self.categoryTitleLabel.text.length == 0 || self.extendTitleLabel.text.length == 0)
        return;
    
    if(self.didClickCreateName) {
        self.didClickCreateName(self.extendTitleLabel.text, self.categoryTitleLabel.text);
    }
    
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
