//
//  SettingsViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 08.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "SettingsViewController.h"
#import "SServicesViewController.h"
#import "DiagnosisListViewController.h"

#import "UserManager.h"
#import "DataManager.h"

#import "PersonalizationViewController.h"

#import <UIAlertView+Blocks/UIAlertView+Blocks.h>
#import "LockViewController.h"

#import "HttpUtil.h"
#import "ProfileViewController.h"

#import "AboutViewController.h"

@interface SettingsViewController ()
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Настройки";
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = getColorSchemeStatus();
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark Option Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 8;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section    {
    return [UIView new];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section    {
    /*UIView* uwv = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 1)];
    UIView* uwv1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 1)];
    uwv1.backgroundColor = [UIColor colorWithRed:239.0f/255.0f green:239.0f/255.0f blue:243.0f/255.0f alpha:1];
    [uwv addSubview:uwv1];
    return uwv;*/
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section   {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section   {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath  {
    if(indexPath.row == 7)
        return 0;
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    
    UITableViewCell *titleCell = [tableView dequeueReusableCellWithIdentifier:@"titleCell2"];
    if (titleCell == nil) {
        titleCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"titleCell2"];
        titleCell.accessoryType = UITableViewCellAccessoryNone;
        titleCell.textLabel.textColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
        titleCell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:12];
    }
    
    switch (indexPath.row) {
        case 0:
            titleCell.textLabel.text = @"Прайс-лист";
            break;
            
        case 1:
            titleCell.textLabel.text = @"Основные диагнозы";
            break;
            
        case 2:
            titleCell.textLabel.text = @"Заболевания";
            break;
            
        case 3:
            titleCell.textLabel.text = @"Персонализация";
            break;
            
        case 4:
            titleCell.textLabel.text = @"Настройка профиля";
            break;
            
        case 5:
            titleCell.textLabel.text = @"О программе";
            break;
            
        case 6:
            titleCell.textLabel.text = @"Выйти";
            titleCell.textLabel.textColor = [UIColor redColor];
            break;
            
            
        default:
            break;
    }
    
    cell = titleCell;
    cell.layoutMargins = UIEdgeInsetsZero;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(indexPath.row == 0)  {
        SServicesViewController *servicesViewController = [SServicesViewController new];
        [self.navigationController pushViewController:servicesViewController animated:YES];
    } else if(indexPath.row == 1)  {
        DiagnosisListViewController *servicesViewController = [[DiagnosisListViewController alloc] initWithContentMode:kDataContentDiagnosis];
        [self.navigationController pushViewController:servicesViewController animated:YES];
    }
    else if(indexPath.row == 2)
    {
        DiagnosisListViewController *servicesViewController = [[DiagnosisListViewController alloc] initWithContentMode:kDataContentIllness];
        [self.navigationController pushViewController:servicesViewController animated:YES];
    }
    else if(indexPath.row == 3)
    {
        PersonalizationViewController *servicesViewController = [[PersonalizationViewController alloc] init];
        [self.navigationController pushViewController:servicesViewController animated:YES];
    }
    else if(indexPath.row == 4)
    {
        ProfileViewController *servicesViewController = [[ProfileViewController alloc] init];
        [self.navigationController pushViewController:servicesViewController animated:YES];
    }
    else if(indexPath.row == 5)
    {
        AboutViewController *servicesViewController = [[AboutViewController alloc] init];
        [self.navigationController pushViewController:servicesViewController animated:YES];
    }
    else  {
        [UIAlertView showWithTitle:@"" message:@"Вы уверены что хотите выйти?"
                 cancelButtonTitle:@"Отмена"
                 otherButtonTitles:@[@"Да"] tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex)   {
                     if(buttonIndex == 1)   {
                         [[UserManager shared] logout:^{
                             
                         } failure:^{
                             //
                         }];
                         [[[UIApplication sharedApplication] delegate] performSelector:@selector(switchToLogin)];
                     }
                 }];
    }
}

@end
