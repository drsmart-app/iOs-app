//
//  SServicesViewController.h
//  DentaBook
//
//  Created by Denis Dubov on 08.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SServicesViewController : UIViewController
@property (nonatomic, retain) NSArray* services;
@end
