//
//  ProfileViewController.h
//  DentaBook
//
//  Created by Mac Mini on 09.01.17.
//  Copyright © 2017 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IBActionSheet.h"

@interface ProfileViewController : UIViewController<UIImagePickerControllerDelegate>
@property (weak, nonatomic) IBOutlet UITextField *fioTextField;
@property (weak, nonatomic) IBOutlet UITextField *positionTextField;
@property (weak, nonatomic) IBOutlet UITextField *clinicTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *addressTextField;
@property (weak, nonatomic) IBOutlet UITextField *licenseTextField;
@property (weak, nonatomic) IBOutlet UIButton *clinicLogoBtn;
@property (strong, nonatomic) NSMutableDictionary* galleryEntity;

@property (nonatomic, strong) IBActionSheet *photoActionSheet;
- (IBAction)clickClinicBtn:(id)sender;
- (IBAction)hideKeyboard:(id)sender;

@end
