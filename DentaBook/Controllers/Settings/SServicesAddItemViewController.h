//
//  AffairAddGroupViewController.h
//  DentaBook
//
//  Created by Denis Dubov on 04.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CategoryTherapy;

@interface SServicesAddItemViewController : UIViewController

-(id) initWithCategory:(NSDictionary*)category;
-(id) initWithItem:(NSDictionary*)category;
@end
