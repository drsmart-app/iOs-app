//
//  UserAgreementViewController.h
//  DentaBook
//
//  Created by Mac Mini on 17.02.17.
//  Copyright © 2017 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserAgreementViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextView *webView;

@end
