//
//  ProfileViewController.m
//  DentaBook
//
//  Created by Mac Mini on 09.01.17.
//  Copyright © 2017 denisdbv. All rights reserved.
//

#import "ProfileViewController.h"
#import "DataBase.h"
#import "ELCImagePickerController.h"
#import <UIAlertView+Blocks/UIAlertView+Blocks.h>
#import "PhotoEditViewController.h"
#import "GalleryManager.h"
#import "HttpUtil.h"

@interface ProfileViewController ()

@end

@implementation ProfileViewController
@synthesize fioTextField, positionTextField, clinicTextField, phoneTextField, emailTextField, addressTextField, licenseTextField, clinicLogoBtn, galleryEntity;
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Настройки профиля";
    
    UIImage *image2 = [UIImage imageNamed:@"back-button"];
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 addTarget:self action:@selector(onBack:)forControlEvents:UIControlEventTouchUpInside];
    [button2 setBackgroundImage:image2 forState:UIControlStateNormal];
    button2.frame = CGRectMake(0, 0, 11, 18);
    UIBarButtonItem *barButton2 = [[UIBarButtonItem alloc] initWithCustomView:button2];
    self.navigationItem.leftBarButtonItem = barButton2;
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(onSave)forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"Сохранить" forState:UIControlStateNormal];
    [button setTitle:@"Сохранить" forState:UIControlStateSelected];
    button.frame = CGRectMake(0, 0, 65, 18);
    button.titleLabel.font = [UIFont systemFontOfSize:12];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    button.contentVerticalAlignment = UIControlContentVerticalAlignmentBottom;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;

    // Do any additional setup after loading the view.
    fioTextField.text = getProfileFio();
    positionTextField.text = getProfilePosition();
    clinicTextField.text = getProfileClinic();
    phoneTextField.text = getProfilePhone();
    emailTextField.text = getProfileEmail();
    addressTextField.text = getProfileAddress();
    licenseTextField.text = getProfileLicense();
    //fioTextField.text = getProfilePhoto();
}

-(void)onSave
{
    setProfile(fioTextField.text, positionTextField.text, clinicTextField.text, phoneTextField.text, emailTextField.text, addressTextField.text,  licenseTextField.text,  @"");
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)clickClinicBtn:(id)sender
{
    [self.view endEditing:YES];
    self.photoActionSheet = [[IBActionSheet alloc] initWithTitle:NSLocalizedString(@"Сделать фотографию", nil)
                                                        delegate:self
                                               cancelButtonTitle:NSLocalizedString(@"Отмена", nil)
                                          destructiveButtonTitle:nil
                                          otherButtonTitlesArray:@[@"Камера", NSLocalizedString(@"Выбрать из галереи", nil)]];
    self.photoActionSheet.tag = 1;
    [self.photoActionSheet showInViewX:self.view];
}

- (IBAction)hideKeyboard:(id)sender {
    
    [fioTextField resignFirstResponder];
    [positionTextField resignFirstResponder];
    [clinicTextField resignFirstResponder];
    [phoneTextField resignFirstResponder];
    [emailTextField resignFirstResponder];
    [addressTextField resignFirstResponder];
    [licenseTextField resignFirstResponder];
}


- (void)actionSheet:(IBActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(actionSheet.tag == 1)    {
        if(buttonIndex == 0){
            
            if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                
                [UIAlertView showWithTitle:@"Ошибка" message:@"Камера не обнаружена" cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
                
                return;
            }
            
            if (/* DISABLES CODE */ (YES)) {
                [self showImagePickerUsingSourceType:UIImagePickerControllerSourceTypeCamera];
            }else{
                [UIAlertView showWithTitle:@"Ошибка" message:@"This app does not have access to your photos or videos" cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
            }
            
        } else if(buttonIndex == 1) {
            
            if (/* DISABLES CODE */ (YES)) {
                [self showImagePickerUsingSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
            }else{
                [UIAlertView showWithTitle:@"Ошибка" message:@"This app does not have access to your photos or videos" cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
            }
        }
    }
}

- (void)showImagePickerUsingSourceType:(UIImagePickerControllerSourceType)type{
    
    // Create the image picker
        UIImagePickerController *imagePickerController = [UIImagePickerController new];
        imagePickerController.allowsEditing = NO;
        imagePickerController.delegate = self;
        imagePickerController.sourceType = type;
        
        [self presentViewController:imagePickerController animated:YES completion:^{
            
        }];
    
}

/*- (void)elcImagePickerController:(ELCImagePickerController *)picker didFinishPickingMediaWithInfo:(NSArray *)info
{
    NSLog(@"Info about images: %@",info);
    if(info.count==1)
    {
        NSString *mediaType = info[0][UIImagePickerControllerMediaType];
        UIImage *photo = nil;
        if ([mediaType isEqualToString:@"ALAssetTypePhoto"])
        {
            photo = info[0][UIImagePickerControllerOriginalImage];//info[UIImagePickerControllerEditedImage];
        }
        
        [self dismissViewControllerAnimated:YES completion:^{
            
            PhotoEditViewController *editViewController = [[PhotoEditViewController alloc] initWithPhoto:photo forProfile:YES];
            [self.navigationController pushViewController:editViewController animated:YES];
        }];
    }
    else
    {
        for(NSDictionary* dd in info)
        {
            galleryEntity = [[NSMutableDictionary alloc] init];//Gallery *galleryEntity = [Gallery MR_createEntity];
            NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
            [dateformate setDateFormat:@"dd.MM.yyyy"];
            NSString *dateString = [dateformate stringFromDate:[NSDate date]];
            galleryEntity[@"createat"] = dateString;
            galleryEntity[@"createdat"] = dateString;
            galleryEntity[@"comment"] = @"";
            galleryEntity[@"id"] = GUIDString();
            galleryEntity[@"profile"] = @"yes";
            UIImage *photo = nil;
            photo = dd[UIImagePickerControllerOriginalImage];
            [[GalleryManager shared] createGallery:galleryEntity success:^{
                [[GalleryManager shared] uploadPhoto:photo forProfile:YES completion:^(NSError *error) {
                    
                }];
            } failure:^{
                //
            }];
        }
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
}*/

- (void)elcImagePickerControllerDidCancel:(ELCImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = info[UIImagePickerControllerMediaType];
    
    UIImage *photo = nil;
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]){
        photo = info[UIImagePickerControllerOriginalImage];//info[UIImagePickerControllerEditedImage];
    }
    
    [self dismissViewControllerAnimated:YES completion:^{
        
        NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
        //NSString *imageSubdirectory = [documentsDirectory stringByAppendingPathComponent:@"logo"];
        
        NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"logo.png"];
        NSError *error = nil;
        // Convert UIImage object into NSData (a wrapper for a stream of bytes) formatted according to PNG spec
        NSData *imageData = UIImagePNGRepresentation(photo);
        [imageData writeToFile:filePath options:NSDataWritingAtomic error:&error];
        NSLog(@"Write returned error: %@", [error localizedDescription]);
        /*PhotoEditViewController *editViewController = [[PhotoEditViewController alloc] initWithPhoto:photo forProfile:YES];
        [self.navigationController pushViewController:editViewController animated:YES];*/
    }];
}
@end
