//
//  SServicesViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 08.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "SServicesViewController.h"
#import "SServicesAddGroupViewController.h"
#import "SServicesListViewController.h"

#import <MagicalRecord/MagicalRecord.h>
#import <SWTableViewCell/SWTableViewCell.h>

#import "CategoryTherapy.h"
#import "TherapyManager.h"

#import "DataBase.h"
#import "CZPicker.h"
#import "PercUtils.h"
#import "HttpUtil.h"

@interface SServicesViewController ()<NSFetchedResultsControllerDelegate, SWTableViewCellDelegate, CZPickerViewDelegate, CZPickerViewDataSource>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@end

@implementation SServicesViewController
@synthesize services;
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Прайс-лист";
    /*self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Добавить"
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(onAdd:)];*/
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(onAdd:)forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"Добавить" forState:UIControlStateNormal];
    [button setTitle:@"Добавить" forState:UIControlStateSelected];
    button.frame = CGRectMake(0, 0, 65, 18);
    button.titleLabel.font = [UIFont systemFontOfSize:12];
    
    UIBarButtonItem *barButton2 = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = barButton2;
    
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    button.contentVerticalAlignment = UIControlContentVerticalAlignmentBottom;
    
    [self fetchTableData];
    
    UIImage *image2 = [UIImage imageNamed:@"back-button"];
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 addTarget:self action:@selector(onBack:)forControlEvents:UIControlEventTouchUpInside];
    [button2 setBackgroundImage:image2 forState:UIControlStateNormal];
    button2.frame = CGRectMake(0, 0, 11, 18);
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button2];
    self.navigationItem.leftBarButtonItem = barButton;
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)onAdd:(id)sender
{
    SServicesAddGroupViewController *servicesAddGroupVC = [SServicesAddGroupViewController new];
    [self.navigationController pushViewController:servicesAddGroupVC animated:YES];
}


- (void) viewDidAppear:(BOOL)animated
{
    [self fetchTableData];
    [self.tableView reloadData];
}

-(void)fetchTableData {
    
    services=[DataBase getDBArray:@"services_group" withKey:@"" andValue:@""];
    /*_fetchedResultsController = nil;
    NSError *error = nil;
    [self.fetchedResultsController performFetch:&error];*/
}

#pragma mark -
#pragma mark Option Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return services.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section    {
    return [UIView new];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section    {
    
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section   {
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section   {
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath  {
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    NSDictionary *categoryTherapy = [services objectAtIndex:indexPath.row];//[self.fetchedResultsController objectAtIndexPath:indexPath];
    
    SWTableViewCell *titleCell = (SWTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"titleCell2"];
    if (titleCell == nil) {
        titleCell = [[SWTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"titleCell2"];
        titleCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        titleCell.delegate = self;
        titleCell.rightUtilityButtons = [self rightButtons];
        titleCell.textLabel.textColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
        titleCell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:14];
        titleCell.detailTextLabel.textColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
        titleCell.detailTextLabel.font = [UIFont fontWithName:@"Helvetica" size:14];
    }
    titleCell.textLabel.text = categoryTherapy[@"title"];
    
    cell = titleCell;
    
    return cell;
}

- (NSArray *)rightButtons
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:231.0f/255.0f green:76.0f/255.0f blue:60.0f/255.0f alpha:1.0f] icon:[PercUtils imageWithImage:[UIImage imageNamed:@"delete_white.png"] scaledToSize:CGSizeMake(20, 25)]];
    
    return rightUtilityButtons;
}

- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell   {
    return YES;
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    switch (index) {
        case 0: {
            CZPickerView *picker = [[CZPickerView alloc] initWithHeaderTitle:@"Удалить?" cancelButtonTitle:@"Отмена" confirmButtonTitle:@"Да"];
            picker.headerBackgroundColor = getColorScheme();
            picker.tag = [self.tableView indexPathForCell:cell].row;
            picker.delegate = self;
            picker.dataSource = self;
            picker.allowMultipleSelection = YES;
            [picker show];
        }
            break;
        default:
            break;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *categoryTherapy = services[indexPath.row];//[self.fetchedResultsController objectAtIndexPath:indexPath];
    SServicesListViewController *servicesGroupVC = [[SServicesListViewController alloc] initWithCategory:categoryTherapy];
    [self.navigationController pushViewController:servicesGroupVC animated:YES];
    
}

-(void)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSString *)czpickerView:(CZPickerView *)pickerView titleForRow:(NSInteger)row
{
    return @"Да";
    /*DentalDiagnosis *denta = [DentalDiagnosis MR_findAllSortedBy:@"remoteId" ascending:YES][row];
     return denta.name;*/
}

- (NSInteger)numberOfRowsInPickerView:(CZPickerView *)pickerView
{
    return 0;
}

- (void)czpickerView:(CZPickerView *)pickerView didConfirmWithItemAtRow:(NSInteger)row{
    
    NSDictionary *categoryTherapy = services[pickerView.tag];//[self.fetchedResultsController objectAtIndexPath:cellIndexPath];
    [[TherapyManager shared] deleteTherapyCategoryById:categoryTherapy[@"id"] success:^{
        [self fetchTableData];
        [self.tableView reloadData];
        //
    } failure:^{
        //
    }];
}

- (void)czpickerView:(CZPickerView *)pickerView didConfirmWithItemsAtRows:(NSArray *)rows   {
    NSDictionary *categoryTherapy = services[pickerView.tag];//[self.fetchedResultsController objectAtIndexPath:cellIndexPath];
    [[TherapyManager shared] deleteTherapyCategoryById:categoryTherapy[@"id"] success:^{
        [self fetchTableData];
        [self.tableView reloadData];
        //
    } failure:^{
        //
    }];
}

- (void)czpickerViewDidClickCancelButton:(CZPickerView *)pickerView{
    NSLog(@"Canceled.");
}


@end
