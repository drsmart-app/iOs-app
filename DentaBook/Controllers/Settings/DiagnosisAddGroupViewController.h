//
//  AffairAddGroupViewController.h
//  DentaBook
//
//  Created by Denis Dubov on 04.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DiagnosisListViewController.h"

@interface DiagnosisAddGroupViewController : UIViewController

@property (copy, nonatomic) void (^didClickCreateName)(NSString *exNameString, NSString *nameString);

@end
