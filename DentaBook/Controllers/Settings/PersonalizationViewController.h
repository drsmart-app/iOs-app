//
//  PersonalizationViewController.h
//  DentaBook
//
//  Created by Mac Mini on 17.12.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PersonalizationViewController : UIViewController
@property (weak, nonatomic) IBOutlet UISwitch *showFioSwitcher;
@property (weak, nonatomic) IBOutlet UISwitch *divideSumSwitcher;
@property (weak, nonatomic) IBOutlet UISegmentedControl *countStrsControl;
@property (weak, nonatomic) IBOutlet UISegmentedControl *schemeControl;
- (IBAction)onSchemeChanged:(id)sender;
- (IBAction)onRowsChanged:(id)sender;
- (IBAction)onNameChanged:(id)sender;
- (IBAction)onSumChanged:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *blueGrayLabel;
@property (weak, nonatomic) IBOutlet UILabel *blueWhiteLabel;
@property (weak, nonatomic) IBOutlet UILabel *greenGrayLabel;
@property (weak, nonatomic) IBOutlet UILabel *greenWhiteLabel;


@end
