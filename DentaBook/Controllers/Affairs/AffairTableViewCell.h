//
//  AffairTableViewCell.h
//  DentaBook
//
//  Created by Denis Dubov on 21.12.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SWTableViewCell/SWTableViewCell.h>

@interface AffairTableViewCell : SWTableViewCell

@property (weak, nonatomic) IBOutlet UIButton *checkButton;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@property (nonatomic, strong) NSIndexPath *indexPath;
@property (copy, nonatomic) void (^didCheckAffair)(NSIndexPath *indexPath, BOOL selected);
@property (weak, nonatomic) IBOutlet UIButton *imgBtn;

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UIView *frontView;
@property (weak, nonatomic) IBOutlet UIButton *tapImg;
@property (weak, nonatomic) IBOutlet UIImageView *imgToZoom;
@property (weak, nonatomic) UIViewController *owner;
@property (weak, nonatomic) IBOutlet UIImageView *hasImageImg;
@end
