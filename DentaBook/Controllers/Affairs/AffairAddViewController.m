//
//  AffairAddViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 04.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "AffairAddViewController.h"
#import "WLPActionSheet.h"
#import "DBFulldateTimeController.h"

#import "CZPicker.h"
#import <MagicalRecord/MagicalRecord.h>
#import <NSDate+Calendar/NSDate+Calendar.h>
#import <JGProgressHUD/JGProgressHUD.h>

#import "AffairGroup.h"
#import "Affair.h"
#import "AffairManager.h"
#import "DataBase.h"
#import "ELCImagePickerController.h"
#import <UIAlertView+Blocks/UIAlertView+Blocks.h>
#import "PhotoEditViewController.h"
#import "GalleryManager.h"
#import "HttpUtil.h"

@interface AffairAddViewController ()<CZPickerViewDataSource, CZPickerViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField *affairNameTextField;
@property (weak, nonatomic) IBOutlet UILabel *affairDeadlineLabel;
@property (weak, nonatomic) IBOutlet UILabel *affairReminderLabel;

@property (nonatomic, strong) NSDictionary *currentAffairGroup;

@property (nonatomic, strong) NSArray *reminderKeyArray;
@property (nonatomic, strong) NSArray *reminderNameArray;

@property (nonatomic, strong) WLPActionSheet *fullDateTimeActionSheet;
@property (nonatomic, strong) DBFulldateTimeController *fullDateTimeController;

@end

@implementation AffairAddViewController {
    NSInteger selectedReminderRow;
    NSDate *selectedDate;
}
@synthesize commentTextView, commentPlaceholder, photoActionSheet, currentAffair;
-(id) initWithAffairGroup:(NSDictionary*)affairGroup {
    self = [super initWithNibName:@"AffairAddViewController" bundle:[NSBundle mainBundle]];
    if(self)    {
        self.currentAffairGroup = affairGroup;
        self.currentAffair = [[NSMutableDictionary alloc] init];
        self.currentAffair[@"id"] = GUIDString();
        self.currentAffair[@"isdone"] = @"false";
        self.currentAffair[@"group_id"] = self.currentAffairGroup[@"id"];
    }
    return self;
}

-(id) initWithAffair:(NSDictionary*)affair {
    self = [super initWithNibName:@"AffairAddViewController" bundle:[NSBundle mainBundle]];
    if(self)    {
        self.currentAffair = [NSMutableDictionary dictionaryWithDictionary:affair];
        /*self.currentAffair[@"id"] = GUIDString();
        self.currentAffair[@"isdone"] = @"false";*/
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Добавить задачу";
    
    selectedReminderRow = 0;
    selectedDate = [NSDate date];
    
    /*self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Готово"
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(onSave:)];*/
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(onSave:)forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"Добавить" forState:UIControlStateNormal];
    [button setTitle:@"Добавить" forState:UIControlStateSelected];
    button.frame = CGRectMake(0, 0, 65, 18);
    button.titleLabel.font = [UIFont systemFontOfSize:12];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    button.contentVerticalAlignment = UIControlContentVerticalAlignmentBottom;
    
    self.fullDateTimeController = [DBFulldateTimeController new];
    
    self.reminderNameArray = @[@"Нет", @"В момент события", @"За 5 мин", @"За 15 мин", @"За 30 мин", @"За 1 ч", @"За 2 ч", @"За 1 дн", @"За 2 дн", @"За 1 нед"];
    self.affairReminderLabel.text = self.reminderNameArray[selectedReminderRow];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm"];
    NSString *strDate = [dateFormatter stringFromDate:selectedDate];
    self.affairDeadlineLabel.text = strDate;
    commentTextView.delegate = self;
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
    
    self.extendedLayoutIncludesOpaqueBars = YES;
    
    //self.navigationController.navigationBar.translucent = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    //
    [self.affairNameTextField becomeFirstResponder];
    commentTextView.text=currentAffair[@"comment"];
    if([((NSString*)currentAffair[@"comment"]) length] >0)
        commentPlaceholder.hidden=YES;
    
    self.affairNameTextField.text=self.currentAffair[@"title"];
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm"];
    selectedDate = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@ %@:%@",self.currentAffair[@"date"],self.currentAffair[@"hours"],self.currentAffair[@"minutes"]]];
    [dateFormatter stringFromDate:selectedDate];;
    
    strDate = [dateFormatter stringFromDate:selectedDate];
    self.affairDeadlineLabel.text = strDate;
    self.affairReminderLabel.text=self.reminderNameArray[[self.currentAffair[@"notify_distance"] integerValue]];
    
}

-(void) viewDidAppear:(BOOL)animated
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)onSave:(id)sender
{
    if(self.affairNameTextField.text.length == 0)
        return;
    
    
    self.currentAffair[@"title"] = self.affairNameTextField.text;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd.MM.yyyy"];
    self.currentAffair[@"date"] = [dateFormatter stringFromDate:selectedDate];
    self.currentAffair[@"notify_distance"] = @(selectedReminderRow);
    self.currentAffair[@"createat"] = [dateFormatter stringFromDate:[NSDate date]];
    [dateFormatter setDateFormat:@"HH"];
    self.currentAffair[@"hours"] = [dateFormatter stringFromDate:selectedDate];
    [dateFormatter setDateFormat:@"mm"];
    self.currentAffair[@"minutes"] = [dateFormatter stringFromDate:selectedDate];
    self.currentAffair[@"comment"] = commentTextView.text;

    //[[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    
    JGProgressHUD *HUD = [JGProgressHUD progressHUDWithStyle:JGProgressHUDStyleDark];
    HUD.textLabel.text = @"Сохранение";
    [HUD showInView:self.view];
    [[AffairManager shared] createAffairItem:self.currentAffair success:^{
        if(selectedReminderRow != 0)    {
            NSDate *date = selectedDate;
            switch (selectedReminderRow) {
                case 2:
                    date = [date dateByAddingMinute:-5];
                    break;
                case 3:
                    date = [date dateByAddingMinute:-15];
                    break;
                case 4:
                    date = [date dateByAddingMinute:-30];
                    break;
                case 5:
                    date = [date dateByAddingHour:-1];
                    break;
                case 6:
                    date = [date dateByAddingHour:-2];
                    break;
                case 7:
                    date = [date dateByAddingDays:-1];
                    break;
                case 8:
                    date = [date dateByAddingDays:-2];
                    break;
                case 9:
                    date = [date dateByAddingWeek:-1];
                    break;
                    
                default:
                    break;
            }
            
            UIApplication *app                = [UIApplication sharedApplication];
            UILocalNotification *notification = [UILocalNotification new];
            notification.timeZone  = [NSTimeZone defaultTimeZone];
            notification.fireDate  = date;
            notification.alertBody = self.affairNameTextField.text;
            notification.soundName = UILocalNotificationDefaultSoundName;
            [app scheduleLocalNotification:notification];
        }
        
        [HUD dismissAnimated:YES];
        [self.navigationController popViewControllerAnimated:YES];
    } failure:^{
        [HUD dismissAnimated:YES];
    }];
}

- (void)datePickerChanged:(UIDatePicker *)datePicker
{
    
}

- (IBAction)onDeadline:(id)sender {
    [self.view endEditing:YES];
    
    self.fullDateTimeActionSheet = [[WLPActionSheet alloc] initWithTitle:@"Срок выполнения" cancelButtonTitle:@"ОК" destructiveButtonTitle:nil withViewController:self.fullDateTimeController];
    self.fullDateTimeActionSheet.tag = 1;
    self.fullDateTimeActionSheet.delegate = self;
    [self.fullDateTimeActionSheet showInView:self.view];
}

-(void)actionSheetDidDone:(WLPActionSheet*)actionSheet  {
    selectedDate = self.fullDateTimeController.datePicker.date;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm"];
    NSString *strDate = [dateFormatter stringFromDate:selectedDate];
    self.affairDeadlineLabel.text = strDate;
}

-(void)actionSheetDidDissapear:(WLPActionSheet*)actionSheet   {
    NSLog(@"DESTRUCTIVE");
}

- (IBAction)onReminder:(id)sender {
    [self.view endEditing:YES];
    
    CZPickerView *picker = [[CZPickerView alloc] initWithHeaderTitle:@"Напоминание" cancelButtonTitle:@"Отмена" confirmButtonTitle:@"Готово"];
    picker.headerBackgroundColor = getColorScheme();
    picker.delegate = self;
    picker.dataSource = self;
    picker.allowMultipleSelection = NO;
    [picker show];
}

-(void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

- (NSString *)czpickerView:(CZPickerView *)pickerView titleForRow:(NSInteger)row{
    return self.reminderNameArray[row];
}

- (NSInteger)numberOfRowsInPickerView:(CZPickerView *)pickerView{
    return self.reminderNameArray.count;
}

- (void)czpickerView:(CZPickerView *)pickerView didConfirmWithItemAtRow:(NSInteger)row{
    NSLog(@"%@ is chosen!", self.reminderNameArray[row]);
    self.affairReminderLabel.text = self.reminderNameArray[row];
    selectedReminderRow = row;
}

- (void)czpickerViewDidClickCancelButton:(CZPickerView *)pickerView{
    NSLog(@"Canceled.");
}
    
- (IBAction)onAddPhoto:(id)sender {
    
    [self.view endEditing:YES];
    self.photoActionSheet = [[IBActionSheet alloc] initWithTitle:NSLocalizedString(@"Сделать фотографию", nil)
                                                        delegate:self
                                               cancelButtonTitle:NSLocalizedString(@"Отмена", nil)
                                          destructiveButtonTitle:nil
                                          otherButtonTitlesArray:@[@"Камера", NSLocalizedString(@"Выбрать из галереи", nil)]];
    self.photoActionSheet.tag = 1;
    [self.photoActionSheet showInView:self.view];
}

- (void)actionSheet:(IBActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(actionSheet.tag == 1)    {
        if(buttonIndex == 0){
            
            if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                
                [UIAlertView showWithTitle:@"Ошибка" message:@"Камера не обнаружена" cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
                
                return;
            }
            
            if (/* DISABLES CODE */ (YES)) {
                [self showImagePickerUsingSourceType:UIImagePickerControllerSourceTypeCamera];
            }else{
                [UIAlertView showWithTitle:@"Ошибка" message:@"This app does not have access to your photos or videos" cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
            }
            
        } else if(buttonIndex == 1) {
            
            if (/* DISABLES CODE */ (YES)) {
                [self showImagePickerUsingSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
            }else{
                [UIAlertView showWithTitle:@"Ошибка" message:@"This app does not have access to your photos or videos" cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
            }
        }
    }
}

- (void)showImagePickerUsingSourceType:(UIImagePickerControllerSourceType)type{
    
    // Create the image picker
    if(type == UIImagePickerControllerSourceTypeCamera)
    {
        UIImagePickerController *imagePickerController = [UIImagePickerController new];
        imagePickerController.allowsEditing = NO;
        imagePickerController.delegate = self;
        imagePickerController.sourceType = type;
        
        [self presentViewController:imagePickerController animated:YES completion:^{
            
        }];
    }
    else
    {
        ELCImagePickerController *elcPicker = [[ELCImagePickerController alloc] initImagePicker];
        elcPicker.maximumImagesCount = 4; //Set the maximum number of images to select, defaults to 4
        elcPicker.returnsOriginalImage = YES; //Only return the fullScreenImage, not the fullResolutionImage
        elcPicker.returnsImage = YES; //Return UIimage if YES. If NO, only return asset location information
        elcPicker.onOrder = YES; //For multiple image selection, display and return selected order of images
        elcPicker.imagePickerDelegate = self;
        
        [self presentViewController:elcPicker animated:YES completion:nil];
    }
    
}

- (void)elcImagePickerController:(ELCImagePickerController *)picker didFinishPickingMediaWithInfo:(NSArray *)info
{
    NSLog(@"Info about images: %@",info);
    if(info.count==1)
    {
        NSString *mediaType = info[0][UIImagePickerControllerMediaType];
        UIImage *photo = nil;
        if ([mediaType isEqualToString:@"ALAssetTypePhoto"])
        {
            photo = info[0][UIImagePickerControllerOriginalImage];//info[UIImagePickerControllerEditedImage];
        }
        
        [self dismissViewControllerAnimated:YES completion:^{
            
            PhotoEditViewController *editViewController = [[PhotoEditViewController alloc] initWithPhoto:photo forAffair:self.currentAffair];
            [self.navigationController pushViewController:editViewController animated:YES];
        }];
    }
    else
    {
        for(NSDictionary* dd in info)
        {
            NSMutableDictionary* galleryEntity = [[NSMutableDictionary alloc] init];//Gallery *galleryEntity = [Gallery MR_createEntity];
            NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
            [dateformate setDateFormat:@"dd.MM.yyyy"];
            NSString *dateString = [dateformate stringFromDate:[NSDate date]];
            galleryEntity[@"createat"] = dateString;
            galleryEntity[@"createdat"] = dateString;
            galleryEntity[@"comment"] = @"";
            galleryEntity[@"affairid"] = self.currentAffair[@"id"];
            galleryEntity[@"id"] = GUIDString();
            UIImage *photo = nil;
            photo = dd[UIImagePickerControllerOriginalImage];
            [[GalleryManager shared] createGallery:galleryEntity success:^{
                [[GalleryManager shared] uploadPhoto:photo toAffair:galleryEntity completion:^(NSError *error) {
                    
                }];
            } failure:^{
                //
            }];
        }
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
}

- (void)elcImagePickerControllerDidCancel:(ELCImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = info[UIImagePickerControllerMediaType];
    
    UIImage *photo = nil;
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]){
        photo = info[UIImagePickerControllerOriginalImage];//info[UIImagePickerControllerEditedImage];
    }
    
    [self dismissViewControllerAnimated:YES completion:^{
        
        PhotoEditViewController *editViewController = [[PhotoEditViewController alloc] initWithPhoto:photo forAffair:self.currentAffair];
        [self.navigationController pushViewController:editViewController animated:YES];
    }];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)textViewDidBeginEditing:(UITextView *)textView
{
    commentPlaceholder.hidden=YES;
}


- (void)textViewDidEndEditing:(UITextView *)textView
{
    if([textView.text length]>0)
    {
    }
    else
    {
        commentPlaceholder.hidden=NO;
    }
}
@end

@implementation UITextView (MYTextView)
- (void)_firstBaselineOffsetFromTop {
    
}

- (void)_baselineOffsetFromBottom {
    
}

@end
