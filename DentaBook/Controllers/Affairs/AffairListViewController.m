//
//  AffairListViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 04.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "AffairListViewController.h"
#import "AffairAddViewController.h"
#import "AffairTableViewCell.h"

#import <MagicalRecord/MagicalRecord.h>
#import <SWTableViewCell/SWTableViewCell.h>

#import "AffairGroup.h"
#import "Affair.h"
#import "AffairManager.h"
#import "DataBase.h"
#import "PercUtils.h"
#import "CZPickerView.h"
#import "HttpUtil.h"
#import "DataManager.h"

@interface AffairListViewController () <CZPickerViewDelegate, CZPickerViewDataSource>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) BOOL isToday;
@property (nonatomic) BOOL isWeek;

@property (nonatomic, strong) NSDictionary *currentAffairGroup;
@end

@implementation AffairListViewController
@synthesize affairs_done, affairs_not_done, onlyActual;
-(id) initWithAffairGroup:(NSDictionary*)affairGroup {
    self = [super initWithNibName:@"AffairListViewController" bundle:[NSBundle mainBundle]];
    if(self)
    {
        self.currentAffairGroup = affairGroup;
        self.isToday = NO;
        self.isWeek = NO;
    }
    return self;
}

-(id) initWithTodayGroup
{
    self = [super initWithNibName:@"AffairListViewController" bundle:[NSBundle mainBundle]];
    if(self)
    {
        self.currentAffairGroup = nil;
        self.isToday = YES;
        self.isWeek = NO;
    }
    return self;
}

-(id) initWithWeekGroup
{
    self = [super initWithNibName:@"AffairListViewController" bundle:[NSBundle mainBundle]];
    if(self)
    {
        self.currentAffairGroup = nil;
        self.isToday = NO;
        self.isWeek = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    onlyActual = false;
    
    self.title = self.currentAffairGroup[@"title"];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([AffairTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([AffairTableViewCell class])];
    
    [self fetchTableData];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    UIImage *image = [UIImage imageNamed:@"patient-add-icon"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(onAddTask:)forControlEvents:UIControlEventTouchUpInside];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, 18, 18);
    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = barButton;
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void) viewDidAppear:(BOOL)animated
{
    [self fetchTableData];
    [self.tableView reloadData];
}

-(void)fetchTableData
{
    unsigned int flags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:flags fromDate:[NSDate date]];
    NSDate* dateOnly = [calendar dateFromComponents:components];
    
    NSDate *startOfTheWeek = dateOnly;

    NSDateComponents *comp = [NSDateComponents new];
     int numberOfDaysInAWeek = 7;
     int weeks = 3; // <-- this example adds 3 weeks
     comp.day = weeks * numberOfDaysInAWeek;
     
    NSDate *endOfWeek = [[NSCalendar currentCalendar] dateByAddingComponents:comp toDate:dateOnly options:0];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd.MM.yyyy"];
    
    affairs_done = [[NSMutableArray alloc] init];
    affairs_not_done = [[NSMutableArray alloc] init];
    
    
    if(!self.isWeek&&!self.isToday)
    {
        NSArray* affairs = [DataBase getDBArray:@"affair" withKey:@"group_id" andValue:self.currentAffairGroup[@"id"]];
        
        for(NSDictionary* d in affairs)
        {
            NSLog(@"isdone %@", d[@"isdone"]);
            if([d[@"isdone"] isEqualToString:@"true"])
            {
                [affairs_done addObject:d];
            }
            else
            {
                [affairs_not_done addObject:d];
            }
        }
    }
    else if(!self.isWeek&&self.isToday)
    {
        NSArray* affairs = [DataBase getDBArray:@"affair" withKey:@"" andValue:@""];
        
        for(NSDictionary* d in affairs)
        {
            NSLog(@"d: %@",d);
            NSDateComponents* components = [calendar components:flags fromDate:[dateFormatter dateFromString:d[@"createat"]]];
            NSDate* affairDateOnly = [calendar dateFromComponents:components];
            
            if(affairDateOnly>=dateOnly&&affairDateOnly<=dateOnly)
            {
                NSLog(@"isdone %@", d[@"isdone"]);
                if([d[@"isdone"] isEqualToString:@"true"])
                {
                    [affairs_done addObject:d];
                }
                else
                {
                    [affairs_not_done addObject:d];
                }
            }
        }
    }
    else if(self.isWeek&&!self.isToday)
    {
        NSArray* affairs = [DataBase getDBArray:@"affair" withKey:@"" andValue:@""];
        
        for(NSDictionary* d in affairs)
        {
            NSLog(@"d: %@",d);
            NSDateComponents* components = [calendar components:flags fromDate:[dateFormatter dateFromString:d[@"createat"]]];
            NSDate* affairDateOnly = [calendar dateFromComponents:components];
            
            if(affairDateOnly>=startOfTheWeek&&affairDateOnly<=endOfWeek)
            {
                NSLog(@"isdone %@", d[@"isdone"]);
                if([d[@"isdone"] isEqualToString:@"true"])
                {
                    [affairs_done addObject:d];
                }
                else
                {
                    [affairs_not_done addObject:d];
                }
            }
        }
    }
}

#pragma mark -
#pragma mark Option Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger count = 0;
    if(section == 0)    {
        count = affairs_not_done.count;
    } else
    {
        if(onlyActual)
            count=0;
        else
            count = affairs_done.count;
    }
    
    return count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section    {
    
    if(section == 1)
    {
        if(affairs_done.count == 0)  {
            return [UIView new];
        }
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 55.0)];
        view.backgroundColor = [UIColor clearColor];
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 280.0, 25)];
        [btn setBackgroundColor:[UIColor colorWithRed:216.0/255.0 green:216.0/255.0 blue:216.0/255.0 alpha:1.0]];
        [btn setTitleColor:[UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0] forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:10];
        if(!onlyActual)
        {
            [btn setTitle:@"Скрыть завершенные задачи" forState:UIControlStateNormal];
            [btn setTitle:@"Скрыть завершенные задачи" forState:UIControlStateSelected];
        }
        else
        {
            [btn setTitle:@"Показать завершенные задачи" forState:UIControlStateNormal];
            [btn setTitle:@"Показать завершенные задачи" forState:UIControlStateSelected];
        }
        [btn addTarget:self action:@selector(hideFinished) forControlEvents:UIControlEventTouchUpInside];

        [view addSubview:btn];
        btn.center = view.center;
        return view;
    }
    
    return [UIView new];
}

- (void) hideFinished
{
    onlyActual = !onlyActual;
    [self.tableView reloadData];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section    {
    
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section   {
    
    if(section == 1)
        return 55.0;
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section   {
    
    return 1.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath  {
    return 62;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    NSDictionary *affair;
    if(indexPath.section == 0)
    {
        affair = affairs_not_done[indexPath.row];//[self.fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:0]];
    }
    else
    {
        affair = affairs_done[/*affairs_not_done.count+*/indexPath.row];
    }
    
    AffairTableViewCell *affairCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([AffairTableViewCell class])];
    affairCell.descriptionLabel.text = affair[@"title"];
    affairCell.delegate = self;
    affairCell.rightUtilityButtons = [self rightButtons];
    
    if([affair[@"isdone"] isEqualToString:@"true"])
    {
        affairCell.descriptionLabel.textColor = [UIColor colorWithRed:155.0/255.0 green:155.0/255.0 blue:155.0/255.0 alpha:1.0];
        affairCell.checkButton.selected = YES;
        
        NSMutableAttributedString *attString=[[NSMutableAttributedString alloc]initWithString:affairCell.descriptionLabel.text];
        [attString addAttribute:NSStrikethroughStyleAttributeName value:[NSNumber numberWithInt:1] range:NSMakeRange(0,[attString length])];
        affairCell.descriptionLabel.attributedText = attString;
        
    }
    else
    {
        affairCell.descriptionLabel.textColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
        affairCell.checkButton.selected = NO;
        
        NSMutableAttributedString *attString=[[NSMutableAttributedString alloc]initWithString:affairCell.descriptionLabel.text];
        [attString addAttribute:NSStrikethroughStyleAttributeName value:[NSNumber numberWithInt:0] range:NSMakeRange(0,[attString length])];
        affairCell.descriptionLabel.attributedText = attString;
    }
    
    affairCell.indexPath = indexPath;
    affairCell.didCheckAffair = ^(NSIndexPath *indexPath, BOOL selected)
    {
        NSMutableDictionary *affair;
        if(indexPath.section == 0)
        {
            affair = affairs_not_done[indexPath.row];//[self.fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:0]];
        }
        else
        {
            affair = affairs_done[/*affairs_not_done.count+*/indexPath.row];//[self.fetchedResultsController2 objectAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:0]];
        }
        affair[@"isdone"] = selected?@"true":@"false";
        [[AffairManager shared] createAffairItem:affair success:nil failure:nil];
        [self fetchTableData];
        [self.tableView reloadData];
    };
    
    affairCell.owner = self;
    
    affairCell.imgToZoom.imageModel.imageCachePolicy = ImageCachePolicyMemoryAndFileCache;
    affairCell.imgToZoom.imageModel.queuePriority = NSOperationQueuePriorityHigh;
    affairCell.imgToZoom.contentMode = UIViewContentModeScaleAspectFit;//UIViewContentModeScaleAspectFit; UIViewContentModeScaleAspectFill
    NSDictionary* imgData = (NSDictionary*)[DataBase getDB:@"photo" withKey:@"affairid" andValue:affair[@"id"]];
    NSArray* tst = [DataBase getDBArray:@"photo" withKey:@"" andValue:@""];
    NSLog(@"photos: %@", tst);
    if(imgData)
    {
        affairCell.imgBtn.hidden = NO;
        affairCell.hasImageImg.hidden = NO;

        
        [affairCell.imgToZoom  setImageWithURLString:[NSString stringWithFormat:[DataManager urlForPath:@"affairs/%@/image"], imgData[@"id"]]
                                             success:^(ImageRequestModel *object, UIImage *image) {
                                                 
                                             } failure:^(ImageRequestModel *object, NSError *error) {
                                                 
                                             } progress:^(ImageRequestModel *object, CGFloat progress) {
                                                 //
                                             }];
    }
    else
    {
        affairCell.imgBtn.hidden = YES;
        affairCell.hasImageImg.hidden = YES;
    }
    
    cell = affairCell;
    
    return cell;
}

- (NSArray *)rightButtons
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:231.0f/255.0f green:76.0f/255.0f blue:60.0f/255.0f alpha:1.0f] icon:[PercUtils imageWithImage:[UIImage imageNamed:@"delete_white.png"] scaledToSize:CGSizeMake(20, 25)]];
    
    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:20.0f/255.0f green:162.0f/255.0f blue:243.0f/255.0f alpha:1.0f] icon:[PercUtils imageWithImage:[UIImage imageNamed:@"pencil_white.png"] scaledToSize:CGSizeMake(10, 25)] ];
    
    return rightUtilityButtons;
}

- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell   {
    return YES;
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    switch (index) {
        case 0:
            {
                
                CZPickerView *picker = [[CZPickerView alloc] initWithHeaderTitle:@"Удалить?" cancelButtonTitle:@"Отмена" confirmButtonTitle:@"Да"];
                picker.headerBackgroundColor = getColorScheme();
                picker.selectedCell = [self.tableView indexPathForCell:cell];
                picker.delegate = self;
                picker.dataSource = self;
                picker.allowMultipleSelection = YES;
                [picker show];
        }
            break;
        case 1:
        {
            NSIndexPath *cellIndexPath = [self.tableView indexPathForCell:cell];
            
            NSDictionary* aff = cellIndexPath.section==0?affairs_not_done[cellIndexPath.row]:affairs_done[cellIndexPath.row];
            AffairAddViewController *addVC = [[AffairAddViewController alloc] initWithAffair:aff];
            [self.navigationController pushViewController:addVC animated:YES];
        }
            
            break;
        default:
            break;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (IBAction)onAddTask:(id)sender {
    AffairAddViewController *addVC = [[AffairAddViewController alloc] initWithAffairGroup:self.currentAffairGroup];
    [self.navigationController pushViewController:addVC animated:YES];
}

- (NSString *)czpickerView:(CZPickerView *)pickerView titleForRow:(NSInteger)row
{
    return @"Да";
}

- (NSInteger)numberOfRowsInPickerView:(CZPickerView *)pickerView
{
    return 0;
}

- (void)czpickerView:(CZPickerView *)pickerView didConfirmWithItemAtRow:(NSInteger)row{
    
    NSDictionary *affair;
    if(pickerView.selectedCell.section == 0)  {
        affair = affairs_not_done[pickerView.selectedCell.row];//[self.fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:cellIndexPath.row inSection:0]];
    } else  {
        affair = affairs_done[/*affairs_not_done.count+*/pickerView.selectedCell.row];//[self.fetchedResultsController2 objectAtIndexPath:[NSIndexPath indexPathForRow:cellIndexPath.row inSection:0]];
    }
    [[AffairManager shared] deleteAffairItemById:affair[@"id"] success:^{
        [self fetchTableData];
        [self.tableView reloadData];
        //
    } failure:^{
        //
    }];
}

- (void)czpickerView:(CZPickerView *)pickerView didConfirmWithItemsAtRows:(NSArray *)rows   {
    
    NSDictionary *affair;
    if(pickerView.selectedCell.section == 0)  {
        affair = affairs_not_done[pickerView.selectedCell.row];//[self.fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:cellIndexPath.row inSection:0]];
    } else  {
        affair = affairs_done[/*affairs_not_done.count+*/pickerView.selectedCell.row];//[self.fetchedResultsController2 objectAtIndexPath:[NSIndexPath indexPathForRow:cellIndexPath.row inSection:0]];
    }
    [[AffairManager shared] deleteAffairItemById:affair[@"id"] success:^{
        [self fetchTableData];
        [self.tableView reloadData];
        //
    } failure:^{
        //
    }];
}

- (void)czpickerViewDidClickCancelButton:(CZPickerView *)pickerView{
    NSLog(@"Canceled.");
}

@end
