//
//  AffairsCategoryTableViewCell.h
//  DentaBook
//
//  Created by Mac Mini on 22.11.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"

@interface AffairsCategoryTableViewCell : SWTableViewCell
@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UIView *frontView;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *txtLabel;
@property (weak, nonatomic) IBOutlet UILabel *detLabel;

@end
