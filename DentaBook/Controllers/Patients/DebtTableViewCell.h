//
//  DebtTableViewCell.h
//  DentaBook
//
//  Created by Mac Mini on 27.11.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DebtTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *commonSumLabel;
@property (weak, nonatomic) IBOutlet UILabel *debtLabel;

@end
