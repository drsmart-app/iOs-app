//
//  BirthdayTableViewCell.h
//  DentaBook
//
//  Created by Mac Mini on 04.01.17.
//  Copyright © 2017 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BirthdayTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *birthdayLabel;

@end
