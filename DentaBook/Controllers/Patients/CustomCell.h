//
//  CustomCell.h
//  DentaBook
//
//  Created by Mac Mini on 23.10.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomIOS7AlertView.h"

@interface CustomCell : UITableViewCell <UITextFieldDelegate, CustomIOS7AlertViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *typeButton;
- (IBAction)tapTypeButton:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *valueField;
@property (retain, nonatomic) NSArray *texts;
@property (retain, nonatomic) NSString *type;
@property (retain, nonatomic) NSString *field_name;
@property (retain, nonatomic) NSString *idi;
- (IBAction)tapRemove:(id)sender;

@end
