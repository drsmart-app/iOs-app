//
//  AddressTableViewCell.h
//  DentaBook
//
//  Created by Mac Mini on 07.11.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomIOS7AlertView.h"
@interface AddressTableViewCell : UITableViewCell<UITextFieldDelegate, CustomIOS7AlertViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *typeButton;
- (IBAction)tapTypeButton:(id)sender;
@property (retain, nonatomic) NSArray *texts;
@property (retain, nonatomic) NSString *type;
@property (retain, nonatomic) NSString *field_name;
@property (retain, nonatomic) NSString *idi;
- (IBAction)tapRemove:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *city_field;
@property (weak, nonatomic) IBOutlet UITextField *streetField;
@property (weak, nonatomic) IBOutlet UITextField *houseField;
@property (weak, nonatomic) IBOutlet UITextField *flatField;
@end
