//
//  ViewPhoneTableViewCell.m
//  DentaBook
//
//  Created by Mac Mini on 07.11.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "ViewPhoneTableViewCell.h"

@implementation ViewPhoneTableViewCell
@synthesize emailBtn, phoneBtn, smsBtn, phoneLabel;
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)tapCall:(id)sender
{
     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", phoneLabel.text]]];
}

- (IBAction)tapSMS:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"smsCalled" object:phoneLabel.text];
}
- (IBAction)tapEmail:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"emailCalled" object:phoneLabel.text];
}
@end
