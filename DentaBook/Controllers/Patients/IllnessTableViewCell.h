//
//  IllnessTableViewCell.h
//  DentaBook
//
//  Created by Mac Mini on 26.02.17.
//  Copyright © 2017 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IllnessTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *illnessLabel;

@end
