//
//  CustomCell.m
//  DentaBook
//
//  Created by Mac Mini on 23.10.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "CustomCell.h"

@implementation CustomCell
@synthesize typeButton, valueField, texts, type, field_name, idi;
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    valueField.delegate = self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)tapTypeButton:(id)sender {
    CustomIOS7AlertView *alertView = [CustomIOS7AlertView alertWithTitle:@"Выберите тип" message:nil andOwnr:sender];

    [alertView setButtonTitles:texts];
    //[alertView setButtonColors:[NSMutableArray arrayWithObjects:[UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:1.0f],[UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:1.0f],[UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:1.0f],[UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:1.0f],[UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:1.0f],[UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:1.0f],nil]];
    [alertView setDelegate:self];
    [alertView setOnButtonTouchUpInside:^(CustomIOS7AlertView *alertView, int buttonIndex) {
        NSLog(@"Block: Button at position %d is clicked on alertView %ld.", buttonIndex, (long)[alertView tag]);
        [alertView close];
    }];
    [alertView show];
}

- (void)customIOS7dialogButtonTouchUpInside: (CustomIOS7AlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    NSLog(@"Button at position %ld is clicked on alertView %ld.", (long)buttonIndex, (long)[alertView tag]);
    [typeButton setTitle:texts[buttonIndex] forState:UIControlStateNormal];
    [typeButton setTitle:texts[buttonIndex] forState:UIControlStateSelected];
    field_name=texts[buttonIndex];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"customChanged" object:@{@"text":valueField.text, @"type":type, @"field_name":field_name, @"id":idi}];
    //self.currentPatient[@"discount"] = [NSNumber numberWithInteger:5*(5-buttonIndex)];
    //[self.tableView reloadData];
}

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"customChanged" object:@{@"text":[[textField text] stringByReplacingCharactersInRange:range withString:string], @"type":type, @"field_name":field_name, @"id":idi}];
    
    if([type isEqualToString:@"phone"])
    {
        NSString* totalString = [/*[NSString stringWithFormat:@"%@%@",textField.text,string]*/[[textField text] stringByReplacingCharactersInRange:range withString:string] stringByReplacingOccurrencesOfString:@"" withString:@""];
        
        if(textField.tag==textField.tag/*102*/) {
            if (range.length == 1)
            {
                // Delete button was hit.. so tell the method to delete the last char.
                textField.text = [self formatPhoneNumber:totalString deleteLastChar:YES];
            }
            else
            {
                textField.text = [self formatPhoneNumber:totalString deleteLastChar:NO ];
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:@"phoneChanged" object:@{@"text":[textField text]}];
            return false;
        }
    }

    return true;
}

- (BOOL) textFieldShouldEndEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"focusLost" object:@{@"text":valueField.text, @"type":type, @"field_name":field_name, @"id":idi}];
    return YES;
}

-(NSString*) formatPhoneNumber:(NSString*) simpleNumber deleteLastChar:(BOOL)deleteLastChar
{
    @try
    {
        @try
        {
            NSString* tst_string = [simpleNumber copy];
            simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
            
            if(simpleNumber.length==0)
            {
                
                if([tst_string hasPrefix:@"+"])
                    return [NSString stringWithFormat:@"+%@", simpleNumber];
                else
                    return simpleNumber;
            }
            // use regex to remove non-digits(including spaces) so we are left with just the numbers
            NSError *error = NULL;
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[\\s-\\(\\)]" options:NSRegularExpressionCaseInsensitive error:&error];
            simpleNumber = [regex stringByReplacingMatchesInString:simpleNumber options:0 range:NSMakeRange(0, [simpleNumber length]) withTemplate:@""];
            
            // check if the number is to long
            if(simpleNumber.length>14) {
                // remove last extra chars.
                simpleNumber = [simpleNumber substringToIndex:14];
            }
            
            if(deleteLastChar) {
                // should we delete the last digit?
                simpleNumber = [simpleNumber substringToIndex:[simpleNumber length] - 1];
            }
            
            // 123 456 7890
            // format the number.. if it's less then 7 digits.. then use this regex.
            if(simpleNumber.length>11)
                simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{2})(\\d{3})(\\d{3})(\\d{2})(\\d{2})(\\d+)"
                                                                       withString:@"$1($2) $3-$4-$5-$6"
                                                                          options:NSRegularExpressionSearch
                                                                            range:NSMakeRange(0, [simpleNumber length])];
            
            else if(simpleNumber.length==11)  // else do this one..
                simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{1})(\\d{3})(\\d{3})(\\d{2})(\\d+)"
                                                                       withString:@"$1($2) $3-$4-$5"
                                                                          options:NSRegularExpressionSearch
                                                                            range:NSMakeRange(0, [simpleNumber length])];
            else if(simpleNumber.length==10)  // else do this one..
                simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{1})(\\d{3})(\\d{3})(\\d{2})(\\d+)"
                                                                       withString:@"$1($2) $3-$4-$5"
                                                                          options:NSRegularExpressionSearch
                                                                            range:NSMakeRange(0, [simpleNumber length])];
            else if(simpleNumber.length==9)  // else do this one..
                simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{1})(\\d{3})(\\d{3})(\\d{1})(\\d+)"
                                                                       withString:@"$1($2) $3-$4-$5"
                                                                          options:NSRegularExpressionSearch
                                                                            range:NSMakeRange(0, [simpleNumber length])];
            else if(simpleNumber.length==8)  // else do this one..
                simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{1})(\\d{3})(\\d{3})(\\d+)"
                                                                       withString:@"$1($2) $3-$4"
                                                                          options:NSRegularExpressionSearch
                                                                            range:NSMakeRange(0, [simpleNumber length])];
            else if(simpleNumber.length==7)  // else do this one..
                simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{1})(\\d{3})(\\d{2})(\\d+)"
                                                                       withString:@"$1($2) $3-$4"
                                                                          options:NSRegularExpressionSearch
                                                                            range:NSMakeRange(0, [simpleNumber length])];
            else if(simpleNumber.length==6)  // else do this one..
                simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{1})(\\d{3})(\\d{1})(\\d+)"
                                                                       withString:@"$1($2) $3-$4"
                                                                          options:NSRegularExpressionSearch
                                                                            range:NSMakeRange(0, [simpleNumber length])];
            else if(simpleNumber.length==5)  // else do this one..
                simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{1})(\\d{3})(\\d+)"
                                                                       withString:@"$1($2) $3"
                                                                          options:NSRegularExpressionSearch
                                                                            range:NSMakeRange(0, [simpleNumber length])];
            else if(simpleNumber.length==4)  // else do this one..
                simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{1})(\\d{3})(\\d+)"
                                                                       withString:@"$1($2)"
                                                                          options:NSRegularExpressionSearch
                                                                            range:NSMakeRange(0, [simpleNumber length])];
            else  // else do this one..
                simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{1})(\\d{3})(\\d{3})(\\d{2})(\\d+)"
                                                                       withString:@"$1($2) $3-$4-$5"
                                                                          options:NSRegularExpressionSearch
                                                                            range:NSMakeRange(0, [simpleNumber length])];
            if([tst_string hasPrefix:@"+"])
                return [NSString stringWithFormat:@"+%@", simpleNumber];
            else
                return simpleNumber;
        }
        @catch(NSException* ex)
        {
            
        }
        
        return simpleNumber;
    }
    @catch(NSException* ex)
    {
        
    }
    
    return simpleNumber;
}


- (IBAction)tapRemove:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"customDeleted" object:@{@"type":type, @"field_name":field_name, @"id":idi}];
}


@end
