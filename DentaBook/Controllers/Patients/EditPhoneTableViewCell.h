//
//  EditPhoneTableViewCell.h
//  DentaBook
//
//  Created by Mac Mini on 11.11.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditPhoneTableViewCell : UITableViewCell<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;

@end
