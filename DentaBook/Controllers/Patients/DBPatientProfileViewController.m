//
//  DBPatientProfileViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 04.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "DBPatientProfileViewController.h"
#import "DBPPHeadTableViewCell.h"
#import "HCSStarRatingView.h"
#import "DBPatienEditViewController.h"
#import "TherapyViewController.h"
#import "TherapyPlanViewController.h"
#import "VisitHistoryViewController.h"
#import "MoneyDepositViewController.h"
#import "ToothCardListViewController.h"
#import "PatientFinanceViewController.h"
#import "IllnessListViewController.h"
#import "PhotoGalleryViewController.h"
#import "UIView+Helpers.h"
#import "UIImageView+ImageDownloader.h"
#import "AddInfoViewController.h"
#import "NewEventViewController.h"
#import "CalendarViewController.h"
#import "IllnessTableViewCell.h"

#import <MagicalRecord/MagicalRecord.h>

#import "Patient+Custom.h"
#import "Therapy.h"
#import "PatientIllnessRef.h"
#import "PatientIllness.h"
#import "DataManager.h"
#import "DataBase.h"
#import "PercUtils.h"
#import "DebtTableViewCell.h"

#import "HttpUtil.h"

@interface DBPatientProfileViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSDictionary *currentPatient;

@property (nonatomic, strong) NSArray *titleItemArray;
@property (nonatomic, strong) NSArray *iconItemArray;

@property (nonatomic, strong) NSString *illnessTitle;
@end

@implementation DBPatientProfileViewController
@synthesize events, visits, last_event, last_past_event;
-(id) initWithPatient:(NSDictionary*)patient {
    self = [super initWithNibName:@"DBPatientProfileViewController" bundle:[NSBundle mainBundle]];
    if(self)    {
        self.currentPatient = patient;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Профиль пациента";
    
    UIImage *image2 = [UIImage imageNamed:@"back-button"];
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 addTarget:self action:@selector(onBack:)forControlEvents:UIControlEventTouchUpInside];
    [button2 setBackgroundImage:image2 forState:UIControlStateNormal];
    button2.frame = CGRectMake(0, 0, 11, 18);
    UIBarButtonItem *barButton2 = [[UIBarButtonItem alloc] initWithCustomView:button2];
    self.navigationItem.leftBarButtonItem = barButton2;
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(editPatient)forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"Изменить" forState:UIControlStateNormal];
    [button setTitle:@"Изменить" forState:UIControlStateSelected];
    button.frame = CGRectMake(0, 0, 60, 18);
    button.titleLabel.font = [UIFont systemFontOfSize:12];
    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = barButton;
    
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    button.contentVerticalAlignment = UIControlContentVerticalAlignmentBottom;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"DBPPHeadTableViewCell" bundle:nil] forCellReuseIdentifier:@"DBPPHeadTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"DebtTableViewCell" bundle:nil] forCellReuseIdentifier:@"DebtTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"IllnessTableViewCell" bundle:nil] forCellReuseIdentifier:@"IllnessTableViewCell"];
    
    self.titleItemArray = @[@"Запись на прием",
                            @"Следующее посещение %@",
                            @"История",
                            @"Последнее посещение %@",
                            @"Внести деньги",
                            @"Финансы",
                            @"",
                            @"План лечения",
                            @"Лечение",
                            @"Зубная карта",
                            @"Фотоальбом",
                            @"Дополнительная информация"];
    self.iconItemArray = @[ @"patient-entrytherapy-icon",
                            @"blue_clock",
                            @"patient-history-icon",
                            @"next_meet",
                            @"patient-entrymoney-icon",
                            @"finances",
                            @"",
                            @"patient-therapyplan-icon",
                            @"patient-therapy-icon",
                            @"patient-toothmap-icon",
                            @"patient-photoalbum-icon",
                            @"patient-additioninfo-icon"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(needPopToList) name:@"needPopToList" object:nil];
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = getColorSchemeStatus();
    }
}

- (void) needPopToList
{
    //[self.navigationController popViewControllerAnimated:YES];
    //[self.navigationController popViewControllerAnimated:YES];
}

-(void) viewWillAppear:(BOOL)animated   {
    [self updateIllnessTitle];
    [self.tableView reloadData];
    
    events = [DataBase getDBArray:@"events" withKey:@"patientid" andValue:self.currentPatient[@"patientid"]];
    
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"dd.MM.yyyy"];
    NSDate* lastVisitDate = [dateformate dateFromString:@"21.12.2200"];
    NSDate* lastPastVisitDate = [dateformate dateFromString:@"21.12.2000"];
    BOOL found=false;
    BOOL found_past=false;
    int lastIndexPath = 0;
    int lastPastPath = 0;
    int i=0;
    for(NSDictionary* d in events)
    {
        if([[dateformate dateFromString:d[@"startdate"]] timeIntervalSince1970]<[lastVisitDate timeIntervalSince1970]&&[[dateformate dateFromString:d[@"startdate"]] timeIntervalSince1970]>=[[NSDate date] timeIntervalSince1970])
        {
            lastVisitDate = [dateformate dateFromString:d[@"startdate"]];
            lastIndexPath = i;
            found = true;
        }
    }
    
    if(found)
    {
        last_event = events[lastIndexPath];
    }
    else
    {
        last_event = nil;
    }
    
    visits=[DataBase getDBArray:@"visits" withKey:@"patientid" andValue:self.currentPatient[@"patientid"]];
    
    for(NSDictionary* d in visits)
    {
        if([[dateformate dateFromString:d[@"startdate"]] timeIntervalSince1970]>[lastPastVisitDate timeIntervalSince1970]&&[[dateformate dateFromString:d[@"startdate"]] timeIntervalSince1970]<=[[NSDate date] timeIntervalSince1970])
        {
            lastPastVisitDate = [dateformate dateFromString:d[@"startdate"]];
            lastPastPath = i;
            found_past = true;
        }
        i++;
    }
    
    if(found_past)
    {
        last_past_event = visits[lastPastPath];
    }
    else
    {
        last_past_event = nil;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) editPatient {
    DBPatienEditViewController *patientEditViewController = [[DBPatienEditViewController alloc] initWithPatient:self.currentPatient];
    [self.navigationController pushViewController:patientEditViewController animated:YES];
}

-(void) updateIllnessTitle  {
    NSMutableArray *array = [NSMutableArray new];
    
    int i=0;
    for(NSDictionary* dict in self.currentPatient[@"illness"])
    {
        if(i>=3)
            break;
        [array addObject:dict[@"title"]];
        
        i++;
    }
    for(;i<3;i++)
    {
        [array addObject:@" "];
    }
    self.illnessTitle = [NSString stringWithFormat:@"%@",[array componentsJoinedByString:@"\n"]];//(i==2)?[NSString stringWithFormat:@"\n%@",[array componentsJoinedByString:@"\n"]]:[array componentsJoinedByString:@"\n"];
}

#pragma mark - UITableView DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView   {
    return 1 + self.titleItemArray.count + ((((NSArray*)self.currentPatient[@"illness"]).count > 0) ? 1 : 0);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section    {
    if(section == 3 + ((((NSArray*)self.currentPatient[@"illness"]).count > 0) ? 1 : 0))    {
        NSArray *therapyArray = [[NSArray alloc] init]/*[Therapy MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"patient == %@ AND isPlan == FALSE", self.currentPatient]]*/;
        if(therapyArray.count > 0)  {
            return 2;
        }
    }
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
        return 110.f;
    else if(indexPath.section == 1 && (((NSArray*)self.currentPatient[@"illness"]).count > 0)) {
        return (((NSArray*)self.currentPatient[@"illness"]).count > 2)?80.0f:70.0f;
    }
    if((indexPath.section==3 && (((NSArray*)self.currentPatient[@"illness"]).count > 0))||(indexPath.section==2 && (((NSArray*)self.currentPatient[@"illness"]).count == 0)))
    {
        if(last_event)
            return 55.f;
        else
            return 0;
    }
    if((indexPath.section==5 && (((NSArray*)self.currentPatient[@"illness"]).count > 0))||(indexPath.section==4 && (((NSArray*)self.currentPatient[@"illness"]).count == 0)))
    {
        if(visits.count>0 && last_past_event)
            return 55.f;
        else
            return 0;
    }
    else
    {
        return 55.f;
    }
}

#pragma mark - UITableView Delegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    if(indexPath.section == 0)
    {
        DBPPHeadTableViewCell *headCell = [tableView dequeueReusableCellWithIdentifier:@"DBPPHeadTableViewCell"];
        
        NSString *fullName = [NSString stringWithFormat:@"%@ %@", self.currentPatient[@"firstname"], self.currentPatient[@"middlename"]];
        headCell.patientFirstName.text = self.currentPatient[@"lastname"];
        headCell.patientLastName.text = fullName;
        headCell.ratingView.value = [self.currentPatient[@"rating"] integerValue];
        headCell.discountLabel.text = [NSString stringWithFormat:@"%@%%", self.currentPatient[@"discount"]];
        //headCell.discountLabel = [self.currentPatient.discount]
        
        headCell.patientLogoImageView.layer.cornerRadius = (headCell.patientLogoImageView.frame.size.width/2);
        UIImage *image = [UIImage imageNamed:@"patient-placeholder-logo"];
        headCell.patientLogoImageView.imageModel.placeholderImage = image;
        headCell.patientLogoImageView.imageModel.failureImage = image;
        headCell.patientLogoImageView.imageModel.cancelImage = image;
        headCell.patientLogoImageView.imageModel.cornerRadius = (headCell.patientLogoImageView.frame.size.width/2);
        headCell.patientLogoImageView.imageModel.imageCachePolicy = ImageCachePolicyMemoryAndFileCache;
        headCell.patientLogoImageView.imageModel.queuePriority = NSOperationQueuePriorityHigh;
        [headCell.patientLogoImageView setImageWithURLString:[NSString stringWithFormat:[DataManager urlForPath:@"patient/%@/image"], self.currentPatient[@"patientid"]]
                                                     success:^(ImageRequestModel *object, UIImage *image) {
                                                         //
                                                     } failure:^(ImageRequestModel *object, NSError *error) {
                                                         //
                                                     } progress:^(ImageRequestModel *object, CGFloat progress) {
                                                         //
                                                     }];
        
        if(((NSString*)self.currentPatient[@"photo"]).length > 0)    {
            headCell.patientLogoImageView.image = [self loadImageFileName:(NSString*)self.currentPatient[@"photo"]];
            headCell.patientLogoImageView.layer.cornerRadius = (headCell.patientLogoImageView.frame.size.width/2);
        }
        
        cell = headCell;
    }
    else  if(indexPath.section == 1 && (((NSArray*)self.currentPatient[@"illness"]).count > 0))
    {
        IllnessTableViewCell *titleCell = [tableView dequeueReusableCellWithIdentifier:@"IllnessTableViewCell"];
        //titleCell = [[IllnessTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"IllnessTableViewCell"];
        
        titleCell.imageView.image = [UIImage imageNamed:@"patient-illness-icon"];
        //titleCell.textLabel.text = @"Заболевания";
        //titleCell.textLabel.textColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
        //titleCell.textLabel.font = [UIFont systemFontOfSize:12];
        titleCell.illnessLabel.text = self.illnessTitle;
        titleCell.illnessLabel.numberOfLines = 0;
        titleCell.illnessLabel.textColor = [UIColor redColor];
        titleCell.illnessLabel.font = [UIFont systemFontOfSize:12];
        //[titleCell.illnessLabel sizeToFit];
        if(((NSArray*)self.currentPatient[@"illness"]).count > 3)
            titleCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        else
            titleCell.accessoryType = UITableViewCellAccessoryNone;
        cell = titleCell;
    }
    else
    {
        UITableViewCell *titleCell = [tableView dequeueReusableCellWithIdentifier:@"titleCell"];
        if (titleCell == nil) {
            titleCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"titleCell"];
            titleCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        
        NSInteger indx = indexPath.section - (1 + ((((NSArray*)self.currentPatient[@"illness"]).count > 0) ? 1 : 0));
        UIImage* img;
        
        if([(NSString*)self.iconItemArray[indx] rangeOfString: @"-" ].location == NSNotFound)
        {
            img= [PercUtils imageWithImage:[UIImage imageNamed:self.iconItemArray[indx]] scaledToSize:CGSizeMake(23, 23)];
        }
        else
        {
            img= [UIImage imageNamed:self.iconItemArray[indx]];
        }
        
        titleCell.imageView.image = img;
        if((indexPath.section==3 && (((NSArray*)self.currentPatient[@"illness"]).count > 0))||(indexPath.section==2 && (((NSArray*)self.currentPatient[@"illness"]).count == 0)))
        {
            if(last_event)
            {
                titleCell.textLabel.textColor = [UIColor colorWithRed:20.0/255.0 green:162.0/255.0 blue:243.0/255.0 alpha:1.0];
                titleCell.accessoryType = UITableViewCellAccessoryNone;
                titleCell.textLabel.text = [NSString stringWithFormat:self.titleItemArray[indexPath.section - (1 + ((((NSArray*)self.currentPatient[@"illness"]).count > 0) ? 1 : 0))],last_event[@"startdate"]];
            }
            else
            {
                titleCell.accessoryType = UITableViewCellAccessoryNone;
            }
        }
        else if((indexPath.section==5 && (((NSArray*)self.currentPatient[@"illness"]).count > 0))||(indexPath.section==4 && (((NSArray*)self.currentPatient[@"illness"]).count == 0)))
        {
            if(visits.count>0 && last_past_event)
            {
                titleCell.textLabel.textColor = [UIColor colorWithRed:20.0/255.0 green:162.0/255.0 blue:243.0/255.0 alpha:1.0];
                titleCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            }
            else
            {
                titleCell.accessoryType = UITableViewCellAccessoryNone;
            }
            
            //titleCell.textLabel.text = self.titleItemArray[indexPath.section - (1 + ((((NSArray*)self.currentPatient[@"illness"]).count > 0) ? 1 : 0))];
            titleCell.textLabel.text = [NSString stringWithFormat:self.titleItemArray[indexPath.section - (1 + ((((NSArray*)self.currentPatient[@"illness"]).count > 0) ? 1 : 0))],last_past_event[@"startdate"]];
        }
        else if((indexPath.section==8 && (((NSArray*)self.currentPatient[@"illness"]).count > 0))||(indexPath.section==7 && (((NSArray*)self.currentPatient[@"illness"]).count == 0)))
        {
            DebtTableViewCell *headCell = [tableView dequeueReusableCellWithIdentifier:@"DebtTableViewCell"];
            
            NSArray* costs = [DataBase getDBArray:@"visits" withKey:@"patientid" andValue:self.currentPatient[@"patientid"]];
            float allCredit=0;
            for(NSDictionary* dc in costs)
            {
                /*float discount = [dc[@"discount"] floatValue];
                for(NSDictionary* it in dc[@"items"])
                {
                    float cost = [it[@"cost"] floatValue]*[it[@"countt"] floatValue];
                    allCredit+=cost*(100.0f-discount)/100.0f;
                }*/
                allCredit+=[dc[@"total_cost_with_discount"] floatValue];
            }
            
            float allDebit=0;
            NSArray* payments = [DataBase getDBArray:@"payment" withKey:@"patientid" andValue:self.currentPatient[@"patientid"]];
            
            for(NSDictionary* dc in payments)
            {
                allDebit += [dc[@"amount"] floatValue];
            }
            
            float debt = allDebit - allCredit;// - [self.currentPatient allPayment];
            
            NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
            [numberFormatter setGroupingSeparator:@" "];
            [numberFormatter setGroupingSize:3];
            [numberFormatter setUsesGroupingSeparator:YES];
            [numberFormatter setDecimalSeparator:@"."];
            [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
            [numberFormatter setMaximumFractionDigits:2];
            
            NSString *theString = [numberFormatter stringFromNumber:@(allCredit)];
            
            headCell.commonSumLabel.text=[NSString stringWithFormat:@"Общее: %@ ₽", theString];
            theString = [numberFormatter stringFromNumber:@(debt)];
            headCell.debtLabel.text=debt<=0?[NSString stringWithFormat:@"Долг: %@ ₽", debt<0?[numberFormatter stringFromNumber:@(-debt)]:@"0"]:[NSString stringWithFormat:@"Аванс: %@ ₽", [numberFormatter stringFromNumber:@(debt)]];
            
            headCell.debtLabel.textColor=debt<0?[UIColor colorWithRed:231.0/255.0 green:76.0/255.0 blue:60.0/255.0 alpha:1.0]:(debt==0?[UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0]:[UIColor colorWithRed:46.0/255.0 green:204.0/255.0 blue:113.0/255.0 alpha:1.0]);
            
            headCell.selectionStyle = UITableViewCellSelectionStyleNone;
            titleCell=headCell;
        }
        else
        {
            titleCell.textLabel.textColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
            titleCell.textLabel.text = self.titleItemArray[indexPath.section - (1 + ((((NSArray*)self.currentPatient[@"illness"]).count > 0) ? 1 : 0))];
        }
        
        
        cell = titleCell;
    }
    cell.textLabel.font = [UIFont systemFontOfSize:12];
    cell.detailTextLabel.font = [UIFont systemFontOfSize:12];
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsMake(0, 10, 0, 10)];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if(indexPath.section == 1 && (((NSArray*)self.currentPatient[@"illness"]).count > 3))  {
        IllnessListViewController *illnessListVC = [[IllnessListViewController alloc] initWithPatient:self.currentPatient isReadOnly:YES];
        [self.navigationController pushViewController:illnessListVC animated:YES];
    }
    
    if(indexPath.section == 3 + ((((NSArray*)self.currentPatient[@"illness"]).count > 0) ? 1 : 0))  {
        if(indexPath.row == 1)  {
            return;
        }
    }
    
    switch (indexPath.section - (1 + ((((NSArray*)self.currentPatient[@"illness"]).count > 0) ? 1 : 0))) {
        case 0: {
            /*NewEventViewController *newEventViewController = [[NewEventViewController alloc] initWithPatient:self.currentPatient];
            [self.navigationController pushViewController:newEventViewController animated:YES];*/
            CalendarViewController* newEventViewController = [[CalendarViewController alloc] initWithPatient:self.currentPatient];
            [self.navigationController pushViewController:newEventViewController animated:YES];
            break;
        }
            
        case 1: {
            VisitHistoryViewController *visitHistoryViewController = [[VisitHistoryViewController alloc] initWithPatient:self.currentPatient];
            [self.navigationController pushViewController:visitHistoryViewController animated:YES];
        }
            break;
        case 2: {
            VisitHistoryViewController *visitHistoryViewController = [[VisitHistoryViewController alloc] initWithPatient:self.currentPatient];
            [self.navigationController pushViewController:visitHistoryViewController animated:YES];
        }
            break;
            
        case 3: {
            VisitHistoryViewController *visitHistoryViewController = [[VisitHistoryViewController alloc] initWithPatient:self.currentPatient];
            visitHistoryViewController.needLast = YES;
            [self.navigationController pushViewController:visitHistoryViewController animated:YES];
        }
            break;
            
        case 4: {
            MoneyDepositViewController *moneyTransferViewController = [[MoneyDepositViewController alloc] initWithPatient:self.currentPatient withMoney:0];
            [self.navigationController pushViewController:moneyTransferViewController animated:YES];
        }
            break;
        case 5: {
            PatientFinanceViewController *financeViewController = [[PatientFinanceViewController alloc] initWithPatient:self.currentPatient];
            [self.navigationController pushViewController:financeViewController animated:YES];
        }
            break;
        case 7: {
            TherapyPlanViewController *therapyPlanViewController = [[TherapyPlanViewController alloc] initWithPatient:self.currentPatient];
            [self.navigationController pushViewController:therapyPlanViewController animated:YES];
        }
            break;
        case 8: {
            TherapyViewController *therapyViewController = [[TherapyViewController alloc] initWithPatient:self.currentPatient placeToPlan:NO andNumber:-1];
            [self.navigationController pushViewController:therapyViewController animated:YES];
        }
            break;
        case 9: {
            ToothCardListViewController *toothCardViewController = [[ToothCardListViewController alloc] initWithPatient:self.currentPatient];
            [self.navigationController pushViewController:toothCardViewController animated:YES];
        }
            break;
        case 10: {
            PhotoGalleryViewController *toothCardViewController = [[PhotoGalleryViewController alloc] initWithPatient:self.currentPatient];
            [self.navigationController pushViewController:toothCardViewController animated:YES];
        }
            break;
        case 11: {
            AddInfoViewController *addViewController = [[AddInfoViewController alloc] initWithPatient:self.currentPatient];
            [self.navigationController pushViewController:addViewController animated:YES];
        }
            break;
            
        default:
            break;
    }
}

- (UIImage*)loadImageFileName:(NSString*)fileName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:
                      [NSString stringWithFormat: @"%@.png", fileName] ];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    return image;
}

@end
