//
//  ViewPhoneTableViewCell.h
//  DentaBook
//
//  Created by Mac Mini on 07.11.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewPhoneTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneTypeLabel;
- (IBAction)tapCall:(id)sender;
- (IBAction)tapSMS:(id)sender;
@property (nonatomic, retain) NSString* text;
- (IBAction)tapEmail:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *emailBtn;
@property (weak, nonatomic) IBOutlet UIButton *phoneBtn;
@property (weak, nonatomic) IBOutlet UIButton *smsBtn;

@end
