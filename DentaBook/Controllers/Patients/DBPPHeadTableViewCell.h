//
//  DBPPHeadTableViewCell.h
//  DentaBook
//
//  Created by Denis Dubov on 04.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HCSStarRatingView;

@interface DBPPHeadTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *patientLogoImageView;
@property (weak, nonatomic) IBOutlet UILabel *patientFirstName;
@property (weak, nonatomic) IBOutlet UILabel *patientLastName;
@property (weak, nonatomic) IBOutlet UILabel *avansLabel;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *ratingView;
- (IBAction)discountBtnClick:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *discountLabel;

@end
