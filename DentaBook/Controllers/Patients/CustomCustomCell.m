//
//  CustomCustomCell.m
//  DentaBook
//
//  Created by Mac Mini on 30.10.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "CustomCustomCell.h"

@implementation CustomCustomCell
@synthesize nameField, valueField, type, field_name, idi;
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    valueField.delegate = self;
    nameField.delegate = self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField == nameField)
        [[NSNotificationCenter defaultCenter] postNotificationName:@"customChanged" object:@{@"text":valueField.text, @"type":type, @"field_name":[[textField text] stringByReplacingCharactersInRange:range withString:string], @"id":idi}];
    else
        [[NSNotificationCenter defaultCenter] postNotificationName:@"customChanged" object:@{@"text":[[textField text] stringByReplacingCharactersInRange:range withString:string], @"type":type, @"field_name":nameField.text, @"id":idi}];
    
    return true;
}

- (IBAction)tapRemove:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"customDeleted" object:@{@"type":type, @"field_name":field_name, @"id":idi}];
}
@end
