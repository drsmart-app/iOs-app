//
//  DBPatienEditViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 05.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "DBPatienEditViewController.h"

#import <MagicalRecord/MagicalRecord.h>
#import <UIAlertView+Blocks/UIAlertView+Blocks.h>
#import <objc/runtime.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <AddressBook/AddressBook.h>
#import <JGProgressHUD/JGProgressHUD.h>
#import <NSDate+Calendar/NSDate+Calendar.h>

#import "DBPPEditHeadTableViewCell.h"
#import "HCSStarRatingView.h"
#import "IBActionSheet.h"
#import "IllnessListViewController.h"
#import "UIImageView+ImageDownloader.h"
#import "DBFullDateController.h"
#import "WLPActionSheet.h"

#import "Patient+Custom.h"
#import "PatientManager.h"
#import "DataManager.h"
#import "DataBase.h"
#import "CustomCell.h"
#import "CustomCustomCell.h"
#import "AddressTableViewCell.h"
#import "EditPhoneTableViewCell.h"
#import "KeyboardListener.h"
#import "PercUtils.h"
#import "UIView+FindFirstResponder.h"
#import "UIView+Toast.h"
#import "HttpUtil.h"

@interface DBPatienEditViewController () <IBActionSheetDelegate, UIImagePickerControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSMutableDictionary *currentPatient;

@property (nonatomic, strong) IBActionSheet *photoActionSheet;

@property (nonatomic, strong) UITextField *phoneTextField;

@property (nonatomic, strong) WLPActionSheet *birthActionSheet;
@property (nonatomic, strong) DBFullDateController *birthDateController;
@property (nonatomic, strong) NSDate *birthDate;
@property (nonatomic) BOOL creationMode;
@property (nonatomic, strong) NSString* illnessTitle;

@end

@implementation DBPatienEditViewController  {
    NSString *fileName;
    NSArray* items;
    NSArray* items_headers;
    NSArray* items_texts;
    UIView *plCropOverlay;
    /*NSMutableDictionary* sections;*/
}
@synthesize custom_fields;

-(id) initWithPatient:(NSDictionary*)patient {
    self = [super initWithNibName:@"DBPatientProfileViewController" bundle:[NSBundle mainBundle]];
    if(self)    {
        items=@[@"phone",@"email",@"occupancy",@"address",@"custom", @"delete"];
        items_headers=@[@"Добавить телефон",@"Добавить Е-mail",@"Добавить род деятельности",@"Добавить адрес",@"Добавить поле",@"Удалить пациента"];
        items_texts=@[@"Телефон",@"Е-mail",@"Род деятельности",@"Адрес",@"Поле",@"Удалить пациента"];
        
        if(patient==nil)
        {
            self.currentPatient = [NSMutableDictionary dictionaryWithDictionary:patient];
            self.currentPatient[@"patientid"] = GUIDString();
            self.currentPatient[@"firstname"] = @"";
            self.currentPatient[@"middlename"] = @"";
            self.currentPatient[@"lastname"] = @"";
            self.currentPatient[@"phone"] = @"";
            self.currentPatient[@"discount"] = @0;
            self.currentPatient[@"illness"] = [[NSMutableArray alloc] init];
            self.currentPatient[@"additional_info"] = [[NSMutableDictionary alloc] init];
            for(NSString* item in items)
            {
                NSArray* content = [DataBase getDBArray:@"custom_fields" withKey:@"category" andValue:item];
                if(content!=nil)
                    self.currentPatient[@"additional_info"][item] = [NSMutableArray arrayWithArray:content];
                else
                    self.currentPatient[@"additional_info"][item] = [[NSMutableArray alloc] init];
                
            }
            
            self.creationMode = YES;
            self.title = @"";

        }
        else
        {
            self.currentPatient = [NSMutableDictionary dictionaryWithDictionary:patient];
            self.creationMode = NO;
            self.title = @"Редактирование профиля";
        }

    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImage *image2 = [UIImage imageNamed:@"back-button"];
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 addTarget:self action:@selector(onBack:)forControlEvents:UIControlEventTouchUpInside];
    [button2 setBackgroundImage:image2 forState:UIControlStateNormal];
    button2.frame = CGRectMake(0, 0, 11, 18);
    UIBarButtonItem *barButton2 = [[UIBarButtonItem alloc] initWithCustomView:button2];
    self.navigationItem.leftBarButtonItem = barButton2;
    
    self.birthDateController = [DBFullDateController new];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(savePatient)forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"Сохранить" forState:UIControlStateNormal];
    [button setTitle:@"Сохранить" forState:UIControlStateSelected];
    button.frame = CGRectMake(0, 0, 65, 18);
    button.titleLabel.font = [UIFont systemFontOfSize:12];
    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = barButton;
    
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    button.contentVerticalAlignment = UIControlContentVerticalAlignmentBottom;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"DBPPEditHeadTableViewCell" bundle:nil] forCellReuseIdentifier:@"DBPPEditHeadTableViewCell"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"CustomCell" bundle:nil] forCellReuseIdentifier:@"customCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"CustomCustomCell" bundle:nil] forCellReuseIdentifier:@"customCustomCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"AddressTableViewCell" bundle:nil] forCellReuseIdentifier:@"addressCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"EditPhoneTableViewCell" bundle:nil] forCellReuseIdentifier:@"editPhoneCell"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeCameraOverlay) name:@"_UIImagePickerControllerUserDidCaptureItem" object:nil ];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addCameraOverlay) name:@"_UIImagePickerControllerUserDidRejectItem" object:nil ];
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
    
    /*if ([self respondsToSelector:@selector(edgesForExtendedLayout)]&&getNeedName())
        self.edgesForExtendedLayout = UIRectEdgeNone;*/
    
    self.extendedLayoutIncludesOpaqueBars = YES;
    
    self.tableView.contentInset = UIEdgeInsetsMake(64,0,0,0);
    
    //self.navigationController.navigationBar.translucent = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
}

-(void)removeCameraOverlay
{
    //plCropOverlay.hidden = YES;
}

-(void)addCameraOverlay
{
    //plCropOverlay.hidden = NO;
}

-(void)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void) viewDidAppear:(BOOL)animated
{
    //[self refreshSections];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showDiscountMenu:) name:@"discountMenuCalled" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeFirstname:) name:@"firstnameChanged" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeLastname:) name:@"lastnameChanged" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ratingChanged:) name:@"ratingChanged" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(customChanged:) name:@"customChanged" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(phoneFieldDidChange:) name:@"phoneChanged" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(customDeleted:) name:@"customDeleted" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(focusLost:) name:@"focusLost" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    NSMutableArray *array = [NSMutableArray new];
    
    int i=0;
    for(NSDictionary* dict in self.currentPatient[@"illness"])
    {
        if(i>=3)
            break;
        [array addObject:dict[@"title"]];
        
        i++;
    }
    self.illnessTitle = [array componentsJoinedByString:@"\n"];
    
    [self.tableView reloadData];
}

- (void) focusLost:(NSNotification*) n
{
    NSString* type=n.object[@"type"];
    NSObject* value=n.object[@"text"];
    
    if([type isEqualToString:@"email"])
    {
        if(![self NSStringIsValidEmail:value])
        {
            [self.view makeToast:@"Неверный формат e-mail"];
            return;
        }
    }
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (void) customChanged:(NSNotification*) n
{
    NSString* type=n.object[@"type"];
    NSString* title=n.object[@"field_name"];
    NSString* idi=n.object[@"id"];
    NSObject* value=n.object[@"text"];
    //@{@"id":GUIDString(), @"field_name":items_texts[section], @"category":items[section], @"value":@""}
    id to_remove=nil;
    
    
    for(NSDictionary* d in self.currentPatient[@"additional_info"][type])
    {
        if([d[@"id"] isEqualToString:idi])
        {
            to_remove=d;
            break;
        }
    }
    
    if(to_remove!=nil)
    {
        [self.currentPatient[@"additional_info"][type] removeObject:to_remove];
    }
    
    [self.currentPatient[@"additional_info"][type] addObject:@{@"field_name":title, @"category":type, @"value":value, @"id":idi}];
    
    //[self.tableView reloadData];
}

- (void) customDeleted:(NSNotification*) n
{
    NSString* type=n.object[@"type"];
    NSString* idi=n.object[@"id"];
    //@{@"id":GUIDString(), @"field_name":items_texts[section], @"category":items[section], @"value":@""}
    id to_remove=nil;
    for(NSDictionary* d in self.currentPatient[@"additional_info"][type])
    {
        if([d[@"id"] isEqualToString:idi])
        {
            to_remove=d;
            break;
        }
    }
    
    if(to_remove!=nil)
    {
        [self.currentPatient[@"additional_info"][type] removeObject:to_remove];
    }
    
    //[self.currentPatient[@"additional_info"][type] addObject:@{@"field_name":title, @"category":type, @"value":value, @"id":idi}];
    
    [self.tableView reloadData];
}

- (void) ratingChanged:(NSNotification*) n
{
    self.currentPatient[@"rating"] = n.object[@"rating"];
}

- (void) changeFirstname:(NSNotification*) n
{
    self.currentPatient[@"firstname"] = n.object[@"text"];
}

- (void) changeLastname:(NSNotification*) n
{
    self.currentPatient[@"lastname"] = n.object[@"text"];
}

- (void) viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    //[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    //[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

-(void)phoneFieldDidChange :(NSNotification*) n
{
    //your code
    self.currentPatient[@"phone"] = n.object[@"text"];
}

-(void) savePatient {
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
    
    if(self.currentPatient) {
        DBPPEditHeadTableViewCell *headCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        NSString *lastName = headCell.patientFirstNameTextField.text;
        if(lastName.length == 0)   {
            [UIAlertView showWithTitle:@"Ошибка" message:@"Введите Фамилию" cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
            return;
        }
        
        NSString *fullName = headCell.patientLastNameTextField.text;
        NSArray *nameArray = [fullName componentsSeparatedByString:@" "];
        if(nameArray.count < 1) {
            [UIAlertView showWithTitle:@"Ошибка" message:@"Введите Имя" cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
            return;
        }
        
        NSString *firstName = nameArray[0];
        
        NSString *middleName = @"";
        if(nameArray.count > 1) {
            NSMutableArray *fullNameArray = [[NSMutableArray alloc] initWithArray:nameArray copyItems:YES];
            [fullNameArray removeObjectAtIndex:0];
            middleName = [fullNameArray componentsJoinedByString:@" "];
        }
        
        self.currentPatient[@"firstname"] = firstName;
        self.currentPatient[@"middlename"] = middleName;
        self.currentPatient[@"lastname"] = lastName;
        self.currentPatient[@"rating"] = @(headCell.ratingView.value);
        self.currentPatient[@"photo"] = fileName;
        self.currentPatient[@"phone"] = self.phoneTextField.text;
        
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"dd.MM.yyyy"];
        self.currentPatient[@"createat"] = [df stringFromDate:[NSDate date]];
        self.currentPatient[@"birthdate"] = [df stringFromDate:self.birthDate];
        //[[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
        
        JGProgressHUD *HUD = [JGProgressHUD progressHUDWithStyle:JGProgressHUDStyleDark];
        HUD.textLabel.text = @"Сохранение";
        [HUD showInView:self.view];
        [[PatientManager shared] updatePatient:self.currentPatient success:^{
            [HUD dismissAnimated:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"needPopToList" object:nil];
            [self.navigationController popToRootViewControllerAnimated:YES];
        } failure:^{
            [HUD dismissAnimated:YES];
        }];
        
    }
    else
    {
        DBPPEditHeadTableViewCell *headCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        NSString *firstName = headCell.patientFirstNameTextField.text;
        if(firstName.length == 0)   {
            [UIAlertView showWithTitle:@"Ошибка" message:@"Введите Фамилию" cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
            return;
        }
        
        NSString *fullName = headCell.patientLastNameTextField.text;
        NSArray *nameArray = [fullName componentsSeparatedByString:@" "];
        if(nameArray.count < 1) {
            [UIAlertView showWithTitle:@"Ошибка" message:@"Введите Имя" cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
            return;
        }
        
        if(!self.birthDate)  {
            [UIAlertView showWithTitle:@"Ошибка" message:@"Заполните дату рождения" cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
            return;
        }
        
        NSString *middleName = nameArray[0];
        
        NSString *lastName = @"";
        if(nameArray.count > 1) {
            NSMutableArray *fullNameArray = [[NSMutableArray alloc] initWithArray:nameArray copyItems:YES];
            [fullNameArray removeObjectAtIndex:0];
            lastName = [fullNameArray componentsJoinedByString:@" "];
        }
        
        NSMutableDictionary *patient = [[NSMutableDictionary alloc] init];//[Patient MR_createEntity];
        //patient.remoteId = @([Patient maxRemoteId] + 1);
        patient[@"firstname"] = firstName;
        patient[@"middlename"] = middleName;
        patient[@"lastname"] = lastName;
        //self.currentPatient.[@"firstname"] = //@(self.birthDate.day);
        //self.currentPatient.[@"firstname"] = //@(self.birthDate.month);
        //self.currentPatient.[@"firstname"] = //@(self.birthDate.year);
        patient[@"rating"] = @(headCell.ratingView.value);
        patient[@"photo"] = fileName;
        patient[@"phone"] = self.phoneTextField.text;
        
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"dd.MM.yyyy"];
        self.currentPatient[@"createat"] = [df stringFromDate:[NSDate date]];
        patient[@"birthdate"] = [df stringFromDate:self.birthDate];
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
        
        JGProgressHUD *HUD = [JGProgressHUD progressHUDWithStyle:JGProgressHUDStyleDark];
        HUD.textLabel.text = @"Сохранение";
        [HUD showInView:self.view];
        [[PatientManager shared] createPatient:patient success:^{
            [HUD dismissAnimated:YES];
            //[[NSNotificationCenter defaultCenter] postNotificationName:@"needPopToList" object:nil];
            [self.navigationController popViewControllerAnimated:YES];
            
        } failure:^{
            [HUD dismissAnimated:YES];
        }];
    }
}

- (void) preSavePatient
{
    
}

#pragma mark - UITableView DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView   {
    return items.count+(self.creationMode?0:1);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section    {
    if(section==0)
        return (self.currentPatient) ? /*5*/4 : 4;
    else
        return ((NSArray*)self.currentPatient[@"additional_info"][items[section-1]]).count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section==0)
    {
        if(indexPath.row == 0)
            return 120.f;
        else if(indexPath.row == 3)
           return (((NSArray*)self.currentPatient[@"illness"]).count > 0)?70.0f:60.f;
        else return 60.0f;
        
    }
    else
    {
        if(indexPath.section-1!=3)
            return 60.f;
        else
            return 140.f;
    }
}

-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section>0)
    {
        if(section>0 && section-1==4)
            return 0;
        else
            return 60.0;
    }
    else
        return 0;
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(section>0 && section-1<=3)
    {
        UIView* uwv = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 60)];
        uwv.backgroundColor = [UIColor clearColor];
        UIView* uwv1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 60)];
        uwv1.backgroundColor = [UIColor whiteColor];
        [uwv addSubview:uwv1];
        uwv.tag = section;
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame=CGRectMake(0, 0, 44, 60);
        //UIButton* btn = [[UIButton alloc] initWithFrame:CGRectMake(5, 5, 34, 34)];
        btn.tag = section-1;
        [btn setImage:[PercUtils imageWithImage:[UIImage imageNamed:@"add.png"] scaledToSize:CGSizeMake(15, 15)] forState:UIControlStateNormal];
        [btn setImage:[PercUtils imageWithImage:[UIImage imageNamed:@"add.png"] scaledToSize:CGSizeMake(15, 15)] forState:UIControlStateSelected];
        [btn addTarget:self action:@selector(addSomething:) forControlEvents:UIControlEventTouchUpInside];
        UILabel *l = [[UILabel alloc] initWithFrame:CGRectMake(44, 12, 270, 34)];
        l.backgroundColor = [UIColor whiteColor];
        l.text= items_headers[section-1];
        l.font = [UIFont systemFontOfSize:12];
        l.textColor = [UIColor colorWithRed:74.0f/255.0f green:74.0f/255.0f blue:74.0f/255.0f alpha:1];
        
        UIButton *btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
        btn1.frame=CGRectMake(45, 0, 260, 44);
        [btn1 addTarget:self action:@selector(addSomething:) forControlEvents:UIControlEventTouchUpInside];
        btn1.tag = section-1;
        [uwv1 addSubview:btn];
        [uwv1 addSubview:btn1];
        [uwv1 addSubview:l];
        uwv1.userInteractionEnabled = YES;
        uwv.userInteractionEnabled = YES;
        CGRect sepFrame = CGRectMake(15, uwv1.frame.size.height-1, uwv1.frame.size.width-30, 1);
        UIView* seperatorView = [[UIView alloc] initWithFrame:sepFrame];
        seperatorView.backgroundColor = [UIColor colorWithRed:224.0f/255.0f green:223.0f/255.0f blue:224.0f/255.0f alpha:1];
        [uwv1 addSubview:seperatorView];
        //
        
        
        return uwv;
    }
    /*else if(section>0 && section-1==4)
    {
        UIView* uwv = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 60)];
        uwv.tag = section;
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame=CGRectMake(0, 0, 244, 60);
        //UIButton* btn = [[UIButton alloc] initWithFrame:CGRectMake(5, 5, 34, 34)];
        btn.tag = section-1;
        [btn setTitle:@"Добавить поле" forState:UIControlStateNormal];
        [btn setTitle:@"Добавить поле" forState:UIControlStateSelected];

        [btn addTarget:self action:@selector(addSomething:) forControlEvents:UIControlEventTouchUpInside];
        [btn setTitleColor:[UIColor colorWithRed:20.0f/255.0f green:162.0f/255.0f blue:243.0f/255.0f alpha:1] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor blueColor] forState:UIControlStateSelected];
        [btn setTitleEdgeInsets:UIEdgeInsetsMake(0, -70, 0, 0)];
        //UILabel *l = [[UILabel alloc] initWithFrame:CGRectMake(50, 5, 270, 34)];
        ////l.backgroundColor = [UIColor clearColor];
        //l.text= items_headers[section-1];
        [uwv addSubview:btn];
        //[uwv addSubview:l];
        btn.titleLabel.font = [UIFont systemFontOfSize:12];
        uwv.userInteractionEnabled = YES;
        CGRect sepFrame = CGRectMake(0, uwv.frame.size.height-1, uwv.frame.size.width, 1);
        UIView* seperatorView = [[UIView alloc] initWithFrame:sepFrame];
        seperatorView.backgroundColor = [UIColor colorWithRed:224.0f/255.0f green:223.0f/255.0f blue:224.0f/255.0f alpha:1];
        [uwv addSubview:seperatorView];
        return uwv;
    }*/
    else if(section-1>4)
    {
        UIView* uwv = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 60)];
        uwv.tag = section;
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame=CGRectMake(0, 0, 244, 60);
        btn.tag = section-1;
        [btn setTitle:@"Удалить пациента" forState:UIControlStateNormal];
        [btn setTitle:@"Удалить пациента" forState:UIControlStateSelected];
        [btn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor redColor] forState:UIControlStateSelected];
        btn.titleLabel.font = [UIFont systemFontOfSize:12];
        [btn addTarget:self action:@selector(removePatient:) forControlEvents:UIControlEventTouchUpInside];
        btn.tintColor = [UIColor redColor];
        [uwv addSubview:btn];
        //[uwv addSubview:l];
        [btn setTitleEdgeInsets:UIEdgeInsetsMake(0, -55, 0, 0)];
        uwv.userInteractionEnabled = YES;
        CGRect sepFrame = CGRectMake(15, uwv.frame.size.height-1, uwv.frame.size.width-30, 1);
        UIView* seperatorView = [[UIView alloc] initWithFrame:sepFrame];
        seperatorView.backgroundColor = [UIColor colorWithRed:224.0f/255.0f green:223.0f/255.0f blue:224.0f/255.0f alpha:1];
        [uwv addSubview:seperatorView];
        return uwv;
    }
    else
    {
        return nil;
    }
}



-(CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if(section>0)
    {
        if(section>0 && section-1==4)
        {
            return 60;
        }
        else
        {
            if(((NSArray*)self.currentPatient[@"additional_info"][items[section-1]]).count>0)
                return 1;
            else return 0;
        }
    }
    else
        return 1;
}

-(UIView *) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if(section>0 && section-1==4)
    {
        UIView* uwv = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 60)];
        uwv.tag = section;
        uwv.backgroundColor = [UIColor whiteColor];
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame=CGRectMake(0, 0, 244, 60);
        //UIButton* btn = [[UIButton alloc] initWithFrame:CGRectMake(5, 5, 34, 34)];
        btn.tag = section-1;
        [btn setTitle:@"Добавить поле" forState:UIControlStateNormal];
        [btn setTitle:@"Добавить поле" forState:UIControlStateSelected];
        
        [btn addTarget:self action:@selector(addSomething:) forControlEvents:UIControlEventTouchUpInside];
        [btn setTitleColor:[UIColor colorWithRed:20.0f/255.0f green:162.0f/255.0f blue:243.0f/255.0f alpha:1] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor blueColor] forState:UIControlStateSelected];
        [btn setTitleEdgeInsets:UIEdgeInsetsMake(0, -70, 0, 0)];
        //UILabel *l = [[UILabel alloc] initWithFrame:CGRectMake(50, 5, 270, 34)];
        ////l.backgroundColor = [UIColor clearColor];
        //l.text= items_headers[section-1];
        [uwv addSubview:btn];
        //[uwv addSubview:l];
        btn.titleLabel.font = [UIFont systemFontOfSize:12];
        uwv.userInteractionEnabled = YES;
        CGRect sepFrame = CGRectMake(15, -1, uwv.frame.size.width-30, 1);
        UIView* seperatorView = [[UIView alloc] initWithFrame:sepFrame];
        seperatorView.backgroundColor = [UIColor colorWithRed:224.0f/255.0f green:223.0f/255.0f blue:224.0f/255.0f alpha:1];
        [uwv addSubview:seperatorView];
        
        CGRect sepFrame1 = CGRectMake(15, uwv.frame.size.height-1, uwv.frame.size.width-30, 1);
        UIView* seperatorView1 = [[UIView alloc] initWithFrame:sepFrame1];
        seperatorView1.backgroundColor = [UIColor colorWithRed:224.0f/255.0f green:223.0f/255.0f blue:224.0f/255.0f alpha:1];
        [uwv addSubview:seperatorView1];
        return uwv;
    }
    else
    {
        UIView* seperatorView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 1)];
        seperatorView.backgroundColor = [UIColor clearColor];
        CGRect sepFrame1 = CGRectMake(15, seperatorView.frame.size.height-1, seperatorView.frame.size.width-30, 1);
        UIView* seperatorView1 = [[UIView alloc] initWithFrame:sepFrame1];
        seperatorView1.backgroundColor = [UIColor colorWithRed:224.0f/255.0f green:223.0f/255.0f blue:224.0f/255.0f alpha:1];
        [seperatorView addSubview:seperatorView1];
        return seperatorView;
    }
}

- (void)addSomething:(id) sender
{
    NSInteger section = ((UIButton*) sender).tag;
    NSArray* texts = [[NSArray alloc] init];
    switch(section)
    {
        case 0: texts=@[@"Сотовый телефон",@"Домашний телефон",@"Рабочий телефон"]; break;
        case 1: texts=@[@"Личный e-mail",@"Рабочий e-mail"]; break;
        case 2: texts=@[@"Основная",@"Второстепенная"]; break;
        case 3: texts=@[@"Домашний адрес",@"Рабочий адрес"]; break;
        case 4: texts=@[@"Поле1",@"Поле2",@"Поле3"]; break;
    }
    int already_rows = [self.tableView numberOfRowsInSection:section+1]>=texts.count?[self.tableView numberOfRowsInSection:section+1]%texts.count:[self.tableView numberOfRowsInSection:section+1];
    
    if(section!=3)
    {
        BOOL all_filled = true;
        for(NSDictionary* checker in self.currentPatient[@"additional_info"][items[section]])
        {
            if([(NSString*)checker[@"value"] length]==0)
            {
                all_filled = NO;
            }
        }
        if(all_filled)
        {
            [self.currentPatient[@"additional_info"][items[section]] addObject:@{@"id":GUIDString(), @"field_name":texts[already_rows], @"category":items[section], @"value":@""}];
        }
        else
        {
            [self.view makeToast:@"Заполните значение"];
        }
    }
    else
        [self.currentPatient[@"additional_info"][items[section]] addObject:@{@"id":GUIDString(), @"field_name":texts[already_rows], @"category":items[section], @"value":@{@"city":@"", @"street":@"", @"house":@"", @"flat":@""}}];

    
    [self.tableView reloadData];
    NSIndexPath *np;
    
    np=[NSIndexPath indexPathForRow:[self.tableView numberOfRowsInSection:section+1]-1 inSection:section+1];
    [self.tableView scrollToRowAtIndexPath:np atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}


- (void)removePatient:(id) sender
{
    [UIAlertView showWithTitle:@"" message:@"Данные пациента будут потеряны безвозвратно. Вы уверены что хотите удалить пациента?"
             cancelButtonTitle:@"Отмена"
             otherButtonTitles:@[@"Да"] tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex)   {
                 if(buttonIndex == 1)   {
                     [[PatientManager shared] destroyPatient:self.currentPatient success:^{
                         [self.navigationController popToRootViewControllerAnimated:YES];
                     } failure:^{
                         //
                     }];
                 }
             }];
}

#pragma mark - UITableView Delegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    if(indexPath.section==0)
    {
        if(indexPath.row == 0)
        {
            DBPPEditHeadTableViewCell *headCell = [tableView dequeueReusableCellWithIdentifier:@"DBPPEditHeadTableViewCell"];
            
            if(self.currentPatient != nil)  {
                NSString *fullName = [NSString stringWithFormat:@"%@ %@", self.currentPatient[@"firstname"], self.currentPatient[@"middlename"]];
                headCell.patientFirstNameTextField.text = self.currentPatient[@"lastname"];
                headCell.patientFirstNameTextField.tag=100500;
                headCell.patientLastNameTextField.text = [fullName length]>1?fullName:nil;
                headCell.patientLastNameTextField.tag=100501;
                headCell.ratingView.value = [self.currentPatient[@"rating"] integerValue];
                headCell.discountLabel.text = [NSString stringWithFormat:@"%@%%", self.currentPatient[@"discount"]];
                
                headCell.patientLogoImageView.layer.cornerRadius = (headCell.patientLogoImageView.frame.size.width/2);
                UIImage *image = [UIImage imageNamed:@"patient-placeholder-logo"];
                headCell.patientLogoImageView.imageModel.placeholderImage = image;
                headCell.patientLogoImageView.imageModel.failureImage = image;
                headCell.patientLogoImageView.imageModel.cancelImage = image;
                headCell.patientLogoImageView.imageModel.cornerRadius = (headCell.patientLogoImageView.frame.size.width/2);
                headCell.patientLogoImageView.imageModel.imageCachePolicy = ImageCachePolicyMemoryAndFileCache;
                headCell.patientLogoImageView.imageModel.queuePriority = NSOperationQueuePriorityHigh;
                [headCell.patientLogoImageView setImageWithURLString:[NSString stringWithFormat:[DataManager urlForPath:@"patient/%@/image"], self.currentPatient[@"patientid"]]
                                                             success:^(ImageRequestModel *object, UIImage *image) {
                                                                 //
                                                             } failure:^(ImageRequestModel *object, NSError *error) {
                                                                 //
                                                             } progress:^(ImageRequestModel *object, CGFloat progress) {
                                                                 //
                                                             }];
            }
            
            headCell.didClickAddPhotoToPatient = ^() {
                [self.view endEditing:YES];
                self.photoActionSheet = [[IBActionSheet alloc] initWithTitle:NSLocalizedString(@"Сделать фотографию", nil)
                                                                    delegate:self
                                                           cancelButtonTitle:NSLocalizedString(@"Отмена", nil)
                                                      destructiveButtonTitle:nil
                                                      otherButtonTitlesArray:@[@"Камера", NSLocalizedString(@"Выбрать из галереи", nil)]];
                self.photoActionSheet.tag = 1;
                [self.photoActionSheet showInView:self.view];
            };
            headCell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            cell = headCell;
        }
        else if(indexPath.row == 1)
        {
            UITableViewCell *titleCell = [tableView dequeueReusableCellWithIdentifier:@"titleCell2"];
            if (titleCell == nil) {
                titleCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"titleCell2"];
                titleCell.accessoryType = UITableViewCellAccessoryNone;
            }
            
            titleCell.textLabel.text = @"День рождения";
            titleCell.textLabel.font = [UIFont systemFontOfSize:12];
            titleCell.textLabel.textColor = [UIColor colorWithRed:74.0f/255.0f green:74.0f/255.0f blue:74.0f/255.0f alpha:1];
            titleCell.separatorInset = UIEdgeInsetsMake(0, 15, 0, 15);
            
            
            if(self.currentPatient[@"birthdate"])  {
                NSDateFormatter *df = [[NSDateFormatter alloc] init];
                [df setDateFormat:@"dd.MM.yyyy"];
                NSString *dateStr = self.currentPatient[@"birthdate"];
                UILabel* lab = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 21)];
                lab.textAlignment = NSTextAlignmentRight;
                lab.font = [UIFont systemFontOfSize:12];
                titleCell.accessoryView = lab;
                lab.text = dateStr;
                lab.textColor = [UIColor colorWithRed:74.0f/255.0f green:74.0f/255.0f blue:74.0f/255.0f alpha:1];
                //titleCell.detailTextLabel.text = dateStr;
            }
            
            [[titleCell viewWithTag:3] removeFromSuperview];
            
            cell = titleCell;
        }
        else if(indexPath.row == 2)
        {
            EditPhoneTableViewCell *titleCell = [tableView dequeueReusableCellWithIdentifier:@"editPhoneCell"];
            if (titleCell == nil) {
                titleCell = [[EditPhoneTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"editPhoneCell"];
                titleCell.accessoryType = UITableViewCellAccessoryNone;
            }
            
            
            self.phoneTextField = titleCell.phoneTextField;
            self.phoneTextField.tag=100502;
            [self.phoneTextField setKeyboardType:UIKeyboardTypePhonePad];
            
            if([(NSString*)self.currentPatient[@"phone"] length] > 0)
            {
                self.phoneTextField.text = self.currentPatient[@"phone"];
            }
            titleCell.selectionStyle = UITableViewCellSelectionStyleDefault;
            
            
            cell = titleCell;
        }
        else  if(indexPath.row == 3)
        {
            UITableViewCell *titleCell = [tableView dequeueReusableCellWithIdentifier:@"titleCell1"];
            if (titleCell == nil) {
                titleCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"titleCell1"];
                titleCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            }
            
            titleCell.textLabel.text = @"Заболевания";
            titleCell.textLabel.font = [UIFont systemFontOfSize:12];
            titleCell.textLabel.textColor = [UIColor colorWithRed:74.0f/255.0f green:74.0f/255.0f blue:74.0f/255.0f alpha:1];
            [[titleCell viewWithTag:3] removeFromSuperview];
            
            titleCell.detailTextLabel.text = self.illnessTitle;
            titleCell.detailTextLabel.numberOfLines = 0;
            titleCell.detailTextLabel.textColor = [UIColor redColor];
            titleCell.detailTextLabel.font = [UIFont systemFontOfSize:12];
            
            cell = titleCell;
        }
    }
    else
    {
        

        if((indexPath.section-1)<3)
        {
            CustomCell *titleCell = [tableView dequeueReusableCellWithIdentifier:@"customCell"];
            switch(indexPath.section-1)
            {
                case 0:
                {
                    titleCell.texts=@[@"Сотовый",@"Домашний",@"Рабочий"];
                    //titleCell.typeButton. =
                    [titleCell.valueField setKeyboardType:UIKeyboardTypePhonePad];
                    break;
                }
                case 1:
                {
                    [titleCell.valueField setKeyboardType:UIKeyboardTypeEmailAddress];
                    titleCell.valueField.autocapitalizationType = UITextAutocapitalizationTypeNone;
                    titleCell.texts=@[@"Личный",@"Рабочий"]; break;
                }
                case 2:
                {
                    [titleCell.valueField setKeyboardType:UIKeyboardTypeDefault];
                    titleCell.valueField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
                    titleCell.texts=@[@"Основная",@"Второстепенная"];
                    break;
                }
            }

            NSDictionary* res = self.currentPatient[@"additional_info"][items[indexPath.section-1]][indexPath.row];
            
            [titleCell.typeButton setTitle:res[@"field_name"] forState:UIControlStateNormal];
            [titleCell.typeButton setTitle:res[@"field_name"] forState:UIControlStateSelected];
            
            
            titleCell.valueField.text = res[@"value"];
            titleCell.type = items[indexPath.section-1];
            titleCell.idi = res[@"id"];
            titleCell.field_name =res[@"field_name"];
            
            cell = titleCell;
        }
        else if((indexPath.section-1)==3)
        {
            AddressTableViewCell *titleCell = [tableView dequeueReusableCellWithIdentifier:@"addressCell"];
            
            NSDictionary* res = self.currentPatient[@"additional_info"][items[indexPath.section-1]][indexPath.row];
            
            titleCell.city_field.text = res[@"value"][@"city"];
            titleCell.streetField.text = res[@"value"][@"street"];
            titleCell.houseField.text = res[@"value"][@"house"];
            titleCell.flatField.text = res[@"value"][@"flat"];
            titleCell.city_field.autocapitalizationType = UITextAutocapitalizationTypeSentences;
            titleCell.streetField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
            titleCell.houseField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
            titleCell.flatField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
            titleCell.type = items[indexPath.section-1];
            titleCell.idi = res[@"id"];
            titleCell.field_name =res[@"field_name"];
            titleCell.texts=@[@"Домашний",@"Рабочий"];
            
            
            
            cell = titleCell;
            
        }
        else if((indexPath.section-1)==4)
        {
            
            CustomCustomCell *titleCell = [tableView dequeueReusableCellWithIdentifier:@"customCustomCell"];
            NSDictionary* res = self.currentPatient[@"additional_info"][items[indexPath.section-1]][indexPath.row];
            
            titleCell.valueField.text = res[@"value"];
            titleCell.type = items[indexPath.section-1];
            titleCell.idi = res[@"id"];
            titleCell.field_name =res[@"field_name"];
            
            cell = titleCell;
        }
        
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(indexPath.section==0)
    {
        if(indexPath.row == 1)  {
            [self.view endEditing:YES];
            self.birthActionSheet = [[WLPActionSheet alloc] initWithTitle:@"Дата рождения" cancelButtonTitle:@"ОК" destructiveButtonTitle:nil withViewController:self.birthDateController];
            self.birthActionSheet.tag = 1;
            self.birthActionSheet.delegate = self;
            [self.birthActionSheet showInView:self.view];
        }
        else if(indexPath.row == 2)
        {
            [self.phoneTextField becomeFirstResponder];
        }
        else if(indexPath.row == 3)
        {
            IllnessListViewController *illnessListVC = [[IllnessListViewController alloc] initWithPatient:self.currentPatient isReadOnly:NO];
            [self.navigationController pushViewController:illnessListVC animated:YES];
        }
    }
}


- (void) showDiscountMenu:(NSNotification*) n
{
    
    CustomIOS7AlertView *alertView = [CustomIOS7AlertView alertWithTitle:@"Выберите тип" message:nil andOwnr: n.object[@"sender"]];
    [alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"25%",@"20%",@"15%",@"10%",@"5%",@"0%", nil]];

    [alertView setDelegate:self];
    [alertView setOnButtonTouchUpInside:^(CustomIOS7AlertView *alertView, int buttonIndex) {
        NSLog(@"Block: Button at position %d is clicked on alertView %d.", buttonIndex, [alertView tag]);
        [alertView close];
    }];
    [alertView show];
    
    //[PopoverView showPopoverAtPoint:/*n.object[@"point"]*/ CGPointMake(10, 10)  inView:self.view withTitle:@"October 2012" withContentView:[[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 300)] delegate:self];
    
}


- (void)customIOS7dialogButtonTouchUpInside: (CustomIOS7AlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    NSLog(@"Button at position %d is clicked on alertView %d.", buttonIndex, [alertView tag]);
    self.currentPatient[@"discount"] = [NSNumber numberWithInteger:5*(5-buttonIndex)];
    [self.tableView reloadData];
}



-(void)actionSheetDidDone:(WLPActionSheet*)actionSheet  {
    if(actionSheet.tag == 1)    {
        self.birthDate = self.birthDateController.datePicker.date;
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"dd.MM.yyyy"];

        self.currentPatient[@"birthdate"] = [dateFormat stringFromDate:self.birthDateController.datePicker.date];
        [self.tableView reloadData];
        
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView  {
    [self.view endEditing:YES];
}

- (void)actionSheet:(IBActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(actionSheet.tag == 1)    {
        if(buttonIndex == 0){
            
            if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                
                [UIAlertView showWithTitle:@"Error" message:@"Нет камеры" cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
                
                return;
            }
            
            if (YES) {
                [self showImagePickerUsingSourceType:UIImagePickerControllerSourceTypeCamera];
            }else{
                [UIAlertView showWithTitle:@"Error" message:@"This app does not have access to your photos or videos" cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
            }
            
        } else if(buttonIndex == 1) {
            
            if (YES) {
                [self showImagePickerUsingSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
            }else{
                [UIAlertView showWithTitle:@"Error" message:@"This app does not have access to your photos or videos" cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
            }
        }
    }
}

- (void)showImagePickerUsingSourceType:(UIImagePickerControllerSourceType)type{
    
    UIImagePickerController *imagePickerController = [UIImagePickerController new];
    imagePickerController.allowsEditing = YES;
    imagePickerController.delegate = self;
    imagePickerController.sourceType = type;
    
    [self presentViewController:imagePickerController animated:YES completion:^{
        
    }];
}




- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    NSString* checker = ([[[navigationController.viewControllers lastObject] class] description]);
    
    if ([checker isEqualToString:@"CAMImagePickerCameraViewController"]||[checker isEqualToString:@"PUUIImageViewController"])
    {
        CGFloat screenHeight = [[UIScreen mainScreen] bounds].size.height;
        
        plCropOverlay = [[[viewController.view.subviews objectAtIndex:1]subviews] objectAtIndex:0];
        
        plCropOverlay.hidden = NO;
        
        int position = 0;
        if([checker isEqualToString:@"CAMImagePickerCameraViewController"])
        {
            if (screenHeight > 568)
            {
                position = 118;
            }
            else if (screenHeight == 568)
            {
                position = 98;
            }
            else
            {
                position = 80;
            }
        }
        else
        {
            if (screenHeight > 568)
            {
                position = 145;
            }
            else if (screenHeight == 568)
            {
                position = 125;
            }
            else
            {
                position = 120;
            }
        }
        
        CAShapeLayer *circleLayer = [CAShapeLayer layer];
        
        UIBezierPath *path2 = [UIBezierPath bezierPathWithOvalInRect:
                               CGRectMake(0.0f, position, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.width)];
        [path2 setUsesEvenOddFillRule:YES];
        
        [circleLayer setPath:[path2 CGPath]];
        
        [circleLayer setFillColor:[[UIColor clearColor] CGColor]];
        UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, screenHeight-70) cornerRadius:0];
        
        [path appendPath:path2];
        [path setUsesEvenOddFillRule:YES];
        
        CAShapeLayer *fillLayer = [CAShapeLayer layer];
        fillLayer.path = path.CGPath;
        fillLayer.fillRule = kCAFillRuleEvenOdd;
        fillLayer.fillColor = [UIColor blackColor].CGColor;
        fillLayer.opacity = 0.8;
        [viewController.view.layer addSublayer:fillLayer];
        
        /*UILabel *moveLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 10, 320, 50)];
         [moveLabel setText:@"Move and Scale"];
         [moveLabel setTextAlignment:NSTextAlignmentCenter];
         [moveLabel setTextColor:[UIColor whiteColor]];
         
         [viewController.view addSubview:moveLabel];*/
    }
}


-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = info[UIImagePickerControllerMediaType];
    
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]){
        UIImage *photo = info[UIImagePickerControllerEditedImage];
        
        [[PatientManager shared] uploadLogo:photo
                                  toPatient:self.currentPatient
                                 completion:^(NSError *error) {
                                     //
                                 }];
        
        [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(NSString*) genStr {
    NSString *alphabet  = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXZY0123456789";
    NSMutableString *s = [NSMutableString stringWithCapacity:10];
    for (NSUInteger i = 0U; i < 10; i++) {
        u_int32_t r = arc4random() % [alphabet length];
        unichar c = [alphabet characterAtIndex:r];
        [s appendFormat:@"%C", c];
    }
    return s;
}

- (void)saveImage: (UIImage*)image withFileName:(NSString*)fileName
{
    if (image != nil)
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                             NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:
                          [NSString stringWithFormat: @"%@.png", fileName]];
        NSData* data = UIImagePNGRepresentation(image);
        [data writeToFile:path atomically:YES];
    }
}

- (UIImage*)loadImageFileName:(NSString*)fileNameX
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:
                      [NSString stringWithFormat: @"%@.png", fileNameX] ];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    return image;
}


- (CGFloat) AACStatusBarHeight
{
    CGSize statusBarSize = [[UIApplication sharedApplication] statusBarFrame].size;
    return MIN(statusBarSize.width, statusBarSize.height);
}

- (void)keyboardWillShow:(NSNotification *)notification
{

    BOOL needed = true;
    UIView* fr=[self.view findFirstResponder];
    if([fr isKindOfClass:[UITextField class]])
    {
        UITextField* tmp=(UITextField*) fr;
        if(tmp.tag>=100500)
        {
            needed=false;
        }
    }
    
    if(needed)
    {
        CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        
        [UIView animateWithDuration:0.3 animations:^{
            CGRect f = self.view.frame;
            f.origin.y = -keyboardSize.height;//+self.navigationController.navigationBar.frame.size.height+[self AACStatusBarHeight];
            self.view.frame = f;
        }];
    }
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    /*KeyboardListener* kl = [KeyboardListener sharedInstance];
    if(kl.isVisible)
    {*/
    BOOL needed = true;
    UIView* fr=[self.view findFirstResponder];
    if([fr isKindOfClass:[UITextField class]])
    {
        UITextField* tmp=(UITextField*) fr;
        if(tmp.tag>=100500)
        {
            needed=false;
        }
    }
    
    if(needed)
    {
        [UIView animateWithDuration:0.3 animations:^{
            CGRect f = self.view.frame;
            f.origin.y = 0.0f;//+self.navigationController.navigationBar.frame.size.height+[self AACStatusBarHeight];
            self.view.frame = f;
        }];
    }
        //kl.visible = NO;
    /*}*/
}

@end
