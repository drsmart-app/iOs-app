//
//  DBPatienEditViewController.h
//  DentaBook
//
//  Created by Denis Dubov on 05.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomIOS7AlertView.h"
#import "PopoverView.h"
@class Patient;

@interface DBPatienEditViewController : UIViewController <CustomIOS7AlertViewDelegate, UITextFieldDelegate, PopoverViewDelegate, UINavigationControllerDelegate>

-(id) initWithPatient:(NSDictionary*)patient;
@property (nonatomic, retain) NSMutableArray* custom_fields;

@end
