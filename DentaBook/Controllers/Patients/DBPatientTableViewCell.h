//
//  DBPatientTableViewCell.h
//  DentaBook
//
//  Created by Denis Dubov on 04.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DBPatientTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *favoriteImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *illnessImageView;
@property (weak, nonatomic) IBOutlet UIImageView *debtorImageView;
@property (weak, nonatomic) IBOutlet UILabel *starsLabel;
@property (weak, nonatomic) IBOutlet UIView *bothView;
@property (weak, nonatomic) IBOutlet UIView *debtView;
@property (weak, nonatomic) IBOutlet UIView *illnessView;

@end
