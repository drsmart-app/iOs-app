//
//  DBPatientsViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 04.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "DBPatientsViewController.h"

#import <MagicalRecord/MagicalRecord.h>

#import "AppDelegate.h"
#import "DBPatientTableViewCell.h"
#import "DBPatientProfileViewController.h"
#import "DBPatienEditViewController.h"
#import "DataBase.h"

#import "Patient+Custom.h"
#import "HttpUtil.h"

@interface DBPatientsViewController () <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, UISearchBarDelegate>
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIFlatSearchBar *searchBarView;
@property (nonatomic, strong) NSString *searchString;
@end

@implementation DBPatientsViewController
@synthesize patients,sections, sortSelector, suggestView, magnifyerImg, searchPlaceholder, slideView;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Пациенты";
    
    self.searchString = @"";
    
    UIImage *image = [UIImage imageNamed:@"patient-add-icon"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(addPatient)forControlEvents:UIControlEventTouchUpInside];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, 18, 18);
    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = barButton;
    
    //self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Add" style:UIBarButtonItemStylePlain target:self action:@selector(addPatient)];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"DBPatientTableViewCell" bundle:nil] forCellReuseIdentifier:@"DBPatientTableViewCell"];
    [sortSelector setSelectedSegmentIndex:1];
    [self fetchData];
    
    
    for (UIView *subview in [[self.searchBarView.subviews lastObject] subviews]) {
        if ([subview isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
            [subview removeFromSuperview];
            break;
        }
    }
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.searchBarView.layer.cornerRadius=10.0f;
    self.searchBarView.layer.masksToBounds=YES;
    self.searchBarView.layer.borderColor=[[UIColor colorWithRed:224.0f/255.0f green:223.0f/255.0f blue:224.0f/255.0f alpha:1] CGColor];
    self.searchBarView.layer.borderWidth= 1.0f;
    self.searchBarView.delegate = self;
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();

        
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = getColorSchemeStatus();
    }
    
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setDefaultTextAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithWhite:74.0f/255.0f alpha:1]}];
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setFont:[UIFont systemFontOfSize:12]];
    self.extendedLayoutIncludesOpaqueBars = YES;
    
    //self.navigationController.navigationBar.translucent = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    /*NSString* less5=@"Поиск                                                                            ";
    NSString* six=@"Поиск                                                                                                           ";
    NSString* sixplus=@"Поиск                                                                                                                               ";
    NSLog(@"self.bounds: %f", self.view.bounds.size.width);
    self.searchBarView.placeholder = self.view.bounds.size.width<=320?less5:(self.view.bounds.size.width<=375?six:sixplus);*/
    self.searchBarView.placeholder = @"";
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    //magnifyerImg.hidden = YES;
    searchPlaceholder.hidden = YES;
    
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void) viewWillAppear:(BOOL)animated
{
    [self fetchData];
    [self.tableView reloadData];
    
    if(sections.count>0)
    {
        self.tableView.hidden = NO;
        self.suggestView.hidden = YES;
    }
    else
    {
        self.tableView.hidden = YES;
        self.suggestView.hidden = NO;
    }
}

-(void) addPatient  {
    DBPatienEditViewController *patientEditViewController = [[DBPatienEditViewController alloc] initWithPatient:nil];
    [self.navigationController pushViewController:patientEditViewController animated:YES];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    self.searchString = searchText;
    
    [self fetchData];
    [self.tableView reloadData];
    
    if(self.searchString.length == 0)   {
        [self.searchBarView performSelector:@selector(resignFirstResponder) withObject:nil afterDelay:0.1];
        [self.view endEditing:YES];
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar   {
    if(self.searchString.length > 0)    {
        [self fetchData];
        [self.tableView reloadData];
    }
    [self.view endEditing:YES];
}

- (NSString*) getDate:(NSString*) patientid
{
    NSString* result = @"Не назначено";
    
    NSArray *newArray=[[DataBase getDBArray:@"events" withKey:@"patientid" andValue:patientid] arrayByAddingObjectsFromArray:[DataBase getDBArray:@"visits" withKey:@"patientid" andValue:patientid]];

    NSArray *events = [[NSSet setWithArray:newArray] allObjects];
    
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"dd.MM.yyyy"];
    NSDate* lastVisitDate = [dateformate dateFromString:@"21.12.1970"];
    BOOL found=false;
    int lastIndexPath = 0;
    int i=0;
    for(NSDictionary* d in events)
    {
        if([[dateformate dateFromString:d[@"startdate"]] timeIntervalSince1970]>[lastVisitDate timeIntervalSince1970]/*&&[[dateformate dateFromString:d[@"startdate"]] timeIntervalSince1970]>=[[NSDate date] timeIntervalSince1970]*/)
        {
            lastVisitDate = [dateformate dateFromString:d[@"startdate"]];
            result = d[@"startdate"];
            lastIndexPath = i;
            found = true;
        }
        i++;
    }
    
    return result;
}

-(void) fetchData   {
    NSArray* tmp1 = [DataBase getDBArray:@"patient" withKey:@"" andValue:@""];
    
    NSMutableArray* tmp = [[NSMutableArray alloc] init];
    
    for(NSDictionary* dic in tmp1)
    {
        NSMutableDictionary* ddd = [NSMutableDictionary dictionaryWithDictionary:dic];
        ddd[@"nextvisit"] = [self getDate:dic[@"patientid"]];
        [tmp addObject:ddd];
    }
    
    NSMutableArray* result = [[NSMutableArray alloc] init];
    NSMutableArray* tmp_sections = [[NSMutableArray alloc] init];
    
    for(NSDictionary* pat in tmp)
    {
        if([self.searchString length]>0)
        {
            if([[NSString stringWithFormat:@"%@ %@ %@",pat[@"lastname"],pat[@"firstname"],pat[@"middlename"]] rangeOfString:self.searchString options:NSCaseInsensitiveSearch].location!=NSNotFound)
            {
                NSMutableDictionary* nmd = [NSMutableDictionary dictionaryWithDictionary:pat];

                if([pat[@"lastname"] length]>0)
                {
                    if(sortSelector.selectedSegmentIndex==1)
                    {
                        [tmp_sections addObject:[[(NSString*)pat[@"lastname"] substringWithRange:NSMakeRange(0, 1)] uppercaseString]];
                    }
                    else
                    {
                        [tmp_sections addObject:(NSString*)pat[@"nextvisit"]];
                    }
                    [tmp_sections addObject:[[(NSString*)pat[@"lastname"] substringWithRange:NSMakeRange(0, 1)] uppercaseString]];
                }
                else
                {
                    if(sortSelector.selectedSegmentIndex==1)
                    {
                        [tmp_sections addObject:[[(NSString*)pat[@"firstname"] substringWithRange:NSMakeRange(0, 1)] uppercaseString]];
                    }
                    else
                    {
                        [tmp_sections addObject:(NSString*)pat[@"nextvisit"]];
                    }
                    [tmp_sections addObject:[[(NSString*)pat[@"firstname"] substringWithRange:NSMakeRange(0, 1)] uppercaseString]];
                }
                [result addObject:nmd];
            }
        }
        else
        {
            NSMutableDictionary* nmd = [NSMutableDictionary dictionaryWithDictionary:pat];
            //NSArray* dd=[DataBase getDBArray:@"patient_illness" withKey:@"patientid" andValue:pat[@"patientid"]];
            //nmd[@"illness"] = dd;
            if([pat[@"lastname"] length]>0)
            {
                if(sortSelector.selectedSegmentIndex==1)
                {
                    //nmd[@"index"] = [[(NSString*)pat[@"firstname"] substringWithRange:NSMakeRange(0, 1)] uppercaseString];
                    [tmp_sections addObject:[[(NSString*)pat[@"lastname"] substringWithRange:NSMakeRange(0, 1)] uppercaseString]];
                }
                else
                {
                    //nmd[@"index"] = (NSString*)pat[@"birthdate"];
                    NSString* tst=(NSString*)pat[@"nextvisit"]==nil?@"":(NSString*)pat[@"nextvisit"];
                    [tmp_sections addObject:tst];
                }
            }
            else
            {
                if(sortSelector.selectedSegmentIndex==1)
                {
                    //nmd[@"index"] = [[(NSString*)pat[@"firstname"] substringWithRange:NSMakeRange(0, 1)] uppercaseString];
                    [tmp_sections addObject:[[(NSString*)pat[@"firstname"] substringWithRange:NSMakeRange(0, 1)] uppercaseString]];
                }
                else
                {
                    //nmd[@"index"] = (NSString*)pat[@"birthdate"];
                    NSString* tst=(NSString*)pat[@"nextvisit"]==nil?@"":(NSString*)pat[@"nextvisit"];
                    [tmp_sections addObject:tst];
                }
            }
            [result addObject:nmd];
        }
    }
    
    tmp_sections = [NSMutableArray arrayWithArray:[tmp_sections valueForKeyPath:@"@distinctUnionOfObjects.self"]];
    
    if(sortSelector.selectedSegmentIndex==0)
    {
        [tmp_sections sortUsingComparator:^NSComparisonResult(id obj1, id obj2)
        {
            if([obj1 isEqualToString:@"Не назначено"] && ![obj2 isEqualToString:@"Не назначено"])
                return NSOrderedDescending;
             NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
             [dateformate setDateFormat:@"dd.MM.yyyy"];
            return [[dateformate dateFromString:obj2] compare:[dateformate dateFromString:obj1]];
        }];
    }

    sections = [[NSMutableArray alloc] init];
    for(NSString* hd in tmp_sections)
    {
        NSMutableDictionary* ndms = [[NSMutableDictionary alloc] init];
        ndms[@"section"] = hd;
        NSMutableArray* mna=[[NSMutableArray alloc] init];
        for(NSDictionary* dict in result)
        {
            if(sortSelector.selectedSegmentIndex==1)
            {
                if([((NSString*)dict[@"lastname"]) length]>0)
                {
                    if([[(NSString*)dict[@"lastname"] uppercaseString] hasPrefix:[hd uppercaseString]])
                    {
                        [mna addObject:dict];
                    }
                }
                else
                {
                    if([[(NSString*)dict[@"firstname"] uppercaseString] hasPrefix:[hd uppercaseString]])
                    {
                        [mna addObject:dict];
                    }
                }
            }
            else
            {
                NSString* tst=(NSString*)dict[@"nextvisit"]==nil?@"":(NSString*)dict[@"nextvisit"];
                if([[tst uppercaseString] isEqualToString:[hd uppercaseString]])
                {
                    [mna addObject:dict];
                }
            }
        }
        ndms[@"content"]=mna;
        [sections addObject:ndms];
    }
    
    if(sortSelector.selectedSegmentIndex==1)
    {
    [sections sortUsingDescriptors:
     @[
       [NSSortDescriptor sortDescriptorWithKey:@"section" ascending:YES]
       ]];
    }
    
    if(tmp.count==0)
        suggestView.hidden = NO;
    else
        suggestView.hidden = YES;
    
    patients = result;
}


#pragma mark - UITableView DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return sections.count;//[[self.fetchedResultsController sections] count];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    //return [sectionInfo numberOfObjects];
    return ((NSArray*)sections[section][@"content"]).count;
}

/*- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    //id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    //return [sectionInfo name];
    return sections[section][@"section"];
}*/
-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 28;
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView* uwv = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 28)];
    uwv.backgroundColor = [UIColor clearColor];
    UIView* uwv1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 28)];
    uwv1.backgroundColor = [UIColor colorWithWhite:255.0f/255.0f alpha:1.0];
    [uwv addSubview:uwv1];
    UILabel *l = [[UILabel alloc] initWithFrame:CGRectMake(12, 6, 270, 16)];
    l.backgroundColor = [UIColor whiteColor];
    l.text= sections[section][@"section"];
    l.font = [UIFont boldSystemFontOfSize:14];
    l.textColor = [UIColor colorWithRed:74.0f/255.0f green:74.0f/255.0f blue:74.0f/255.0f alpha:1];
    [uwv addSubview:l];
        //
    return uwv;
}

#pragma mark - UITableView Delegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DBPatientTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DBPatientTableViewCell"];

    NSDictionary *patient = sections[indexPath.section][@"content"][indexPath.row];//[self.fetchedResultsController objectAtIndexPath:indexPath];
    
    NSString *fullNameString = [NSString stringWithFormat:@"%@ %@ %@", patient[@"lastname"], patient[@"firstname"], patient[@"middlename"]];
    cell.favoriteImageView.hidden = ([patient[@"rating"] integerValue] == 0);
    cell.starsLabel.hidden = ([patient[@"rating"] integerValue] == 0);
    cell.starsLabel.text = [NSString stringWithFormat:@"%@",patient[@"rating"]];
    cell.nameLabel.text = fullNameString;

    
    //cell.debtorImageView.hidden = ([patient allDebt] == 0);
    NSArray* costs = [DataBase getDBArray:@"visits" withKey:@"patientid" andValue:patient[@"patientid"]];
    float allCredit=0;
    for(NSDictionary* dc in costs)
    {
        float discount = [dc[@"discount"] floatValue];
        for(NSDictionary* it in dc[@"items"])
        {
            float cost = [it[@"cost"] floatValue]*[it[@"countt"] floatValue];
            allCredit+=cost*(100.0f-discount)/100.0f;
        }
    }
    
    float allDebit=0;
    NSArray* payments = [DataBase getDBArray:@"payment" withKey:@"patientid" andValue:patient[@"patientid"]];
    for(NSDictionary* dc in payments)
    {
        allDebit += [dc[@"amount"] floatValue];
    }
    
    BOOL is_debtor = !((allDebit - allCredit) >= 0);
    
    NSArray* pat_il = patient[@"illness"];
    if(pat_il.count>0&&is_debtor)
    {
        //cell.illnessImageView.hidden = (pat_il.count == 0);
        cell.illnessView.hidden=YES;
        cell.bothView.hidden=NO;
        cell.debtView.hidden=YES;
    }
    else if(pat_il.count>0&&!is_debtor)
    {
        cell.illnessView.hidden=NO;
        cell.bothView.hidden=YES;
        cell.debtView.hidden=YES;
        //cell.illnessImageView.hidden = YES;
    }
    else if(pat_il.count<=0&&is_debtor)
    {
        cell.illnessView.hidden=YES;
        cell.bothView.hidden=YES;
        cell.debtView.hidden=NO;
        //cell.illnessImageView.hidden = YES;
    }
    else
    {
        cell.illnessView.hidden=YES;
        cell.bothView.hidden=YES;
        cell.debtView.hidden=YES;
        //cell.illnessImageView.hidden = YES;
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *patient = sections[indexPath.section][@"content"][indexPath.row];//[self.fetchedResultsController objectAtIndexPath:indexPath];
    DBPatientProfileViewController *patientProfileVC = [[DBPatientProfileViewController alloc] initWithPatient:patient];
    [kNavigationController pushViewController:patientProfileVC animated:YES];
}


- (IBAction)changeSort:(id)sender {
    [self fetchData];
    slideView.frame = (self.sortSelector.selectedSegmentIndex==1)?CGRectMake(8, 93, 73, 3):CGRectMake(90, 93, 63, 3);
    [self.tableView reloadData];
}
- (IBAction)tapSelector:(id)sender
{
    self.sortSelector.selectedSegmentIndex=self.sortSelector.selectedSegmentIndex==0?1:0;
    
    [self changeSort:self];
}
@end
