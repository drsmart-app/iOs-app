//
//  FinancialIntervalViewController.h
//  DentaBook
//
//  Created by Denis Dubov on 24.12.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FinancialIntervalViewController : UIViewController

@property (copy, nonatomic) void (^didChooseInterval)(NSDate *startDate, NSDate *endDate);

@end
