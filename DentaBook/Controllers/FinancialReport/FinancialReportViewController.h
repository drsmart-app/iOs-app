//
//  FinancialReportViewController.h
//  DentaBook
//
//  Created by Denis Dubov on 03.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FinancialReportViewController : UIViewController
@property (nonatomic, retain) NSMutableArray* expences;
@property (nonatomic, retain) NSMutableArray* payments;
@property (nonatomic, retain) NSMutableArray* debts;
@property (weak, nonatomic) IBOutlet UIView *selectorView;
@property (weak, nonatomic) IBOutlet UIView *slideView;
@end
