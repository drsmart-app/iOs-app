//
//  FinancialReportViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 03.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "FinancialReportViewController.h"
#import "JJTopMenuViewController.h"
#import "NTMonthYearPicker.h"
#import "FinancialViewCell1.h"
#import "DBPatientProfileViewController.h"
#import "PutExpenseViewController.h"
#import "UIView+Helpers.h"
#import "WLPActionSheet.h"
#import "DBMonthYearController.h"
#import "FinancialIntervalViewController.h"

#import <MagicalRecord/MagicalRecord.h>
#import <NSDate+Calendar/NSDate+Calendar.h>

#import "Expense.h"
#import "ExpenseCategory.h"
#import "ExpenseSubcategory.h"
#import "Patient+Custom.h"
#import "Payment.h"
#import "DataBase.h"
#import "FRIncomeTableViewCell.h"
#import "FRTotalTableViewCell.h"
#import "HttpUtil.h"
#import "DBPatientProfileViewController.h"
#import "TherapyReadViewController.h"

typedef enum    {
    kFinancialCommonMode = 0,   //общие
    kFinancialIncomMode,        //доходы
    kFinancialExpenseMode,      //расходы
    kFinancialDebtsMode         //долги
} FinancialContentMode;

@interface FinancialReportViewController () <NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) JJTopMenuViewController *menuVC;
@property (weak, nonatomic) IBOutlet UILabel *selectedMonthLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic) FinancialContentMode financialMode;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (nonatomic, strong) WLPActionSheet *monthYearActionSheet;
@property (nonatomic, strong) DBMonthYearController *monthYearDateController;

@property (weak, nonatomic) IBOutlet UIButton *previousButton;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@end

@implementation FinancialReportViewController   {
    UIView *monthPickerContainerView;
    
    CGFloat incomSumm;
    CGFloat expenseSumm;
    CGFloat debtSumm;
    
    BOOL isIntervalMode;
    NSDate *selectedDateInOneMode;
    NSDate *selectedStartDateInIntervalMode;
    NSDate *selectedEndDateInIntervalMode;
}

@synthesize expences, payments, debts, selectorView, slideView;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Финансовый отчет";
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([FinancialViewCell1 class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([FinancialViewCell1 class])];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([FRIncomeTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([FRIncomeTableViewCell class])];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([FRTotalTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([FRTotalTableViewCell class])];
    
    isIntervalMode = NO;
    
    [self setupMenu];
    [self setupMonthPicker];
    
    UIImage *image2 = [UIImage imageNamed:@"back-button"];
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 addTarget:self action:@selector(onBack:)forControlEvents:UIControlEventTouchUpInside];
    [button2 setBackgroundImage:image2 forState:UIControlStateNormal];
    button2.frame = CGRectMake(0, 0, 11, 18);
    UIBarButtonItem *barButton2 = [[UIBarButtonItem alloc] initWithCustomView:button2];
    self.navigationItem.leftBarButtonItem = barButton2;
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(onInterval)forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"Интервал" forState:UIControlStateNormal];
    [button setTitle:@"Интервал" forState:UIControlStateSelected];
    button.frame = CGRectMake(0, 0, 65, 18);
    button.titleLabel.font = [UIFont systemFontOfSize:12];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    button.contentVerticalAlignment = UIControlContentVerticalAlignmentBottom;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];

    
    self.financialMode = kFinancialCommonMode;
    [self updateFinancialContent];
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = getColorSchemeStatus();
    }
    
    self.extendedLayoutIncludesOpaqueBars = YES;
}

-(void)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void) onInterval  {
    FinancialIntervalViewController *finIntervalVC = [FinancialIntervalViewController new];
    finIntervalVC.didChooseInterval = ^(NSDate *startDate, NSDate *endDate) {
        isIntervalMode = YES;
        selectedStartDateInIntervalMode = startDate;
        selectedEndDateInIntervalMode = endDate;
        
        [self updateMonthLabel];
        [self updateFinancialContent];
    };
    [self.navigationController pushViewController:finIntervalVC animated:YES];
}

-(void) setupMenu   {
    JJMenuItem *menuItem = [JJMenuItem initWithTitle:@"Общее" image:nil badgeValue:@"" keyPath:@"menu1"];
    JJMenuItem *menuItem2 = [JJMenuItem initWithTitle:@"Доход" image:nil badgeValue:@"" keyPath:@"menu2"];
    JJMenuItem *menuItem3 = [JJMenuItem initWithTitle:@"Расход" image:nil badgeValue:@"" keyPath:@"menu3"];
    JJMenuItem *menuItem4 = [JJMenuItem initWithTitle:@"Долги" image:nil badgeValue:@"" keyPath:@"menu4"];
    self.menuVC = [[JJTopMenuViewController alloc] initWithMenuItems:@[menuItem, menuItem2, menuItem3, menuItem4] selectedItemColor:[UIColor redColor] selectedItemTextColor:[UIColor blackColor]];
    self.menuVC.view.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    self.menuVC.view.frame = CGRectMake(0, 63, self.view.bounds.size.width, 50);
    self.menuVC.delegate = self;
    //[self.view addSubview:self.menuVC.view];
    [self.view insertSubview:self.menuVC.view belowSubview:selectorView];
    [self.menuVC selectMenuItemAtIndex:self.financialMode];
}

-(void) setupMonthPicker    {
    self.monthYearDateController = [DBMonthYearController new];
    selectedDateInOneMode = self.monthYearDateController.datePicker.date;
    
    self.nextButton.hidden = YES;
    [self updateMonthLabel];
}

#pragma mark - JJTopMenuDelegate
-(void)topMenu:(JJTopMenuViewController *)menu didSelectMenuItemAtIndex:(NSUInteger)index
{
    NSLog(@"Selected menu at index: %lu", (unsigned long)index);
    
    self.financialMode = (FinancialContentMode)index;
    [self updateFinancialContent];
    
    if(index==0)
    {
        slideView.frame = CGRectMake(9,118,52,3);
    }
    else if(index==1)
    {
        slideView.frame = CGRectMake(71,118,52,3);
    }
    else if(index==2)
    {
        slideView.frame = CGRectMake(132,118,55,3);
    }
    else
    {
        slideView.frame = CGRectMake(194,118,52,3);
    }
}

- (IBAction)onPreviousMonth:(id)sender {
    [self setDatePath:-1];
}

- (IBAction)onNextMonth:(id)sender {
    [self setDatePath:1];
}

- (IBAction)onPresentMonthChooser:(id)sender {
    [self showMonthPicker];
}

-(void) setDatePath:(NSInteger)pathIndex    {
    isIntervalMode = NO;
    
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    NSCalendar *cal = [NSCalendar currentCalendar];
    
    [comps setDay:0];
    [comps setMonth:pathIndex];
    [comps setYear:0];
    
    NSDate *tempDate = [cal dateByAddingComponents:comps toDate:selectedDateInOneMode options:0];
    
    [comps setDay:0];
    [comps setMonth:pathIndex + 1];
    [comps setYear:0];
    NSDate *tempDate2 = [cal dateByAddingComponents:comps toDate:selectedDateInOneMode options:0];
    if([tempDate2 compare:[NSDate date]] == NSOrderedDescending)  {
        self.nextButton.hidden = YES;
    } else  {
        self.nextButton.hidden = NO;
    }
    
    if([tempDate compare:[NSDate date]] == NSOrderedDescending)  {
        return;
    }
    
    self.monthYearDateController.datePicker.date = tempDate;
    selectedDateInOneMode = tempDate;
    
    [self updateMonthLabel];
    [self updateFinancialContent];
}

-(void) showMonthPicker {
    self.monthYearActionSheet = [[WLPActionSheet alloc] initWithTitle:@"Дата" cancelButtonTitle:@"ОК" destructiveButtonTitle:nil withViewController:self.monthYearDateController];
    self.monthYearActionSheet.tag = 1;
    self.monthYearActionSheet.delegate = self;
    [self.monthYearActionSheet showInView:self.view];
}

-(void)actionSheetDidDone:(WLPActionSheet*)actionSheet  {
    selectedDateInOneMode = self.monthYearDateController.datePicker.date;
    
    [self updateMonthLabel];
    [self updateFinancialContent];
    
    if([selectedDateInOneMode compare:[NSDate date]] == NSOrderedDescending)  {
        self.nextButton.hidden = YES;
    } else  {
        self.nextButton.hidden = NO;
    }
}

-(void)actionSheetDidDissapear:(WLPActionSheet*)actionSheet   {
    NSLog(@"DESTRUCTIVE");
}

-(void) updateMonthLabel    {
    if(isIntervalMode)  {
        if([selectedStartDateInIntervalMode isEqualToDate:selectedEndDateInIntervalMode])   {
            NSDateFormatter *df = [[NSDateFormatter alloc] init];
            [df setDateFormat:@"dd MMMM yyyy"];
            NSString *dateStr = [df stringFromDate:selectedStartDateInIntervalMode];
            self.selectedMonthLabel.text = [NSString stringWithFormat:@"%@", dateStr];
        } else  {
            NSDateFormatter *df = [[NSDateFormatter alloc] init];
            [df setDateFormat:@"dd.MM.yyyy"];
            NSString *dateStartStr = [df stringFromDate:selectedStartDateInIntervalMode];
            NSString *dateEndStr = [df stringFromDate:selectedEndDateInIntervalMode];
            self.selectedMonthLabel.text = [NSString stringWithFormat:@"%@ - %@", dateStartStr, dateEndStr];
        }
    } else  {
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"LLLL yyyy"];
        NSString *dateStr = [df stringFromDate:selectedDateInOneMode];
        dateStr = [[[dateStr substringToIndex:1] uppercaseString] stringByAppendingString:[dateStr substringFromIndex:1]];
        self.selectedMonthLabel.text = [NSString stringWithFormat:@"%@", dateStr];
    }
}

- (void)onDatePicked:(UITapGestureRecognizer *)gestureRecognizer {
    
}

#pragma mark - Actions
-(void) updateFinancialContent
{
    NSLog(@"Menu Index: %lu", (unsigned long)[self.menuVC getIndexForSelectedMenu]);
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd.MM.yyyy"];
    
    expences = [[NSMutableArray alloc] init];
    payments = [[NSMutableArray alloc] init];
    debts = [[NSMutableArray alloc] init];
    
    NSDate *normalizeSelectedDate = [self normalizedDateWithDate:selectedDateInOneMode];
    
    switch (self.financialMode)
    {
        case kFinancialCommonMode:
        {
            expenseSumm = 0;
            NSArray* tmp_expences = [DataBase getDBArray:@"expence" withKey:@"" andValue:@""];//= [Expense MR_findAll];
            for(NSDictionary *expenseEntity in tmp_expences)
            {
                NSDate* createAt = [dateFormatter dateFromString:expenseEntity[@"createat"]];
                if(isIntervalMode)
                {
                    
                    if(([createAt isGreaterOrEqualToDate:selectedStartDateInIntervalMode]) && ([createAt isLessOrEqualToDate:selectedEndDateInIntervalMode]))
                    {
                        expenseSumm += [expenseEntity[@"amount"] floatValue];
                        [expences addObject:expenseEntity];
                    }
                }
                else
                {
                    NSDate *expenseDate = [self normalizedDateWithDate:createAt];
                    if([expenseDate isEqualToDate:normalizeSelectedDate])
                    {
                        expenseSumm += [expenseEntity[@"amount"] floatValue];
                        [expences addObject:expenseEntity];
                    }
                }
            }
            
            incomSumm = 0;
            NSArray* tmp_payments = [DataBase getDBArray:@"payment" withKey:@"" andValue:@""];
            for(NSDictionary *paymentEntity in tmp_payments)
            {
                NSDate* createAt = [dateFormatter dateFromString:paymentEntity[@"createat"]];
                if(isIntervalMode)
                {
                    if(([createAt isGreaterOrEqualToDate:selectedStartDateInIntervalMode]) && ([createAt isLessOrEqualToDate:selectedEndDateInIntervalMode]))
                    {
                        incomSumm += [paymentEntity[@"amount"] floatValue];
                        [payments addObject:paymentEntity];
                    }
                }
                else
                {
                    NSDate *paymentDate = [self normalizedDateWithDate:createAt];;
                    if([paymentDate isEqualToDate:normalizeSelectedDate])
                    {
                        incomSumm += [paymentEntity[@"amount"] floatValue];
                        [payments addObject:paymentEntity];
                    }
                }
            }
            
            //_fetchedResultsController = nil;
        }
            break;
        case kFinancialIncomMode:
        {
            incomSumm = 0;
            NSArray* tmp_payments = [DataBase getDBArray:@"payment" withKey:@"" andValue:@""];
            for(NSDictionary *paymentEntity in tmp_payments)
            {
                NSDate* createAt = [dateFormatter dateFromString:paymentEntity[@"createat"]];
                if(isIntervalMode)
                {
                    if(([createAt isGreaterOrEqualToDate:selectedStartDateInIntervalMode]) && ([createAt isLessOrEqualToDate:selectedEndDateInIntervalMode]))
                    {
                        incomSumm += [paymentEntity[@"amount"] floatValue];
                        [payments addObject:paymentEntity];
                    }
                }
                else
                {
                    NSDate *paymentDate = [self normalizedDateWithDate:createAt];;
                    if([paymentDate isEqualToDate:normalizeSelectedDate])
                    {
                        incomSumm += [paymentEntity[@"amount"] floatValue];
                        [payments addObject:paymentEntity];
                    }
                }
            }
            
            //[self fetchTableData];
        }
            break;
        case kFinancialExpenseMode:
        {
            expenseSumm = 0;
            NSArray* tmp_expences = [DataBase getDBArray:@"expence" withKey:@"" andValue:@""];//= [Expense MR_findAll];
            for(NSDictionary *expenseEntity in tmp_expences)
            {
                NSDate* createAt = [dateFormatter dateFromString:expenseEntity[@"createat"]];
                if(isIntervalMode)
                {
                    
                    if(([createAt isGreaterOrEqualToDate:selectedStartDateInIntervalMode]) && ([createAt isLessOrEqualToDate:selectedEndDateInIntervalMode]))
                    {
                        expenseSumm += [expenseEntity[@"amount"] floatValue];
                        [expences addObject:expenseEntity];
                    }
                }
                else
                {
                    NSDate *expenseDate = [self normalizedDateWithDate:createAt];
                    if([expenseDate isEqualToDate:normalizeSelectedDate])
                    {
                        expenseSumm += [expenseEntity[@"amount"] floatValue];
                        [expences addObject:expenseEntity];
                    }
                }
            }
            
            //[self fetchTableData];
        }
            break;
            
        case kFinancialDebtsMode:
        {
            debtSumm = 0.0f;
            debts = [DataBase getDBArray:@"patient" withKey:@"" andValue:@""];
            NSMutableArray* debts_tmp=[[NSMutableArray alloc] init];
            
            for(NSDictionary* d in debts)
            {
                NSMutableDictionary*dbt = [NSMutableDictionary dictionaryWithDictionary:d];
                NSDate* createAt = [dateFormatter dateFromString:d[@"createat"]];
                float patient_expences = 0;
                for(NSDictionary* th in[DataBase getDBArray:@"visits" withKey:@"patientid" andValue:d[@"patientid"]])
                {
                    /*for(NSDictionary* it in th[@"items"])
                    {
                            //expenseSumm += [expenseEntity[@"amount"] floatValue];
                            patient_expences -= [it[@"cost"] floatValue]*[it[@"countt"] floatValue];
                    }*/
                    patient_expences-=[th[@"total_cost_with_discount"] floatValue];
                    /*if(isIntervalMode)
                    {
                        for(NSDictionary* it in th[@"items"])
                        {
                            if(([createAt isGreaterOrEqualToDate:selectedStartDateInIntervalMode]) && ([createAt isLessOrEqualToDate:selectedEndDateInIntervalMode]))
                            {
                                //expenseSumm += [expenseEntity[@"amount"] floatValue];
                                patient_expences -= [it[@"cost"] floatValue]*[it[@"countt"] floatValue];
                            }
                        }
                    }
                    else
                    {
                        NSDate *expenseDate = [self normalizedDateWithDate:createAt];
                        for(NSDictionary* it in th[@"items"])
                        {
                            if([expenseDate isEqualToDate:normalizeSelectedDate])
                            {
                                //expenseSumm += [expenseEntity[@"amount"] floatValue];
                                patient_expences -= [it[@"cost"] floatValue]*[it[@"countt"] floatValue];
                            }
                        }
                    }*/

                }
                
                NSArray* tmp_payments = [DataBase getDBArray:@"payment" withKey:@"patientid" andValue:d[@"patientid"]];
                for(NSDictionary *paymentEntity in tmp_payments)
                {
                    patient_expences += [paymentEntity[@"amount"] floatValue];
                    /*NSDate* createAt = [dateFormatter dateFromString:paymentEntity[@"createat"]];
                    if(isIntervalMode)
                    {
                        if(([createAt isGreaterOrEqualToDate:selectedStartDateInIntervalMode]) && ([createAt isLessOrEqualToDate:selectedEndDateInIntervalMode]))
                        {
                            patient_expences += [paymentEntity[@"amount"] floatValue];
                        }
                    }
                    else
                    {
                        NSDate *paymentDate = [self normalizedDateWithDate:createAt];;
                        if([paymentDate isEqualToDate:normalizeSelectedDate])
                        {
                            patient_expences += [paymentEntity[@"amount"] floatValue];
                        }
                    }*/
                }
                
                if(patient_expences<0)
                {
                    dbt[@"debt"] = @(patient_expences);
                    [debts_tmp addObject:dbt];
                }
            }
            
            debts = debts_tmp;
            
            
            //cell.debtorImageView.hidden = ([patient allDebt] == 0);
            NSArray* costs = [DataBase getDBArray:@"visits" withKey:@"" andValue:@""];
            float allCredit=0;
            for(NSDictionary* dc in costs)
            {
                NSDate* createAt = [dateFormatter dateFromString:dc[@"createat"]];
                float discount = [dc[@"discount"] floatValue];

                if(isIntervalMode)
                {

                    allCredit+=[dc[@"total_cost_with_discount"] floatValue];
                }
                else
                {
                    allCredit+=[dc[@"total_cost_with_discount"] floatValue];
                }
            }
            
            float allDebit=0;

            NSArray* tmp_payments = [DataBase getDBArray:@"payment" withKey:@"" andValue:@""];
            for(NSDictionary *paymentEntity in tmp_payments)
            {
                allDebit += [paymentEntity[@"amount"] floatValue];
                /*
                NSDate* createAt = [dateFormatter dateFromString:paymentEntity[@"createat"]];
                if(isIntervalMode)
                {
                    if(([createAt isGreaterOrEqualToDate:selectedStartDateInIntervalMode]) && ([createAt isLessOrEqualToDate:selectedEndDateInIntervalMode]))
                    {
                        allDebit += [paymentEntity[@"amount"] floatValue];
                        [payments addObject:paymentEntity];
                    }
                }
                else
                {
                    NSDate *paymentDate = [self normalizedDateWithDate:createAt];;
                    if([paymentDate isEqualToDate:normalizeSelectedDate])
                    {
                        allDebit += [paymentEntity[@"amount"] floatValue];
                        [payments addObject:paymentEntity];
                    }
                }*/
            }

            /*NSArray *allPatient = [Patient MR_findAll];
            for(Patient *patientEntity in allPatient)
            {
                debtSumm += [patientEntity allDebt];
            }
            
            _fetchedResultsController = nil;*/
            debtSumm = (allDebit - allCredit)>0?0:-1*(allDebit - allCredit);
        }
            break;
            
        default:
            break;
    }
    
    [self.tableView reloadData];
}

-(void)fetchTableData
{
    /*_fetchedResultsController = nil;
    NSError *error = nil;
    [self.fetchedResultsController performFetch:&error];*/
}


#pragma mark -
#pragma mark Option Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(self.financialMode == kFinancialCommonMode)
        return 1;
    else
    {
        if(self.financialMode == kFinancialDebtsMode)
            return 1 + debts.count;
        if(self.financialMode == kFinancialIncomMode)
            return 1 + payments.count;
        if(self.financialMode == kFinancialExpenseMode)
            return 1 + expences.count;
    }
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.financialMode == kFinancialCommonMode)
        return 3;
    else
        return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    return [UIView new];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section    {
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section   {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section   {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath  {
    if(self.financialMode == kFinancialIncomMode&&indexPath.section > 0)
        return 65.0f;
    
    if(self.financialMode == kFinancialExpenseMode&&indexPath.section > 0)
        return 65.0f;
    
    if(self.financialMode == kFinancialDebtsMode&&indexPath.section > 0)
        return 65.0f;
    
    return 50.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setGroupingSeparator:@" "];
    [numberFormatter setGroupingSize:3];
    [numberFormatter setUsesGroupingSeparator:YES];
    [numberFormatter setDecimalSeparator:@"."];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [numberFormatter setMaximumFractionDigits:2];
    
    if(self.financialMode == kFinancialCommonMode)
    {
        FRTotalTableViewCell *finCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([FRTotalTableViewCell class])];
        if(indexPath.row == 0)
        {
            finCell.titleLabel.text = @"Доход";
            NSString *theString = [numberFormatter stringFromNumber:@(incomSumm)];
            finCell.descriptionLabel.text = [NSString stringWithFormat:@"+ %@", theString];
            finCell.descriptionLabel.textColor = [UIColor colorWithRed:46.0/255.0 green:204.0/255.0 blue:113.0/255.0 alpha:1.0];
        }
        else if(indexPath.row == 1)
        {
            finCell.titleLabel.text = @"Расход";
            NSString *theString = [numberFormatter stringFromNumber:@(expenseSumm)];
            finCell.descriptionLabel.text = [NSString stringWithFormat:@"- %@", theString];
            finCell.descriptionLabel.textColor = [UIColor colorWithRed:231.0/255.0 green:76.0/255.0 blue:60.0/255.0 alpha:1.0];
        }
        else
        {
            finCell.titleLabel.text = @"Итог";
            NSString *theString = [numberFormatter stringFromNumber:@(fabsf(incomSumm-expenseSumm))];
            finCell.descriptionLabel.text = ((incomSumm-expenseSumm)>0)?[NSString stringWithFormat:@"+ %@", theString]:[NSString stringWithFormat:@"- %@", theString];
            finCell.descriptionLabel.textColor = ((incomSumm-expenseSumm)>0)?[UIColor colorWithRed:46.0/255.0 green:204.0/255.0 blue:113.0/255.0 alpha:1.0]:[UIColor colorWithRed:231.0/255.0 green:76.0/255.0 blue:60.0/255.0 alpha:1.0];
        }
        cell = finCell;
    }
    else  if(self.financialMode == kFinancialIncomMode)
    {
        if(indexPath.section == 0)
        {
            FRTotalTableViewCell *finCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([FRTotalTableViewCell class])];
            if(indexPath.row == 0)
            {
                finCell.titleLabel.text = @"Доход";
                NSString *theString = [numberFormatter stringFromNumber:@(incomSumm)];
                finCell.descriptionLabel.text = [NSString stringWithFormat:@"+ %@ ₽", theString];
                finCell.descriptionLabel.textColor = [UIColor colorWithRed:46.0/255.0 green:204.0/255.0 blue:113.0/255.0 alpha:1.0];
            }
            cell = finCell;
        }
        else
        {
            NSDictionary* paymentEntity = payments[indexPath.section-1];
            NSDictionary* patientEnriry = (NSDictionary*)[DataBase getDB:@"patient" withKey:@"patientid" andValue:paymentEntity[@"patientid"]];
            FRIncomeTableViewCell *finCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([FRIncomeTableViewCell class])];
            finCell.nameLabel.text = [NSString stringWithFormat:@"%@ %@ %@", patientEnriry[@"firstname"], patientEnriry[@"middlename"], patientEnriry[@"lastname"]];
            
            NSString *theString = [numberFormatter stringFromNumber:@([paymentEntity[@"amount"] floatValue])];
            
            finCell.amountLabel.text = [NSString stringWithFormat:@"+ %@ ₽", theString];
            finCell.amountLabel.textColor = [UIColor blackColor];
            finCell.dateLabel.text = paymentEntity[@"createat"];
            cell = finCell;
        }
    }
    else  if(self.financialMode == kFinancialExpenseMode)
    {
        if(indexPath.section == 0)
        {
            FRTotalTableViewCell *finCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([FRTotalTableViewCell class])];
            if(indexPath.row == 0)
            {
                finCell.titleLabel.text = @"Расход";
                NSString *theString = [numberFormatter stringFromNumber:@(expenseSumm)];
                finCell.descriptionLabel.text = [NSString stringWithFormat:@"- %@ ₽", theString];
                finCell.descriptionLabel.textColor = [UIColor colorWithRed:231.0/255.0 green:76.0/255.0 blue:60.0/255.0 alpha:1.0];
            }
            cell = finCell;
        }
        else
        {
            NSDictionary *expenseEntity = expences[indexPath.section-1];//self.fetchedResultsController.fetchedObjects[indexPath.section-1];
            NSDictionary *expenseCategory = (NSDictionary*)[DataBase getDB:@"expence_category" withKey:@"id" andValue:expenseEntity[@"categoryid"]];
            NSDictionary *expenseSubCategory = (NSDictionary*)[DataBase getDB:@"expence_subcategory" withKey:@"id" andValue:expenseEntity[@"subcategoryid"]];
            
            FinancialViewCell1 *finCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([FinancialViewCell1 class])];
            finCell.titleLabel.text = [NSString stringWithFormat:@"%@:%@", expenseCategory[@"title"], expenseSubCategory[@"title"]];
            
            NSString *theString = [numberFormatter stringFromNumber:@([expenseEntity[@"amount"] floatValue])];
            
            finCell.descriptionLabel.text = [NSString stringWithFormat:@"- %@ ₽", theString];
            //finCell.descriptionLabel.textColor = [UIColor blackColor];
            cell = finCell;
        }
    }
    else
    {
        if(indexPath.section == 0)
        {
            FRTotalTableViewCell *finCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([FRTotalTableViewCell class])];
            if(indexPath.row == 0)
            {
                finCell.titleLabel.text = @"Общий долг";
                NSString *theString = [numberFormatter stringFromNumber:@(debtSumm)];
                finCell.descriptionLabel.text = [NSString stringWithFormat:@"%@ ₽", theString];
                finCell.descriptionLabel.textColor = [UIColor colorWithRed:231.0/255.0 green:76.0/255.0 blue:60.0/255.0 alpha:1.0];
            }
            cell = finCell;
        }
        else
        {
            NSDictionary *debtEntity = debts[indexPath.section-1];//self.fetchedResultsController.fetchedObjects[indexPath.section-1];
            
            FinancialViewCell1 *finCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([FinancialViewCell1 class])];
            finCell.titleLabel.text = [NSString stringWithFormat:@"%@ %@", debtEntity[@"lastname"], debtEntity[@"firstname"]];
            
            NSString *theString = [numberFormatter stringFromNumber:@([debtEntity[@"debt"] floatValue])];
            
            finCell.descriptionLabel.text = [NSString stringWithFormat:@"%@ ₽", theString];
            //finCell.descriptionLabel.textColor = [UIColor blackColor];
            cell = finCell;
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(indexPath.section == 0)
        return;
    
    if(self.financialMode == kFinancialIncomMode)
    {
        NSDictionary* paymentEntity = payments[indexPath.section-1];
        
        BOOL isVisit = paymentEntity[@"visitid"]?(([paymentEntity[@"visitid"] length]>0)):false;
        
        if(isVisit)
        {
            NSDictionary* therapyEntity = (NSDictionary*)[DataBase getDB:@"visits" withKey:@"id" andValue:paymentEntity[@"visitid"]];
           
            TherapyReadViewController *therapyReadOnlyVC = [[TherapyReadViewController alloc] initWithTherapy:therapyEntity];
            [self.navigationController pushViewController:therapyReadOnlyVC animated:YES];
        }
        else
        {
            NSDictionary* patientEnriry = (NSDictionary*)[DataBase getDB:@"patient" withKey:@"patientid" andValue:paymentEntity[@"patientid"]];
            DBPatientProfileViewController *patientVC = [[DBPatientProfileViewController alloc] initWithPatient:patientEnriry];
            [self.navigationController pushViewController:patientVC animated:YES];
        }
    }
    else if(self.financialMode == kFinancialExpenseMode)
    {
        NSDictionary *expenseEntity = expences[indexPath.section-1];//self.fetchedResultsController.fetchedObjects[indexPath.section-1];
        PutExpenseViewController *putExpenseVC = [[PutExpenseViewController alloc] initWithExpense:expenseEntity];
        [self.navigationController pushViewController:putExpenseVC animated:YES];
    }
    else if(self.financialMode == kFinancialDebtsMode)
    {
        NSDictionary *patientEntity = debts[indexPath.section-1];//self.fetchedResultsController.fetchedObjects[indexPath.section-1];
        DBPatientProfileViewController *patientProfileVC = [[DBPatientProfileViewController alloc] initWithPatient:patientEntity];
        [self.navigationController pushViewController:patientProfileVC animated:YES];
    }
}

-(NSDate*)normalizedDateWithDate:(NSDate*)date
{
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth)
                                               fromDate: date];
    return [calendar dateFromComponents:components];
}

@end
