//
//  FRIncomeTableViewCell.h
//  DentaBook
//
//  Created by Mac Mini on 03.12.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FRIncomeTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet UIImageView *illnessImg;
@property (weak, nonatomic) IBOutlet UIImageView *debtorImg;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet UIView *frontView;

@end
