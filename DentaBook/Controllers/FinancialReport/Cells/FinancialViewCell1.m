//
//  FinancialViewCell1.m
//  DentaBook
//
//  Created by Denis Dubov on 03.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "FinancialViewCell1.h"

@implementation FinancialViewCell1
@synthesize frontView;
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    /*shadowView.layer.cornerRadius=3;
     shadowView.layer.masksToBounds=YES;
     frontView.layer.cornerRadius=3;
     frontView.layer.masksToBounds=YES;*/
    frontView.layer.shadowColor = [UIColor blackColor].CGColor;
    frontView.layer.shadowOpacity = 0.5;
    frontView.layer.shadowRadius = 2;
    frontView.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
    frontView.layer.cornerRadius = 2;
    frontView.layer.masksToBounds = NO;
}

@end
