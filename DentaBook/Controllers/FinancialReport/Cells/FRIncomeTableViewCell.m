//
//  FRIncomeTableViewCell.m
//  DentaBook
//
//  Created by Mac Mini on 03.12.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "FRIncomeTableViewCell.h"

@implementation FRIncomeTableViewCell
@synthesize frontView;
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    /*shadowView.layer.cornerRadius=3;
     shadowView.layer.masksToBounds=YES;
     frontView.layer.cornerRadius=3;
     frontView.layer.masksToBounds=YES;*/
    frontView.layer.shadowColor = [UIColor blackColor].CGColor;
    frontView.layer.shadowOpacity = 0.5;
    frontView.layer.shadowRadius = 2;
    frontView.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
    frontView.layer.cornerRadius = 2;
    frontView.layer.masksToBounds = NO;
}

@end
