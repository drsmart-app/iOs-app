//
//  AffairGroup+Custom.h
//  DentaBook
//
//  Created by Denis Dubov on 10.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "AffairGroup.h"
#import "FastEasyMapping.h"

@interface AffairGroup (Custom)

+ (FEMManagedObjectMapping *)mapObject;

@end
