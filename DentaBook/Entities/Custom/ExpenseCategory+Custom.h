//
//  ExpenseCategory+Custom.h
//  DentaBook
//
//  Created by Denis Dubov on 11.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "ExpenseCategory.h"
#import "FastEasyMapping.h"

@interface ExpenseCategory (Custom)

+ (FEMManagedObjectMapping *)mapObject;

@end
