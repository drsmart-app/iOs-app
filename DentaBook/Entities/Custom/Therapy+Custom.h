//
//  Therapy+Custom.h
//  DentaBook
//
//  Created by Denis Dubov on 26.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "Therapy.h"
#import "FastEasyMapping.h"

@interface Therapy (Custom)

-(NSInteger) allCost;
-(NSInteger) allCostWithDiscount;
-(NSArray*) allTherapyItem;

+ (FEMManagedObjectMapping *)mapObject;

@end
