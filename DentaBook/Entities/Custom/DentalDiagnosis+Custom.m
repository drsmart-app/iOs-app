//
//  DentalDiagnosis+Custom.m
//  DentaBook
//
//  Created by Denis Dubov on 05.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "DentalDiagnosis+Custom.h"

@implementation DentalDiagnosis (Custom)

+ (FEMManagedObjectMapping *)mapObject {
    
    return [FEMManagedObjectMapping mappingForEntityName:@"DentalDiagnosis"
                                           configuration:^(FEMManagedObjectMapping *mapping) {
                                               [mapping setPrimaryKey:@"remoteId"];  // object uniquing
                                               
                                               [mapping addAttributesFromDictionary:@{@"remoteId": @"id"}];
                                               [mapping addAttributesFromDictionary:@{@"name": @"title"}];
                                               [mapping addAttributesFromDictionary:@{@"symbolName": @"subTitle"}];
                                               
                                               [mapping addAttribute:[FEMAttribute mappingOfProperty:@"createdAt"
                                                                                           toKeyPath:@"createdAt"
                                                                                          dateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"]];
                                               
                                               [mapping addAttribute:[FEMAttribute mappingOfProperty:@"updatedAt"
                                                                                           toKeyPath:@"updatedAt"
                                                                                          dateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"]];
                                           }];
}

@end
