//
//  ExpenseSubcategory+Custom.m
//  DentaBook
//
//  Created by Denis Dubov on 11.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "ExpenseSubcategory+Custom.h"

@implementation ExpenseSubcategory (Custom)

+ (FEMManagedObjectMapping *)mapObject {
    
    return [FEMManagedObjectMapping mappingForEntityName:@"ExpenseSubcategory"
                                           configuration:^(FEMManagedObjectMapping *mapping) {
                                               [mapping setPrimaryKey:@"remoteId"];  // object uniquing
                                               
                                               [mapping addAttributesFromDictionary:@{@"remoteId": @"id"}];
                                               [mapping addAttributesFromDictionary:@{@"title": @"title"}];
                                               [mapping addAttribute:[FEMAttribute mappingOfProperty:@"createAt"
                                                                                           toKeyPath:@"date"
                                                                                          dateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"]];
                                               
                                               [mapping addAttribute:[FEMAttribute mappingOfProperty:@"createdAt"
                                                                                           toKeyPath:@"createdAt"
                                                                                          dateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"]];
                                               
                                               [mapping addAttribute:[FEMAttribute mappingOfProperty:@"updatedAt"
                                                                                           toKeyPath:@"updatedAt"
                                                                                          dateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"]];
                                           }];
}

@end
