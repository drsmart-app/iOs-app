//
//  PatientIllness+Custom.h
//  DentaBook
//
//  Created by Denis Dubov on 05.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "PatientIllness.h"
#import "FastEasyMapping.h"

@interface PatientIllness (Custom)

+ (FEMManagedObjectMapping *)mapObject;

@end
