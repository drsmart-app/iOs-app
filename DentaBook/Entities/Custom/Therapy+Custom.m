//
//  Therapy+Custom.m
//  DentaBook
//
//  Created by Denis Dubov on 26.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "Therapy+Custom.h"
#import "TherapyItem+Custom.h"
#import "Service.h"

#import <MagicalRecord/MagicalRecord.h>

@implementation Therapy (Custom)

-(NSInteger) allCost    {
    NSInteger cost = 0;
    
    for(TherapyItem *buyRef in self.therapyItems)   {
        Service *serviceRef = [Service MR_findFirstByAttribute:@"remoteId" withValue:buyRef.serviceId];
        cost += [serviceRef.cost integerValue] * [buyRef.count integerValue];
    }
    
    return cost;
}

-(NSInteger) allCostWithDiscount    {
    NSInteger cost = 0;
    
    for(TherapyItem *buyRef in self.therapyItems)   {
        Service *serviceRef = [Service MR_findFirstByAttribute:@"remoteId" withValue:buyRef.serviceId];
        cost += [serviceRef.cost integerValue] * [buyRef.count integerValue];
    }
    return cost - ((cost * [self.discount integerValue])/100);
}

-(NSArray*) allTherapyItem   {
    return [TherapyItem MR_findAllSortedBy:@"createAt" ascending:YES withPredicate:[NSPredicate predicateWithFormat:@"therapy == %@", self]];
}

+ (FEMManagedObjectMapping *)mapObject {
    
    return [FEMManagedObjectMapping mappingForEntityName:@"Therapy"
                                           configuration:^(FEMManagedObjectMapping *mapping) {
                                               [mapping setPrimaryKey:@"remoteId"];  // object uniquing
                                               
                                               [mapping addAttributesFromDictionary:@{@"remoteId": @"id"}];
                                               [mapping addAttributesFromDictionary:@{@"name": @"title"}];
                                               [mapping addAttributesFromArray:@[@"discount", @"isPlan"]];
                                               [mapping addAttribute:[FEMAttribute mappingOfProperty:@"createAt"
                                                                                           toKeyPath:@"date"
                                                                                          dateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"]];
                                               [mapping addToManyRelationshipMapping:[TherapyItem mapObject] forProperty:@"therapyItems" keyPath:@"Items"];
                                               
                                               [mapping addAttribute:[FEMAttribute mappingOfProperty:@"createdAt"
                                                                                           toKeyPath:@"createdAt"
                                                                                          dateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"]];
                                               
                                               [mapping addAttribute:[FEMAttribute mappingOfProperty:@"updatedAt"
                                                                                           toKeyPath:@"updatedAt"
                                                                                          dateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"]];
                                           }];
}

@end
