//
//  Patient+Custom.h
//  DentaBook
//
//  Created by Denis Dubov on 04.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "Patient.h"
#import "FastEasyMapping.h"

@interface Patient (Custom)

+(long) maxRemoteId;

- (NSString *) firstNameInitial;
-(NSInteger) allCost;
-(NSInteger) allCostWithDiscount;
-(NSInteger) allPayment;
-(NSInteger) allDebt;

+ (FEMManagedObjectMapping *)mapObject;
+ (FEMManagedObjectMapping *)mapObjectSync;

@end
