//
//  Tooth+Custom.h
//  DentaBook
//
//  Created by Denis Dubov on 08.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "Tooth.h"
#import "FastEasyMapping.h"

@interface Tooth (Custom)

+ (FEMManagedObjectMapping *)mapObjectSync;

@end
