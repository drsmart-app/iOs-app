//
//  DentalDiagnosis+Custom.h
//  DentaBook
//
//  Created by Denis Dubov on 05.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "DentalDiagnosis.h"
#import "FastEasyMapping.h"

@interface DentalDiagnosis (Custom)

+ (FEMManagedObjectMapping *)mapObject;

@end
