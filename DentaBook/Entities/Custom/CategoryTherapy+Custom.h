//
//  CategoryTherapy+Custom.h
//  DentaBook
//
//  Created by Denis Dubov on 06.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "CategoryTherapy.h"
#import "FastEasyMapping.h"

@interface CategoryTherapy (Custom)

+ (FEMManagedObjectMapping *)mapObject;

@end
