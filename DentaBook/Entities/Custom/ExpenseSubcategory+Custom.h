//
//  ExpenseSubcategory+Custom.h
//  DentaBook
//
//  Created by Denis Dubov on 11.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "ExpenseSubcategory.h"
#import "FastEasyMapping.h"

@interface ExpenseSubcategory (Custom)

+ (FEMManagedObjectMapping *)mapObject;

@end
