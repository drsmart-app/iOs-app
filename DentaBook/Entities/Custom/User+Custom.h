//
//  User+Custom.h
//  DentaBook
//
//  Created by Denis Dubov on 27.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "User.h"
#import "FastEasyMapping.h"

@interface User (Custom)

+(User*) currentUser;

+ (FEMManagedObjectMapping *)mapObject;

@end
