//
//  Payment+Custom.h
//  DentaBook
//
//  Created by Denis Dubov on 10.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "Payment.h"
#import "FastEasyMapping.h"

@interface Payment (Custom)

+ (FEMManagedObjectMapping *)mapObject;

@end
