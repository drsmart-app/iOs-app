//
//  Expense+Custom.h
//  DentaBook
//
//  Created by Denis Dubov on 11.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "Expense.h"
#import "FastEasyMapping.h"

@interface Expense (Custom)

+ (FEMManagedObjectMapping *)mapObject;

@end
