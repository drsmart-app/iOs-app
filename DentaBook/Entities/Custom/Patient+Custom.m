//
//  Patient+Custom.m
//  DentaBook
//
//  Created by Denis Dubov on 04.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "Patient+Custom.h"
#import "Therapy+Custom.h"
#import "Payment.h"
#import "ToothList+Custom.h"
#import "Therapy+Custom.m"
#import "Payment+Custom.h"
#import "Gallery+Custom.h"

#import <MagicalRecord/MagicalRecord.h>

@implementation Patient (Custom)

+(long) maxRemoteId {
    long maxRemoteId = 0;
    for(Patient *patient in [Patient MR_findAll])   {
        if([patient.remoteId longValue] > maxRemoteId)
            maxRemoteId = [patient.remoteId longValue];
    }
    
    return maxRemoteId;
}

- (NSString *) firstNameInitial {
    [self willAccessValueForKey:@"firstNameInitial"];
    NSString * initial = [[[self firstName] substringToIndex:1] uppercaseString];
    [self didAccessValueForKey:@"firstNameInitial"];

    return initial;
}

-(NSInteger) allCost    {
    NSInteger cost = 0;
    
    NSArray *therapyArray = [Therapy MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"patient == %@ AND isPlan == FALSE", self]];
    
    for(Therapy *buyRef in therapyArray)   {
        cost += [buyRef allCost];
    }
    
    return cost;
}

-(NSInteger) allCostWithDiscount    {
    NSInteger cost = 0;
    
    NSArray *therapyArray = [Therapy MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"patient == %@ AND isPlan == FALSE", self]];
    
    for(Therapy *buyRef in therapyArray)   {
        cost += [buyRef allCostWithDiscount];
    }
    
    return cost;
}

-(NSInteger) allPayment {
    NSInteger cost = 0;
    NSArray *paymentArray = [Payment MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"patient == %@", self]];
    
    for(Payment *payment in paymentArray)   {
        cost += [payment.amount integerValue];
    }
    
    return cost;
}

-(NSInteger) allDebt    {
    NSInteger common = [self allCostWithDiscount];
    NSInteger debt = common - [self allPayment];
    if(debt < 0) debt = 0;
    
    return debt;
}

+ (FEMManagedObjectMapping *)mapObject {
    
    return [FEMManagedObjectMapping mappingForEntityName:@"Patient"
                                           configuration:^(FEMManagedObjectMapping *mapping) {
                                               [mapping setPrimaryKey:@"remoteId"];  // object uniquing
                                               
                                               [mapping addAttributesFromDictionary:@{@"remoteId": @"id"}];
                                               [mapping addAttributesFromArray:@[@"firstName", @"lastName", @"middleName", @"rating", @"phoneNumber"]];
                                               
                                               [mapping addAttribute:[FEMAttribute mappingOfProperty:@"createdAt"
                                                                                           toKeyPath:@"createdAt"
                                                                                          dateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"]];
                                               
                                               [mapping addAttribute:[FEMAttribute mappingOfProperty:@"updatedAt"
                                                                                           toKeyPath:@"updatedAt"
                                                                                          dateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"]];
                                           }];
}

+ (FEMManagedObjectMapping *)mapObjectSync {
    
    return [FEMManagedObjectMapping mappingForEntityName:@"Patient"
                                           configuration:^(FEMManagedObjectMapping *mapping) {
                                               [mapping setPrimaryKey:@"remoteId"];  // object uniquing
                                               
                                               [mapping addAttributesFromDictionary:@{@"remoteId": @"id"}];
                                               [mapping addAttributesFromArray:@[@"firstName", @"lastName", @"middleName", @"rating", @"phoneNumber"]];
                                               [mapping addToManyRelationshipMapping:[ToothList mapObjectSync] forProperty:@"tooth_list" keyPath:@"Toothes"];
                                               [mapping addToManyRelationshipMapping:[Therapy mapObject] forProperty:@"therapies" keyPath:@"Therapies"];
                                               [mapping addToManyRelationshipMapping:[Payment mapObject] forProperty:@"payments" keyPath:@"Payments"];
                                               [mapping addToManyRelationshipMapping:[Gallery mapObject] forProperty:@"galleries" keyPath:@"Galleries"];
                                               
                                               [mapping addAttribute:[FEMAttribute mappingOfProperty:@"createdAt"
                                                                                           toKeyPath:@"createdAt"
                                                                                          dateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"]];
                                               
                                               [mapping addAttribute:[FEMAttribute mappingOfProperty:@"updatedAt"
                                                                                           toKeyPath:@"updatedAt"
                                                                                          dateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"]];
                                           }];
}

@end
