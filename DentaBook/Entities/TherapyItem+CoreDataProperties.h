//
//  TherapyItem+CoreDataProperties.h
//  DentaBook
//
//  Created by Denis Dubov on 08.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "TherapyItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface TherapyItem (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *categoryId;
@property (nullable, nonatomic, retain) NSNumber *count;
@property (nullable, nonatomic, retain) NSDate *createAt;
@property (nullable, nonatomic, retain) NSNumber *serviceId;
@property (nullable, nonatomic, retain) NSNumber *toothId;
@property (nullable, nonatomic, retain) NSDate *createdAt;
@property (nullable, nonatomic, retain) NSNumber *remoteId;
@property (nullable, nonatomic, retain) NSDate *updatedAt;
@property (nullable, nonatomic, retain) Therapy *therapy;

@end

NS_ASSUME_NONNULL_END
