//
//  Service+CoreDataProperties.h
//  DentaBook
//
//  Created by Denis Dubov on 06.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Service.h"

NS_ASSUME_NONNULL_BEGIN

@class CategoryTherapy;

@interface Service (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *cost;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSNumber *remoteId;
@property (nullable, nonatomic, retain) NSDate *createdAt;
@property (nullable, nonatomic, retain) NSDate *updatedAt;
@property (nullable, nonatomic, retain) CategoryTherapy *category;

@end

NS_ASSUME_NONNULL_END
