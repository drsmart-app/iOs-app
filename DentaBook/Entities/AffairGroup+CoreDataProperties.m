//
//  AffairGroup+CoreDataProperties.m
//  DentaBook
//
//  Created by Denis Dubov on 10.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "AffairGroup+CoreDataProperties.h"

@implementation AffairGroup (CoreDataProperties)

@dynamic createAt;
@dynamic title;
@dynamic createdAt;
@dynamic remoteId;
@dynamic updatedAt;
@dynamic affairs;
@dynamic user;

@end
