//
//  User+CoreDataProperties.h
//  DentaBook
//
//  Created by Denis Dubov on 05.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "User.h"

NS_ASSUME_NONNULL_BEGIN

@class AffairGroup;
@class Event;

@interface User (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *remoteId;
@property (nullable, nonatomic, retain) NSString *login;
@property (nullable, nonatomic, retain) NSDate *createdAt;
@property (nullable, nonatomic, retain) NSDate *updatedAt;
@property (nullable, nonatomic, retain) NSSet<AffairGroup *> *affair_groups;
@property (nullable, nonatomic, retain) NSSet<Event *> *events;
@property (nullable, nonatomic, retain) NSSet<Expense *> *expenses;

@end

@interface User (CoreDataGeneratedAccessors)

- (void)addAffair_groupsObject:(AffairGroup *)value;
- (void)removeAffair_groupsObject:(AffairGroup *)value;
- (void)addAffair_groups:(NSSet<AffairGroup *> *)values;
- (void)removeAffair_groups:(NSSet<AffairGroup *> *)values;

- (void)addEventsObject:(Event *)value;
- (void)removeEventsObject:(Event *)value;
- (void)addEvents:(NSSet<Event *> *)values;
- (void)removeEvents:(NSSet<Event *> *)values;

- (void)addExpensesObject:(Expense *)value;
- (void)removeExpensesObject:(Expense *)value;
- (void)addExpenses:(NSSet<Expense *> *)values;
- (void)removeExpenses:(NSSet<Expense *> *)values;

@end

NS_ASSUME_NONNULL_END
