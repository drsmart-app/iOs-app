//
//  Gallery+CoreDataProperties.m
//  DentaBook
//
//  Created by Denis Dubov on 12.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Gallery+CoreDataProperties.h"

@implementation Gallery (CoreDataProperties)

@dynamic createdAt;
@dynamic remoteId;
@dynamic updatedAt;
@dynamic smallPhoto;
@dynamic comment;
@dynamic createAt;
@dynamic patient;

@end
