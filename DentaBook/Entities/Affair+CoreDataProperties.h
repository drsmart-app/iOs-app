//
//  Affair+CoreDataProperties.h
//  DentaBook
//
//  Created by Denis Dubov on 10.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Affair.h"

NS_ASSUME_NONNULL_BEGIN

@interface Affair (CoreDataProperties)

@property (nullable, nonatomic, retain) NSDate *createAt;
@property (nullable, nonatomic, retain) NSDate *date;
@property (nullable, nonatomic, retain) NSNumber *isDone;
@property (nullable, nonatomic, retain) NSNumber *notifyDistance;
@property (nullable, nonatomic, retain) NSString *title;
@property (nullable, nonatomic, retain) NSDate *createdAt;
@property (nullable, nonatomic, retain) NSNumber *remoteId;
@property (nullable, nonatomic, retain) NSDate *updatedAt;
@property (nullable, nonatomic, retain) AffairGroup *affair_group;

@end

NS_ASSUME_NONNULL_END
