//
//  Patient+CoreDataProperties.h
//  DentaBook
//
//  Created by Denis Dubov on 17.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Patient.h"

NS_ASSUME_NONNULL_BEGIN

@class PatientIllnessRef, ToothList, Gallery;

@interface Patient (CoreDataProperties)

@property (nullable, nonatomic, retain) NSDate *createdAt;
@property (nullable, nonatomic, retain) NSString *firstName;
@property (nullable, nonatomic, retain) NSString *lastName;
@property (nullable, nonatomic, retain) NSString *middleName;
@property (nullable, nonatomic, retain) NSNumber *money;
@property (nullable, nonatomic, retain) NSString *phoneNumber;
@property (nullable, nonatomic, retain) NSString *patientId;
@property (nullable, nonatomic, retain) NSString *photoFileName;
@property (nullable, nonatomic, retain) NSNumber *rating;
@property (nullable, nonatomic, retain) NSNumber *remoteId;
@property (nullable, nonatomic, retain) NSDate *updatedAt;
@property (nullable, nonatomic, retain) NSNumber *birthDay;
@property (nullable, nonatomic, retain) NSNumber *birthMonth;
@property (nullable, nonatomic, retain) NSNumber *birthYear;
@property (nullable, nonatomic, retain) NSSet<Gallery *> *galleries;
@property (nullable, nonatomic, retain) NSSet<PatientIllnessRef *> *patientIllness;
@property (nullable, nonatomic, retain) NSSet<Payment *> *payments;
@property (nullable, nonatomic, retain) NSSet<Therapy *> *therapies;
@property (nullable, nonatomic, retain) NSSet<ToothList *> *tooth_list;

@end

@interface Patient (CoreDataGeneratedAccessors)

- (void)addGalleriesObject:(Gallery *)value;
- (void)removeGalleriesObject:(Gallery *)value;
- (void)addGalleries:(NSSet<Gallery *> *)values;
- (void)removeGalleries:(NSSet<Gallery *> *)values;

- (void)addPatientIllnessObject:(PatientIllnessRef *)value;
- (void)removePatientIllnessObject:(PatientIllnessRef *)value;
- (void)addPatientIllness:(NSSet<PatientIllnessRef *> *)values;
- (void)removePatientIllness:(NSSet<PatientIllnessRef *> *)values;

- (void)addPaymentsObject:(Payment *)value;
- (void)removePaymentsObject:(Payment *)value;
- (void)addPayments:(NSSet<Payment *> *)values;
- (void)removePayments:(NSSet<Payment *> *)values;

- (void)addTherapiesObject:(Therapy *)value;
- (void)removeTherapiesObject:(Therapy *)value;
- (void)addTherapies:(NSSet<Therapy *> *)values;
- (void)removeTherapies:(NSSet<Therapy *> *)values;

- (void)addTooth_listObject:(ToothList *)value;
- (void)removeTooth_listObject:(ToothList *)value;
- (void)addTooth_list:(NSSet<ToothList *> *)values;
- (void)removeTooth_list:(NSSet<ToothList *> *)values;

@end

NS_ASSUME_NONNULL_END
