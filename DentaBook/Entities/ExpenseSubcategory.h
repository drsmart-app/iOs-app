//
//  ExpenseSubcategory.h
//  DentaBook
//
//  Created by Denis Dubov on 27.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ExpenseCategory;

NS_ASSUME_NONNULL_BEGIN

@interface ExpenseSubcategory : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "ExpenseSubcategory+CoreDataProperties.h"
