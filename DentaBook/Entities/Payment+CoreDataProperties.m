//
//  Payment+CoreDataProperties.m
//  DentaBook
//
//  Created by Denis Dubov on 10.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Payment+CoreDataProperties.h"

@implementation Payment (CoreDataProperties)

@dynamic amount;
@dynamic createAt;
@dynamic remoteId;
@dynamic createdAt;
@dynamic updatedAt;
@dynamic patient;

@end
