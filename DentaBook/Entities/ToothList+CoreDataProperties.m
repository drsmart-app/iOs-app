//
//  ToothList+CoreDataProperties.m
//  DentaBook
//
//  Created by Denis Dubov on 08.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ToothList+CoreDataProperties.h"

@implementation ToothList (CoreDataProperties)

@dynamic comment;
@dynamic createAt;
@dynamic type;
@dynamic createdAt;
@dynamic updatedAt;
@dynamic remoteId;
@dynamic patient;
@dynamic tooths;

@end
