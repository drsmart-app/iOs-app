//
//  PatientIllnessRef+CoreDataProperties.h
//  DentaBook
//
//  Created by Denis Dubov on 30.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "PatientIllnessRef.h"

NS_ASSUME_NONNULL_BEGIN

@interface PatientIllnessRef (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *index;
@property (nullable, nonatomic, retain) Patient *patient;

@end

NS_ASSUME_NONNULL_END
