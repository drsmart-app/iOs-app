//
//  Therapy+CoreDataProperties.m
//  DentaBook
//
//  Created by Denis Dubov on 08.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Therapy+CoreDataProperties.h"

@implementation Therapy (CoreDataProperties)

@dynamic createAt;
@dynamic discount;
@dynamic isPlan;
@dynamic name;
@dynamic remoteId;
@dynamic createdAt;
@dynamic updatedAt;
@dynamic patient;
@dynamic therapyItems;

@end
