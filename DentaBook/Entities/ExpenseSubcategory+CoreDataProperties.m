//
//  ExpenseSubcategory+CoreDataProperties.m
//  DentaBook
//
//  Created by Denis Dubov on 11.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ExpenseSubcategory+CoreDataProperties.h"

@implementation ExpenseSubcategory (CoreDataProperties)

@dynamic createAt;
@dynamic remoteId;
@dynamic title;
@dynamic createdAt;
@dynamic updatedAt;
@dynamic category;

@end
