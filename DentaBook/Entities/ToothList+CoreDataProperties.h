//
//  ToothList+CoreDataProperties.h
//  DentaBook
//
//  Created by Denis Dubov on 08.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ToothList.h"

NS_ASSUME_NONNULL_BEGIN

@class Tooth;

@interface ToothList (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *comment;
@property (nullable, nonatomic, retain) NSDate *createAt;
@property (nullable, nonatomic, retain) NSNumber *type;
@property (nullable, nonatomic, retain) NSDate *createdAt;
@property (nullable, nonatomic, retain) NSDate *updatedAt;
@property (nullable, nonatomic, retain) NSNumber *remoteId;
@property (nullable, nonatomic, retain) Patient *patient;
@property (nullable, nonatomic, retain) NSSet<Tooth *> *tooths;

@end

@interface ToothList (CoreDataGeneratedAccessors)

- (void)addToothsObject:(Tooth *)value;
- (void)removeToothsObject:(Tooth *)value;
- (void)addTooths:(NSSet<Tooth *> *)values;
- (void)removeTooths:(NSSet<Tooth *> *)values;

@end

NS_ASSUME_NONNULL_END
