//
//  PatientIllnessRef.h
//  DentaBook
//
//  Created by Denis Dubov on 30.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Patient;

NS_ASSUME_NONNULL_BEGIN

@interface PatientIllnessRef : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "PatientIllnessRef+CoreDataProperties.h"
