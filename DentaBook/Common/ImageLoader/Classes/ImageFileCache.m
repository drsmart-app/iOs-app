//
//  ImageFileCache.m
//  WelpyApp
//
//  Created by Alexander Bukov on 3/10/15.
//  Copyright (c) 2015 Welpy Inc. All rights reserved.
//

#import "ImageFileCache.h"

#import <RNCryptor/RNCryptor.h>
#import <RNCryptor/RNDecryptor.h>
#import <RNCryptor/RNEncryptor.h>

#import "UIImage+Common.h"

//! Cache name(folder name)
static NSString* const kUNFileCacheName = @"com.welpy.default.FileCache";
static const NSInteger kDefaultCacheMaxCacheAge = 60 * 60 * 24 * 7; // 1 week
static const NSInteger kDefaultCacheMaxSize = 512 * 1000 * 1000; //! 512 MB

@implementation ImageFileCache

//! dispatch queue for loading images from file
static dispatch_queue_t cache_image_queue;
static dispatch_queue_t file_queue() {
    if (cache_image_queue == NULL) {
        cache_image_queue = dispatch_queue_create("com.welpy.file-cache", 0);
    }
    return cache_image_queue;
}

#pragma mark _______________________ Class Methods _________________________

#pragma mark ____________________________ Init _____________________________

- (id)init{
    
    self = [super init];
    if (self) {
        
        //! Setup default limitations
        self.maxCacheSize = kDefaultCacheMaxSize;
        self.maxCacheAge = kDefaultCacheMaxCacheAge;
        
        //! Cache folder mast be in Library/Caches and not by iCloud sync
        self.cachePath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches"]
                          stringByAppendingPathComponent:kUNFileCacheName];
        
        //! Create file cache path if necessray
        if (![[NSFileManager defaultManager] fileExistsAtPath:self.cachePath])
        {
            [[NSFileManager defaultManager] createDirectoryAtPath:self.cachePath
                                      withIntermediateDirectories:YES
                                                       attributes:nil
                                                            error:nil];
            //! Remove iCloud sync
            NSURL *url = [NSURL fileURLWithPath:self.cachePath];
            [self addSkipBackupAttributeToItemAtURL:url];
        }
    }
    return self;
}

#pragma mark _______________________ Privat Methods ________________________

//! Remove iCloud sync
- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL*)URL
{
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
}

#pragma mark _______________________ Public Methods ________________________

//! Search image and return result
- (void)imageForKey:(NSString*)md5Hash
            didFind:(void(^)(ImageFileCache* cache, UIImage* image, NSString* md5Hash))find
    didFindOriginal:(void(^)(ImageFileCache* cache, UIImage* image, NSString* md5Hash))findOriginal
          didNoFind:(void(^)(ImageFileCache* cache))notFind{
    
    NSString* fullFilePath = [self.cachePath stringByAppendingPathComponent:md5Hash];

    //! Using global queue to fix non concurent performance
    dispatch_queue_t global = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(global, ^{
        
        @autoreleasepool {
            
            if ([[NSFileManager defaultManager] fileExistsAtPath:fullFilePath]){
                
                NSData* imageData = [NSData dataWithContentsOfFile:fullFilePath];
                
                //! Decrypt image data
                NSError *error = nil;

                NSData *decryptedData = [RNDecryptor decryptData:imageData withPassword:@"aPassword" error:&error];

                if (error) {
                    NSLog(@"ERROR: %@", error);
                }
                
                //! Create and decompress image
                UIImage* image = [UIImage imageWithData:decryptedData];
                image = [UIImage decodedImageWithImage:image];
                
                if (image) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        find(self, image, md5Hash);
                    });
                }else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        notFind(self);
                    });
                }
                
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    notFind(self);
                });
            }
        }
    });
}

//! Method to cache image
- (void)cacheImage:(UIImage*)image forKey:(NSString *)key{
    
    if (!image || !key) {
        NSLog(@"Can't cache image to file. Image or key are mising");
        return;
    }
    
    dispatch_async(file_queue(), ^{
        @autoreleasepool {
            NSString* fullFilePath = [self.cachePath stringByAppendingPathComponent:key];
            
            //! Get image data and encrypt it
            NSData* imageData = UIImagePNGRepresentation(image);

            NSError *error = nil;

            NSData *encryptedData = [RNEncryptor encryptData:imageData
                                                withSettings:kRNCryptorAES256Settings
                                                    password:@"aPassword"
                                                       error:&error];
            [encryptedData writeToFile:fullFilePath atomically:YES];
        }
    });
    
}

- (void)deleteImageForKey:(NSString *)key   {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError* error = nil;
    
    if (!key) {
        NSLog(@"Can't find cache");
        return;
    }
    
    NSString* fullFilePath = [self.cachePath stringByAppendingPathComponent:key];
    
    BOOL success = [fileManager removeItemAtPath:fullFilePath error:&error];
    if (!success || error) {
        NSLog(@"Can't remove file when cleare file cache. Error = %@", error);
    }
}


//! Method remove all files from cache folder
- (void)removeAllFiles{
    
    NSFileManager* fileManager = [NSFileManager defaultManager];
    NSError* error = nil;
    
    NSArray* filesArray = [fileManager contentsOfDirectoryAtPath:self.cachePath error:&error];
    
    for (NSString* file in filesArray) {
        BOOL success = [fileManager removeItemAtPath:[NSString stringWithFormat:@"%@/%@", self.cachePath, file] error:&error];
        if (!success || error) {
            NSLog(@"Can't remove file when cleare file cache. Error = %@", error);
        }
    }
}


//! Get cached files count and size
- (void)calculateSizeWithCompletionBlock:(void (^)(NSUInteger fileCount, NSUInteger totalSize))completionBlock {
    
    NSURL *diskCacheURL = [NSURL fileURLWithPath:self.cachePath isDirectory:YES];
    NSFileManager* fileManager = [NSFileManager defaultManager];
    
    dispatch_async(file_queue(), ^{
        NSUInteger fileCount = 0;
        NSUInteger totalSize = 0;
        
        NSDirectoryEnumerator *fileEnumerator = [fileManager enumeratorAtURL:diskCacheURL
                                                   includingPropertiesForKeys:@[NSFileSize]
                                                                      options:NSDirectoryEnumerationSkipsHiddenFiles
                                                                 errorHandler:NULL];
        
        for (NSURL *fileURL in fileEnumerator) {
            NSNumber *fileSize;
            [fileURL getResourceValue:&fileSize forKey:NSURLFileSizeKey error:NULL];
            totalSize += [fileSize unsignedIntegerValue];
            fileCount += 1;
        }
        
        if (completionBlock) {
            dispatch_async(dispatch_get_main_queue(), ^{
                completionBlock(fileCount, totalSize);
            });
        }
    });
}


//! Clean cache in background task
- (void)backgroundCleanDisk {
    UIApplication *application = [UIApplication sharedApplication];
    __block UIBackgroundTaskIdentifier bgTask = [application beginBackgroundTaskWithExpirationHandler:^{
        // Clean up any unfinished task business by marking where you
        // stopped or ending the task outright.
        [application endBackgroundTask:bgTask];
        bgTask = UIBackgroundTaskInvalid;
    }];
    
    // Start the long-running task and return immediately.
    [self cleanDiskWithCompletionBlock:^{
        [application endBackgroundTask:bgTask];
        bgTask = UIBackgroundTaskInvalid;
    }];
}

//! Clean cache asyncronously
- (void)cleanDiskWithCompletionBlock:(void(^)())completionBlock {
    NSFileManager* fileManager = [NSFileManager defaultManager];
    
    dispatch_async(file_queue(), ^{
        NSURL *diskCacheURL = [NSURL fileURLWithPath:self.cachePath isDirectory:YES];
        NSArray *resourceKeys = @[NSURLIsDirectoryKey, NSURLContentModificationDateKey, NSURLTotalFileAllocatedSizeKey];
        
        // This enumerator prefetches useful properties for our cache files.
        NSDirectoryEnumerator *fileEnumerator = [fileManager enumeratorAtURL:diskCacheURL
                                                   includingPropertiesForKeys:resourceKeys
                                                                      options:NSDirectoryEnumerationSkipsHiddenFiles
                                                                 errorHandler:NULL];
        
        NSDate *expirationDate = [NSDate dateWithTimeIntervalSinceNow:-self.maxCacheAge];
        NSMutableDictionary *cacheFiles = [NSMutableDictionary dictionary];
        NSUInteger currentCacheSize = 0;
        
        // Enumerate all of the files in the cache directory.  This loop has two purposes:
        //
        //  1. Removing files that are older than the expiration date.
        //  2. Storing file attributes for the size-based cleanup pass.
        NSMutableArray *urlsToDelete = [[NSMutableArray alloc] init];
        for (NSURL *fileURL in fileEnumerator) {
            NSDictionary *resourceValues = [fileURL resourceValuesForKeys:resourceKeys error:NULL];
            
            // Skip directories.
            if ([resourceValues[NSURLIsDirectoryKey] boolValue]) {
                continue;
            }
            
            // Remove files that are older than the expiration date;
            NSDate *modificationDate = resourceValues[NSURLContentModificationDateKey];
            if ([[modificationDate laterDate:expirationDate] isEqualToDate:expirationDate]) {
                [urlsToDelete addObject:fileURL];
                continue;
            }
            
            // Store a reference to this file and account for its total size.
            NSNumber *totalAllocatedSize = resourceValues[NSURLTotalFileAllocatedSizeKey];
            currentCacheSize += [totalAllocatedSize unsignedIntegerValue];
            [cacheFiles setObject:resourceValues forKey:fileURL];
        }
        
        for (NSURL *fileURL in urlsToDelete) {
            [fileManager removeItemAtURL:fileURL error:nil];
        }
        
        // If our remaining disk cache exceeds a configured maximum size, perform a second
        // size-based cleanup pass.  We delete the oldest files first.
        if (self.maxCacheSize > 0 && currentCacheSize > self.maxCacheSize) {
            // Target half of our maximum cache size for this cleanup pass.
            const NSUInteger desiredCacheSize = self.maxCacheSize / 2;
            
            // Sort the remaining cache files by their last modification time (oldest first).
            NSArray *sortedFiles = [cacheFiles keysSortedByValueWithOptions:NSSortConcurrent
                                                            usingComparator:^NSComparisonResult(id obj1, id obj2) {
                                                                return [obj1[NSURLContentModificationDateKey] compare:obj2[NSURLContentModificationDateKey]];
                                                            }];
            
            // Delete files until we fall below our desired cache size.
            for (NSURL *fileURL in sortedFiles) {
                if ([fileManager removeItemAtURL:fileURL error:nil]) {
                    NSDictionary *resourceValues = cacheFiles[fileURL];
                    NSNumber *totalAllocatedSize = resourceValues[NSURLTotalFileAllocatedSizeKey];
                    currentCacheSize -= [totalAllocatedSize unsignedIntegerValue];
                    
                    if (currentCacheSize < desiredCacheSize) {
                        break;
                    }
                }
            }
        }
        if (completionBlock) {
            dispatch_async(dispatch_get_main_queue(), ^{
                completionBlock();
            });
        }
    });
}

@end
