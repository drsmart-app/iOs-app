//
//  UIImage+Common.m
//  WelpyApp
//
//  Created by Alexander Bukov on 3/10/15.
//  Copyright (c) 2015 Welpy Inc. All rights reserved.
//

#import "UIImage+Common.h"

@implementation UIImage (Common)

//! Scale method
- (UIImage*)scaledImageWithScaleValue:(CGFloat)scaleValue{
    
    CGSize size = CGSizeMake(self.size.width * scaleValue, self.size.height * scaleValue);
    
    return [self imageResizedToSize:size];

}

//! Resize image
- (UIImage*)imageResizedToSize:(CGSize)size
{
    // Check for Retina display and then double the size of image (we assume size is in points)
    if ([[UIScreen mainScreen] respondsToSelector: @selector(scale)])
    {
        CGFloat scale = [[UIScreen mainScreen] scale];
        
        size.width  *= scale;
        size.height *= scale;
    }
    
    // Create context on which image will be drawn
    UIGraphicsBeginImageContext(size);
    
    // Draw image on this context used provided size
    [self drawInRect: CGRectMake(0, 0, size.width, size.height)];
    
    // Convert context to an image
    UIImage* resizedImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // Remove context
    UIGraphicsEndImageContext();
    
    return resizedImage;
}


//! Add corner radius to image
- (UIImage *)imageWithRoundedCornersSize:(float)cornerRadius
{
    CGRect frame = CGRectMake(0, 0, self.size.width, self.size.height);
    
    // Begin a new image that will be the new image with the rounded corners
    // (here with the size of an UIImageView)
    UIGraphicsBeginImageContextWithOptions(self.size, NO, 2.0);
    
    // Add a clip before drawing anything, in the shape of an rounded rect
    [[UIBezierPath bezierPathWithRoundedRect:frame
                                cornerRadius:cornerRadius] addClip];
    // Draw your image
    [self drawInRect:frame];
    
    // Get the image, here setting the UIImageView image
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    // Lets forget about that we were drawing
    UIGraphicsEndImageContext();
    
    return image;
}


//! Crop image to square size
- (UIImage *)imageByCroppingToSquare{
    
    CGFloat min = self.size.width > self.size.height ? self.size.height : self.size.width;
    
    double x = (self.size.width - min) / 2.0;
    double y = (self.size.height - min) / 2.0;
    
    CGRect cropRect = CGRectMake(x, y, min, min);
    CGImageRef imageRef = CGImageCreateWithImageInRect([self CGImage], cropRect);
    
    UIImage *cropped = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    return cropped;
}


//! Decompress image to fix performance
+ (UIImage *)decodedImageWithImage:(UIImage *)image {
    if (image.images) {
        // Do not decode animated images
        return image;
    }
    
    CGImageRef imageRef = image.CGImage;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imageRef), CGImageGetHeight(imageRef));
    CGRect imageRect = (CGRect){.origin = CGPointZero, .size = imageSize};
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGBitmapInfo bitmapInfo = CGImageGetBitmapInfo(imageRef);
    
    int infoMask = (bitmapInfo & kCGBitmapAlphaInfoMask);
    BOOL anyNonAlpha = (infoMask == kCGImageAlphaNone ||
                        infoMask == kCGImageAlphaNoneSkipFirst ||
                        infoMask == kCGImageAlphaNoneSkipLast);
    
    // CGBitmapContextCreate doesn't support kCGImageAlphaNone with RGB.
    // https://developer.apple.com/library/mac/#qa/qa1037/_index.html
    if (infoMask == kCGImageAlphaNone && CGColorSpaceGetNumberOfComponents(colorSpace) > 1) {
        // Unset the old alpha info.
        bitmapInfo &= ~kCGBitmapAlphaInfoMask;
        
        // Set noneSkipFirst.
        bitmapInfo |= kCGImageAlphaNoneSkipFirst;
    }
    // Some PNGs tell us they have alpha but only 3 components. Odd.
    else if (!anyNonAlpha && CGColorSpaceGetNumberOfComponents(colorSpace) == 3) {
        // Unset the old alpha info.
        bitmapInfo &= ~kCGBitmapAlphaInfoMask;
        bitmapInfo |= kCGImageAlphaPremultipliedFirst;
    }
    
    // It calculates the bytes-per-row based on the bitsPerComponent and width arguments.
    CGContextRef context = CGBitmapContextCreate(NULL,
                                                 imageSize.width,
                                                 imageSize.height,
                                                 CGImageGetBitsPerComponent(imageRef),
                                                 0,
                                                 colorSpace,
                                                 bitmapInfo);
    CGColorSpaceRelease(colorSpace);
    
    // If failed, return undecompressed image
    if (!context) return image;
    
    CGContextDrawImage(context, imageRect, imageRef);
    CGImageRef decompressedImageRef = CGBitmapContextCreateImage(context);
    
    CGContextRelease(context);
    
    UIImage *decompressedImage = [UIImage imageWithCGImage:decompressedImageRef scale:image.scale orientation:image.imageOrientation];
    CGImageRelease(decompressedImageRef);
    return decompressedImage;
}


@end
