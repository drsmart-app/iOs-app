//
//  ImageCacheManager.h
//  WelpyApp
//
//  Created by Alexander Bukov on 3/10/15.
//  Copyright (c) 2015 Welpy Inc. All rights reserved.
//

#import "ImageCache.h"
#import "ImageFileCache.h"

@interface ImageCacheManager : NSObject

//! Default caches
@property (strong, nonatomic) ImageFileCache* defaultFileCache;
@property (strong, nonatomic) ImageCache* defaultMemoryCach;

+ (ImageCacheManager*)shared;

@end