//
//  ImageCache.h
//  WelpyApp
//
//  Created by Alexander Bukov on 3/10/15.
//  Copyright (c) 2015 Welpy Inc. All rights reserved.
//

#import "UIKit/UIKit.h"

@class ImageRequestModel;
//! Memory cache class interface.
@interface ImageCache : NSCache


- (void)imageForKey:(NSString*)key
            didFind:(void(^)(ImageCache* cache, UIImage* image))find
    didFindOriginal:(void(^)(ImageCache* cache, UIImage* image))findOriginal
          didNoFind:(void(^)(ImageCache* cache))notFind;

- (void)cacheImage:(UIImage*)image forKey:(NSString*)key;
- (void)deleteImageForKey:(NSString*)key;

@end
