//
//  ImageCache.m
//  WelpyApp
//
//  Created by Alexander Bukov on 3/10/15.
//  Copyright (c) 2015 Welpy Inc. All rights reserved.
//

#import "ImageCache.h"
#import "ImageRequestModel.h"

//! Memory cache implementation
@implementation ImageCache

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)imageForKey:(NSString*)key
            didFind:(void(^)(ImageCache* cache, UIImage* image))find
    didFindOriginal:(void(^)(ImageCache* cache, UIImage* image))findOriginal
          didNoFind:(void(^)(ImageCache* cache))notFind{
    
    UIImage* image = [self objectForKey:key];
    //image = nil;
    if (image) {
        find(self ,image);
    }else{
        notFind(self);
    }
}



- (void)cacheImage:(UIImage*)image forKey:(NSString*)key{
    //return;
    [self setObject:image forKey:key];
}

- (void)deleteImageForKey:(NSString*)key{
    [self removeObjectForKey:key];
}

@end
