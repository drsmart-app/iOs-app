//
//  ImageNotificationDownloader.h
//  WelpyApp
//
//  Created by Alexander Bukov on 3/10/15.
//  Copyright (c) 2015 Welpy Inc. All rights reserved.
//

#import "ImageRequestModel.h"

@interface ImageDownloader : NSObject

+ (ImageDownloader*)shared;

//! Check is image olready loading
- (BOOL)isLoadingImageForHash:(NSString*)md5Hash;

//! Check loading status for url
- (BOOL)isLoadingImagesForUrlString:(NSString*)urlString;


//! Start load with parameters
- (void)startLoadForUrlString:(NSString*)urlString
              timeOutInterval:(NSTimeInterval)timeOutInterval
                      md5Hash:(NSString*)md5Hash
                  scaleToSize:(CGSize)scaleToSize
              imageSizePolicy:(ImageSizePolicy)imageSizePolicy
                 cornerRadius:(CGFloat)cornerRadius
                  memoryCache:(ImageCache*)memoryCache
                    fileCache:(ImageFileCache*)fileCache
                    isRounded:(BOOL)isRounded
             imageCachePolicy:(ImageCachePolicy)imageCachePolicy
                queuePriority:(NSOperationQueuePriority)queuePriority
                          key:(NSString*)key;

//! Update pririty for load operation with url string
- (void)setPriority:(NSOperationQueuePriority)queuePriority forUrlString:(NSString*)urlString;

//! Get loading progress for hash
- (CGFloat)loadingProgressForHash:(NSString*)md5Hash;

//! Cancel methods
- (void)cancelAllLoadingOperations;
- (void)cancelAllLoadingOperationsWithKey:(NSString*)key;

@end