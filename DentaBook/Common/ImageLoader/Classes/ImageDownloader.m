//
//  ImageNotificationDownloader.m
//  WelpyApp
//
//  Created by Alexander Bukov on 3/10/15.
//  Copyright (c) 2015 Welpy Inc. All rights reserved.
//

#import "ImageDownloader.h"
#import "ImageRequestModel.h"
#import "LoadOperation.h"
#import "ImageCache.h"
#import "ImageFileCache.h"
#import "UIImage+Common.h"

const static NSInteger defaultMaxImageLoadOperations = 8;

@interface ImageDownloader()

//! Max concurent operations
@property (assign, nonatomic) NSInteger maxImageLoadOperations;

//! Images load queue
@property (strong, nonatomic) NSOperationQueue* imageLoadOperationQueue;

//! For fast search operation by keys
@property (strong, nonatomic) NSMutableDictionary* loadingImagesHeshToOperationDictionary;
@property (strong, nonatomic) NSMutableDictionary* loadingImagesUrlStringToOperationDictionary;
@property (strong, nonatomic) NSMutableDictionary* keyToOperationDictionary;

@end

@implementation ImageDownloader
#pragma mark _______________________ Class Methods _________________________

#pragma mark - Shared Instance and Init
+ (ImageDownloader *)shared {
    
    static ImageDownloader *shared = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        shared = [[self alloc] init];
    });
    
    return shared;
}

#pragma mark ____________________________ Init _____________________________

-(id)init{
    
    self = [super init];
    if (self) {
   		// your initialization here
        
        //! Setup default values
        self.maxImageLoadOperations = defaultMaxImageLoadOperations;
        self.imageLoadOperationQueue = [NSOperationQueue new];
        self.imageLoadOperationQueue.maxConcurrentOperationCount = self.maxImageLoadOperations;
        
        self.loadingImagesHeshToOperationDictionary = [NSMutableDictionary new];
        self.loadingImagesUrlStringToOperationDictionary = [NSMutableDictionary new];
        self.keyToOperationDictionary = [NSMutableDictionary new];


    }
    return self;
}

#pragma mark _______________________ Privat Methods ________________________

//! Return request for image downloading
- (NSMutableURLRequest*)requestForModel:(ImageRequestModel*)model{
    
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:model.urlString]
                                                                cachePolicy:NSURLRequestReloadIgnoringCacheData
                                                            timeoutInterval:model.timeOutInterval];
    return request;
}

#pragma mark _______________________ Delegates _____________________________



#pragma mark _______________________ Public Methods ________________________

//! Check loading status for hash
- (BOOL)isLoadingImageForHash:(NSString*)md5Hash{
    
    if ([self.loadingImagesHeshToOperationDictionary objectForKey:md5Hash]) {
        return YES;
    };
    return NO;
}

//! Check loading status for url
- (BOOL)isLoadingImagesForUrlString:(NSString*)urlString{
    
    if ([self.loadingImagesUrlStringToOperationDictionary objectForKey:urlString]) {
        return YES;
    };
    return NO;
}

//! Update pririty for load operation with url string
- (void)setPriority:(NSOperationQueuePriority)queuePriority forUrlString:(NSString*)urlString{
    LoadOperation* operation = [self.loadingImagesUrlStringToOperationDictionary objectForKey:urlString];
    operation.queuePriority = queuePriority;
}

//! Get loading progress for hash
- (CGFloat)loadingProgressForHash:(NSString*)md5Hash{
    LoadOperation* operation = [self.loadingImagesHeshToOperationDictionary objectForKey:md5Hash];
    return operation.progress;
}

//! Start load with parameters
- (void)startLoadForUrlString:(NSString*)urlString
              timeOutInterval:(NSTimeInterval)timeOutInterval
                      md5Hash:(NSString*)md5Hash
                  scaleToSize:(CGSize)scaleToSize
              imageSizePolicy:(ImageSizePolicy)imageSizePolicy
                 cornerRadius:(CGFloat)cornerRadius
                  memoryCache:(ImageCache*)memoryCache
                    fileCache:(ImageFileCache*)fileCache
                    isRounded:(BOOL)isRounded
             imageCachePolicy:(ImageCachePolicy)imageCachePolicy
                queuePriority:(NSOperationQueuePriority)queuePriority
                          key:(NSString*)key

{
    

    
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]
                                                                cachePolicy:NSURLRequestReloadIgnoringCacheData
                                                            timeoutInterval:timeOutInterval];    
    //NSLog(@"Loading for hash = %@", md5Hash);
    
    //! NSOperation for image loading
    LoadOperation* operation = [[LoadOperation alloc] initWithRequest:request];
    operation.queuePriority = queuePriority;
    
    operation.md5Hash = md5Hash;
    
    //! processingBlock for image transformation
    operation.processingBlock = ^UIImage*(LoadOperation* op, NSData* connectionData){
        //(operation background thread)
        
        UIImage* image = [UIImage imageWithData:connectionData];
        
        if (image == nil) {
            return nil;
        }
        
        UIImage* resImage = nil;
        
        CGFloat horizontalRatio = scaleToSize.width / image.size.width;
        CGFloat verticalRatio = scaleToSize.height / image.size.height;
        
        //! Resize image if necessary
        switch (imageSizePolicy) {
            case ImageSizePolicyOriginalSize:
                resImage = image;
                break;
            case ImageSizePolicyResizeToRect:
                resImage = [image imageResizedToSize:scaleToSize];
                break;
            case ImageSizePolicyScaleAspectFill:;
                resImage = [image scaledImageWithScaleValue:MAX(horizontalRatio, verticalRatio)];
                break;
                
            case ImageSizePolicyScaleAspectFit:;
                resImage = [image scaledImageWithScaleValue:MIN(horizontalRatio, verticalRatio)];
                break;
            case ImageSizePolicyCropSquare:;
                resImage = [image imageByCroppingToSquare];
                break;
                
            default:
                break;
        }
        
        //! Create rounded image or image with corner radius
        if (isRounded) {
            
            resImage = [resImage imageByCroppingToSquare];
            resImage = [resImage imageWithRoundedCornersSize:resImage.size.width / 2];
            
        }else if (cornerRadius > 0){
            resImage = [resImage imageWithRoundedCornersSize:cornerRadius];
        }
        
        //! Decode image to fix performace
        resImage = [UIImage decodedImageWithImage:resImage];
        
        //! Save to caches if necessary
        if (imageCachePolicy != ImageCachePolicyIgnoreCache) {
            //! Save image to memory cache if necessary
            [memoryCache cacheImage:resImage forKey:md5Hash];
        }
        
        if (imageCachePolicy == ImageCachePolicyMemoryAndFileCache) {
            //! Save image to file cache if necessary
            [fileCache cacheImage:resImage forKey:md5Hash];
        }
        
        
        return resImage;
    };
    
    
    operation.successBlock = ^(LoadOperation* op, UIImage* image){
        
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@_success", md5Hash]
                                                            object:image];
        
        //! Remove operation from loading list
        [self.loadingImagesHeshToOperationDictionary removeObjectForKey:md5Hash];
        [self.loadingImagesUrlStringToOperationDictionary removeObjectForKey:urlString];
        [self removeOperation:op forKey:key];
        
        //NSLog(@"Loaded for hash = %@", md5Hash);
        
    };
    
    operation.failureBlock = ^(LoadOperation* op, NSError* error){
        
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@_error", md5Hash]
                                                            object:error];
        
        //! Remove operation from loading list
        [self.loadingImagesHeshToOperationDictionary removeObjectForKey:md5Hash];
        [self.loadingImagesUrlStringToOperationDictionary removeObjectForKey:urlString];
        [self removeOperation:op forKey:key];
    
        //NSLog(@"Failed for hash = %@", md5Hash);
    };
    
    operation.progressBlock = ^(LoadOperation* op, float progressValue){
        
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@_progress", md5Hash]
                                                            object:[NSNumber numberWithFloat:progressValue]];
    };
    
    operation.cancelBlock = ^(LoadOperation* op){
        NSLog(@"Operation canceled");
        
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@_canceled", md5Hash]
                                                            object:op];
        
        //! Remove operation from loading list
        [self.loadingImagesHeshToOperationDictionary removeObjectForKey:md5Hash];
        [self.loadingImagesUrlStringToOperationDictionary removeObjectForKey:urlString];
        [self removeOperation:op forKey:key];
    };
    
    //! Add operation to loading list
    [self.loadingImagesHeshToOperationDictionary setObject:operation forKey:md5Hash];
    [self.loadingImagesUrlStringToOperationDictionary setObject:operation forKey:urlString];
    [self addOperation:operation forKey:key];
    
    //! Add to queue
    [self.imageLoadOperationQueue addOperation:operation];
}

- (void)addOperation:(LoadOperation*)operation forKey:(NSString*)key{
    if (!key || !operation) {
        return;
    }
    
    NSMutableArray* operationsWithKey = [self.keyToOperationDictionary objectForKey:key];
    if (!operationsWithKey) {
        operationsWithKey = [NSMutableArray new];
    }
    [operationsWithKey addObject:operation];
    [self.keyToOperationDictionary setObject:operationsWithKey forKey:key];
}

- (void)removeOperation:(LoadOperation*)operation forKey:(NSString*)key{
    if (!key || !operation) {
        return;
    }
    
    NSMutableArray* operationsWithKey = [self.keyToOperationDictionary objectForKey:key];
    if (!operationsWithKey) {
        return;
    }
    [operationsWithKey removeObject:operation];
    [self.keyToOperationDictionary setObject:operationsWithKey forKey:key];
}

//! Cancell all operations
- (void)cancelAllLoadingOperations{
    for (NSString* key in self.loadingImagesUrlStringToOperationDictionary.allKeys) {
        LoadOperation* loadOperation = [self.loadingImagesUrlStringToOperationDictionary objectForKey:key];
        [loadOperation cancel];
    }
}

//! Cancel operations with group key
- (void)cancelAllLoadingOperationsWithKey:(NSString*)key{
    
    if (!key) {
        return;
    }
    
    NSMutableArray* operationsWithKey = [self.keyToOperationDictionary objectForKey:key];
    if (!operationsWithKey) {
        return;
    }
    
    NSMutableArray* temp = [NSMutableArray arrayWithArray:operationsWithKey];
    for (LoadOperation* loadOperation in temp) {
        [loadOperation cancel];
    }
    
    [self.keyToOperationDictionary removeObjectForKey:key];

}

#pragma mark _______________________ Notifications _________________________

@end
