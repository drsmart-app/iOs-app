//
//  UIImageView+ImageDownloader.m
//  WelpyApp
//
//  Created by Alexander Bukov on 3/10/15.
//  Copyright (c) 2015 Welpy Inc. All rights reserved.
//

#import "UIImageView+ImageDownloader.h"
#import <objc/runtime.h>

//! Keys for associate objects
static char kImageModelAssiciareObjectKey;
static char kProgressViewAssiciareObjectKey;
static char kSpinnerViewAssiciareObjectKey;
static char kButtonAssiciareObjectKey;
static char kButtonActionBlockAssiciareObjectKey;

//! Cover button call back block
typedef void (^CoverButtonActionBlock)(UIImageView* sender, UIButton* button);

//! Declaration of associated objects
@interface  UIImageView (_ImageDownloader)
@property (readwrite, strong, nonatomic) ImageRequestModel* imageModel;
@property (readwrite, strong, nonatomic) UIProgressView *progressView;
@property (readwrite, strong, nonatomic) UIActivityIndicatorView *spinnerView;
@property (readwrite, strong, nonatomic) UIButton *coverButton;
@property (readwrite, copy, nonatomic) CoverButtonActionBlock coverButtonActionBlock;

@end

@implementation UIImageView (_ImageDownloader)
@dynamic imageModel;
@dynamic progressView;
@dynamic spinnerView;
@dynamic coverButton;
@dynamic coverButtonActionBlock;
@end

@implementation UIImageView (ImageDownloader)

#pragma mark - Set/Get
- (void)setImageModel:(ImageRequestModel*)imageModel{
    objc_setAssociatedObject(self, &kImageModelAssiciareObjectKey, imageModel, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (ImageRequestModel*)imageModel{
    if ((ImageRequestModel*)objc_getAssociatedObject(self, &kImageModelAssiciareObjectKey) == nil) {
        [self createImageModel];
    }
    return (ImageRequestModel*)objc_getAssociatedObject(self, &kImageModelAssiciareObjectKey);
}


- (void)setProgressView:(UIProgressView *)progressView{
    objc_setAssociatedObject(self, &kProgressViewAssiciareObjectKey, progressView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (UIProgressView*)progressView{
    return (UIProgressView*)objc_getAssociatedObject(self, &kProgressViewAssiciareObjectKey);
}


- (void)setSpinnerView:(UIActivityIndicatorView *)spinnerView{
    objc_setAssociatedObject(self, &kSpinnerViewAssiciareObjectKey, spinnerView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (UIActivityIndicatorView*)spinnerView{
    return (UIActivityIndicatorView*)objc_getAssociatedObject(self, &kSpinnerViewAssiciareObjectKey);
}


- (void)setCoverButton:(UIButton *)coverButton{
    objc_setAssociatedObject(self, &kButtonAssiciareObjectKey, coverButton, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (UIButton*)coverButton{
    return (UIButton*)objc_getAssociatedObject(self, &kButtonAssiciareObjectKey);
}


- (void)setCoverButtonActionBlock:(void (^)(UIImageView *, UIButton *))coverButtonActionBlock{
    objc_setAssociatedObject(self, &kButtonActionBlockAssiciareObjectKey,
                             nil, OBJC_ASSOCIATION_COPY_NONATOMIC);
    objc_setAssociatedObject(self, &kButtonActionBlockAssiciareObjectKey,
                             coverButtonActionBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
- (CoverButtonActionBlock)coverButtonActionBlock{
    return (CoverButtonActionBlock)objc_getAssociatedObject(self, &kButtonActionBlockAssiciareObjectKey);
}
#pragma mark

//! Setup image model
- (void)createImageModel{
    
    ImageRequestModel* imageModel = [[ImageRequestModel alloc] init];
    self.imageModel = imageModel;
}

//! Base method for loading image (from memory cache, file cache or network)
- (void)setImageWithURLString:(NSString*)urlString
                          success:(void (^)(ImageRequestModel* object, UIImage* image))success
                       failure:(void (^)(ImageRequestModel* object, NSError* error))failure
                      progress:(void (^)(ImageRequestModel* object, CGFloat progress))progress{

    __weak UIImageView* wSelf = self;
    
    self.imageModel.successBlock = ^(ImageRequestModel* object, UIImage* image){
        wSelf.image = image;
        wSelf.progressView.hidden = YES;
        wSelf.spinnerView.hidden = YES;
        [wSelf.spinnerView stopAnimating];
        
        if (success) {
            success(object, image);
        }
    };
    self.imageModel.failureBlock = ^(ImageRequestModel* object, NSError* error){
        wSelf.image = wSelf.imageModel.failureImage;
        wSelf.progressView.hidden = YES;
        wSelf.spinnerView.hidden = YES;
        [wSelf.spinnerView stopAnimating];
        
        if (failure) {
            failure(object, error);
        }
    };
    
    self.imageModel.progressBlock = ^(ImageRequestModel* object, CGFloat progressValue){
        
        wSelf.progressView.progress = progressValue;
        
        if (progress) {
            progress(object, progressValue);
        }
    };
    
    self.imageModel.placeholderBlock = ^(ImageRequestModel* object, UIImage* image){
        wSelf.image = image;
        wSelf.spinnerView.hidden = NO;
        [wSelf.spinnerView startAnimating];
    };
    
    self.imageModel.startedLoadingBlock = ^(ImageRequestModel* object, UIImage* image){
        wSelf.image = image;
        wSelf.progressView.hidden = NO;
        wSelf.progressView.progress = 0;
        wSelf.spinnerView.hidden = NO;
        [wSelf.spinnerView startAnimating];
    };
    
    self.imageModel.canceledLoadingBlock = ^(ImageRequestModel* object, UIImage* image){
        wSelf.progressView.hidden = YES;
        wSelf.spinnerView.hidden = YES;
        [wSelf.spinnerView stopAnimating];
        
        wSelf.image = image;
    };

    
    self.imageModel.urlString = urlString;
    
    //NSLog(@"Category hash = %@", self.imageModel.md5Hash);
    
    [self.imageModel start];
    
}

//! Method create progress view.
- (void)addProgressView{
    
    //! Create progress view once for UIImageView object
    if ([self progressView] == nil) {
        UIProgressView* progressView = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
        progressView.frame = CGRectMake(0, self.frame.size.height - self.frame.size.height / 10, self.frame.size.width - self.frame.size.width / 10, 9);
        progressView.center = CGPointMake(CGRectGetMidX(self.bounds), progressView.center.y);
        progressView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth;
        [self addSubview:progressView];
        self.progressView = progressView;
    }
}

//! Method create spinner view.
- (void)addSpinnerView{
    
    //! Create spiner view once for UIImageView object
    if ([self spinnerView] == nil) {
        UIActivityIndicatorView* spinnerView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        spinnerView.center = self.center;
        spinnerView.hidden = YES;
        self.spinnerView = spinnerView;
        self.spinnerView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        [self addSubview:spinnerView];
    }
}

//! Method create cover button.
- (void)addCoverButton{
    
    //! Create button once for UIImageView object
    if ([self coverButton] == nil) {
        UIButton* coverButton = [UIButton new];
        coverButton.frame = self.bounds;
        coverButton.center = self.center;
        self.coverButton = coverButton;
        [coverButton addTarget:self action:@selector(coverButtonClicked:)
              forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:coverButton];
        self.userInteractionEnabled = YES;
    }
}

//! Cover button action
- (void)coverButtonClicked:(UIButton*)sender{
    if (self.coverButtonActionBlock) {
        self.coverButtonActionBlock(self, sender);
    }
}

@end
