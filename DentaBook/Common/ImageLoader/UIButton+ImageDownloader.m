//
//  UIButton+ImageDownloader.m
//  WelpyApp
//
//  Created by Denis Dubov on 14.05.15.
//  Copyright (c) 2015 Welpy Inc. All rights reserved.
//

#import "UIButton+ImageDownloader.h"
#import <objc/runtime.h>

//! Keys for associate objects
static char kButtonImageSizeKey;
static char kImageModelAssiciareObjectKey;
static char kProgressViewAssiciareObjectKey;
static char kSpinnerViewAssiciareObjectKey;
static char kButtonAssiciareObjectKey;
static char kButtonActionBlockAssiciareObjectKey;

//! Cover button call back block
typedef void (^CoverButtonActionBlock)(UIView* sender, UIButton* button);

//! Declaration of associated objects
@interface  UIButton (_ImageDownloader)
@property (readwrite, strong, nonatomic) ImageRequestModel* imageModel;
@property (readwrite, strong, nonatomic) UIProgressView *progressView;
@property (readwrite, strong, nonatomic) UIActivityIndicatorView *spinnerView;
@property (readwrite, strong, nonatomic) UIButton *coverButton;
@property (readwrite, copy, nonatomic) CoverButtonActionBlock coverButtonActionBlock;

@property (nonatomic) NSValue *imageButtonSizeValue;

@end

@implementation UIButton (_ImageDownloader)
@dynamic imageModel;
@dynamic progressView;
@dynamic spinnerView;
@dynamic coverButton;
@dynamic coverButtonActionBlock;

@dynamic imageButtonSizeValue;
@end

@implementation UIButton (ImageDownloader)

#pragma mark - Set/Get
-(void)setImageButtonSizeValue:(NSValue*)imageButtonSizeValue   {
    objc_setAssociatedObject(self, &kButtonImageSizeKey, imageButtonSizeValue, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(NSValue*) imageButtonSizeValue   {
    return (NSValue*)objc_getAssociatedObject(self, &kButtonImageSizeKey);
}

- (void)setImageModel:(ImageRequestModel*)imageModel{
    objc_setAssociatedObject(self, &kImageModelAssiciareObjectKey, imageModel, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (ImageRequestModel*)imageModel{
    if ((ImageRequestModel*)objc_getAssociatedObject(self, &kImageModelAssiciareObjectKey) == nil) {
        [self createImageModel];
    }
    return (ImageRequestModel*)objc_getAssociatedObject(self, &kImageModelAssiciareObjectKey);
}


- (void)setProgressView:(UIProgressView *)progressView{
    objc_setAssociatedObject(self, &kProgressViewAssiciareObjectKey, progressView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (UIProgressView*)progressView{
    return (UIProgressView*)objc_getAssociatedObject(self, &kProgressViewAssiciareObjectKey);
}


- (void)setSpinnerView:(UIActivityIndicatorView *)spinnerView{
    objc_setAssociatedObject(self, &kSpinnerViewAssiciareObjectKey, spinnerView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (UIActivityIndicatorView*)spinnerView{
    return (UIActivityIndicatorView*)objc_getAssociatedObject(self, &kSpinnerViewAssiciareObjectKey);
}


- (void)setCoverButton:(UIButton *)coverButton{
    objc_setAssociatedObject(self, &kButtonAssiciareObjectKey, coverButton, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (UIButton*)coverButton{
    return (UIButton*)objc_getAssociatedObject(self, &kButtonAssiciareObjectKey);
}


- (void)setCoverButtonActionBlock:(void (^)(UIImageView *, UIButton *))coverButtonActionBlock{
    objc_setAssociatedObject(self, &kButtonActionBlockAssiciareObjectKey,
                             nil, OBJC_ASSOCIATION_COPY_NONATOMIC);
    objc_setAssociatedObject(self, &kButtonActionBlockAssiciareObjectKey,
                             coverButtonActionBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
- (CoverButtonActionBlock)coverButtonActionBlock{
    return (CoverButtonActionBlock)objc_getAssociatedObject(self, &kButtonActionBlockAssiciareObjectKey);
}
#pragma mark

//! Setup image model
- (void)createImageModel{
    
    ImageRequestModel* imageModel = [[ImageRequestModel alloc] init];
    self.imageModel = imageModel;
}

//! Base method for loading image (from memory cache, file cache or network)
- (void)setImageWithURLString:(NSString*)urlString
                      success:(void (^)(ImageRequestModel* object, UIImage* image))success
                      failure:(void (^)(ImageRequestModel* object, NSError* error))failure
                     progress:(void (^)(ImageRequestModel* object, CGFloat progress))progress{
    
    __weak UIButton* wSelf = self;
    
    self.imageModel.successBlock = ^(ImageRequestModel* object, UIImage* image){
        [wSelf setBackgroundImage:image forState:UIControlStateNormal];
        [wSelf setBackgroundImage:nil forState:UIControlStateHighlighted];
        wSelf.progressView.hidden = YES;
        wSelf.spinnerView.hidden = YES;
        [wSelf.spinnerView stopAnimating];
        
        if (success) {
            success(object, image);
        }
    };
    self.imageModel.failureBlock = ^(ImageRequestModel* object, NSError* error){
        [wSelf setBackgroundImage:wSelf.imageModel.failureImage forState:UIControlStateNormal];
        [wSelf setBackgroundImage:nil forState:UIControlStateHighlighted];
        wSelf.progressView.hidden = YES;
        wSelf.spinnerView.hidden = YES;
        [wSelf.spinnerView stopAnimating];
        
        if (failure) {
            failure(object, error);
        }
    };
    
    self.imageModel.progressBlock = ^(ImageRequestModel* object, CGFloat progressValue){
        
        wSelf.progressView.progress = progressValue;
        
        if (progress) {
            progress(object, progressValue);
        }
    };
    
    self.imageModel.placeholderBlock = ^(ImageRequestModel* object, UIImage* image){
        [wSelf setBackgroundImage:image forState:UIControlStateNormal];
        [wSelf setBackgroundImage:nil forState:UIControlStateHighlighted];
        wSelf.spinnerView.hidden = NO;
        [wSelf.spinnerView startAnimating];
    };
    
    self.imageModel.startedLoadingBlock = ^(ImageRequestModel* object, UIImage* image){
        [wSelf setBackgroundImage:image forState:UIControlStateNormal];
        [wSelf setBackgroundImage:nil forState:UIControlStateHighlighted];
        wSelf.progressView.hidden = NO;
        wSelf.progressView.progress = 0;
        wSelf.spinnerView.hidden = NO;
        [wSelf.spinnerView startAnimating];
    };
    
    self.imageModel.canceledLoadingBlock = ^(ImageRequestModel* object, UIImage* image){
        wSelf.progressView.hidden = YES;
        wSelf.spinnerView.hidden = YES;
        [wSelf.spinnerView stopAnimating];
        
        [wSelf setBackgroundImage:image forState:UIControlStateNormal];
        [wSelf setBackgroundImage:nil forState:UIControlStateHighlighted];
    };
    
    
    self.imageModel.urlString = urlString;
    
    //NSLog(@"Category hash = %@", self.imageModel.md5Hash);
    
    [self.imageModel start];
    
}

-(UIImage*) resizeImage:(UIImage*)image {
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    imageView.frame = CGRectMake(0, 0, [self.imageButtonSizeValue CGSizeValue].width, [self.imageButtonSizeValue CGSizeValue].height);
    imageView.clipsToBounds = YES;
    imageView.layer.cornerRadius = imageView.frame.size.width/2.0f;
    imageView.layer.borderWidth = 1.0f;
    imageView.layer.borderColor = [UIColor colorWithRed:96.0/255.0 green:171.0/255.0 blue:138.0/255.0 alpha:1.0].CGColor;
    
    return [self imageWithView:imageView];
}

- (UIImage *) imageWithView:(UIView *)view
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}

- (void)setImageWithURLString:(NSString*)urlString
                withImageSize:(CGSize)originalImageSize
                      success:(void (^)(ImageRequestModel* object, UIImage* image))success
                      failure:(void (^)(ImageRequestModel* object, NSError* error))failure
                     progress:(void (^)(ImageRequestModel* object, CGFloat progress))progress{
    
    __weak UIButton* wSelf = self;
    
    self.imageButtonSizeValue = [NSValue valueWithCGSize:originalImageSize];
    
    self.imageModel.successBlock = ^(ImageRequestModel* object, UIImage* image){
        UIImage *rezImage = [wSelf resizeImage:image];
        [wSelf setImage:rezImage forState:UIControlStateNormal];
        [wSelf setImage:nil forState:UIControlStateHighlighted];
        wSelf.progressView.hidden = YES;
        wSelf.spinnerView.hidden = YES;
        [wSelf.spinnerView stopAnimating];
        
        if (success) {
            success(object, rezImage);
        }
    };
    self.imageModel.failureBlock = ^(ImageRequestModel* object, NSError* error){
        UIImage *rezImage = [wSelf resizeImage:wSelf.imageModel.failureImage];
        [wSelf setImage:rezImage forState:UIControlStateNormal];
        [wSelf setImage:nil forState:UIControlStateHighlighted];
        wSelf.progressView.hidden = YES;
        wSelf.spinnerView.hidden = YES;
        [wSelf.spinnerView stopAnimating];
        
        if (failure) {
            failure(object, error);
        }
    };
    
    self.imageModel.progressBlock = ^(ImageRequestModel* object, CGFloat progressValue){
        
        wSelf.progressView.progress = progressValue;
        
        if (progress) {
            progress(object, progressValue);
        }
    };
    
    self.imageModel.placeholderBlock = ^(ImageRequestModel* object, UIImage* image){
        UIImage *rezImage = [wSelf resizeImage:image];
        [wSelf setImage:rezImage forState:UIControlStateNormal];
        [wSelf setImage:nil forState:UIControlStateHighlighted];
        wSelf.spinnerView.hidden = NO;
        [wSelf.spinnerView startAnimating];
    };
    
    self.imageModel.startedLoadingBlock = ^(ImageRequestModel* object, UIImage* image){
        UIImage *rezImage = [wSelf resizeImage:image];
        [wSelf setImage:rezImage forState:UIControlStateNormal];
        [wSelf setImage:nil forState:UIControlStateHighlighted];
        wSelf.progressView.hidden = NO;
        wSelf.progressView.progress = 0;
        wSelf.spinnerView.hidden = NO;
        [wSelf.spinnerView startAnimating];
    };
    
    self.imageModel.canceledLoadingBlock = ^(ImageRequestModel* object, UIImage* image){
        wSelf.progressView.hidden = YES;
        wSelf.spinnerView.hidden = YES;
        [wSelf.spinnerView stopAnimating];
        
        UIImage *rezImage = [wSelf resizeImage:image];
        [wSelf setImage:rezImage forState:UIControlStateNormal];
        [wSelf setImage:nil forState:UIControlStateHighlighted];
    };
    
    
    self.imageModel.urlString = urlString;
    
    //NSLog(@"Category hash = %@", self.imageModel.md5Hash);
    
    [self.imageModel start];
    
}

//! Method create progress view.
- (void)addProgressView{
    
    //! Create progress view once for UIImageView object
    if ([self progressView] == nil) {
        UIProgressView* progressView = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
        progressView.frame = CGRectMake(0, self.frame.size.height - self.frame.size.height / 10, self.frame.size.width - self.frame.size.width / 10, 9);
        progressView.center = CGPointMake(CGRectGetMidX(self.bounds), progressView.center.y);
        progressView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth;
        [self addSubview:progressView];
        self.progressView = progressView;
    }
}

//! Method create spinner view.
- (void)addSpinnerView{
    
    //! Create spiner view once for UIImageView object
    if ([self spinnerView] == nil) {
        UIActivityIndicatorView* spinnerView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        spinnerView.center = self.center;
        spinnerView.hidden = YES;
        self.spinnerView = spinnerView;
        self.spinnerView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        [self addSubview:spinnerView];
    }
}

//! Method create cover button.
- (void)addCoverButton{
    
    //! Create button once for UIImageView object
    if ([self coverButton] == nil) {
        UIButton* coverButton = [UIButton new];
        coverButton.frame = self.bounds;
        coverButton.center = self.center;
        self.coverButton = coverButton;
        [coverButton addTarget:self action:@selector(coverButtonClicked:)
              forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:coverButton];
        self.userInteractionEnabled = YES;
    }
}

//! Cover button action
- (void)coverButtonClicked:(UIButton*)sender{
    if (self.coverButtonActionBlock) {
        self.coverButtonActionBlock(self, sender);
    }
}

@end
