//
//  ImageRequestModel.m
//  WelpyApp
//
//  Created by Alexander Bukov on 3/10/15.
//  Copyright (c) 2015 Welpy Inc. All rights reserved.
//

#import "ImageRequestModel.h"
#import "ImageDownloader.h"
#import "ImageCacheManager.h"
#import <CommonCrypto/CommonHMAC.h>

@interface ImageRequestModel ()
//! Parameters hash
@property (strong, nonatomic) NSString* md5Hash;

@end

@implementation ImageRequestModel

#pragma mark _______________________ Class Methods _________________________

#pragma mark ____________________________ Init _____________________________

- (void)dealloc
{
    [self cancelObserveImage];
    self.progressBlock = nil;
    self.successBlock = nil;
    self.failureBlock = nil;
    self.placeholderBlock = nil;
    self.startedLoadingBlock = nil;
    self.canceledLoadingBlock = nil;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        //! Setup defaults
        self.memoryCache = [ImageCacheManager shared].defaultMemoryCach;
        self.fileCache = [ImageCacheManager shared].defaultFileCache;
        
        self.scaleToSize = CGSizeZero;
        self.imageSizePolicy = ImageSizePolicyOriginalSize; //! Do not resize
        self.isRounded = NO;
        self.cornerRadius = 0;
        
        self.imageCachePolicy = ImageCachePolicyMemoryCache;
        
        self.timeOutInterval = 30; //! Seconds
        
    }
    return self;
}


#pragma mark _______________________ Privat Methods ________________________

#pragma mark - Notif oserving
- (void)observeImageWithKey:(NSString*)key{
    //! Observe load operation results
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didChangeProgress:)
                                                 name:[NSString stringWithFormat:@"%@_progress", key]
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didLoadImage:)
                                                 name:[NSString stringWithFormat:@"%@_success", key]
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didGetError:)
                                                 name:[NSString stringWithFormat:@"%@_error", key]
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didCancel:)
                                                 name:[NSString stringWithFormat:@"%@_canceled", key]
                                               object:nil];

    
}


- (void)cancelObserveImage{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
#pragma mark

//! Method to start search in caches or load image
- (void)start{
    
    //! Save variabels
    NSString* md5Hash = self.md5Hash;
    NSString* urlString = self.urlString;
    NSTimeInterval timeOutInterval = self.timeOutInterval;
    CGSize scaleToSize = self.scaleToSize;
    ImageSizePolicy imageSizePolicy = self.imageSizePolicy;
    CGFloat cornerRadius = self.cornerRadius;
    ImageCache* memoryCache = self.memoryCache;
    ImageFileCache* fileCache = self.fileCache;
    BOOL isRounded = self.isRounded;
    ImageCachePolicy imageCachePolicy = self.imageCachePolicy;
    NSOperationQueuePriority queuePriority = self.queuePriority;
    NSString* key = self.key;

    //! Reobserve for new hash
    [self cancelObserveImage];
    [self observeImageWithKey:md5Hash];

    //! Try to find in memory cache
    [memoryCache imageForKey:md5Hash
                     didFind:^(ImageCache *cache, UIImage *image) {
                         if (self.successBlock) {
                             self.successBlock(self, image);
                         }
                     }
             didFindOriginal:^(ImageCache *cache, UIImage *image) {
                 //! Not implemented
             }
                   didNoFind:^(ImageCache *cache) {
                       //! Can't find in memory cache
                       //! Set placeholder image
                       if (self.placeholderBlock) {
                           self.placeholderBlock(self, self.placeholderImage);
                       }
                       
                       //! Check if this necessary find in file cache
                       if (self.imageCachePolicy == ImageCachePolicyMemoryAndFileCache) {
                           
                           //! Try to find in file cache
                           [fileCache imageForKey:md5Hash
                                          didFind:^(ImageFileCache *cache, UIImage *image, NSString* md5Hash) {

                                              //! Save to memory cache too
                                              [memoryCache cacheImage:image forKey:md5Hash];
                                              
                                              //! If ImageRequestModel still observing same hash, call success
                                              if ([md5Hash isEqualToString:self.md5Hash]) {
                                                  if (self.successBlock) {
                                                      self.successBlock(self, image);
                                                  }
                                              }
                                          }
                                  didFindOriginal:^(ImageFileCache *cache, UIImage *image, NSString* md5Hash) {
                                      //! Not implemented
                                  }
                                        didNoFind:^(ImageFileCache *cache) {
                                            //! Did not find in file cache
                                            //! Call block and start load image if necessary
                                            if (self.startedLoadingBlock) {
                                                self.startedLoadingBlock(self, self.placeholderImage);
                                            }
                                            
                                            [self activateImageLoadingForUrlString:urlString
                                                                   timeOutInterval:timeOutInterval
                                                                           md5Hash:md5Hash
                                                                       scaleToSize:scaleToSize
                                                                   imageSizePolicy:imageSizePolicy
                                                                      cornerRadius:cornerRadius
                                                                       memoryCache:memoryCache
                                                                         fileCache:fileCache
                                                                         isRounded:isRounded
                                                                  imageCachePolicy:imageCachePolicy
                                                                     queuePriority:queuePriority
                                                                               key:key];
                                            
                                        }];

                           
                       }else{
                           //! Did not find in memory cache (file cache is turned off)
                           //! Call block and start load image if necessary

                           if (self.startedLoadingBlock) {
                               self.startedLoadingBlock(self, self.placeholderImage);
                           }
                           
                           [self activateImageLoadingForUrlString:urlString
                                                  timeOutInterval:timeOutInterval
                                                          md5Hash:md5Hash
                                                      scaleToSize:scaleToSize
                                                  imageSizePolicy:imageSizePolicy
                                                     cornerRadius:cornerRadius
                                                      memoryCache:memoryCache
                                                         fileCache:fileCache
                                                        isRounded:isRounded
                                                 imageCachePolicy:imageCachePolicy
                                                    queuePriority:queuePriority
                                                              key:key];
                       }

                   }];

}


//! Start load image if necessary
- (void)activateImageLoadingForUrlString:(NSString*)urlString
                         timeOutInterval:(NSTimeInterval)timeOutInterval
                                 md5Hash:(NSString*)md5Hash
                             scaleToSize:(CGSize)scaleToSize
                         imageSizePolicy:(ImageSizePolicy)imageSizePolicy
                            cornerRadius:(CGFloat)cornerRadius
                             memoryCache:(ImageCache*)memoryCache
                               fileCache:(ImageFileCache*)fileCache
                               isRounded:(BOOL)isRounded
                        imageCachePolicy:(ImageCachePolicy)imageCachePolicy
                           queuePriority:(NSOperationQueuePriority)queuePriority
                                     key:(NSString*)key{
    
    //! Check is image already in load
    BOOL isLoading = [[ImageDownloader shared] isLoadingImageForHash:md5Hash];
    
    if (isLoading) {
        //NSLog(@"Olready loading this image");
        //! Update load priority
        [[ImageDownloader shared] setPriority:queuePriority forUrlString:urlString];
        
        //! Update progress value for images with same hash
        CGFloat progress = [[ImageDownloader shared] loadingProgressForHash:md5Hash];
        if (self.progressBlock) {
            self.progressBlock(self, progress);
        }
        
    }else{
        
        //! Start load image with parameters (call backs will be via notifications)
        [[ImageDownloader shared] startLoadForUrlString:urlString
                                        timeOutInterval:timeOutInterval
                                                md5Hash:md5Hash
                                            scaleToSize:scaleToSize
                                        imageSizePolicy:imageSizePolicy
                                           cornerRadius:cornerRadius
                                            memoryCache:memoryCache
                                              fileCache:fileCache
                                              isRounded:isRounded
                                       imageCachePolicy:imageCachePolicy
                                          queuePriority:queuePriority
                                                    key:key];
    }
}

//! Calculate md5
- (NSString *)md5StringFromString:(NSString*)string
{
    if ([string length] == 0) {
        return nil;
    }
    // Borrowed from: http://stackoverflow.com/questions/652300/using-md5-hash-on-a-string-in-cocoa
    const char *cStr = [string UTF8String];
    unsigned char result[16];
    CC_MD5(cStr, (CC_LONG)strlen(cStr), result);
    return [NSString stringWithFormat:@"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",result[0], result[1], result[2], result[3], result[4], result[5], result[6], result[7],result[8], result[9], result[10], result[11],result[12], result[13], result[14], result[15]];
}

#pragma mark - Setters
//! Setters to reset hash
- (void)setUrlString:(NSString *)urlString{
    if (![_urlString isEqualToString:urlString]) {
        self.md5Hash = nil;
    }
    _urlString = urlString;
}

- (void)setCornerRadius:(CGFloat)cornerRadius{
    if (_cornerRadius != cornerRadius) {
        self.md5Hash = nil;
    }
    _cornerRadius = cornerRadius;
}

- (void)setIsRounded:(BOOL)isRounded{
    if (_isRounded != isRounded) {
        self.md5Hash = nil;
    }
    _isRounded = isRounded;
}

- (void)setScaleToSize:(CGSize)scaleToSize{
    if (CGSizeEqualToSize(_scaleToSize, scaleToSize)) {
        self.md5Hash = nil;
    }
    _scaleToSize = scaleToSize;
}

- (void)setImageSizePolicy:(ImageSizePolicy)imageSizePolicy{
    if (_imageSizePolicy != imageSizePolicy) {
        self.md5Hash = nil;
    }
    _imageSizePolicy = imageSizePolicy;
}
#pragma mark

#pragma mark _______________________ Public Methods ________________________

//! Calculate hash for image parameters
- (NSString*)md5Hash{
    
    if (_md5Hash == nil) {
        
        NSMutableString* stringToHash = [NSMutableString stringWithString:self.urlString];
//        [stringToHash appendFormat:@"_cornerRadius=%f", self.cornerRadius];
//        [stringToHash appendFormat:@"_isRounded=%d", self.isRounded];
//        [stringToHash appendFormat:@"_scaleToSize.width=%fheight=%f", self.scaleToSize.width, self.scaleToSize.height];
//        [stringToHash appendFormat:@"_imageSizePolicy=%d", self.imageSizePolicy];
        self.md5Hash = [self md5StringFromString:stringToHash];
    }
    
    return _md5Hash;
}

- (NSString*)getMD5Hash:(NSString*)url  {
    
    NSMutableString* stringToHash = [NSMutableString stringWithString:url];
    
    return [self md5StringFromString:stringToHash];
}


#pragma mark _______________________ Delegates _____________________________

#pragma mark _______________________ Notifications _________________________

//! Image load operation's responce
- (void)didChangeProgress:(NSNotification*)notif{
    
    NSNumber* progress = notif.object;
    
    if (self.progressBlock) {
        self.progressBlock(self, [progress floatValue]);
    }
}

- (void)didLoadImage:(NSNotification*)notif{
    
    UIImage* image = notif.object;
    self.image = image;
    
    if (self.successBlock) {
        self.successBlock(self, image);
    }
}

- (void)didGetError:(NSNotification*)notif{
    
    NSError* error = notif.object;
    self.image = nil;
    
    if (self.failureBlock) {
        self.failureBlock(self, error);
    }
}

- (void)didCancel:(NSNotification*)notif{
    
    //notif.object;
    
    if (self.canceledLoadingBlock) {
        self.canceledLoadingBlock(self, self.cancelImage);
    }
}

@end
