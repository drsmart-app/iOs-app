//
//  UIButton+ImageDownloader.h
//  WelpyApp
//
//  Created by Denis Dubov on 14.05.15.
//  Copyright (c) 2015 Welpy Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageRequestModel.h"

@interface UIButton (ImageDownloader)

//! Base method for loading image (from memory cache, file cache or network)
- (void)setImageWithURLString:(NSString*)urlString
                      success:(void (^)(ImageRequestModel* object, UIImage* image))success
                      failure:(void (^)(ImageRequestModel* object, NSError* error))failure
                     progress:(void (^)(ImageRequestModel* object, CGFloat progress))progress;

- (void)setImageWithURLString:(NSString*)urlString
                withImageSize:(CGSize)originalImageSize
                      success:(void (^)(ImageRequestModel* object, UIImage* image))success
                      failure:(void (^)(ImageRequestModel* object, NSError* error))failure
                     progress:(void (^)(ImageRequestModel* object, CGFloat progress))progress;

//! Image model to cache and download image
- (void)setImageModel:(ImageRequestModel*)imageModel;
- (ImageRequestModel*)imageModel;

//! Add progress view
- (void)addProgressView;
- (UIProgressView*)progressView;

//! Add spinner view
- (void)addSpinnerView;
- (UIActivityIndicatorView*)spinnerView;

//! Method create cover button and call back.
- (void)addCoverButton;
- (UIButton*)coverButton;
- (void)setCoverButtonActionBlock:(void (^)(UIImageView *, UIButton *))coverButtonActionBlock;

@end
