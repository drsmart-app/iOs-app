//
//  UIView+FindFirstResponder.m
//  DentaBook
//
//  Created by Mac Mini on 13.11.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "UIView+FindFirstResponder.h"

@implementation UIView (FindFirstResponder)
- (id)findFirstResponder
{
    id result = nil;
    if (self.isFirstResponder) {
        result = self;
    }
    for (UIView *subView in self.subviews) {
        if ([subView isFirstResponder])
        {
            result = subView;
            break;
        }
        else
        {
            result=[subView findFirstResponder];
            if(result) break;
        }
    }
    return result;
}
@end
