//
//  UIView+FindFirstResponder.h
//  DentaBook
//
//  Created by Mac Mini on 13.11.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (FindFirstResponder)
- (id)findFirstResponder;
@end
