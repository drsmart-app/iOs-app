 //
//  HTTPUtil.m
//  Dict
//
//  Created by Aleksandr Hruschev on 06.06.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HTTPUtil.h"
#import "SBJSON.h"
#import <CFNetwork/CFNetwork.h>
#import "DataBase.h"
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "PercUtils.h"
#import "NetworkChecker.h"
//#import "Order.h"
#import "NSObject+ComplexJSON.h"
//#import "TRStringExtensions.h"
#import <CoreLocation/CoreLocation.h>

@implementation HTTPUtil

NSString *TranId;


void clearCookies()
{
    NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSArray *cookies = [cookieStorage cookiesForURL:[NSURL URLWithString:getURL(@"")]];
    for (NSHTTPCookie *cookie in cookies)
    {
        NSLog(@"Deleting cookie for domain: %@", [cookie domain]);
        [cookieStorage deleteCookie:cookie];
    }
}

NSString* makeHTTPRequest(NSString* PostWrappedRequest)
{
    if([NetworkChecker hasConnection])
    {
        NSError *connectError=nil;
        NSURL *connect=[NSURL URLWithString:getURL(@"")];
        
        NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:connect];
        
        NSString* request_length=[[NSString alloc] initWithFormat:@"%lu",(unsigned long)[PostWrappedRequest length]];
        
        [request setHTTPMethod:@"POST"];
        
        [request setValue:request_length forHTTPHeaderField:@"Content-Length"];
        
        [request setValue:@"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Accept"];
        
        
        request.HTTPBody=[PostWrappedRequest dataUsingEncoding:NSUTF8StringEncoding];
        
        NSURLResponse *resp=nil;
        
        NSData *response=[NSURLConnection sendSynchronousRequest:request returningResponse:&resp error:&connectError];
        
        return [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
    }
    else 
    {
        
        return @"{\"state\":\"-100\",\"desc\":\"No connection\"}";
    }
}

NSString* makeJSONRequest(NSObject *requestData, NSString* method, bool request_cookie)
{
    if([NetworkChecker hasConnection])
    {
        NSError *connectError=nil;
        NSURL *connect=[NSURL URLWithString:getURL(method)];//@"actions/Register"
        NSString *JSONWrappedRequest;
        if([NSStringFromClass([requestData class]) hasPrefix:@"NS"]||[NSStringFromClass([requestData class]) hasPrefix:@"__NS"])
            JSONWrappedRequest=[requestData JSONRepresentation];
        else
            JSONWrappedRequest=[requestData _JSON];
        
        NSLog(@"POST data:%@ of class: %@",JSONWrappedRequest, NSStringFromClass([requestData class]));
        
        NSString *postWrappedRequest=[NSString stringWithFormat:@"json=%@", JSONWrappedRequest];
        
        NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:connect];
        
        NSString* request_length=[[NSString alloc] initWithFormat:@"%lu",(unsigned long)[postWrappedRequest length]];
        
        [request setHTTPMethod:@"POST"];
        
        [request setValue:request_length forHTTPHeaderField:@"Content-Length"];
        
        //[request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [request setValue:@"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        request.HTTPBody=[postWrappedRequest dataUsingEncoding:NSUTF8StringEncoding];
        
        NSLog(@"%@, %@", [[NSString alloc] initWithData:request.HTTPBody encoding:NSUTF8StringEncoding], [request allHTTPHeaderFields]);
        
        NSHTTPURLResponse *resp=nil;
       
        NSData *response=[NSURLConnection sendSynchronousRequest:request returningResponse:&resp error:&connectError];

        return [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
    }
    else
    {
        
        return @"{\"state\":\"-100\",\"err\":\"Нет подключения к серверу\"}";
    }

}


NSString* getLogin()
{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    const char *t_login=[[defaults objectForKey:@"login"] cStringUsingEncoding:NSUTF8StringEncoding]; 
    NSString * login;

    if(t_login!=nil)
        login=[[NSString alloc] initWithCString:t_login encoding:NSUTF8StringEncoding];
    else
        return @"";
    return login;
}

NSString* getOffertaAccepted()
{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    const char *t_login=[[defaults objectForKey:@"offerta_accepted"] cStringUsingEncoding:NSUTF8StringEncoding];
    NSString * login;
    
    if(t_login!=nil)
        login=[[NSString alloc] initWithCString:t_login encoding:NSUTF8StringEncoding];
    else
        return @"";
    return login;
}

void setOffertaAccepted()
{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    [defaults setObject:[[NSString alloc] initWithFormat:@"%@",@"true"] forKey:@"offerta_accepted"];
    [defaults synchronize];
}

NSString* getPwd()
{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    const char *_password=[[defaults objectForKey:@"password"] cStringUsingEncoding:NSUTF8StringEncoding];  
    if(_password==nil)
        _password="";
    NSString * password=[[NSString alloc] initWithCString:_password encoding:NSUTF8StringEncoding];
    return password;
}

NSString* getURL(NSString* opPath)
{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    const char *_URL=[[defaults objectForKey:@"sync_url"] cStringUsingEncoding:NSUTF8StringEncoding];
    NSString * URL;
    if(_URL==nil)
    {
        URL=[[NSString alloc] initWithCString:/*"http://monet.17minut.ru/Client"*/"http://212.45.24.180/Client" encoding:NSUTF8StringEncoding];
    }
    else
    {
        URL=[[NSString alloc] initWithCString:_URL encoding:NSUTF8StringEncoding];
    }
    
    if(opPath.length>0)
        URL=[[NSString alloc] initWithFormat:@"%@/%@",URL, opPath];//context of operation must be inserted
        
    return URL;
}

NSString* getExamURL()
{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    const char *_URL=[[defaults objectForKey:@"exam_url"] cStringUsingEncoding:NSUTF8StringEncoding];
    NSString * URL;
    if(_URL==nil)
    {
        URL=[[NSString alloc] initWithCString:/*"http://monet.17minut.ru/Client"*/"http://212.45.24.180/Client" encoding:NSUTF8StringEncoding];
    }
    else 
    {
        URL=[[NSString alloc] initWithCString:_URL encoding:NSUTF8StringEncoding];
    }//context of operation must be inserted
    return URL;
}

UIColor* getColorScheme()
{
    int scheme = 0;
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    const char *_URL=[[defaults objectForKey:@"color"] cStringUsingEncoding:NSUTF8StringEncoding];
    if(_URL==nil)
    {
        scheme = 0;
    }
    else
    {
         scheme = [[[NSString alloc] initWithCString:_URL encoding:NSUTF8StringEncoding] intValue];
    }//context of operation must be inserted
    
    if(scheme==0)
    {
        return [UIColor colorWithRed:72.0f/255.0f green:100.0f/255.0f blue:124.0f/255.0f alpha:1.0f];
    }
    else
    {
        return [UIColor colorWithRed:0.0f/255.0f green:175.0f/255.0f blue:150.0f/255.0f alpha:1.0f];
    }
}

UIColor* getColorSchemeTooth()
{
    int scheme = 0;
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    const char *_URL=[[defaults objectForKey:@"color"] cStringUsingEncoding:NSUTF8StringEncoding];
    if(_URL==nil)
    {
        scheme = 0;
    }
    else
    {
        scheme = [[[NSString alloc] initWithCString:_URL encoding:NSUTF8StringEncoding] intValue];
    }//context of operation must be inserted
    
    if(scheme==0)
    {
        return [UIColor colorWithRed:41.0f/255.0f green:127.0f/255.0f blue:184.0f/255.0f alpha:1.0f];
    }
    else
    {
        return [UIColor colorWithRed:0.0f/255.0f green:175.0f/255.0f blue:150.0f/255.0f alpha:1.0f];
    }
}

UIColor* getColorSchemeStatus()
{
    int scheme = 0;
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    const char *_URL=[[defaults objectForKey:@"color"] cStringUsingEncoding:NSUTF8StringEncoding];
    if(_URL==nil)
    {
        scheme = 0;
    }
    else
    {
        scheme = [[[NSString alloc] initWithCString:_URL encoding:NSUTF8StringEncoding] intValue];
    }//context of operation must be inserted
    
    if(scheme==0)
    {
        return [UIColor colorWithRed:45.0f/255.0f green:62.0f/255.0f blue:80.0f/255.0f alpha:1.0f];
    }
    else
    {
        return [UIColor colorWithRed:0.0f/255.0f green:94.0f/255.0f blue:92.0f/255.0f alpha:1.0f];
    }
}

UIColor* getColorSchemeXZ()
{
    int scheme = 0;
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    const char *_URL=[[defaults objectForKey:@"color"] cStringUsingEncoding:NSUTF8StringEncoding];
    if(_URL==nil)
    {
        scheme = 0;
    }
    else
    {
        scheme = [[[NSString alloc] initWithCString:_URL encoding:NSUTF8StringEncoding] intValue];
    }//context of operation must be inserted
    
    if(scheme==0)
    {
        return [UIColor colorWithRed:41.0f/255.0f green:127.0f/255.0f blue:184.0f/255.0f alpha:1.0f];
    }
    else
    {
        return [UIColor colorWithRed:1.0f/255.0f green:175.0f/255.0f blue:150.0f/255.0f alpha:1.0f];
    }
}

UIColor* getColorSchemeBar()
{
    int scheme = 0;
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    const char *_URL=[[defaults objectForKey:@"color"] cStringUsingEncoding:NSUTF8StringEncoding];
    if(_URL==nil)
    {
        scheme = 0;
    }
    else
    {
        scheme = [[[NSString alloc] initWithCString:_URL encoding:NSUTF8StringEncoding] intValue];
    }//context of operation must be inserted
    
    if(scheme==0)
    {
        return [UIColor colorWithRed:72.0f/255.0f green:100.0f/255.0f blue:124.0f/255.0f alpha:1.0f];
    }
    else
    {
        return [UIColor colorWithRed:72.0f/255.0f green:124.0f/255.0f blue:122.0f/255.0f alpha:1.0f];
    }
}

NSString* getColorSchemeImg()
{
    int scheme = 0;
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    const char *_URL=[[defaults objectForKey:@"color"] cStringUsingEncoding:NSUTF8StringEncoding];
    if(_URL==nil)
    {
        scheme = 0;
    }
    else
    {
        scheme = [[[NSString alloc] initWithCString:_URL encoding:NSUTF8StringEncoding] intValue];
    }//context of operation must be inserted
    
    if(scheme==0)
    {
        return @"background.png";
    }
    else
    {
        return @"background_green.png";
    }
}

int getColorSchemeInt()
{
    int scheme = 0;
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    const char *_URL=[[defaults objectForKey:@"color"] cStringUsingEncoding:NSUTF8StringEncoding];
    if(_URL==nil)
    {
        scheme = 0;
    }
    else
    {
        scheme = [[[NSString alloc] initWithCString:_URL encoding:NSUTF8StringEncoding] intValue];
    }//context of operation must be inserted
    
    return scheme;
}

void setColorScheme(int scheme)
{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    [defaults setObject:[[NSString alloc] initWithFormat:@"%d",scheme] forKey:@"color"];
    [defaults synchronize];
}

int getRowsCount()
{
    int scheme = 3;
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    const char *_URL=[[defaults objectForKey:@"rows_count"] cStringUsingEncoding:NSUTF8StringEncoding];
    if(_URL==nil)
    {
        scheme = 3;
    }
    else
    {
        scheme = [[[NSString alloc] initWithCString:_URL encoding:NSUTF8StringEncoding] intValue];
    }//context of operation must be inserted
    
    return scheme;
}

void setRowsCount(int scheme)
{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    [defaults setObject:[[NSString alloc] initWithFormat:@"%d",scheme] forKey:@"rows_count"];
    [defaults synchronize];
}

BOOL getNeedName()
{
    int scheme = 0;
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    const char *_URL=[[defaults objectForKey:@"need_name"] cStringUsingEncoding:NSUTF8StringEncoding];
    if(_URL==nil)
    {
        scheme = 0;
    }
    else
    {
        scheme = [[[NSString alloc] initWithCString:_URL encoding:NSUTF8StringEncoding] intValue];
    }//context of operation must be inserted
    
    return scheme==1;
}

void setNeedName(BOOL scheme)
{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    [defaults setObject:[[NSString alloc] initWithFormat:@"%d",scheme?1:0] forKey:@"need_name"];
    [defaults synchronize];
}

BOOL getNeedSumSplit()
{
    int scheme = 0;
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    const char *_URL=[[defaults objectForKey:@"sum_split"] cStringUsingEncoding:NSUTF8StringEncoding];
    if(_URL==nil)
    {
        scheme = 0;
    }
    else
    {
        scheme = [[[NSString alloc] initWithCString:_URL encoding:NSUTF8StringEncoding] intValue];
    }//context of operation must be inserted
    
    return scheme==1;
}

void setNeedSumSplit(BOOL scheme)
{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    [defaults setObject:[[NSString alloc] initWithFormat:@"%d",scheme?1:0] forKey:@"sum_split"];
    [defaults synchronize];
}

void setLoginPassword(NSString* loginStr, NSString* passwordStr)
{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    [defaults setObject:loginStr forKey:@"login"];
    [defaults setObject:passwordStr forKey:@"password"];
    [defaults synchronize];
}

void setAppSettings(NSString* loginStr, NSString* passwordStr, NSString *URLString, NSString *ExamURLString)
{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    [defaults setObject:loginStr forKey:@"login"];
    [defaults setObject:passwordStr forKey:@"password"];
    [defaults setObject:URLString forKey:@"sync_url"];
    [defaults setObject:ExamURLString forKey:@"exam_url"];
    [defaults synchronize];
}

/*
 PROFILE
 */
void setProfile(NSString* fio, NSString* position, NSString* clinic, NSString* phone, NSString* email, NSString* address,  NSString* license,  NSString* photo_id)
{
    //fioTextField, positionTextField, clinicTextField, phoneTextField, emailTextField, addressTextField, licenseTextField, clinicLogoBtn
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    [defaults setObject:fio forKey:@"fio"];
    [defaults setObject:position forKey:@"position"];
    [defaults setObject:clinic forKey:@"clinic"];
    [defaults setObject:phone forKey:@"phone"];
    [defaults setObject:email forKey:@"email"];
    [defaults setObject:address forKey:@"address"];
    [defaults setObject:license forKey:@"license"];
    [defaults setObject:photo_id forKey:@"photo_id"];

    [defaults synchronize];
}

NSString* getProfileFio()
{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    const char *t_login=[[defaults objectForKey:@"fio"] cStringUsingEncoding:NSUTF8StringEncoding];
    NSString * login;
    
    if(t_login!=nil)
        login=[[NSString alloc] initWithCString:t_login encoding:NSUTF8StringEncoding];
    else
        return @"";
    return login;
}

NSString* getProfilePosition()
{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    const char *t_login=[[defaults objectForKey:@"position"] cStringUsingEncoding:NSUTF8StringEncoding];
    NSString * login;
    
    if(t_login!=nil)
        login=[[NSString alloc] initWithCString:t_login encoding:NSUTF8StringEncoding];
    else
        return @"";
    return login;
}

NSString* getProfileClinic()
{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    const char *t_login=[[defaults objectForKey:@"clinic"] cStringUsingEncoding:NSUTF8StringEncoding];
    NSString * login;
    
    if(t_login!=nil)
        login=[[NSString alloc] initWithCString:t_login encoding:NSUTF8StringEncoding];
    else
        return @"";
    return login;
}

NSString* getProfilePhone()
{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    const char *t_login=[[defaults objectForKey:@"phone"] cStringUsingEncoding:NSUTF8StringEncoding];
    NSString * login;
    
    if(t_login!=nil)
        login=[[NSString alloc] initWithCString:t_login encoding:NSUTF8StringEncoding];
    else
        return @"";
    return login;
}

NSString* getProfileEmail()
{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    const char *t_login=[[defaults objectForKey:@"email"] cStringUsingEncoding:NSUTF8StringEncoding];
    NSString * login;
    
    if(t_login!=nil)
        login=[[NSString alloc] initWithCString:t_login encoding:NSUTF8StringEncoding];
    else
        return @"";
    return login;
}

NSString* getProfileAddress()
{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    const char *t_login=[[defaults objectForKey:@"address"] cStringUsingEncoding:NSUTF8StringEncoding];
    NSString * login;
    
    if(t_login!=nil)
        login=[[NSString alloc] initWithCString:t_login encoding:NSUTF8StringEncoding];
    else
        return @"";
    return login;
}

NSString* getProfileLicense()
{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    const char *t_login=[[defaults objectForKey:@"license"] cStringUsingEncoding:NSUTF8StringEncoding];
    NSString * login;
    
    if(t_login!=nil)
        login=[[NSString alloc] initWithCString:t_login encoding:NSUTF8StringEncoding];
    else
        return @"";
    return login;
}

NSString* getProfilePhoto()
{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    const char *t_login=[[defaults objectForKey:@"photo"] cStringUsingEncoding:NSUTF8StringEncoding];
    NSString * login;
    
    if(t_login!=nil)
        login=[[NSString alloc] initWithCString:t_login encoding:NSUTF8StringEncoding];
    else
        return @"";
    return login;
}
@end
