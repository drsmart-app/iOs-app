//
//  NSObject+ComplexJSON.h
//  OpenTaxiPassenger
//
//  Created by Instream on 17.07.14.
//  Copyright (c) 2014 Instream. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (ComplexJSON)

- (NSString*) _JSON;
- (NSDictionary*) parseObject;
@end
