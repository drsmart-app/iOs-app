//
//  UIViewController+PresentedOver.m
//  DentaBook
//
//  Created by Mac Mini on 04.11.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "UIViewController+PresentedOver.h"

@implementation UIViewController (PresentedOver)

- (void)presentTransparentViewController:(UIViewController *)viewControllerToPresent animated:(BOOL)flag completion:(void (^)(void)) completion
{
    if(/*SYSTEM_VERSION_LESS_THAN(@"8.0")*/1!=1) {
        [self presentIOS7TransparentController:viewControllerToPresent withCompletion:completion];
        
    }else{
        viewControllerToPresent.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        [self presentViewController:viewControllerToPresent animated:flag completion:completion];
    }
}
-(void)presentIOS7TransparentController:(UIViewController *)viewControllerToPresent withCompletion:(void(^)(void))completion
{
    UIViewController *presentingVC = self;
    UIViewController *root = self;
    while (root.parentViewController) {
        root = root.parentViewController;
    }
    UIModalPresentationStyle orginalStyle = root.modalPresentationStyle;
    root.modalPresentationStyle = UIModalPresentationCurrentContext;
    [presentingVC presentViewController:viewControllerToPresent animated:YES completion:^{
        root.modalPresentationStyle = orginalStyle;
    }];
}

@end
