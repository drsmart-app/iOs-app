//
//  UILabel+FontAppearance.m
//  DentaBook
//
//  Created by Mac Mini on 26.01.17.
//  Copyright © 2017 denisdbv. All rights reserved.
//

#import "UILabel+FontAppearance.h"

@implementation UILabel (FontAppearance)
- (void)setAppearanceFont:(UIFont *)font
{
    if (self.tag == 1001) {
        return;
    }
    
    BOOL isBold = (self.font.fontDescriptor.symbolicTraits & UIFontDescriptorTraitBold);
    const CGFloat* colors = CGColorGetComponents(self.textColor.CGColor);
    
    if (self.font.pointSize == 14) {
        // set font for UIAlertController title
        self.font = [UIFont systemFontOfSize:11];
        
    } else if (self.font.pointSize == 13) {
        // set font for UIAlertController message
        self.font = [UIFont systemFontOfSize:11];
    } else if (isBold) {
        // set font for UIAlertAction with UIAlertActionStyleCancel
        self.font = [UIFont systemFontOfSize:12];
    } else if ((*colors) == 1) {
        // set font for UIAlertAction with UIAlertActionStyleDestructive
        self.font = [UIFont systemFontOfSize:13];
    } else {
        // set font for UIAlertAction with UIAlertActionStyleDefault
        self.font = [UIFont systemFontOfSize:14];
    }
    self.textColor = [UIColor colorWithWhite:74.0f/255.0f alpha:1];
    self.tag = 1001;
}

- (UIFont *)appearanceFont
{
    return self.font;
}

@end
