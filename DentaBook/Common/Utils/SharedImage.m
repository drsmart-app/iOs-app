//
//  SharedImage.m
//  DentaBook
//
//  Created by Mac Mini on 30.01.17.
//  Copyright © 2017 denisdbv. All rights reserved.
//

#import "SharedImage.h"

@implementation SharedImage

@synthesize img;

+(SharedImage*) sharedInstance {
    static dispatch_once_t pred;
    static id shared = nil;
    dispatch_once(&pred, ^{
        shared = [[super alloc] initUniqueInstance];
    });
    return shared;
}

-(SharedImage*) initUniqueInstance {
    return [super init];
}

- (NSString*) description
{
    return [[NSString alloc] initWithFormat:@"%@",img];
}

@end
