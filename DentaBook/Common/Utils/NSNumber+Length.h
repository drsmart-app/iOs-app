//
//  NSNumber+Length.h
//  OpenTaxiPassenger
//
//  Created by Instream on 16.09.14.
//  Copyright (c) 2014 Instream. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber (Length)
- (NSUInteger) length;

@end
