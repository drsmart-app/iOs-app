//
//  NetworkChecker.h
//  Remindme
//
//  Created by Instream on 21.03.13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkChecker : NSObject
+(BOOL)hasConnection;
+ (BOOL) hasConnectionX:(NSString*) hostname;
@end
