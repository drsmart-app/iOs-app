//
//  UIColors+Smart.m
//  OpenTaxiPassenger
//
//  Created by Instream on 26.09.14.
//  Copyright (c) 2014 Instream. All rights reserved.
//

#import "UIColors+Smart.h"

@implementation UIColor (Smart)
+(UIColor*) _yellow
{
    return [UIColor colorWithRed:197.0f/256.0f green:152.0f/256.0f blue:6.0f/256.0f alpha:1.0f];
}

+(UIColor*) _green
{
    return [UIColor colorWithRed:93.0f/256.0f green:166.0f/256.0f blue:63.0f/256.0f alpha:1.0f];
}
@end
