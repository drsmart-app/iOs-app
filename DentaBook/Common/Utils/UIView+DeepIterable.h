//
//  UIView+DeepIterable.h
//  DentaBook
//
//  Created by Mac Mini on 26.01.17.
//  Copyright © 2017 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (DeepIterable)
- (void)applyKey:(NSObject*) key forPath:(NSString*) path;
@end
