//
//  UIView+DeepIterable.m
//  DentaBook
//
//  Created by Mac Mini on 26.01.17.
//  Copyright © 2017 denisdbv. All rights reserved.
//

#import "UIView+DeepIterable.h"

@implementation UIView (DeepIterable)
- (void)applyKey:(NSObject*) key forPath:(NSString*) path
{
    @try {
        NSLog(@"%@", self);
        if([NSStringFromClass([self class]) hasPrefix:@"UILabel"])
        {
            ((UILabel*)self).font=[UIFont systemFontOfSize:14];
            CGAffineTransform scaleTransform = CGAffineTransformScale(CGAffineTransformIdentity, 0.8, 0.8);
            self.transform = scaleTransform;
        }
        [self setValue:key forKeyPath: path];
    } @catch (NSException *exception) {
    } @finally {
    }
    
    for (UIView *subview in self.subviews)
    {
        [subview applyKey: key forPath:path];
    }
}
@end
