//
//  UIViewController+PresentedOver.h
//  DentaBook
//
//  Created by Mac Mini on 04.11.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (PresentedOver)
 - (void) presentTransparentViewController:(UIViewController *)viewControllerToPresent animated:(BOOL)flag completion:(void (^)(void))completion;
@end
