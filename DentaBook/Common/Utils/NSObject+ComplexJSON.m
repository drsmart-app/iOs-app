//
//  NSObject+ComplexJSON.m
//  OpenTaxiPassenger
//
//  Created by Instream on 17.07.14.
//  Copyright (c) 2014 Instream. All rights reserved.
//

#import "NSObject+ComplexJSON.h"
#import "SBJson.h"
#import <objC/runtime.h>

@implementation NSObject (ComplexJSON)


static const char *getPropertyType(objc_property_t property)
{
    const char *attributes = property_getAttributes(property);
    char buffer[1 + strlen(attributes)];
    strcpy(buffer, attributes);
    char *state = buffer, *attribute;
    while ((attribute = strsep(&state, ",")) != NULL) {
        if (attribute[0] == 'T') {
            return (const char *)[[NSData dataWithBytes:(attribute + 3) length:strlen(attribute) - 4] bytes];
        }
    }
    return "@";
}
- (NSString*) _JSON
{
    NSDictionary *muteDictionary = [self parseObject];
    NSError *jsonError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:muteDictionary options:0 error:&jsonError];
    return [/*[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding*/muteDictionary JSONRepresentation];
}

- (NSDictionary*) parseObject
{
    NSDateFormatter* df=[[NSDateFormatter alloc] init];
    df.dateFormat=@"dd.MM.yyyy HH:mm";
    
    NSMutableDictionary *muteDictionary = [NSMutableDictionary dictionary];
    
    id YourClass = objc_getClass([NSStringFromClass([self class]) cStringUsingEncoding:NSUTF8StringEncoding]);
    unsigned int outCount, i;
    objc_property_t *properties = class_copyPropertyList(YourClass, &outCount);
    for (i = 0; i < outCount; i++) {
        objc_property_t property = properties[i];
        NSString *propertyName = [NSString stringWithCString:property_getName(property) encoding:NSUTF8StringEncoding];
        NSString *propType = [NSString stringWithCString:getPropertyType(property)];
        SEL propertySelector = NSSelectorFromString(propertyName);
        if ([self respondsToSelector:propertySelector])
        {
            if([propType hasPrefix:@"NS"])//regular type
            {
                if([propType isEqualToString:@"NSDate"])
                {
                    [muteDictionary setValue:[df stringFromDate:[self performSelector:propertySelector]] forKey:propertyName];
                }
                else
                    [muteDictionary setValue:[self performSelector:propertySelector] forKey:propertyName];
            }
            else
            {
                [muteDictionary setValue:[[self performSelector:propertySelector] parseObject] forKey:propertyName];
            }
        }
    }
    return muteDictionary;
}

@end
