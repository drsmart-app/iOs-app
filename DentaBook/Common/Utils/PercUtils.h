//
//  PercUtils.h
//  Remindme
//
//  Created by Instream on 12.03.13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PercUtils : NSObject

NSDate* roundTo(NSDate* input, int minutes);
//path to device_token
+ (NSString*) tokenFilePath;
//path to tips
+ (NSString*) tipsFilePath;
//path to device_token
+ (NSString*) getDeviceToken;
//path to monitored reminders
//+ (NSString*) dataFilePath;
//path to last location update date
//+ (NSString*) locationDateFilePath;
// path to current entered locations
+ (NSString*) locationEnteredFilePath;
////////////////////////////////////////////////////////////////////////////
////////////////////////// MONITORING PERSISTENCE METHODS //////////////////
// add reminder to monitored reminders list (high precision on)
+ (void) addMonitoredReminder:(NSString*) _rem_id;
// remove reminder from monitored reminders list 
+ (BOOL) removeMonitoredReminder:(NSString*) _rem_id;
// mark reminder as enetered but not exited 
+ (void) addEnteredReminderLocation:(NSString*) _rem_id;
// unmark reminder as enetered
+ (BOOL) removeEnteredReminderLocation:(NSString*) _rem_id;
//check reminder in entered locations
+ (BOOL) checkReminderInEnteredLocations:(NSString*) rem_id;
//set last location update date
+ (void) setLastLocationDate;
//get last location update date
+ (NSDate*) getLastLocationDate;
//clear all of supplementary data
+ (void) clearSupplementaryData;
//get location alarm possibility
+ (NSString*) getLocationProbability:(float) radius;
//time in seconds from last location date update
+ (int) getLastLocationInterval;
//generate singlecolor image
+ (UIImage *)imageWithColor:(UIColor *)color;
//cuts hostname from URL
+ (NSString *)getRootDomain:(NSString *)domain;
+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;
+ (NSString*) offertaFilePath;


NSString* nvl(NSString* input,NSString* placeholder);
NSString* nvl2(NSString* input,NSString* concat,NSString* placeholder);
NSString* nvl3(NSString* input,NSString* concat,NSString* placeholder);
NSString* substr(NSString* input, int len);
+(UIColor*) _yellow;
+(UIColor*) _green;

BOOL NSStringIsValidEmail(NSString *checkString);
@end
