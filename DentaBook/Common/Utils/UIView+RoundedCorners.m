//
//  UIView+RoundedCorners.m
//  OpenTaxiPassenger
//
//  Created by Instream on 26.09.14.
//  Copyright (c) 2014 Instream. All rights reserved.
//

#import "UIView+RoundedCorners.h"
#import <QuartzCore/QuartzCore.h>

@implementation UIView (RoundedCorners)

- (void) makeRoundCorners
{
    for(UIView* vv in self.subviews)
    {
        if([vv isKindOfClass:[UIButton class]])
        {
            vv.layer.cornerRadius=5;
            vv.clipsToBounds = YES;
        }
        for(UIView* vvv in vv.subviews)
        {
            if([vvv isKindOfClass:[UIButton class]])
            {
                vvv.layer.cornerRadius=5;
                vvv.clipsToBounds = YES;
            }
            for(UIView* vvvv in vvv.subviews)
            {
                if([vvvv isKindOfClass:[UIButton class]])
                {
                    vvv.layer.cornerRadius=5;
                    vvv.clipsToBounds = YES;
                }
            }
        }
    }
}

@end
