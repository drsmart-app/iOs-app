//
//  UITextField+Insets.m
//  DentaBook
//
//  Created by Denis Dubov on 16.03.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "UITextField+Insets.h"

@implementation UITextField (Insets)

- (CGRect)textRectForBounds:(CGRect)bounds {
    return [self rectForBounds:bounds];
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
    return [self rectForBounds:bounds];
}

- (CGRect)placeholderRectForBounds:(CGRect)bounds {
    return [self rectForBounds:bounds];
}

//here 40 - is your x offset
- (CGRect)rectForBounds:(CGRect)bounds {
    return CGRectInset(bounds, 20, 3);
}

@end
