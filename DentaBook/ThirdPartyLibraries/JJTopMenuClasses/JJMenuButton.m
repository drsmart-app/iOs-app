//
//  JJMenuButton.m
//
//
//  Created by Jhon Lopez on 2/4/14.
//  Copyright (c) 2014 jaiversin. All rights reserved.
//

#import "JJMenuButton.h"
#import <QuartzCore/QuartzCore.h>

@implementation JJMenuButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

-(UIColor *)selectedItemColor
{
    if (!_selectedItemColor) {
        _selectedItemColor = [UIColor colorWithRed:20.0/255.0 green:163.0/255.0 blue:242.0/255.0 alpha:1.0];
    }
    return _selectedItemColor;
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    UIBezierPath *bezierPath;
    
    self.layer.borderWidth = 1.0;
    self.layer.borderColor = [UIColor whiteColor].CGColor;
    
    // Draw top line
    bezierPath = [UIBezierPath bezierPath];
    [bezierPath moveToPoint:CGPointMake(0.0, 0.0)];
    [bezierPath addLineToPoint:CGPointMake(CGRectGetWidth(rect), 0.0)];
    [[UIColor whiteColor] setStroke];
    [bezierPath setLineWidth:1.0];
    [bezierPath stroke];
    
    // Draw bottom line
    bezierPath = [UIBezierPath bezierPath];
    [bezierPath moveToPoint:CGPointMake(0.0, CGRectGetHeight(rect))];
    [bezierPath addLineToPoint:CGPointMake(CGRectGetWidth(rect), CGRectGetHeight(rect))];
    [[UIColor whiteColor] setStroke];
    [bezierPath setLineWidth:1.0];
    [bezierPath stroke];
    
    // Draw an indicator line if tab is selected
    if (self.selected)
    {
        
        bezierPath = [UIBezierPath bezierPath];
        
        // Draw the indicator
        
        switch (self.menuPosition)
        {
            case JJMenuPositionBottom:
                [bezierPath moveToPoint:CGPointMake(0.0, CGRectGetHeight(rect) - 1.0)];
                [bezierPath addLineToPoint:CGPointMake(CGRectGetWidth(rect), CGRectGetHeight(rect) - 1.0)];
                break;
            case JJMenuPositionTop:
                [bezierPath moveToPoint:CGPointMake(0.0, 0.0)];
                [bezierPath addLineToPoint:CGPointMake(CGRectGetWidth(rect), 0.0)];
                break;
            default:
                [bezierPath moveToPoint:CGPointMake(0.0, CGRectGetHeight(rect) - 1.0)];
                [bezierPath addLineToPoint:CGPointMake(CGRectGetWidth(rect), CGRectGetHeight(rect) - 1.0)];
                break;
        }
        //20 162 243
        [[UIColor colorWithRed:20.0f/255.0f green:162.0f/255.0f blue:243.0f/255.0f alpha:1.0f] setStroke];
        [bezierPath setLineWidth:5.0];
        [bezierPath stroke];
        /*[bezierPath setLineWidth:4.0];
        [self.selectedItemColor setStroke];
        [bezierPath stroke];*/
    }

}


-(void)setSelected:(BOOL)selected
{
    [super setSelected:selected];
    [self setNeedsDisplay];
}

@end
