//
//  WLPActionSheet.m
//  WelpyApp
//
//  Created by Denis Dubov on 01.08.14.
//  Copyright (c) 2014 Welpy Inc. All rights reserved.
//

#import "WLPActionSheet.h"
#import "HttpUtil.h"

@interface WLPActionSheet()
@property (nonatomic, strong) UIViewController *contentController;
@end

@implementation WLPActionSheet

#pragma mark WLPActionSheet Set up methods

- (id)init {
    
    self = [self initWithFrame:CGRectMake(0, 0, 0, 0)];
    self.backgroundColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0];
    self.buttons = [[NSMutableArray alloc] init];
    
    self.transparentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), CGRectGetHeight([UIScreen mainScreen].bounds))];
    self.transparentView.backgroundColor = [UIColor blackColor];
    self.transparentView.alpha = 0.0f;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionDissapear)];
    tap.numberOfTapsRequired = 1;
    [self.transparentView addGestureRecognizer:tap];
    
    return self;
}

- (id)initWithTitle:(NSString *)title cancelButtonTitle:(NSString *)cancelTitle destructiveButtonTitle:(NSString *)destructiveTitle withViewController:(UIViewController*)viewController    {
    self = [self init];
    
    WLPActionSheetButton *cancelButton = [[WLPActionSheetButton alloc] initWithAllCornersRounded];
    [cancelButton setTitle:cancelTitle forState:UIControlStateAll];
    [cancelButton setTitleColor:[UIColor colorWithRed:165.0/255.0 green:166.0/255.0 blue:166.0/255.0 alpha:1.0] forState:UIControlStateAll];
    cancelButton.titleLabel.font = [UIFont systemFontOfSize:12];
    [self.buttons addObject:cancelButton];
    
    if(destructiveTitle.length > 0) {
        WLPActionSheetButton *destructiveButton = [[WLPActionSheetButton alloc] initWithAllCornersRounded];
        destructiveButton.tag = 2;
        [destructiveButton setTitle:destructiveTitle forState:UIControlStateAll];
        [destructiveButton setTitleColor:[UIColor colorWithRed:1.000 green:0.229 blue:0.000 alpha:1.000] forState:UIControlStateAll];
        [destructiveButton setOriginalTextColor:[UIColor colorWithRed:1.000 green:0.229 blue:0.000 alpha:1.000]];
        destructiveButton.titleLabel.font = [UIFont systemFontOfSize:12];
        [self.buttons addObject:destructiveButton];
    }
    
    [self setUpTheActions];
    
    self.contentController = viewController;
    self.title = title;
    
    return self;
}

- (id)initWithTitle:(NSString *)title cancelButtonTitle:(NSString *)cancelTitle destructiveButtonTitle:(NSString *)destructiveTitle andMoreButton:(NSString *)moreTitle withViewController:(UIViewController*)viewController    {
    self = [self init];
    
    if(moreTitle.length > 0) {
        WLPActionSheetButton *moreButton = [[WLPActionSheetButton alloc] initWithAllCornersRounded];
        moreButton.tag = 3;
        [moreButton setTitle:moreTitle forState:UIControlStateAll];
        [moreButton setTitleColor:[UIColor colorWithRed:1.000 green:0.229 blue:0.000 alpha:1.000] forState:UIControlStateAll];
        moreButton.titleLabel.font = [UIFont systemFontOfSize:12];
        [moreButton setOriginalTextColor:[UIColor colorWithRed:1.000 green:0.229 blue:0.000 alpha:1.000]];
        [self.buttons addObject:moreButton];
    }
    
    WLPActionSheetButton *cancelButton = [[WLPActionSheetButton alloc] initWithAllCornersRounded];
    [cancelButton setTitle:cancelTitle forState:UIControlStateAll];
    [cancelButton setTitleColor:[UIColor colorWithRed:165.0/255.0 green:166.0/255.0 blue:166.0/255.0 alpha:1.0] forState:UIControlStateAll];
    cancelButton.titleLabel.font = [UIFont systemFontOfSize:12];
    [self.buttons addObject:cancelButton];
    
    if(destructiveTitle.length > 0) {
        WLPActionSheetButton *destructiveButton = [[WLPActionSheetButton alloc] initWithAllCornersRounded];
        destructiveButton.tag = 2;
        [destructiveButton setTitle:destructiveTitle forState:UIControlStateAll];
        [destructiveButton setTitleColor:[UIColor colorWithRed:1.000 green:0.229 blue:0.000 alpha:1.000] forState:UIControlStateAll];
        [destructiveButton setOriginalTextColor:[UIColor colorWithRed:1.000 green:0.229 blue:0.000 alpha:1.000]];
        destructiveButton.titleLabel.font = [UIFont systemFontOfSize:12];
        [self.buttons addObject:destructiveButton];
    }
    
    [self setUpTheActions];
    
    self.contentController = viewController;
    self.title = title;
    
    return self;
}

- (void)setUpTheActions {
    
    for (WLPActionSheetButton *button in self.buttons) {
        if ([button isKindOfClass:[WLPActionSheetButton class]]) {
            [button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [button addTarget:self action:@selector(highlightPressedButton:) forControlEvents:UIControlEventTouchDown];
            [button addTarget:self action:@selector(unhighlightPressedButton:) forControlEvents:UIControlEventTouchUpInside | UIControlEventTouchCancel | UIControlEventTouchDragExit];
        }
    }
}

#pragma mark IBActionSheet Other Properties methods

- (void)setTitle:(NSString *)title {
    self.titleView = [[WLPActionSheetTitleView alloc] initWithTitle:title font:[UIFont fontWithName:@"GothamPro" size:17.5]];
    
    if(self.buttons.count == 1)
        [self setUpTheActionSheet];
    else
        [self setUpTheActionSheet2];
}

- (void)setUpTheActionSheet {
    float height;
    float width;
    
    if (UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation])) {
        width = CGRectGetWidth([UIScreen mainScreen].bounds);
    } else {
        width = CGRectGetHeight([UIScreen mainScreen].bounds);
    }
    
    /*11 - отступ с низу до кнопки
     42 - высота кнопки
     13.5 - отступы между контентов - кнопкой и тайтлом
     47 - высота тайтла
     */
    height = 11 + 42 + 13.5 + self.contentController.view.frame.size.height + 13.5 + 47;
    
    self.frame = CGRectMake(0, 0, width, height);
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    CGPoint pointOfReference = CGPointMake(CGRectGetWidth(self.frame) / 2.0, CGRectGetHeight(self.frame) - 32);
    [self addSubview:[self.buttons lastObject]];
    [[self.buttons lastObject] setCenter:pointOfReference];
    
    pointOfReference = CGPointMake(pointOfReference.x, pointOfReference.y - 34.5 - self.contentController.view.frame.size.height/2);
    [self addSubview:self.contentController.view];
    [self.contentController.view setCenter:pointOfReference];
    
    if (self.titleView) {
        [self addSubview:self.titleView];
        self.titleView.center = CGPointMake(self.center.x, CGRectGetHeight(self.titleView.frame) / 2.0);
    }
}

- (void)setUpTheActionSheet2 {
    float height;
    float width;
    
    if (UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation])) {
        width = CGRectGetWidth([UIScreen mainScreen].bounds);
    } else {
        width = CGRectGetHeight([UIScreen mainScreen].bounds);
    }
    
    /*11 - отступ с низу до кнопки
     42 - высота кнопки
     13.5 - отступы между контентов - кнопкой и тайтлом
     47 - высота тайтла
     */
    height = 11 + (self.buttons.count)*42 + (self.buttons.count-1)*11 + 13.5 + self.contentController.view.frame.size.height + 13.5 + 47;
    
    self.frame = CGRectMake(0, 0, width, height);
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    CGPoint pointOfReference = CGPointMake(CGRectGetWidth(self.frame) / 2.0, CGRectGetHeight(self.frame) - 32);
    [self addSubview:[self.buttons lastObject]];
    [[self.buttons lastObject] setCenter:pointOfReference];
    
    pointOfReference = CGPointMake(CGRectGetWidth(self.frame) / 2.0, pointOfReference.y - 53);
    [self addSubview:[self.buttons firstObject]];
    [[self.buttons firstObject] setCenter:pointOfReference];
    
    pointOfReference = CGPointMake(pointOfReference.x, pointOfReference.y - 34.5 - self.contentController.view.frame.size.height/2);
    [self addSubview:self.contentController.view];
    [self.contentController.view setCenter:pointOfReference];
    
    /*if (self.titleView) {
        [self addSubview:self.titleView];
        self.titleView.center = CGPointMake(self.center.x, CGRectGetHeight(self.titleView.frame) / 2.0);
    }*/
}

- (void)highlightPressedButton:(WLPActionSheetButton *)button {
    
    [UIView animateWithDuration:0.15f
                     animations:^() {
                         
                         button.alpha = .80f;
                         
                     }];
}

- (void)unhighlightPressedButton:(WLPActionSheetButton *)button {
    
    [UIView animateWithDuration:0.3f
                     animations:^() {
                         
                         button.alpha = 1.0f;
                     }];
    
}

- (void)buttonClicked:(WLPActionSheetButton *)button {
    [self removeFromView];
    
    if(button.tag == 3)
    {
        if([self.delegate respondsToSelector:@selector(actionSheetDidMore:)])
        {
            [self.delegate actionSheetDidMore:self];
        }
    }
    if(button.tag == 2)
    {
        if([self.delegate respondsToSelector:@selector(actionSheetDidDestructive:)])
        {
            [self.delegate actionSheetDidDestructive:self];
        }
    }
    else
    {
        if([self.delegate respondsToSelector:@selector(actionSheetDidDone:)])
        {
            [self.delegate actionSheetDidDone:self];
        }
    }
}

- (void)showInView:(UIView *)theView {
    
    [theView addSubview:self];
    [theView insertSubview:self.transparentView belowSubview:self];
    
    CGRect theScreenRect = [UIScreen mainScreen].bounds;
    
    float height;
    float x;
    
    if (UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation])) {
        height = CGRectGetHeight(theScreenRect);
        x = CGRectGetWidth(theView.frame) / 2.0;
        self.transparentView.frame = CGRectMake(self.transparentView.center.x, self.transparentView.center.y, CGRectGetWidth(theScreenRect), CGRectGetHeight(theScreenRect));
    } else {
        height = CGRectGetWidth(theScreenRect);
        x = CGRectGetHeight(theView.frame) / 2.0;
        self.transparentView.frame = CGRectMake(self.transparentView.center.x, self.transparentView.center.y, CGRectGetHeight(theScreenRect), CGRectGetWidth(theScreenRect));
    }
    
    self.center = CGPointMake(x, height + CGRectGetHeight(self.frame) / 2.0-50);
    self.transparentView.center = CGPointMake(x, height / 2.0);
    
    //[self.contentController viewWillAppear:YES];
    
    if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
        
        
        [UIView animateWithDuration:0.2f
                              delay:0.0f
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^() {
                             self.transparentView.alpha = 0.4f;
                             self.center = CGPointMake(x, (height - 20) - CGRectGetHeight(self.frame) / 2.0);
                             
                         } completion:^(BOOL finished) {
                             
                         }];
    } else {
        
        [UIView animateWithDuration:0.5f
                              delay:0
             usingSpringWithDamping:0.6f
              initialSpringVelocity:0
                            options:UIViewAnimationOptionCurveLinear
                         animations:^{
                             self.transparentView.alpha = 0.4f;
                             self.center = CGPointMake(x, height - CGRectGetHeight(self.frame) / 2.0-50);
                             
                         } completion:^(BOOL finished) {
                             
                         }];
    }
}

-(void) actionDissapear {
    [self removeFromView];
    
    if([self.delegate respondsToSelector:@selector(actionSheetDidDissapear:)])   {
        [self.delegate actionSheetDidDissapear:self];
    }
}

- (void)removeFromView {
    
    if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
        
        [UIView animateWithDuration:0.2f
                              delay:0.0f
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^() {
                             self.transparentView.alpha = 0.0f;
                             self.center = CGPointMake(CGRectGetWidth(self.frame) / 2.0, CGRectGetHeight([UIScreen mainScreen].bounds) + CGRectGetHeight(self.frame) / 2.0);
                             
                         } completion:^(BOOL finished) {
                             [self.transparentView removeFromSuperview];
                             [self removeFromSuperview];
                         }];
    } else {
        
        [UIView animateWithDuration:0.5f
                              delay:0
             usingSpringWithDamping:0.6f
              initialSpringVelocity:0
                            options:UIViewAnimationOptionCurveLinear
                         animations:^{
                             self.transparentView.alpha = 0.0f;
                             self.center = CGPointMake(CGRectGetWidth(self.frame) / 2.0, CGRectGetHeight([UIScreen mainScreen].bounds) + CGRectGetHeight(self.frame) / 2.0);
                             
                         } completion:^(BOOL finished) {
                             [self.transparentView removeFromSuperview];
                             [self removeFromSuperview];
                         }];
    }
}

@end


#pragma mark - WLPActionSheetButton

@implementation WLPActionSheetButton


- (id)init {
    
    float width;
    
    if (UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation])) {
        width = CGRectGetWidth([UIScreen mainScreen].bounds);
    } else {
        width = CGRectGetHeight([UIScreen mainScreen].bounds);
    }
    self = [self initWithFrame:CGRectMake(0, 0, width - 16, 42)];
    
    self.backgroundColor = [UIColor whiteColor];
    self.originalBackgroundColor = self.backgroundColor;
    self.titleLabel.font = [UIFont fontWithName:@"GothamPro-Bold" size:15];
    [self setTitleColor:[UIColor colorWithRed:29.0/255.0 green:123.0/255.0 blue:84.0/255.0 alpha:1.0] forState:UIControlStateAll];
    self.originalTextColor = [UIColor colorWithRed:29.0/255.0 green:123.0/255.0 blue:84.0/255.0 alpha:1.0];
    
    self.alpha = 1.0f; //0.95f;
    
    return self;
}

- (id)initWithTopCornersRounded {
    self = [self init];
    [self setMaskTo:self byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight];
    return self;
}

- (id)initWithBottomCornersRounded {
    self = [self init];
    [self setMaskTo:self byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight];
    return self;
}

- (id)initWithAllCornersRounded {
    self = [self init];
    [self setMaskTo:self byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight | UIRectCornerBottomLeft | UIRectCornerBottomRight];
    return self;
}

-(void) setButtonImage:(UIImage*)image  {
    [self setImage:image forState:UIControlStateAll];
    self.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 14);
    [self setAdjustsImageWhenHighlighted:NO];
}

- (void)setTextColor:(UIColor *)color {
    [self setTitleColor:color forState:UIControlStateAll];
}

- (void)setMaskTo:(UIView*)view byRoundingCorners:(UIRectCorner)corners
{
    UIBezierPath *rounded = [UIBezierPath bezierPathWithRoundedRect:view.bounds
                                                  byRoundingCorners:corners
                                                        cornerRadii:CGSizeMake(4.0, 4.0)];
    CAShapeLayer *shape = [[CAShapeLayer alloc] init];
    [shape setPath:rounded.CGPath];
    view.layer.mask = shape;
}
@end



#pragma mark - WLPActionSheetTitleView

@implementation WLPActionSheetTitleView

- (id)initWithTitle:(NSString *)title font:(UIFont *)font {
    
    self = [self init];
    
    float width;
    float labelBuffer;
    
    if (UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation])) {
        width = CGRectGetWidth([UIScreen mainScreen].bounds);
        labelBuffer = 47;
    } else {
        width = CGRectGetHeight([UIScreen mainScreen].bounds);
        labelBuffer = 24;
    }
    
    self.alpha = 1.0f; //.95f;
    self.backgroundColor = getColorScheme(); //[UIColor colorWithRed:234.0/255.0 green:233.0/255.0 blue:233.0/255.0 alpha:1.0];
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width - labelBuffer, 44)];
    self.titleLabel.textColor = [UIColor whiteColor]; ///[UIColor colorWithRed:108.0/255.0 green:108.0/255.0 blue:108.0/255.0 alpha:1.0];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.numberOfLines = 0;
    self.titleLabel.text = title;
    self.titleLabel.backgroundColor = [UIColor clearColor];
    
    if (font) {
        self.titleLabel.font = [UIFont systemFontOfSize:14];//font;
    } else {
        self.titleLabel.font = [UIFont systemFontOfSize:14];
    }
    
    [self.titleLabel sizeToFit];
    
    if ((CGRectGetHeight(self.titleLabel.frame) + 30) < 47) {
        self.frame = CGRectMake(0, 0, width, 47);
    } else {
        self.frame = CGRectMake(0, 0, width, CGRectGetHeight(self.titleLabel.frame) + 30);
    }
    
    [self addSubview:self.titleLabel];
    self.titleLabel.center = self.center;
    
    return self;
}

- (void)setMaskTo:(UIView*)view byRoundingCorners:(UIRectCorner)corners
{
    UIBezierPath *rounded = [UIBezierPath bezierPathWithRoundedRect:view.bounds
                                                  byRoundingCorners:corners
                                                        cornerRadii:CGSizeMake(4.0, 4.0)];
    CAShapeLayer *shape = [[CAShapeLayer alloc] init];
    [shape setPath:rounded.CGPath];
    view.layer.mask = shape;
}

@end

