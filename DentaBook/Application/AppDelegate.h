//
//  AppDelegate.h
//  DentaBook
//
//  Created by Denis Dubov on 04.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "DBSideMenuViewController.h"

#define kMainViewController [(AppDelegate *)[[UIApplication sharedApplication] delegate] sideMenuViewController]
#define kNavigationController (UINavigationController *)[[(AppDelegate *)[[UIApplication sharedApplication] delegate] sideMenuViewController] rootViewController]

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (nonatomic, strong) DBSideMenuViewController *sideMenuViewController;
@property (nonatomic, strong) UIView *statusBg;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

-(void) switchToLogin;
-(void) switchToApp;

@end

