//
//  DBLayoutManager.h
//  DentaBook
//
//  Created by Denis Dubov on 07.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DBLayoutManager : NSObject

+ (DBLayoutManager *)shared;

-(void) openSyncScreen;
-(void) closeSyncScreen;

@end
