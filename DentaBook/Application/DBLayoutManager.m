//
//  DBLayoutManager.m
//  DentaBook
//
//  Created by Denis Dubov on 07.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "DBLayoutManager.h"
#import "SyncViewController.h"

@interface DBLayoutManager()
@property (nonatomic) SyncViewController *syncViewController;
@property (nonatomic) UIWindow *syncWindow;
@end

@implementation DBLayoutManager

+ (DBLayoutManager *)shared {
    
    static DBLayoutManager *shared = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        shared = [[self alloc] init];
    });
    
    return shared;
}

-(id)init{
    
    self = [super init];
    if (self) {
        
    }
    return self;
}

-(void) openSyncScreen    {
    UIWindow *syncWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    syncWindow.windowLevel = UIWindowLevelAlert;
    
    SyncViewController *syncViewController = [SyncViewController new];
    syncViewController.modalPresentationStyle = UIModalPresentationFullScreen;
    syncViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    self.syncViewController = syncViewController;
    
    syncWindow.rootViewController = syncViewController;
    self.syncWindow = syncWindow;
    [self.syncWindow makeKeyAndVisible];
}

-(void) closeSyncScreen {
    [UIView animateWithDuration:0.5 animations:^{
        self.syncViewController.view.alpha = 0.0f;
    } completion:^(BOOL finished) {
        self.syncViewController = nil;
        [[UIApplication sharedApplication].delegate.window makeKeyAndVisible];
         self.syncWindow = nil;
    }];
}

@end
