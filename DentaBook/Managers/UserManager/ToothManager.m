//
//  ToothManager.m
//  DentaBook
//
//  Created by Denis Dubov on 08.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "ToothManager.h"
#import "SafeCategories.h"

#import <ABMultiton/ABMultiton.h>
#import <MagicalRecord/MagicalRecord.h>

#import "Patient+Custom.h"
#import "Tooth+Custom.h"
#import "ToothList+Custom.h"
#import "DataManager.h"
#import "DataBase.h"

@implementation ToothManager

+ (id)sharedInstance
{
    return [ABMultiton sharedInstanceOfClass:[self class]];
}

-(id)init{
    
    self = [super init];
    if (self) {
        //
    }
    return self;
}

-(void) createToothMap:(NSDictionary*)toothListEntity
               success:(void (^)())success
               failure:(void (^)())failure
{
    
    /*NSMutableArray *toothItemArray = [NSMutableArray new];
    for(Tooth *tooth in toothListEntity.tooths) {
        id params = @{@"index": tooth.index,
                      @"comment": (tooth.comment.length > 0)?tooth.comment:@"",
                      @"diagnosisId": tooth.dentalDiagnosisId};
        [toothItemArray addObject:params];
    }
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSString *dateString = [dateFormatter stringFromDate:toothListEntity.createAt];
    
    id params = @{@"id": toothListEntity.patient.remoteId,
                  @"type": toothListEntity.type,
                  @"comment": (toothListEntity.comment.length > 0)?toothListEntity.comment:@"",
                  @"date": dateString,
                  @"items": toothItemArray};*/
    
    [DataManager loadCookies];
    [DataBase putDB:toothListEntity toTable:@"toothmap" withKey:@"id"];
    success();
    /*[DataManager post:[NSString stringWithFormat:@"tooth"]
               params:params
              success:^(AFHTTPRequestOperation *operation, id json) {
                  
                  if ([json isKindOfClass:[NSDictionary class]]) {
                      FEMManagedObjectMapping *mapping = [ToothList mapObject];
                      [FEMManagedObjectDeserializer fillObject:toothListEntity fromExternalRepresentation:json usingMapping:mapping];
                      [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                      
                      success();
                  } else {
                      failure();
                  }
                  
              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  failure();
              }];*/
}

-(void) updateToothMap:(NSDictionary*)toothListEntity
               success:(void (^)())success
               failure:(void (^)())failure   {
    
    /*NSMutableArray *toothItemArray = [NSMutableArray new];
    for(Tooth *tooth in toothListEntity.tooths) {
        id params = @{@"index": tooth.index,
                      @"comment": (tooth.comment.length > 0)?tooth.comment:@"",
                      @"diagnosisId": tooth.dentalDiagnosisId};
        [toothItemArray addObject:params];
    }
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSString *dateString = [dateFormatter stringFromDate:toothListEntity.createAt];
    
    id params = @{@"id": toothListEntity.remoteId,
                  @"type": toothListEntity.type,
                  @"comment": (toothListEntity.comment.length > 0)?toothListEntity.comment:@"",
                  @"date": dateString,
                  @"items": toothItemArray};*/
    
    [DataManager loadCookies];
    [DataBase putDB:toothListEntity toTable:@"toothmap" withKey:@"id"];
    success();
    /*[DataManager put:[NSString stringWithFormat:@"tooth"]
              params:params
             success:^(AFHTTPRequestOperation *operation, id json) {
                 
                 if ([json isKindOfClass:[NSDictionary class]]) {
                     FEMManagedObjectMapping *mapping = [ToothList mapObject];
                     [FEMManagedObjectDeserializer fillObject:toothListEntity fromExternalRepresentation:json usingMapping:mapping];
                     [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                     
                     success();
                 } else {
                     failure();
                 }
                 
             } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 failure();
             }];*/
}

@end
