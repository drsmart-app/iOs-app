//
//  UserManager.h
//  DentaBook
//
//  Created by Denis Dubov on 04.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import <ABMultiton/ABMultitonProtocol.h>

@interface UserManager : NSObject <ABMultitonProtocol>

-(void) registerWithEmail:(NSString*)email
                 password:(NSString*)password
                  success:(void (^)())success
                  failure:(void (^)())failure;

-(void) loginWithEmail:(NSString*)email
              password:(NSString*)password
               success:(void (^)())success
               failure:(void (^)())failure;

-(void) logout:(void (^)())success
       failure:(void (^)())failure;

@end
