//
//  DiagnosisManager.h
//  DentaBook
//
//  Created by Denis Dubov on 05.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import <ABMultiton/ABMultitonProtocol.h>
#import <AFNetworking/AFNetworking.h>

@interface DiagnosisManager : NSObject <ABMultitonProtocol>

-(void) createDiagnosisByTitle:(NSString*)title
                      subTitle:(NSString*)subtitle
                       success:(void (^)())success
                       failure:(void (^)())failure;

-(void) deleteDiagnosisById:(NSString*)title
                    success:(void (^)())success
                    failure:(void (^)())failure;

-(AFHTTPRequestOperation*) syncDiagnosis:(void (^)())success
                                 failure:(void (^)())failure;

@end
