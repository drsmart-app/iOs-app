//
//  TherapyManager.h
//  DentaBook
//
//  Created by Denis Dubov on 06.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import <ABMultiton/ABMultitonProtocol.h>
#import <AFNetworking/AFNetworking.h>

@interface TherapyManager : NSObject <ABMultitonProtocol>

-(void) createTherapyCategoryByTitle:(NSString*)title
                             success:(void (^)())success
                             failure:(void (^)())failure;
-(void) deleteTherapyCategoryById:(NSString*)_id
                          success:(void (^)())success
                          failure:(void (^)())failure;

-(void) createTherapyServiceByTitle:(NSString*)title
                               cost:(double)cost
                     linkToRemoteId:(NSNumber*)remoteId
                            success:(void (^)())success
                            failure:(void (^)())failure;
-(void) deleteTherapyServiceById:(NSString*)_id
                         success:(void (^)())success
                         failure:(void (^)())failure;

-(AFHTTPRequestOperation*) syncTherapyCategory:(void (^)())success
                                       failure:(void (^)())failure;
-(AFHTTPRequestOperation*) syncTherapyServices:(void (^)())success
                                       failure:(void (^)())failure;

@end
