//
//  UserManager.m
//  DentaBook
//
//  Created by Denis Dubov on 04.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "UserManager.h"
#import <ABMultiton/ABMultiton.h>
#import <MagicalRecord/MagicalRecord.h>

#import "User+Custom.h"
#import "DataManager.h"

#import "CDManager.h"
#import "SafeCategories.h"
#import "ImageCacheManager.h"

@implementation UserManager

+ (id)sharedInstance
{
    return [ABMultiton sharedInstanceOfClass:[self class]];
}

-(id)init{
    
    self = [super init];
    if (self) {
        //
    }
    return self;
}

-(void) registerWithEmail:(NSString*)email
                 password:(NSString*)password
                  success:(void (^)())success
                  failure:(void (^)())failure  {
    
    id params = @{@"username": email,
                  @"password": password};
    
    [DataManager post:[NSString stringWithFormat:@"register"]
               params:params
              success:^(AFHTTPRequestOperation *operation, id json) {
                  [DataManager saveCookies];
                  
                  if ([json isKindOfClass:[NSDictionary class]]) {
                      NSNumber* remoteID = [json safeNumberObjectForKey:@"id"];
                      
                      User* user = [User MR_findFirstByAttribute:@"remoteId" withValue:remoteID];
                      if (!user) {
                          user = [User MR_createEntity];
                      }
                      
                      FEMManagedObjectMapping *mapping = [User mapObject];
                      [FEMManagedObjectDeserializer fillObject:user fromExternalRepresentation:json usingMapping:mapping];
                      [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                      
                      success();
                  } else {
                      failure();
                  }
              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  failure();
              }];
}

-(void) loginWithEmail:(NSString*)email
              password:(NSString*)password
               success:(void (^)())success
               failure:(void (^)())failure  {
    
    id params = @{@"username": email,
                  @"password": password};
    
    [DataManager post:[NSString stringWithFormat:@"login"]
               params:params
              success:^(AFHTTPRequestOperation *operation, id json) {
                  [DataManager saveCookies];
                  
                  if ([json isKindOfClass:[NSDictionary class]]) {
                      NSNumber* remoteID = [json safeNumberObjectForKey:@"id"];
                      
                      User* user = [User MR_findFirstByAttribute:@"remoteId" withValue:remoteID];
                      if (!user) {
                          user = [User MR_createEntity];
                      }
                      
                      FEMManagedObjectMapping *mapping = [User mapObject];
                      [FEMManagedObjectDeserializer fillObject:user fromExternalRepresentation:json usingMapping:mapping];
                      [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                      
                      success();
                  } else {
                      failure();
                  }
              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  failure();
              }];
}

-(void) logout:(void (^)())success
       failure:(void (^)())failure  {
    
    [DataManager loadCookies];
    [DataManager post:[NSString stringWithFormat:@"logout"]
               params:nil
              success:^(AFHTTPRequestOperation *operation, id json) {
                  success();
              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  failure();
              }];
    
    [self clearAll];
}

-(void) clearAll    {
    [DataManager clearCoockie];
    [DataManager deleteCoockies];
    
    [[ImageCacheManager shared].defaultFileCache removeAllFiles];
    [[ImageCacheManager shared].defaultMemoryCach removeAllObjects];
    
    [[CDManager shared] cleanAndResetupDB];
}

@end
