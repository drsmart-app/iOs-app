//
//  GalleryManager.h
//  DentaBook
//
//  Created by Denis Dubov on 12.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import <ABMultiton/ABMultitonProtocol.h>
#import <AFNetworking/AFNetworking.h>

@class Patient, Gallery;

@interface GalleryManager : NSObject <ABMultitonProtocol>

-(void) createGallery:(NSDictionary*)galleryEnriry
              success:(void (^)())success
              failure:(void (^)())failure;

- (void)uploadPhoto:(UIImage*)image
          toPatient:(NSDictionary*)galleryEnriry
         completion:(void(^)(NSError* error))completion;

- (void)uploadPhoto:(UIImage*)image
           toAffair:(NSDictionary*)galleryEntity
         completion:(void(^)(NSError* error))completion;

- (void)uploadPhoto:(UIImage*)image
          toExpence:(NSDictionary*)galleryEntity
         completion:(void(^)(NSError* error))completion;

- (void)uploadPhoto:(UIImage*)image
          forProfile:(BOOL) profile
         completion:(void(^)(NSError* error))completion;

@end
