//
//  PatientTherapyManager.h
//  DentaBook
//
//  Created by Denis Dubov on 08.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import <ABMultiton/ABMultitonProtocol.h>
#import <AFNetworking/AFNetworking.h>

@class Therapy;

@interface PatientTherapyManager : NSObject <ABMultitonProtocol>

-(void) createTherapyMap:(NSDictionary*)therapyEntity
                 success:(void (^)())success
                 failure:(void (^)())failure;

-(void) updateTherapyMap:(NSDictionary*)therapyEntity
                 success:(void (^)())success
                 failure:(void (^)())failure;

@end
