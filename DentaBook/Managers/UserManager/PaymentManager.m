//
//  PaymentManager.m
//  DentaBook
//
//  Created by Denis Dubov on 10.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "PaymentManager.h"
#import "SafeCategories.h"

#import <ABMultiton/ABMultiton.h>
#import <MagicalRecord/MagicalRecord.h>

#import "Patient+Custom.h"
#import "Payment+Custom.h"
#import "DataManager.h"
#import "DataBase.h"

@implementation PaymentManager

+ (id)sharedInstance
{
    return [ABMultiton sharedInstanceOfClass:[self class]];
}

-(id)init{
    
    self = [super init];
    if (self) {
        //
    }
    return self;
}

-(void) createPayment:(NSDictionary*)paymentEntity
              success:(void (^)())success
              failure:(void (^)())failure  {
    
    //NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    //NSString *dateString = [dateFormatter stringFromDate:paymentEntity.createAt];
    
    /*id params = @{@"id": paymentEntity.patient.remoteId,
                  @"amount": paymentEntity.amount,
                  @"date": dateString};*/
    
    [DataManager loadCookies];
    [DataBase putDB:paymentEntity toTable:@"payment" withKey:@"id"];
    success();
    /*[DataManager post:[NSString stringWithFormat:@"payment"]
               params:params
              success:^(AFHTTPRequestOperation *operation, id json) {
                  
                  if ([json isKindOfClass:[NSDictionary class]]) {
                      FEMManagedObjectMapping *mapping = [Payment mapObject];
                      [FEMManagedObjectDeserializer fillObject:paymentEntity fromExternalRepresentation:json usingMapping:mapping];
                      [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                      
                      success();
                  } else {
                      failure();
                  }
                  
              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  failure();
              }];*/
}

@end
