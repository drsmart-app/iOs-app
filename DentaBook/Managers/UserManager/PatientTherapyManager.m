//
//  PatientTherapyManager.m
//  DentaBook
//
//  Created by Denis Dubov on 08.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "PatientTherapyManager.h"
#import "SafeCategories.h"

#import <ABMultiton/ABMultiton.h>
#import <MagicalRecord/MagicalRecord.h>

#import "Patient+Custom.h"
#import "Therapy+Custom.h"
#import "TherapyItem+Custom.h"
#import "DataManager.h"
#import "DataBase.h"

@implementation PatientTherapyManager

+ (id)sharedInstance
{
    return [ABMultiton sharedInstanceOfClass:[self class]];
}

-(id)init{
    
    self = [super init];
    if (self) {
        //
    }
    return self;
}

-(void) createTherapyMap:(NSDictionary*)therapyEntity
                 success:(void (^)())success
                 failure:(void (^)())failure
{
    
    /*NSMutableArray *thearpyItemArray = [NSMutableArray new];
    for(TherapyItem *therapyItem in therapyEntity.therapyItems)
    {
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        NSString *dateString = [dateFormatter stringFromDate:therapyItem.createAt];
        
        id params = @{@"categoryId": therapyItem.categoryId,
                      @"serviceId": therapyItem.serviceId,
                      @"toothId": therapyItem.toothId,
                      @"count": therapyItem.count,
                      @"date": dateString};
        [thearpyItemArray addObject:params];
    }
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSString *dateString = [dateFormatter stringFromDate:therapyEntity.createAt];
    
    id params = @{@"id": therapyEntity.patient.remoteId,
                  @"title": (therapyEntity.name.length > 0) ? therapyEntity.name : @"",
                  @"discount": therapyEntity.discount,
                  @"isPlan": therapyEntity.isPlan,
                  @"date": dateString,
                  @"items": thearpyItemArray};*/
    
    [DataManager loadCookies];
    [DataBase putDB:therapyEntity toTable:@"patient_therapy" withKey:@"id"];
    success();
    /*[DataManager post:[NSString stringWithFormat:@"therapy"]
               params:params
              success:^(AFHTTPRequestOperation *operation, id json) {
                  
                  if ([json isKindOfClass:[NSDictionary class]]) {
                      FEMManagedObjectMapping *mapping = [Therapy mapObject];
                      [FEMManagedObjectDeserializer fillObject:therapyEntity fromExternalRepresentation:json usingMapping:mapping];
                      [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                      
                      success();
                  } else {
                      failure();
                  }
                  
              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  failure();
              }];*/
}

-(void) updateTherapyMap:(NSDictionary*)therapyEntity
                 success:(void (^)())success
                 failure:(void (^)())failure
{
    
    /*NSMutableArray *thearpyItemArray = [NSMutableArray new];
    for(TherapyItem *therapyItem in therapyEntity.therapyItems) {
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        NSString *dateString = [dateFormatter stringFromDate:therapyItem.createAt];
        
        id params = @{@"categoryId": therapyItem.categoryId,
                      @"serviceId": therapyItem.serviceId,
                      @"toothId": therapyItem.toothId,
                      @"count": therapyItem.count,
                      @"date": dateString};
        [thearpyItemArray addObject:params];
    }
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSString *dateString = [dateFormatter stringFromDate:therapyEntity.createAt];
    
    id params = @{@"id": therapyEntity.remoteId,
                  @"title": (therapyEntity.name.length > 0) ? therapyEntity.name : @"",
                  @"discount": therapyEntity.discount,
                  @"isPlan": therapyEntity.isPlan,
                  @"date": dateString,
                  @"items": thearpyItemArray};*/
    
    [DataManager loadCookies];
    [DataBase putDB:therapyEntity toTable:@"patient_therapy" withKey:@"id"];
    success();
    /*[DataManager put:[NSString stringWithFormat:@"therapy"]
              params:params
             success:^(AFHTTPRequestOperation *operation, id json) {
                  
                  if ([json isKindOfClass:[NSDictionary class]]) {
                      FEMManagedObjectMapping *mapping = [Therapy mapObject];
                      [FEMManagedObjectDeserializer fillObject:therapyEntity fromExternalRepresentation:json usingMapping:mapping];
                      [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                      
                      success();
                  } else {
                      failure();
                  }
                  
              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  failure();
              }];*/
}

@end
