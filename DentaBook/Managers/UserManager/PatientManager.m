//
//  PatientManager.m
//  DentaBook
//
//  Created by Denis Dubov on 08.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "PatientManager.h"
#import "SafeCategories.h"

#import <ABMultiton/ABMultiton.h>
#import <MagicalRecord/MagicalRecord.h>

#import "Patient+Custom.h"
#import "DataManager.h"

#import "ImageRequestModel.h"
#import "ImageFileCache.h"
#import "ImageCache.h"
#import "DataBase.h"

@implementation PatientManager

+ (id)sharedInstance
{
    return [ABMultiton sharedInstanceOfClass:[self class]];
}

-(id)init{
    
    self = [super init];
    if (self) {
        //
    }
    return self;
}

-(void) createPatient:(NSDictionary*)patientEntity
              success:(void (^)())success
              failure:(void (^)())failure  {
    
    /*id params = @{@"firstname": patientEntity[@"firstname"],
                  @"lastname": patientEntity[@"lastname"],
                  @"middlename": patientEntity[@"middlename"],
                  @"rating": patientEntity[@"rating"],
                  @"phonenumber": patientEntity[@"phonenumber"],
                  @"birthdate": patientEntity[@"birthdate"],
                  @"patientid":patientEntity[@"patientid"]};*/
    
    [DataManager loadCookies];
    [DataBase putDB:patientEntity toTable:@"patient" withKey:@"patientid"];
    success();
    /*[DataManager post:[NSString stringWithFormat:@"patient"]
               params:params
              success:^(AFHTTPRequestOperation *operation, id json) {
                  
                  if ([json isKindOfClass:[NSDictionary class]]) {
                      FEMManagedObjectMapping *mapping = [Patient mapObject];
                      [FEMManagedObjectDeserializer fillObject:patientEntity fromExternalRepresentation:json usingMapping:mapping];
                      [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                      
                      success();
                  } else {
                      failure();
                  }
                  
              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  failure();
              }];*/
}

-(void) updatePatient:(NSDictionary*)patientEntity
              success:(void (^)())success
              failure:(void (^)())failure   {
    
    /*id params = @{@"firstname": patientEntity[@"firstname"],
                  @"lastname": patientEntity[@"lastname"],
                  @"middlename": patientEntity[@"middlename"],
                  @"rating": patientEntity[@"rating"],
                  @"phonenumber": patientEntity[@"phonenumber"],
                  @"birthdate": patientEntity[@"birthdate"],
                  @"patientid":patientEntity[@"patientid"]};*/
    
    [DataManager loadCookies];
    [DataBase putDB:patientEntity toTable:@"patient" withKey:@"patientid"];
    success();
    /*[DataManager put:[NSString stringWithFormat:@"patient"]
               params:params
              success:^(AFHTTPRequestOperation *operation, id json) {
                  
                  if ([json isKindOfClass:[NSDictionary class]]) {
                      FEMManagedObjectMapping *mapping = [Patient mapObject];
                      [FEMManagedObjectDeserializer fillObject:patientEntity fromExternalRepresentation:json usingMapping:mapping];
                      [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                      
                      success();
                  } else {
                      failure();
                  }
                  
              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  failure();
              }];*/
}

-(void) destroyPatient:(NSDictionary*)patient
               success:(void (^)())success
               failure:(void (^)())failure  {
    [DataManager loadCookies];
    [DataBase killDB:@"patient" withKey:@"patientid" andValue:patient[@"patientid"]];
    [DataBase killDB:@"payment" withKey:@"patientid" andValue:patient[@"patientid"]];
    [DataBase killDB:@"visits" withKey:@"patientid" andValue:patient[@"patientid"]];
    [DataBase killDB:@"events" withKey:@"patientid" andValue:patient[@"patientid"]];
    
    success();
    /*[DataManager delete:[NSString stringWithFormat:@"patient/%ld", [patient.remoteId longValue]]
               params:nil
              success:^(AFHTTPRequestOperation *operation, id json) {
                  
                  [patient MR_deleteEntity];
                  [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                  success();
                  
              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  failure();
              }];*/
}

-(AFHTTPRequestOperation*) syncPatients:(void (^)())success
                                failure:(void (^)())failure  {
    return [DataManager createGetHTTPRequestOperation:[NSString stringWithFormat:@"patient"]
                                               params:nil
                                              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                  if ([responseObject isKindOfClass:[NSArray class]]) {
                                                      [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
                                                          for (NSDictionary* jsonDict in responseObject) {
                                                              if ([jsonDict isKindOfClass:[NSDictionary class]]) {
                                                                  NSNumber* remoteID = [jsonDict safeNumberObjectForKey:@"id"];
                                                                  
                                                                  Patient* patientEntity = [Patient MR_findFirstByAttribute:@"remoteId" withValue:remoteID inContext:localContext];
                                                                  if (!patientEntity) {
                                                                      patientEntity = [Patient MR_createEntityInContext:localContext];
                                                                  }
                                                                  
                                                                  FEMManagedObjectMapping *mapping = [Patient mapObjectSync];
                                                                  [FEMManagedObjectDeserializer fillObject:patientEntity fromExternalRepresentation:jsonDict usingMapping:mapping];
                                                              }
                                                          }
                                                          
                                                      } completion:^(BOOL _success, NSError *error) {
                                                          success();
                                                      }];
                                                  }else{
                                                      failure();
                                                  }
                                              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                  failure();
                                              }];
}

- (void)uploadLogo:(UIImage*)image
         toPatient:(NSDictionary*)patient
        completion:(void(^)(NSError* error))completion {
    
    if(!image)
        return;
    
    ImageRequestModel *imageRequestModel = [[ImageRequestModel alloc] init];
    imageRequestModel.urlString = [NSString stringWithFormat:[DataManager urlForPath:@"patient/%@/image"], patient[@"patientid"]];
    [imageRequestModel.fileCache cacheImage:image forKey:imageRequestModel.md5Hash];
    [imageRequestModel.memoryCache cacheImage:image forKey:imageRequestModel.md5Hash];
    
    /*[DataManager post:[NSString stringWithFormat:@"patient/%ld/image", [patient.remoteId longValue]]
               params:nil
                image:image
           imageField:@"image" success:^(AFHTTPRequestOperation *operation, id json) {
               NSLog(@"Image upload successfully");
               completion(nil);
           } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
               NSLog(@"Image upload failure");
               completion(error);
           }];*/
}

@end
