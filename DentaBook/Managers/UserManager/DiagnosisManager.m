//
//  DiagnosisManager.m
//  DentaBook
//
//  Created by Denis Dubov on 05.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "DiagnosisManager.h"
#import "SafeCategories.h"

#import <ABMultiton/ABMultiton.h>
#import <MagicalRecord/MagicalRecord.h>

#import "DentalDiagnosis+Custom.h"
#import "DataManager.h"
#import "DataBase.h"

@implementation DiagnosisManager

+ (id)sharedInstance
{
    return [ABMultiton sharedInstanceOfClass:[self class]];
}

-(id)init{
    
    self = [super init];
    if (self) {
        //
    }
    return self;
}

-(void) createDiagnosisByTitle:(NSString*)title
                      subTitle:(NSString*)subtitle
                     success:(void (^)())success
                     failure:(void (^)())failure  {
    
    id params = @{@"title": title,
                  @"subtitle": subtitle};
    
    [DataManager loadCookies];
    [DataBase putDB:@{@"id":GUIDString(), @"title":title, @"subtitle":subtitle} toTable:@"diagnosis" withKey:@"title"];
    [DataManager post:[NSString stringWithFormat:@"diagnosis"]
               params:params
              success:^(AFHTTPRequestOperation *operation, id json) {
                  
                  if ([json isKindOfClass:[NSDictionary class]]) {
                      NSNumber* remoteID = [json safeNumberObjectForKey:@"id"];
                      
                      DentalDiagnosis* diagnosisEntity = [DentalDiagnosis MR_findFirstByAttribute:@"remoteId" withValue:remoteID];
                      if (!diagnosisEntity) {
                          diagnosisEntity = [DentalDiagnosis MR_createEntity];
                      }
                      
                      FEMManagedObjectMapping *mapping = [DentalDiagnosis mapObject];
                      [FEMManagedObjectDeserializer fillObject:diagnosisEntity fromExternalRepresentation:json usingMapping:mapping];
                      [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                      
                      success();
                  } else {
                      failure();
                  }
                  
              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  failure();
              }];
}

-(void) deleteDiagnosisById:(NSString*)_id
                  success:(void (^)())success
                  failure:(void (^)())failure  {
    
    [DataManager loadCookies];
    [DataBase killDB:@"diagnosis" withKey:@"id" andValue:_id];
    success();
    /*[DataManager delete:[NSString stringWithFormat:@"diagnosis/%ld", [remoteId longValue]]
                 params:nil
                success:^(AFHTTPRequestOperation *operation, id json) {
                    
                    DentalDiagnosis* diagnosisEntity = [DentalDiagnosis MR_findFirstByAttribute:@"remoteId" withValue:remoteId];
                    if (diagnosisEntity) {
                        [diagnosisEntity MR_deleteEntity];
                        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                    }
                    success();
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    failure();
                }];*/
}

-(AFHTTPRequestOperation*) syncDiagnosis:(void (^)())success
                                 failure:(void (^)())failure  {
    return [DataManager createGetHTTPRequestOperation:[NSString stringWithFormat:@"diagnosis"]
                                               params:nil
                                              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                  if ([responseObject isKindOfClass:[NSArray class]]) {
                                                      [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
                                                          for (NSDictionary* jsonDict in responseObject) {
                                                              if ([jsonDict isKindOfClass:[NSDictionary class]]) {
                                                                  NSNumber* remoteID = [jsonDict safeNumberObjectForKey:@"id"];
                                                                  
                                                                  DentalDiagnosis* diagnosisEntity = [DentalDiagnosis MR_findFirstByAttribute:@"remoteId" withValue:remoteID inContext:localContext];
                                                                  if (!diagnosisEntity) {
                                                                      diagnosisEntity = [DentalDiagnosis MR_createEntityInContext:localContext];
                                                                  }
                                                                  
                                                                  FEMManagedObjectMapping *mapping = [DentalDiagnosis mapObject];
                                                                  [FEMManagedObjectDeserializer fillObject:diagnosisEntity fromExternalRepresentation:jsonDict usingMapping:mapping];
                                                              }
                                                          }
                                                          
                                                      } completion:^(BOOL _success, NSError *error) {
                                                          success();
                                                      }];
                                                  }else{
                                                      failure();
                                                  }
                                              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                  failure();
                                              }];
}

@end
