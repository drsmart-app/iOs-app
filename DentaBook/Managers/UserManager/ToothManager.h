//
//  ToothManager.h
//  DentaBook
//
//  Created by Denis Dubov on 08.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import <ABMultiton/ABMultitonProtocol.h>
#import <AFNetworking/AFNetworking.h>

@class ToothList;

@interface ToothManager : NSObject <ABMultitonProtocol>

-(void) createToothMap:(NSDictionary*)toothListEntity
               success:(void (^)())success
               failure:(void (^)())failure;

-(void) updateToothMap:(NSDictionary*)toothListEntity
               success:(void (^)())success
               failure:(void (^)())failure;

@end
