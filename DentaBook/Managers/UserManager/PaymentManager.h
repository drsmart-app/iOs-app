//
//  PaymentManager.h
//  DentaBook
//
//  Created by Denis Dubov on 10.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import <ABMultiton/ABMultitonProtocol.h>

@class Payment;

@interface PaymentManager : NSObject <ABMultitonProtocol>

-(void) createPayment:(NSDictionary*)paymentEntity
              success:(void (^)())success
              failure:(void (^)())failure;

@end
