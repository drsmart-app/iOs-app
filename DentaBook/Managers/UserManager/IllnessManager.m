//
//  IllnessManager.m
//  DentaBook
//
//  Created by Denis Dubov on 05.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "IllnessManager.h"
#import "SafeCategories.h"

#import <ABMultiton/ABMultiton.h>
#import <MagicalRecord/MagicalRecord.h>

#import "PatientIllness+Custom.h"
#import "DataManager.h"
#import "DataBase.h"

@implementation IllnessManager

+ (id)sharedInstance
{
    return [ABMultiton sharedInstanceOfClass:[self class]];
}

-(id)init{
    
    self = [super init];
    if (self) {
        //
    }
    return self;
}

-(void) createIllnessByTitle:(NSString*)title
                     success:(void (^)())success
                     failure:(void (^)())failure  {
    
    id params = @{@"id":GUIDString(), @"title": title};
    
    [DataManager loadCookies];
    [DataBase putDB:params toTable:@"illness" withKey:@"title"];
    success();
    /*[DataManager post:[NSString stringWithFormat:@"illness"]
               params:params
              success:^(AFHTTPRequestOperation *operation, id json) {
                  
                  if ([json isKindOfClass:[NSDictionary class]]) {
                      NSNumber* remoteID = [json safeNumberObjectForKey:@"id"];
                      
                      PatientIllness* illnessEntity = [PatientIllness MR_findFirstByAttribute:@"remoteId" withValue:remoteID];
                      if (!illnessEntity) {
                          illnessEntity = [PatientIllness MR_createEntity];
                      }
                      
                      FEMManagedObjectMapping *mapping = [PatientIllness mapObject];
                      [FEMManagedObjectDeserializer fillObject:illnessEntity fromExternalRepresentation:json usingMapping:mapping];
                      [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                      
                      success();
                  } else {
                      failure();
                  }

              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  failure();
              }];*/
}

-(void) deleteIllnessById:(NSString*)_id
                  success:(void (^)())success
                  failure:(void (^)())failure  {
    
    [DataManager loadCookies];
    [DataBase killDB:@"illness" withKey:@"id" andValue:_id];
    success();
    /*[DataManager delete:[NSString stringWithFormat:@"illness/%ld", [remoteId longValue]]
               params:nil
              success:^(AFHTTPRequestOperation *operation, id json) {
                  
                  PatientIllness* illnessEntity = [PatientIllness MR_findFirstByAttribute:@"remoteId" withValue:remoteId];
                  if (illnessEntity) {
                      [illnessEntity MR_deleteEntity];
                      [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                  }
                  
                  success();
                  
              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  failure();
              }];*/
}

-(AFHTTPRequestOperation*) syncIllness:(void (^)())success
                               failure:(void (^)())failure  {
    return [DataManager createGetHTTPRequestOperation:[NSString stringWithFormat:@"illness"]
                                                                     params:nil
                                                                    success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                                        if ([responseObject isKindOfClass:[NSArray class]]) {
                                                                            [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
                                                                                for (NSDictionary* jsonDict in responseObject) {
                                                                                    if ([jsonDict isKindOfClass:[NSDictionary class]]) {
                                                                                        NSNumber* remoteID = [jsonDict safeNumberObjectForKey:@"id"];
                                                                                        
                                                                                        PatientIllness* illnessEntity = [PatientIllness MR_findFirstByAttribute:@"remoteId" withValue:remoteID inContext:localContext];
                                                                                        if (!illnessEntity) {
                                                                                            illnessEntity = [PatientIllness MR_createEntityInContext:localContext];
                                                                                        }
                                                                                        
                                                                                        FEMManagedObjectMapping *mapping = [PatientIllness mapObject];
                                                                                        [FEMManagedObjectDeserializer fillObject:illnessEntity fromExternalRepresentation:jsonDict usingMapping:mapping];
                                                                                    }
                                                                                }
                                                                                
                                                                            } completion:^(BOOL _success, NSError *error) {
                                                                                success();
                                                                            }];
                                                                        }else{
                                                                            failure();
                                                                        }
                                                                    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                                        failure();
                                                                    }];
}

@end
