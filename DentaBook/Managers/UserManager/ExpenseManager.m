//
//  ExpenseManager.m
//  DentaBook
//
//  Created by Denis Dubov on 11.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "ExpenseManager.h"
#import "SafeCategories.h"

#import <ABMultiton/ABMultiton.h>
#import <MagicalRecord/MagicalRecord.h>

#import "Patient+Custom.h"
#import "ExpenseCategory+Custom.h"
#import "ExpenseSubcategory+Custom.h"
#import "Expense+Custom.h"
#import "DataManager.h"
#import "DataBase.h"

@implementation ExpenseManager

+ (id)sharedInstance
{
    return [ABMultiton sharedInstanceOfClass:[self class]];
}

-(id)init{
    
    self = [super init];
    if (self) {
        //
    }
    return self;
}

-(void) createExpenseCategory:(NSDictionary*)expenseCategory
                      success:(void (^)())success
                      failure:(void (^)())failure  {
    
    /*NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSString *dateString = [dateFormatter stringFromDate:expenseCategory.createAt];
    
    id params = @{@"title": expenseCategory.title,
                  @"date": dateString};*/
    
    [DataManager loadCookies];
    [DataBase putDB:expenseCategory toTable:@"expence_category" withKey:@"id"];
    success();
    /*[DataManager post:[NSString stringWithFormat:@"expenseCategory"]
               params:params
              success:^(AFHTTPRequestOperation *operation, id json) {
                  
                  if ([json isKindOfClass:[NSDictionary class]]) {
                      FEMManagedObjectMapping *mapping = [ExpenseCategory mapObject];
                      [FEMManagedObjectDeserializer fillObject:expenseCategory fromExternalRepresentation:json usingMapping:mapping];
                      [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                      
                      success();
                  } else {
                      failure();
                  }
                  
              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  failure();
              }];*/
}

-(void) deleteExpenseCategoryById:(NSString*)remoteId
                          success:(void (^)())success
                          failure:(void (^)())failure  {
    
    [DataManager loadCookies];
    [DataBase killDB:@"expence_category" withKey:@"id" andValue:remoteId];
    success();
    /*[DataManager delete:[NSString stringWithFormat:@"expenseCategory/%ld", [remoteId longValue]]
                 params:nil
                success:^(AFHTTPRequestOperation *operation, id json) {
                    
                    ExpenseCategory* affairEntity = [ExpenseCategory MR_findFirstByAttribute:@"remoteId" withValue:remoteId];
                    if (affairEntity) {
                        [affairEntity MR_deleteEntity];
                        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                    }
                    
                    success();
                    
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    failure();
                }];*/
}

-(void) createExpenseSubcategory:(NSDictionary*)expenseSubcategory
                         success:(void (^)())success
                         failure:(void (^)())failure  {
    
    /*NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSString *dateString = [dateFormatter stringFromDate:expenseSubcategory.createAt];
    
    id params = @{@"id": expenseSubcategory.category.remoteId,
                  @"title": expenseSubcategory.title,
                  @"date": dateString};*/
    
    [DataManager loadCookies];
    [DataBase putDB:expenseSubcategory toTable:@"expence_subcategory" withKey:@"id"];
    success();
    /*[DataManager post:[NSString stringWithFormat:@"expenseSubcategory"]
               params:params
              success:^(AFHTTPRequestOperation *operation, id json) {
                  
                  if ([json isKindOfClass:[NSDictionary class]]) {
                      FEMManagedObjectMapping *mapping = [ExpenseSubcategory mapObject];
                      [FEMManagedObjectDeserializer fillObject:expenseSubcategory fromExternalRepresentation:json usingMapping:mapping];
                      [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                      
                      success();
                  } else {
                      failure();
                  }
                  
              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  failure();
              }];*/
}

-(void) deleteExpenseSubcategoryById:(NSString*)remoteId
                             success:(void (^)())success
                             failure:(void (^)())failure  {
    
    [DataManager loadCookies];
    [DataBase killDB:@"expence_subcategory" withKey:@"id" andValue:remoteId];
    success();
    /*[DataManager delete:[NSString stringWithFormat:@"expenseSubcategory/%ld", [remoteId longValue]]
                 params:nil
                success:^(AFHTTPRequestOperation *operation, id json) {
                    
                    ExpenseSubcategory* affairEntity = [ExpenseSubcategory MR_findFirstByAttribute:@"remoteId" withValue:remoteId];
                    if (affairEntity) {
                        [affairEntity MR_deleteEntity];
                        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                    }
                    
                    success();
                    
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    failure();
                }];*/
}

-(void) createExpense:(NSDictionary*)expense
              success:(void (^)())success
              failure:(void (^)())failure  {
    
    /*NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSString *dateString = [dateFormatter stringFromDate:expense.createAt];
    
    id params = @{@"categoryId": expense.categoryId,
                  @"subCategoryId": expense.subCategoryId,
                  @"comment": (expense.comment.length > 0) ? expense.comment : @"",
                  @"amount": expense.amount,
                  @"date": dateString};*/
    
    [DataManager loadCookies];
    [DataBase putDB:expense toTable:@"expence" withKey:@"id"];
    success();
    /*[DataManager post:[NSString stringWithFormat:@"expense"]
               params:params
              success:^(AFHTTPRequestOperation *operation, id json) {
                  
                  if ([json isKindOfClass:[NSDictionary class]]) {
                      FEMManagedObjectMapping *mapping = [Expense mapObject];
                      [FEMManagedObjectDeserializer fillObject:expense fromExternalRepresentation:json usingMapping:mapping];
                      [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                      
                      success();
                  } else {
                      failure();
                  }
                  
              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  failure();
              }];*/
}

-(void) deleteExpenseById:(NSString*)remoteId
                  success:(void (^)())success
                  failure:(void (^)())failure  {
    
    [DataManager loadCookies];
    [DataBase killDB:@"expence" withKey:@"id" andValue:remoteId];
    success();
    /*[DataManager delete:[NSString stringWithFormat:@"expense/%ld", [remoteId longValue]]
                 params:nil
                success:^(AFHTTPRequestOperation *operation, id json) {
                    
                    Expense* affairEntity = [Expense MR_findFirstByAttribute:@"remoteId" withValue:remoteId];
                    if (affairEntity) {
                        [affairEntity MR_deleteEntity];
                        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                    }
                    
                    success();
                    
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    failure();
                }];*/
}

/*-(AFHTTPRequestOperation*) syncExpense:(void (^)())success
                               failure:(void (^)())failure  {
    return [DataManager createGetHTTPRequestOperation:[NSString stringWithFormat:@"expense"]
                                               params:nil
                                              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                  if ([responseObject isKindOfClass:[NSArray class]]) {
                                                      [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
                                                          for (NSDictionary* jsonDict in responseObject) {
                                                              if ([jsonDict isKindOfClass:[NSDictionary class]]) {
                                                                  NSNumber* remoteID = [jsonDict safeNumberObjectForKey:@"id"];
                                                                  
                                                                  Expense* affairGroupEntity = [Expense MR_findFirstByAttribute:@"remoteId" withValue:remoteID inContext:localContext];
                                                                  if (!affairGroupEntity) {
                                                                      affairGroupEntity = [Expense MR_createEntityInContext:localContext];
                                                                  }
                                                                  
                                                                  FEMManagedObjectMapping *mapping = [Expense mapObject];
                                                                  [FEMManagedObjectDeserializer fillObject:affairGroupEntity fromExternalRepresentation:jsonDict usingMapping:mapping];
                                                              }
                                                          }
                                                          
                                                      } completion:^(BOOL _success, NSError *error) {
                                                          success();
                                                      }];
                                                  }else{
                                                      failure();
                                                  }
                                              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                  failure();
                                              }];
}

-(AFHTTPRequestOperation*) syncExpenseFixtures:(void (^)())success
                                       failure:(void (^)())failure  {
    return [DataManager createGetHTTPRequestOperation:[NSString stringWithFormat:@"expenseFixtures"]
                                               params:nil
                                              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                  if ([responseObject isKindOfClass:[NSArray class]]) {
                                                      [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
                                                          for (NSDictionary* jsonDict in responseObject) {
                                                              if ([jsonDict isKindOfClass:[NSDictionary class]]) {
                                                                  NSNumber* remoteID = [jsonDict safeNumberObjectForKey:@"id"];
                                                                  
                                                                  ExpenseCategory* affairGroupEntity = [ExpenseCategory MR_findFirstByAttribute:@"remoteId" withValue:remoteID inContext:localContext];
                                                                  if (!affairGroupEntity) {
                                                                      affairGroupEntity = [ExpenseCategory MR_createEntityInContext:localContext];
                                                                  }
                                                                  
                                                                  FEMManagedObjectMapping *mapping = [ExpenseCategory mapObject];
                                                                  [FEMManagedObjectDeserializer fillObject:affairGroupEntity fromExternalRepresentation:jsonDict usingMapping:mapping];
                                                              }
                                                          }
                                                          
                                                      } completion:^(BOOL _success, NSError *error) {
                                                          success();
                                                      }];
                                                  }else{
                                                      failure();
                                                  }
                                              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                  failure();
                                              }];
}*/

@end
