//
//  CDStack.h
//  WelpyApp
//
//  Created by Alexander Bukov on 3/19/15.
//  Copyright (c) 2015 Welpy Inc. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface CDStack : NSObject

@property (strong, nonatomic) NSManagedObjectContext* managedObjectContext;
@property (strong, nonatomic) NSManagedObjectModel* managedObjectModel;
@property (strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (strong, nonatomic) NSURL *modelURL;
@property (strong, nonatomic) NSURL *storeURL;

- (id)initWithModelURL:(NSURL*)modelURL storeURL:(NSURL*)storeURL;

- (NSError*)save;
- (BOOL)hasChanges;

@end
