//
//  DataManager.m
//  DentaBook
//
//  Created by Denis Dubov on 04.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "DataManager.h"
#import <sys/socket.h>
#import <netinet/in.h>
#import <SystemConfiguration/SystemConfiguration.h>

@implementation DataManager

+(DataManager *)current
{
    static DataManager* x;
    if (x == nil) x = [[DataManager alloc]init];
    return x;
}

+(BOOL) isInternetReachable
{
    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;
    
    const struct sockaddr *address = (const struct sockaddr*)&zeroAddress;
    
    SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithAddress(kCFAllocatorDefault, address);
    
    BOOL isReachable = NO;
    
    if(reachability != NULL) {
        SCNetworkReachabilityFlags flags;
        
        if (SCNetworkReachabilityGetFlags(reachability, &flags)) {
            if ((flags & kSCNetworkReachabilityFlagsReachable) == 0) {
                isReachable = NO;
            } else if ((flags & kSCNetworkReachabilityFlagsConnectionRequired) == 0) {
                isReachable = YES;
            } else if ((((flags & kSCNetworkReachabilityFlagsConnectionOnDemand ) != 0) ||
                        (flags & kSCNetworkReachabilityFlagsConnectionOnTraffic) != 0)) {
                if ((flags & kSCNetworkReachabilityFlagsInterventionRequired) == 0) {
                    isReachable = YES;
                }
            } else if ((flags & kSCNetworkReachabilityFlagsIsWWAN) == kSCNetworkReachabilityFlagsIsWWAN) {
                isReachable = YES;
            }
        }
        
        CFRelease(reachability);
    }
    
    return isReachable;
}

+(NSString*)host
{
    NSString *host = nil;
    //host = @"http://localhost:8080/";
    host = @"http://188.166.15.242:8080/";
    return host;
}

+(NSString*)urlForPath:(NSString*)path
{
    return [[self host] stringByAppendingString:path];
}

+(void)get:(NSString*)path params:(NSDictionary*)params
   success:(void (^)(AFHTTPRequestOperation *, id)) success
   failure:(void (^)(AFHTTPRequestOperation *, NSError *))failure
{
    NSLog(@"GET %@ Params: %@", [self urlForPath:path], params);
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[self urlForPath:path]
      parameters:params
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             NSLog(@"(%@) JSON: %@", path, responseObject);
             
             success(operation, responseObject);
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"(%@) Error: %@ ResponseString: %@", path, error, operation.responseString);
             failure(operation, error);
             
             if(operation.response.statusCode == 401)  {
                 //[NotifyManager raise:needLogoutNotification];
             }
         }];
}

+(void)post:(NSString*)path params:(NSDictionary*)params
    success:(void (^)(AFHTTPRequestOperation *, id)) success
    failure:(void (^)(AFHTTPRequestOperation *, NSError *))failure
{
    NSLog(@"POST %@ Params: %@", [self urlForPath:path], params);
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager POST:[self urlForPath:path]
       parameters:params
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSLog(@"(%@) JSON: %@", path, responseObject);
              
              success(operation, responseObject);
              
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"(%@) Error: %@ ResponseString: %@", path, error, operation.responseString);
              failure(operation, error);
              
              if(operation.response.statusCode == 401)  {
                  //[NotifyManager raise:needLogoutNotification];
              }
          }];
}

+(void)put:(NSString*)path params:(NSDictionary*)params
   success:(void (^)(AFHTTPRequestOperation *, id)) success
   failure:(void (^)(AFHTTPRequestOperation *, NSError *))failure
{
    NSLog(@"PUT %@ Params: %@", [self urlForPath:path], params);
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager PUT:[self urlForPath:path]
      parameters:params
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             NSLog(@"(%@) JSON: %@", path, responseObject);
             
             success(operation, responseObject);
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"(%@) Error: %@ ResponseString: %@", path, error, operation.responseString);
             failure(operation, error);
             
             if(operation.response.statusCode == 401)  {
                 //[NotifyManager raise:needLogoutNotification];
             }
         }];
}

+(void)delete:(NSString*)path params:(NSDictionary*)params
      success:(void (^)(AFHTTPRequestOperation *, id)) success
      failure:(void (^)(AFHTTPRequestOperation *, NSError *))failure
{
    NSLog(@"DELETE %@ Params: %@", [self urlForPath:path], params);
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager DELETE:[self urlForPath:path]
         parameters:params
            success:^(AFHTTPRequestOperation *operation, id responseObject) {
                NSLog(@"(%@) JSON: %@", path, responseObject);
                
                success(operation, responseObject);
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                NSLog(@"(%@) Error: %@ ResponseString: %@", path, error, operation.responseString);
                failure(operation, error);
                
                if(operation.response.statusCode == 401)  {
                    //[NotifyManager raise:needLogoutNotification];
                }
            }];
}

+(void)post:(NSString*)path params:(NSDictionary*)params
      image:(UIImage*)image imageField:(NSString*)imageField
    success:(void (^)(AFHTTPRequestOperation* operation, id json)) success
    failure:(void (^)(AFHTTPRequestOperation* operation, NSError* error))failure
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:[self urlForPath:path]
       parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
           if (image){
               NSData* data = UIImageJPEGRepresentation(image, 0.8); //UIImagePNGRepresentation(image);
               NSString* fileName = [imageField stringByAppendingString:@".jpg"];
               [formData appendPartWithFileData:data
                                           name:imageField
                                       fileName:fileName
                                       mimeType:@"image/jpg"];
           }
       }success:^(AFHTTPRequestOperation *operation, id responseObject) {
           NSLog(@"(%@) JSON: %@", path, responseObject);
           
           success(operation, responseObject);
           
       } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
           NSLog(@"(%@) Error: %@ ResponseString: %@", path, error, operation.responseString);
           failure(operation, error);
           
           if(operation.response.statusCode == 401)  {
               //[NotifyManager raise:needLogoutNotification];
           }
       }];
}

+(AFHTTPRequestOperation*) createHTTPRequestOperation:(NSMutableURLRequest*)request
                                              success:(void (^)(AFHTTPRequestOperation* operation, id responseObject))success
                                              failure:(void (^)(AFHTTPRequestOperation* operation, NSError* error))failure  {
    
    NSLog(@"- %@", request.description);
    
    AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    op.responseSerializer = [AFJSONResponseSerializer serializer];
    [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"(%@) JSON: %@", request.URL.path, responseObject);
        success(operation, responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"(%@) Error: %@ ResponseString: %@", request.URL.path, error, operation.responseString);
        failure(operation, error);
        
        if(operation.response.statusCode == 401)  {
            //[NotifyManager raise:needLogoutNotification];
        }
    }];
    
    return op;
}

+(AFHTTPRequestOperation*) createGetHTTPRequestOperation:(NSString*)path
                                                  params:(NSDictionary*)params
                                                 success:(void (^)(AFHTTPRequestOperation* operation, id responseObject))success
                                                 failure:(void (^)(AFHTTPRequestOperation* operation, NSError* error))failure  {
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"GET"
                                                                                 URLString:[self urlForPath:path]
                                                                                parameters:params
                                                                                     error:nil];
    return [DataManager createHTTPRequestOperation:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(operation, responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(operation, error);
    }];
}

+(AFHTTPRequestOperation*) createPostHTTPRequestOperation:(NSString*)path
                                                   params:(NSDictionary*)params
                                                  success:(void (^)(AFHTTPRequestOperation* operation, id responseObject))success
                                                  failure:(void (^)(AFHTTPRequestOperation* operation, NSError* error))failure  {
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"POST"
                                                                                 URLString:[self urlForPath:path]
                                                                                parameters:params
                                                                                     error:nil];
    return [DataManager createHTTPRequestOperation:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(operation, responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(operation, error);
    }];
}

+(void)saveCookies {
    
    NSData *cookiesData = [NSKeyedArchiver archivedDataWithRootObject: [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject: cookiesData forKey: @"sessionCookies"];
    [defaults synchronize];
}

+(BOOL)loadCookies {
    
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey: @"sessionCookies"];
    if(data)    {
        NSArray *cookies = [NSKeyedUnarchiver unarchiveObjectWithData: data];
        if(cookies.count == 0)  {
            return NO;
        }
        
        NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
        
        //NSLog(@"-------------------- LOAD COOKIE --------------------");
        for (NSHTTPCookie *cookie in cookies){
            [cookieStorage setCookie: cookie];
            //NSLog(@"%@ = %@", cookie.name, cookie.properties);
        }
        //NSLog(@"-----------------------------------------------------");
        
        return YES;
    }
    
    return NO;
}

+(void)clearCoockie    {
    NSArray* cookies = [NSHTTPCookieStorage sharedHTTPCookieStorage].cookies;
    for (NSHTTPCookie* cookie in cookies) {
        [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
    }
}

+(void)deleteCoockies  {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"sessionCookies"];
    [defaults synchronize];
}

@end
