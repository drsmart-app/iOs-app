//
//  DataManager.h
//  DentaBook
//
//  Created by Denis Dubov on 04.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>

@interface DataManager : NSObject

+(DataManager*)current;

+(BOOL) isInternetReachable;
+(NSString*)host;
+(NSString*)urlForPath:(NSString*)path;

+(void)get:(NSString*)path params:(NSDictionary*)params
   success:(void (^)(AFHTTPRequestOperation* operation, id json)) success
   failure:(void (^)(AFHTTPRequestOperation* operation, NSError* error))failure;

+(void)post:(NSString*)path params:(NSDictionary*)params
    success:(void (^)(AFHTTPRequestOperation* operation, id json)) success
    failure:(void (^)(AFHTTPRequestOperation* operation, NSError* error))failure;

+(void)put:(NSString*)path params:(NSDictionary*)params
   success:(void (^)(AFHTTPRequestOperation *, id)) success
   failure:(void (^)(AFHTTPRequestOperation *, NSError *))failure;

+(void)delete:(NSString*)path params:(NSDictionary*)params
success:(void (^)(AFHTTPRequestOperation *, id)) success
failure:(void (^)(AFHTTPRequestOperation *, NSError *))failure;

+(void)post:(NSString*)path params:(NSDictionary*)params
      image:(UIImage*)image imageField:(NSString*)imageField
    success:(void (^)(AFHTTPRequestOperation* operation, id json)) success
    failure:(void (^)(AFHTTPRequestOperation* operation, NSError* error))failure;

+(AFHTTPRequestOperation*) createGetHTTPRequestOperation:(NSString*)path
                                                  params:(NSDictionary*)params
                                                 success:(void (^)(AFHTTPRequestOperation* operation, id responseObject))success
                                                 failure:(void (^)(AFHTTPRequestOperation* operation, NSError* error))failure;
+(AFHTTPRequestOperation*) createPostHTTPRequestOperation:(NSString*)path
                                                   params:(NSDictionary*)params
                                                  success:(void (^)(AFHTTPRequestOperation* operation, id responseObject))success
                                                  failure:(void (^)(AFHTTPRequestOperation* operation, NSError* error))failure;

+(void)saveCookies;
+(BOOL)loadCookies;
+(void)clearCoockie;
+(void)deleteCoockies;

@end
